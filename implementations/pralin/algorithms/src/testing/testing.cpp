#include "testing.h"

#include <cmath>

#include <pralin/values/tensor>

using namespace pralin::algorithms::testing;

void create_tensor_serie::process(values::tensor* _value) const
{
  std::size_t count = std::floor((get_parameters().get_max() - get_parameters().get_min())
                                 / get_parameters().get_step());
  double* array = new double[count];
  array[0] = get_parameters().get_min();
  for(std::size_t idx = 1; idx < count; ++idx)
  {
    array[idx] = array[idx - 1] + get_parameters().get_step();
  }
  *_value = values::tensor::create_default(pralin::values::meta_data::empty(),
                                           values::tensor::creation_flag::transfert_pointer, array,
                                           count);
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("pralin/testing", description("Algorithms used in pralin unit tests."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::testing::create_tensor_serie, "pralin/testing",
                          "create_tensor_serie", pralin::algorithm_priority::cpu,
                          description("Create a tensor serie for testing.",
                                      "This algorithm is used in unit test to create a tensor with "
                                      "values between `min` and `max`, incremented by `step`."))
