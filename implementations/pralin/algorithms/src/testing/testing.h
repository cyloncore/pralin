#pragma once

#include <pralin/implementation>
#include <pralin/values/scalar>

#include <pralin/definitions/operators>

namespace pralin::algorithms::testing
{
  PRALIN_PARAMETERS(serie, PRALIN_PARAMETER(double, 0.0), min, PRALIN_PARAMETER(double, 5.0), max,
                    PRALIN_PARAMETER(double, 1.0), step);
  class create_tensor_serie
      : public implementation<create_tensor_serie, definitions::operators::create<values::tensor>,
                              serie_parameters>
  {
    friend implementation_t;
    create_tensor_serie() = default;
    ~create_tensor_serie() = default;
  public:
    void process(values::tensor* _value) const final;
  };
} // namespace pralin::algorithms::testing
