#include <pralin/tests/catch.h>

#include <pralin/algorithms/arithmetic>

TEST_CASE("binary", "[binary]")
{

  pralin::algorithms::arithmetic::addition addition_algo;
  pralin::values::scalar a(10);
  pralin::values::scalar b(12);
  pralin::values::scalar c1;
  addition_algo.process(a, b, &c1);
  REQUIRE(c1.cast<int>() == 22);
  pralin::abstract_algorithm* algo = &addition_algo;
  pralin::values_vector input{a, b};
  pralin::values::scalar c2;
  pralin::values_ptr_vector output{&c2};
  algo->process(input, output);
  REQUIRE(output[0].to_pointer<pralin::values::scalar*>()->cast<int>() == 22);
  REQUIRE(c2.cast<int>() == 22);
}
