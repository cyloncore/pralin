#pragma once

#include <pralin/implementation>

#include <pralin/definitions/operators>
#include <pralin/values/scalar>

namespace pralin::algorithms::arithmetic
{
  PRALIN_PARAMETERS(operators, PRALIN_OPTIONAL_PARAMETER(pralin::values::scalar), offset,
                    PRALIN_OPTIONAL_PARAMETER(pralin::values::scalar), scale);

  template<template<typename> class _op_, class _RT_>
  class binary : public implementation<binary<_op_, _RT_>,
                                       definitions::operators::binary<pralin::values::scalar, _RT_>,
                                       operators_parameters>
  {
    using scalar_type = pralin::values::scalar_type;
  public:
    void process(const pralin::values::scalar& _a, const pralin::values::scalar& _b,
                 _RT_* _c) const final;
  private:
    static scalar_type select_type(scalar_type _a, scalar_type _b);
    static std::tuple<bool, bool, int> type_trait(scalar_type _t);
    static scalar_type type(bool _is_signed, bool _is_float, int _bits);
  };

  template<template<typename> class _op_>
  class logical_binary
      : public implementation<logical_binary<_op_>, definitions::operators::binary<bool>,
                              operators_parameters>
  {
  public:
    void process(const bool& _a, const bool& _b, bool* _c) const final
    {
      *_c = _op_<bool>()(_a, _b);
    }
  };

  template<template<typename> class _op_, class _RT_>
  std::tuple<bool, bool, int> binary<_op_, _RT_>::type_trait(scalar_type _t)
  {
    switch(_t)
    {
    case scalar_type::uint_8:
      return std::make_tuple(false, false, 8);
    case scalar_type::uint_16:
      return std::make_tuple(false, false, 16);
    case scalar_type::uint_32:
      return std::make_tuple(false, false, 32);
    case scalar_type::uint_64:
      return std::make_tuple(false, false, 64);
    case scalar_type::int_8:
      return std::make_tuple(true, false, 8);
    case scalar_type::int_16:
      return std::make_tuple(true, false, 16);
    case scalar_type::int_32:
      return std::make_tuple(true, false, 32);
    case scalar_type::int_64:
      return std::make_tuple(true, false, 64);
    case scalar_type::float_32:
      return std::make_tuple(true, true, 32);
    case scalar_type::float_64:
      return std::make_tuple(true, true, 64);
    }
    pralin_fatal("Invalid numerical literal.");
  }
  template<template<typename> class _op_, class _RT_>
  typename pralin::values::scalar_type binary<_op_, _RT_>::type(bool _is_signed, bool _is_float,
                                                                int _bits)
  {
    if(_is_float)
    {
      return _bits <= 32 ? scalar_type::float_32 : scalar_type::float_64;
    }
    else if(_is_signed)
    {
      return _bits <= 8 ? scalar_type::int_8
                        : (_bits <= 16 ? scalar_type::int_16
                                       : (_bits <= 32 ? scalar_type::int_32 : scalar_type::int_64));
    }
    else
    {
      return _bits <= 8
               ? scalar_type::uint_8
               : (_bits <= 16 ? scalar_type::uint_16
                              : (_bits <= 32 ? scalar_type::uint_32 : scalar_type::uint_64));
    }
  }

  template<template<typename> class _op_, class _RT_>
  typename pralin::values::scalar_type binary<_op_, _RT_>::select_type(scalar_type _a,
                                                                       scalar_type _b)
  {
    if(_a == _b)
      return _a;
    else
    {
      auto const [is_a_signed, is_a_float, a_bits] = type_trait(_a);
      auto const [is_b_signed, is_b_float, b_bits] = type_trait(_b);
      return type(is_a_signed or is_b_signed, is_a_float or is_b_float, std::max(a_bits, b_bits));
    }
  }
  template<template<typename> class _op_, class _RT_>
  void binary<_op_, _RT_>::process(const pralin::values::scalar& _a,
                                   const pralin::values::scalar& _b, _RT_* _c) const
  {
    scalar_type selected_type = select_type(_a.get_type(), _b.get_type());
    auto compute = [&_a, &_b, &_c]<typename T>(T) { *_c = _op_<T>()(_a.cast<T>(), _b.cast<T>()); };
    switch(selected_type)
    {
    case scalar_type::uint_8:
      compute(uint8_t());
      break;
    case scalar_type::uint_16:
      compute(uint16_t());
      break;
    case scalar_type::uint_32:
      compute(uint32_t());
      break;
    case scalar_type::uint_64:
      compute(uint64_t());
      break;
    case scalar_type::int_8:
      compute(int8_t());
      break;
    case scalar_type::int_16:
      compute(int16_t());
      break;
    case scalar_type::int_32:
      compute(int32_t());
      break;
    case scalar_type::int_64:
      compute(int64_t());
      break;
    case scalar_type::float_32:
      compute(float());
      break;
    case scalar_type::float_64:
      compute(double());
      break;
    }
  }

  using addition = binary<std::plus, values::scalar>;
  using inferior = binary<std::less, bool>;
  using logical_and = logical_binary<std::logical_and>;
  using logical_or = logical_binary<std::logical_or>;
} // namespace pralin::algorithms::arithmetic
