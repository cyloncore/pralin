#include "arithmetic.h"

using namespace pralin::algorithms::arithmetic;

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("pralin/arithmetic", description("Arithmetic operations."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::arithmetic::addition, "pralin/arithmetic", "addition",
                          pralin::algorithm_priority::cpu,
                          description("Perform the addition between two numbers."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::arithmetic::inferior, "pralin/arithmetic", "inferior",
                          pralin::algorithm_priority::cpu,
                          description("Perform the inferior operation between two numbers."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::arithmetic::logical_and, "pralin/arithmetic", "and",
                          pralin::algorithm_priority::cpu,
                          description("Perform the logical `and` operation between two booleans."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::arithmetic::logical_or, "pralin/arithmetic", "or",
                          pralin::algorithm_priority::cpu,
                          description("Perform the logical `or` operation between two booleans."))
