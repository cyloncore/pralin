#include "point_cloud.h"

#include <pralin/utilities/scalar_array_adaptor>
#include <pralin/values/geometry>
#include <pralin/values/lidar_scan>
#include <pralin/values/polygon>

#include <pralin/backends/eigen>

#include "transform.h"

using namespace pralin::algorithms::point_cloud;

void transformation::process(const values::point_cloud& _input, values::point_cloud* _output) const
{
  Eigen::Affine3d transform_2 = Eigen::Affine3d::Identity();
  transform_2.translation() << get_parameters().get_tx(), get_parameters().get_ty(),
    get_parameters().get_tz();

  std::vector<uint8_t> data;
  data.resize(_input.get_count() * _input.get_point_size());
  _input.copy_to(data.data());

  transform(data.data(), _input.get_count(), _input.get_point_size(), _input.get_fields(),
            pralin::backends::eigen::transformation_backend(transform_2));

  *_output = values::point_cloud(
    _input.get_meta_data(), _input.get_fields(),
    new values::point_cloud::default_backend(data, _input.get_count(), _input.get_point_size()));
}

void crop::process(
  const values::point_cloud& _input, const values::polygon& _polygon,
  const std::optional<values::geometry::transformation_provider>& _transformation_provider,
  values::point_cloud* _output) const
{
  values::polygon polygon = _polygon;

  values::geometry::transformation transfo;
  // Transform the polygon in the same spatial reference as the point cloud
  if(_transformation_provider)
  {
    transfo = _transformation_provider->get_transformation(_input, _polygon.get_meta_data());
  }

  // Prepare for cropping

  pralin::values::point_cloud_builder pcb
    = pralin::values::point_cloud_builder::create_generic(_input.get_fields());
  pcb.get_meta_data_builder().set(_input.get_meta_data());

  std::size_t point_size = _input.get_point_size();
  adaptor_info ai(_input.get_fields());

  std::vector<uint8_t> output_pt;
  output_pt.resize(point_size);

  for(std::size_t i = 0; i < _input.get_count(); ++i)
  {
    const uint8_t* pt_ptr = _input.get_point(i);
    pralin::utilities::scalar_const_array_adaptor x(ai.st_x, pt_ptr + ai.idx_x, 1);
    pralin::utilities::scalar_const_array_adaptor y(ai.st_y, pt_ptr + ai.idx_y, 1);
    pralin::utilities::scalar_const_array_adaptor z(ai.st_z, pt_ptr + ai.idx_z, 1);

    pralin::values::geometry::point pt(x.at(0).cast<double>(), y.at(0).cast<double>(),
                                       z.at(0).cast<double>(), 0);

    if(transfo.is_valid())
    {
      pt = transfo.transform(pt);
    }

    if(polygon.is_inside({pt.get_x(), pt.get_y()}))
    {
      pcb.add_point(pt_ptr);
    }
  }

  *_output = pcb.to_point_cloud();
  pralin_assert(_output->get_count() > 0);
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("pralin/arithmetic", description("Point cloud operations."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::point_cloud::transformation, "pralin/point_cloud", "transformation",
  pralin::algorithm_priority::cpu,
  description(
    "Transform the coordinates of a point cloud.",
    "Takes a point cloud as `input`, and apply to each points the transformation defined by the "
    "translation (`tx`, `ty`, `tz`) and return the transformed point cloud as `output`."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::point_cloud::crop, "pralin/point_cloud", "crop",
  pralin::algorithm_priority::cpu,
  description("Crop a point cloud according to a polygon.",
              "Filter the `input` point cloud to keep the points that belongs in the `polygon`."))
