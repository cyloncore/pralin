#include <Eigen/Geometry>
#include <pralin/utilities/scalar_array_adaptor>
#include <pralin/values/point_cloud>

#include "adaptor_info.h"

namespace pralin::algorithms::point_cloud
{
  template<typename _Transfo_>
  inline void transform(uint8_t* _data, std::size_t _count, std::size_t _point_size,
                        const std::vector<values::point_cloud::field>& _fields,
                        const _Transfo_& _transform)
  {
    adaptor_info ai(_fields);

    for(std::size_t index = 0; index < _count; ++index)
    {
      pralin::utilities::scalar_array_adaptor x(ai.st_x, _data + index * _point_size + ai.idx_x, 1);
      pralin::utilities::scalar_array_adaptor y(ai.st_y, _data + index * _point_size + ai.idx_y, 1);
      pralin::utilities::scalar_array_adaptor z(ai.st_z, _data + index * _point_size + ai.idx_z, 1);
      pralin::values::geometry::point pt{x.at<double>(0), y.at<double>(0), z.at<double>(0), 0};
      pt = _transform.transform(pt);
      x.set(0, pt.get_x());
      y.set(0, pt.get_y());
      z.set(0, pt.get_z());
    }
  }
} // namespace pralin::algorithms::point_cloud
