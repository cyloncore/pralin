#pragma once

#include <pralin/implementation>
#include <pralin/values/scalar>

#include <pralin/definitions/point_cloud>

#include "filters.h"

namespace pralin::algorithms::point_cloud
{
  class scan_to_point_cloud
      : public implementation<scan_to_point_cloud,
                              ::pralin::definitions::point_cloud::scan_to_point_cloud>
  {
    friend implementation_t;
  public:
    scan_to_point_cloud() = default;
    ~scan_to_point_cloud() = default;
    void process(const values::lidar_scan& _input,
                 const values::geometry::transformation_provider& _transformation_provider,
                 const values::point_cloud_builder& _builder) const final;
  };
  namespace details
  {
    inline std::vector<std::string> get_default_field_names() { return {"x", "y", "z"}; }
  } // namespace details
  PRALIN_PARAMETERS(
    create_default_point_cloud_builder, PRALIN_PARAMETER(std::string, std::string()), frame_id,
    PRALIN_PARAMETER(pralin::values::scalar_type, pralin::values::scalar_type::float_32),
    scalar_type, PRALIN_PARAMETER(std::vector<std::string>, details::get_default_field_names()),
    fields_names, PRALIN_PARAMETER(pralin::any_value_map, pralin::any_value_map()), meta_data);
  class create_default_point_cloud_builder
      : public implementation<create_default_point_cloud_builder,
                              ::pralin::definitions::point_cloud::create_point_cloud_builder,
                              create_default_point_cloud_builder_parameters>
  {
    friend implementation_t;
  public:
    create_default_point_cloud_builder() = default;
    ~create_default_point_cloud_builder() = default;
    void process(values::point_cloud_builder* _builder) const final;
  };
} // namespace pralin::algorithms::point_cloud
