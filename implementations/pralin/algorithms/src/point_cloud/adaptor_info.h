#include <pralin/values/point_cloud>

namespace pralin::algorithms::point_cloud
{
  struct adaptor_info
  {
    adaptor_info(const std::vector<values::point_cloud::field> _fields)
    {
      for(const values::point_cloud::field& field : _fields)
      {
        switch(field.get_type())
        {
        case values::point_cloud::field::type::x:
          st_x = field.get_scalar_type();
          idx_x = field.get_offset();
          break;
        case values::point_cloud::field::type::y:
          st_y = field.get_scalar_type();
          idx_y = field.get_offset();
          break;
        case values::point_cloud::field::type::z:
          st_z = field.get_scalar_type();
          idx_z = field.get_offset();
          break;
        case values::point_cloud::field::type::other:
          break;
        }
      }
    }
    using st = values::scalar_type;
    st st_x = st::uint_8, st_y = st::uint_8, st_z = st::uint_8;
    std::size_t idx_x = 0, idx_y = 0, idx_z = 0;
  };
} // namespace pralin::algorithms::point_cloud
