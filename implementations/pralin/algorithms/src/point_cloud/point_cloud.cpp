#include "point_cloud.h"

#include <pralin/utilities/scalar_array_adaptor>
#include <pralin/values/geometry>
#include <pralin/values/lidar_scan>
#include <pralin/values/point_cloud>

#include <pralin/backends/eigen>

using namespace pralin::algorithms::point_cloud;

void scan_to_point_cloud::process(
  const values::lidar_scan& _input,
  const values::geometry::transformation_provider& _transformation_provider,
  const values::point_cloud_builder& _builder) const
{
  pralin::values::geometry::pose dst_pose = _builder.get_meta_data_builder().get_pose();

  if(not dst_pose.is_valid())
  {
    _builder.get_meta_data_builder().set_pose(_transformation_provider.transform(
      _input.get_meta_data().get_pose(), _builder.get_meta_data_builder()));
  }

  values::geometry::transformation transfo
    = _transformation_provider.get_transformation(_input, _builder.get_meta_data_builder());

  pralin::utilities::scalar_const_array_adaptor ranges = _input.get_ranges();

  for(std::size_t i = 0; i < ranges.size(); ++i)
  {
    double r = ranges.at<double>(i);
    if(_input.get_range_min() < r and r < _input.get_range_max())
    {
      float angle = _input.get_angle_min() + i * _input.get_angle_increment();
      Eigen::Vector3d pt;
      pt << std::cos(angle) * r, std::sin(angle) * r, 0;
      pralin::values::geometry::point rpt = transfo.transform(backends::eigen::to_point(pt, 0));
      _builder.add_point(_input.get_scalar_unit(), rpt.get_x(), rpt.get_y(), rpt.get_z());
    }
  }
}

void create_default_point_cloud_builder::process(values::point_cloud_builder* _builder) const
{
  *_builder = values::point_cloud_builder::create_default(get_parameters().get_scalar_type(),
                                                          get_parameters().get_fields_names());
  _builder->get_meta_data_builder()
    .set(get_parameters().get_meta_data())
    .set_frame_id(get_parameters().get_frame_id());
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::point_cloud::scan_to_point_cloud, "pralin/point_cloud", "scan_to_point_cloud",
  pralin::algorithm_priority::cpu,
  description("Aggregate 2D Lidar Scans into a point cloud.",
              "`scan` is converted in a set of 3D points, using the `transformation_provider` to "
              "transform the points from the input frame to the output frame. The points are added "
              "to a point cloud builder (`builder`). It is expected that this algorithm is called "
              "multiple time for each `scan`."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::point_cloud::create_default_point_cloud_builder,
                          "pralin/point_cloud", "create_default_point_cloud_builder",
                          pralin::algorithm_priority::cpu,
                          description("Create a default point cloud builder."))
