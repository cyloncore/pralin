#pragma once

#include <pralin/implementation>

#include <pralin/definitions/point_cloud>

namespace pralin::algorithms::point_cloud
{
  PRALIN_PARAMETERS(transformation, PRALIN_PARAMETER(double, 0.0), tx,
                    PRALIN_PARAMETER(double, 0.0), ty, PRALIN_PARAMETER(double, 0.0), tz);

  /**
   * Apply a transformation on a point cloud (translation and rotation)
   */
  class transformation : public implementation<transformation, definitions::point_cloud::filter,
                                               transformation_parameters>
  {
    friend implementation_t;
    transformation() = default;
    ~transformation() = default;
  public:
    void process(const values::point_cloud& _input, values::point_cloud* _output) const final;
  };

  class crop : public implementation<crop, definitions::point_cloud::crop>
  {
    friend implementation_t;
    crop() = default;
    ~crop() = default;
  public:
    void process(
      const values::point_cloud& _input, const values::polygon& _polygon,
      const std::optional<values::geometry::transformation_provider>& _transformation_provider,
      values::point_cloud* _output) const final;
  };
} // namespace pralin::algorithms::point_cloud
