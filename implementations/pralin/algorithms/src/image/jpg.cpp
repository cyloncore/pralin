#include "image.h"

#include <jpgd.h>
#include <jpge.h>

#include <pralin/values/image>

using namespace pralin::algorithms::image;

jpg_decoder::jpg_decoder() {}

namespace
{
  class istream_jpeg_decoder_stream : public jpgd::jpeg_decoder_stream
  {
  public:
    istream_jpeg_decoder_stream(const pralin::values::input_stream& _stream) : m_stream(_stream) {}
    virtual ~istream_jpeg_decoder_stream() {}

    // The read() method is called when the internal input buffer is empty.
    // Parameters:
    // pBuf - input buffer
    // max_bytes_to_read - maximum bytes that can be written to pBuf
    // pEOF_flag - set this to true if at end of stream (no more bytes remaining)
    // Returns -1 on error, otherwise return the number of bytes actually written to the buffer
    // (which may be 0). Notes: This method will be called in a loop until you set *pEOF_flag to
    // true or the internal buffer is full.
    int read(jpgd::uint8* pBuf, int max_bytes_to_read, bool* pEOF_flag) override
    {
      if(m_stream->eof())
      {
        *pEOF_flag = true;
        return 0;
      }
      if(m_stream->fail())
      {
        return -1;
      }
      m_stream->read(reinterpret_cast<char*>(pBuf), max_bytes_to_read);
      return m_stream->gcount();
    }
  private:
    pralin::values::input_stream m_stream;
  };
} // namespace

void jpg_decoder::process(const values::input_stream& _stream, values::image* _output) const
{
  int channels;
  int width;
  int height;
  istream_jpeg_decoder_stream stream(_stream);
  unsigned char* decompressedImage
    = jpgd::decompress_jpeg_image_from_stream(&stream, &width, &height, &channels, 3);
  if(decompressedImage)
  {
    values::image::colorspace cs;
    switch(channels)
    {
    case 1:
      cs = values::image::colorspace::grayscale();
      break;
    case 3:
      cs = values::image::colorspace::rgb();
      break;
    case 4:
      cs = values::image::colorspace::rgba();
      break;
    }
    *_output
      = values::image::create_default(pralin::values::meta_data::empty(), true, decompressedImage,
                                      width, height, channels, values::scalar_type::uint_8, cs);
  }
  else
  {
    throw pralin::exception("Failed to decode jpeg image");
  }
}

jpg_encoder::jpg_encoder() {}

namespace
{
  class ostream_jpeg_output_stream : public jpge::output_stream
  {
  public:
    ostream_jpeg_output_stream(const pralin::values::output_stream& _stream) : m_stream(_stream) {}
    virtual ~ostream_jpeg_output_stream(){};
    bool put_buf(const void* Pbuf, int len) override
    {
      m_stream->write(reinterpret_cast<const char*>(Pbuf), len);
      return true;
    }
  private:
    pralin::values::output_stream m_stream;
  };
} // namespace

void jpg_encoder::process(const values::output_stream& _stream, const values::image& _input) const
{
  values::image input = _input.cast<uint8_t>();

  ostream_jpeg_output_stream stream(_stream);
  jpge::params comp_params;
  comp_params.m_quality = get_parameters().get_quality();
  jpge::jpeg_encoder dst_image;
  if(not dst_image.init(&stream, input.get_width(), input.get_height(), input.get_channels(),
                        comp_params))
  {
    throw pralin::exception("Failed to initialise compression to jpeg");
    return;
  }

  for(uint pass_index = 0; pass_index < dst_image.get_total_passes(); pass_index++)
  {
    for(std::size_t i = 0; i < input.get_height(); i++)
    {
      const jpge::uint8* pScanline = input.get_data() + i * input.get_width_step();
      if(not dst_image.process_scanline(pScanline))
      {
        throw pralin::exception("Failed to process line {}", i);
        return;
      }
    }
    if(not dst_image.process_scanline(NULL))
    {
      throw pralin::exception("Failed to complete reading jpeg file");
      return;
    }
  }

  dst_image.deinit();
}
