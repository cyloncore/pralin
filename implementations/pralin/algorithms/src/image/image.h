#pragma once

#include <pralin/implementation>

#include <pralin/definitions/image>

namespace pralin::algorithms::image
{

  class jpg_decoder : public implementation<jpg_decoder, definitions::image::decoder>
  {
  public:
    jpg_decoder();
    void process(const values::input_stream& _stream, values::image* _output) const final;
  };
  PRALIN_PARAMETERS(jpg_encoder, PRALIN_PARAMETER(int, 90), quality);
  class jpg_encoder
      : public implementation<jpg_encoder, definitions::image::encoder, jpg_encoder_parameters>
  {
  public:
    jpg_encoder();
    void process(const values::output_stream& _stream, const values::image& _input) const final;
  };
} // namespace pralin::algorithms::image
