#pragma once

#include <pralin/implementation>
#include <pralin/values/iterator>

#include <pralin/definitions/operators>

namespace pralin::algorithms::values::iterator
{
  template<typename _T_, template<typename> class _TIterator_>
  class filter
      : public implementation<filter<_T_, _TIterator_>,
                              definitions::operators::transform<pralin::values::iterator<_T_>,
                                                                pralin::values::iterator<_T_>>,
                              typename _TIterator_<_T_>::parameters>
  {
    friend implementation<filter<_T_, _TIterator_>,
                          definitions::operators::transform<pralin::values::iterator<_T_>,
                                                            pralin::values::iterator<_T_>>,
                          typename _TIterator_<_T_>::parameters>;
    filter() = default;
    ~filter() = default;
  public:
    void process(const pralin::values::iterator<_T_>& _iterator,
                 pralin::values::iterator<_T_>* _output) const final
    {
      *_output = pralin::values::iterator<_T_>::template create<_TIterator_<_T_>>(
        _iterator, this->get_parameters());
    }
  };
  PRALIN_PARAMETERS(skip, PRALIN_PARAMETER(std::size_t, 1), skip_count);

  template<typename _T_>
  class skip_iterator : public pralin::values::iterator<_T_>::abstract_backend
  {
  public:
    using parameters = skip_parameters;
    skip_iterator(const pralin::values::iterator<_T_>& _iterator,
                  const skip_parameters& _parameters)
        : m_iterator(_iterator), m_parameters(_parameters)
    {
    }
    ~skip_iterator() {}
  public:
    _T_ next() override
    {
      _T_ v = m_iterator.next();
      for(std::size_t i = 0; i < m_parameters.get_skip_count() and m_iterator.has_next(); ++i)
      {
        m_iterator.next();
      }
      return v;
    }
    bool has_next() const override { return m_iterator.has_next(); }
  private:
    pralin::values::iterator<_T_> m_iterator;
    skip_parameters m_parameters;
  };

  template<typename _T_>
  using skip = filter<_T_, skip_iterator>;
} // namespace pralin::algorithms::values::iterator
