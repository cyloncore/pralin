#include "iterator.h"

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("pralin/values/iterator",
                       description("Algorithms related to manipulating iterators."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::values::iterator::skip<pralin::any_value>,
                          "pralin/values/iterator", "skip", pralin::algorithm_priority::cpu,
                          description("Skip value in an iterator",
                                      "Move forward an iterator by `skip_count`."))
