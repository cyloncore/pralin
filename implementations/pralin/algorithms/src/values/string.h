#pragma once

#include <pralin/implementation>
#include <pralin/values/meta_data>

#include <pralin/definitions/operators>

namespace pralin::algorithms::values::string
{
  PRALIN_PARAMETERS(format, PRALIN_PARAMETER(std::string, ""), format);
  class format
      : public implementation<format, definitions::operators::transform<any_value, std::string>,
                              format_parameters>
  {
    friend implementation_t;
    format() = default;
    ~format() = default;
  public:
    void process(const any_value& _value, std::string* _output) const final;
  };
  class concatenate
      : public implementation<concatenate, definitions::operators::binary<std::string>>
  {
    friend implementation_t;
    concatenate() = default;
    ~concatenate() = default;
  public:
    void process(const std::string& _a, const std::string& _b, std::string* _output) const final;
  };
} // namespace pralin::algorithms::values::string
