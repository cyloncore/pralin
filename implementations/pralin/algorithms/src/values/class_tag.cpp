#include "class_tag.h"

#include <pralin/values/tensor>

using namespace pralin::algorithms::values::class_tag;

void get_label::process(const pralin::values::class_tag& _class_tag, std::string* _label) const
{
  *_label = _class_tag.label;
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("pralin/values/class_tag",
                       description("Algorithms related to manipulating class_tags."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::values::class_tag::get_label,
                          "pralin/values/class_tag", "get_label", pralin::algorithm_priority::cpu,
                          description("Get the label of a tag as string."))
