#include "meta_data.h"

#include <pralin/values/tensor>

using namespace pralin::algorithms::values::meta_data;

void get::process(const pralin::values::meta_data& _meta_data, any_value* _value) const
{
  *_value = _meta_data.get<pralin::any_value>(get_parameters().get_name());
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("pralin/values/meta_data",
                       description("Algorithms related to manipulating meta_data."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::values::meta_data::get, "pralin/values/meta_data",
                          "get", pralin::algorithm_priority::cpu,
                          description("Get a value from meta data.",
                                      "Get the value `name` from the `input`."))
