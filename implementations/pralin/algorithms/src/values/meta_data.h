#pragma once

#include <pralin/implementation>
#include <pralin/values/meta_data>

#include <pralin/definitions/operators>

namespace pralin::algorithms::values::meta_data
{
  PRALIN_PARAMETERS(get, PRALIN_PARAMETER(std::string, ""), name);
  class get
      : public implementation<
          get, definitions::operators::get<pralin::values::meta_data, any_value>, get_parameters>
  {
    friend implementation_t;
    get() = default;
    ~get() = default;
  public:
    void process(const pralin::values::meta_data& _meta_data, any_value* _value) const final;
  };
} // namespace pralin::algorithms::values::meta_data
