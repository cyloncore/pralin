#include "string.h"

using namespace pralin::algorithms::values::string;

void format::process(const any_value& _value, std::string* _output) const
{
  try
  {
    *_output = std::vformat(get_parameters().get_format(), std::make_format_args(_value));
  }
  catch(const std::format_error& _fe)
  {
    throw pralin::exception("Failed to format '{}' with '{}', error is: '{}'",
                            get_parameters().get_format(), _value, _fe.what());
  }
}

void concatenate::process(const std::string& _a, const std::string& _b, std::string* _output) const
{
  *_output = _a + _b;
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("pralin/values/string",
                       description("Algorithms related to manipulating strings."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::values::string::format, "pralin/values/string",
                          "format", pralin::algorithm_priority::cpu,
                          description("Format any value to a string. "
                                      "This requires the value to support formatting."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::values::string::concatenate, "pralin/values/string",
                          "concatenate", pralin::algorithm_priority::cpu,
                          description("Concatenate two strings."))
