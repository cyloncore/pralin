#pragma once

#include <pralin/implementation>
#include <pralin/values/class_tag>

#include <pralin/definitions/operators>

namespace pralin::algorithms::values::class_tag
{
  class get_label
      : public implementation<get_label,
                              definitions::operators::get<pralin::values::class_tag, std::string>>
  {
    friend implementation_t;
    get_label() = default;
    ~get_label() = default;
  public:
    void process(const pralin::values::class_tag& _mclass_tag, std::string* _label) const final;
  };
} // namespace pralin::algorithms::values::class_tag
