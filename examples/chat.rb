#!/usr/bin/env ruby

require 'pralin'

Pralin::AlgorithmsRegistry.load_default_definitions

chat_algo = Pralin::AlgorithmsRegistry.create "ggml/llama", "chat", { "module" => "test_data",
  "filename" => "ggml-open-llama-3b-v2-q4_0.bin",
  "source" => "https://huggingface.co/sam2ai/open_llama_3b_odia_q4-0_gguf/resolve/main/ggml-model-q4_1.gguf?download=true"
  }

response = Pralin::Output.new()
ctx = Pralin::Output.new()

chat_algo.process "Hello, I am a human.", ctx, response, ctx

puts "user> Hello, I am a human"
puts "chat.rb> #{response.value}"