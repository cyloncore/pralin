include_directories(BEFORE ${CMAKE_BINARY_DIR})

add_executable(pralin_object_detect main.cpp)
target_link_libraries(pralin_object_detect pralin ${CMAKE_THREAD_LIBS_INIT})
