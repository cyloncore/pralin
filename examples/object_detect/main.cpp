#include <iostream>

#include <pralin/algorithm_definition>
#include <pralin/algorithms_registry>
#include <pralin/config_p.h>
#include <pralin/definitions/image>

#include <pralin/values/class_tag>
#include <pralin/values/image>
#include <pralin/values/polygon>
#include <pralin/values/tagged>

#define SHOW_RESULTS

using namespace std::string_literals;

int main(int _argc, char** _argv)
{
  pralin::algorithms_registry::load_definitions_from(PRALIN_BUILD_DEFINITIONS_DIRECTORY, false);
  if(_argc != 2)
  {
    pralin_fatal("Invalid numer of arguments!");
  }
  std::string filename = _argv[1];
  pralin::definitions::image::reader::instance image_reader;
  pralin::algorithms_registry::create(&image_reader, "imread");
  if(not image_reader.is_valid())
  {
    pralin_fatal("Could not find 'imread' algorithm.");
  }

  pralin::values::image image;
  image_reader.process(filename, &image);

#ifdef SHOW_RESULTS
  pralin::definitions::image::viewer::instance image_viewer;
  pralin::algorithms_registry::create(&image_viewer, "imshow", {{"title", "original"s}});
  if(not image_viewer.is_valid())
  {
    pralin_fatal("Could not find 'image_viewer' algorithm");
  }
  image_viewer.process(image);
#endif

  std::vector<pralin::definitions::image::object_detection::instance> object_detects;
  pralin::algorithms_registry::create_all(&object_detects);
  if(object_detects.size() == 0)
  {
    std::abort();
  }
  int i = 0;
  for(pralin::definitions::image::object_detection::instance algo : object_detects)
  {
    std::cout << "===================" << algo.get_full_name()
              << "===================" << std::endl;
    pralin::values::tagged_polygons_vector<pralin::values::class_tag> result;
    try
    {
      algo.process(image, &result);
    }
    catch(const pralin::exception& ex)
    {
      std::cout << "Failed to run algorith: " << ex.what() << std::endl;
      continue;
    }

    std::cout << "Found: " << result.size() << " objects:" << std::endl;
    for(pralin::values::tagged_polygon<pralin::values::class_tag> obj : result)
    {
      pralin::values::point center = pralin::values::rect_interface(obj.value).center();
      pralin::values::point size = pralin::values::rect_interface(obj.value).size();
      std::cout << " - " << center.x << ", " << center.y << "[" << size.x << "x" << size.y
                << "]: " << obj.tag.label << " (" << obj.tag.confidence << ")\n";
    }
#ifdef SHOW_RESULTS
    pralin::definitions::image::object_detection_viewer::instance object_detection_viewer;
    pralin::algorithms_registry::create(&object_detection_viewer, "imshow_object_detections",
                                        {{"title", "detection" + std::to_string(i++)}});
    object_detection_viewer.process(image, result);
#endif
  }
#ifdef SHOW_RESULTS
  while(true)
  {
  }
#endif
  return 0;
}
