#include <pralin/algorithm_definition>
#include <pralin/algorithms_registry>
#include <pralin/definitions/nlp>
#include <pralin/values/class_tag>

#include <iostream>

int main(int _argc, char** _argv)
{
  pralin::algorithms_registry::load_default_definitions();

  if(_argc != 2)
  {
    std::cerr << "pralin_example_fasttext_classification 'some sentence as argument'\n"
                 "For instance './pralin_example_fasttext_classification_algo_definition \"Why not "
                 "put knives in the dishwasher?\"'"
              << std::endl;
    return -1;
  }

  std::string text = _argv[1];

  using namespace std::string_literals;
  pralin::any_value_map params{
    {"module", "test_data"s},
    {"filename", "model_cooking.bin"s},
    {"source",
     "https://gitlab.com/cyloncore/pralin_data/-/raw/stable/models/fasttext/model_cooking.bin"s}};

  pralin::definitions::nlp::classification::instance classification;
  pralin::algorithms_registry::create(&classification, "fasttext", "classification", params);

  pralin::values::class_vector detections;
  classification.process(text, &detections);

  std::cout << "Got " << detections.size() << " detections for sentence '" << text << "'"
            << std::endl;
  for(const pralin::values::class_tag& ct_detect : detections)
  {
    std::cout << ct_detect.confidence << ": " << ct_detect.label << std::endl;
  }
  return 0;
}
