function(pralin_setup NAMESPACE)
  add_custom_target(pralin_specs ALL)
  set(PRALIN_MODULE_NAMESPACE ${NAMESPACE} PARENT_SCOPE)
endfunction()

function(pralin_declare_module NAME )
  if (NAME)
  else()
    message(FATAL_ERROR "Missing NAME in pralin_declare_module call")
  endif()

  cmake_parse_arguments(pralin_declare_module "CUDA;NO_HEADERS_INSTALL;ADD_HEADERS_TO_DOXYGEN" "PREFIX" "SOURCES;HEADERS;LIBRARIES" ${ARGN})

  if(pralin_declare_module_PREFIX)
    set(LIBRARY_NAME pralin_${pralin_declare_module_PREFIX}_${NAME}_algorithms)
  else()
    set(LIBRARY_NAME pralin_${NAME}_algorithms)
  endif()
  
  add_library(${LIBRARY_NAME} SHARED ${pralin_declare_module_SOURCES} )
  target_link_libraries(${LIBRARY_NAME} PUBLIC pralin PRIVATE ${pralin_declare_module_LIBRARIES})
  set(CURRENT_LD_LIBRARY_PATH "$ENV{LD_LIBRARY_PATH}")
  # message(FATAL_ERROR ${CURRENT_LD_LIBRARY_PATH})

  if(PRALIN_SPECGEN_EXECUTABLE)
    set(CT_EXECUTABLE ${PRALIN_SPECGEN_EXECUTABLE})
  elseif(PRALIN_EXECUTABLE)
  set(CT_EXECUTABLE ${PRALIN_EXECUTABLE})
  set(CT_COMMAND specgen)
  else()
  endif()

  add_custom_target(${LIBRARY_NAME}_specs
                    COMMAND ${CMAKE_COMMAND} -E env LD_LIBRARY_PATH="${PRALIN_SPECGEN_LD_LIBRARY_PATH}:${CURRENT_LD_LIBRARY_PATH}" ${CT_EXECUTABLE} ${CT_COMMAND} $<TARGET_FILE:${LIBRARY_NAME}> ${CMAKE_CURRENT_BINARY_DIR}/pralin_specs/${LIBRARY_NAME}
                    DEPENDS ${PRALIN_SPECGEN_EXECUTABLE} ${PRALIN_EXECUTABLE}
                    BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/pralin_specs/${LIBRARY_NAME})

  if(NOT INSTALL_LIB_DIR)
    message(FATAL_ERROR "Empty INSTALL_LIB_DIR.")
  endif()
  if(NOT INSTALL_SPECS_DIR)
    message(FATAL_ERROR "Empty INSTALL_SPECS_DIR.")
  endif()
  install(TARGETS ${LIBRARY_NAME} LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT shlib)
  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/pralin_specs/${LIBRARY_NAME} DESTINATION ${INSTALL_SPECS_DIR})
  
  if(NOT pralin_declare_module_NO_HEADERS_INSTALL)
    if(NOT INSTALL_INCLUDE_DIR)
      message(FATAL_ERROR "Empty INSTALL_INCLUDE_DIR.")
    endif()
    install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.h ${pralin_declare_module_HEADERS} DESTINATION ${INSTALL_INCLUDE_DIR}/${PRALIN_MODULE_NAMESPACE}/algorithms/src/${pralin_declare_module_PREFIX}/${NAME})
    install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/../../${NAME} DESTINATION ${INSTALL_INCLUDE_DIR}/${PRALIN_MODULE_NAMESPACE}/algorithms/${pralin_declare_module_PREFIX})
  endif()
  if(pralin_declare_module_ADD_HEADERS_TO_DOXYGEN)
  pralin_add_headers_to_doxygen(${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.h ${HEADERS} ${CMAKE_CURRENT_SOURCE_DIR}/../../${NAME})
  endif()
  # Make examples work without install
  
  if(PRALIN_BUILD_DEFINITIONS_DIRECTORY)
    add_custom_target(${LIBRARY_NAME}_copy 
                      COMMAND ${CMAKE_COMMAND} -E make_directory ${PRALIN_BUILD_DEFINITIONS_DIRECTORY}
                      COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_BINARY_DIR}/pralin_specs/${LIBRARY_NAME} ${PRALIN_BUILD_DEFINITIONS_DIRECTORY}
                      COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:${LIBRARY_NAME}> ${PRALIN_BUILD_DEFINITIONS_DIRECTORY}/../..
                      DEPENDS ${LIBRARY_NAME}_specs)
    
    add_dependencies(pralin_specs ${LIBRARY_NAME}_copy)
  else()
    add_dependencies(pralin_specs ${LIBRARY_NAME}_specs)
  endif()
endfunction()

function(pralin_declare_backend NAME)
  if (NAME)
  else()
    message(FATAL_ERROR "Missing NAME in pralin_declare_backend call")
  endif()

  cmake_parse_arguments(pralin_declare_backend "" "" "SOURCES;LINK_LIBRARIES;HEADERS" ${ARGN})

  set(LIBRARY_NAME pralin_${NAME}_backend)
  add_library(${LIBRARY_NAME} SHARED ${pralin_declare_backend_SOURCES})
  target_link_libraries(${LIBRARY_NAME} pralin ${pralin_declare_backend_LINK_LIBRARIES})
  install(TARGETS ${LIBRARY_NAME} EXPORT pralinTargets ${INSTALL_TARGETS_DEFAULT_ARGS})
  install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.h ${pralin_declare_backend_HEADERS} DESTINATION ${INSTALL_INCLUDE_DIR}/${PRALIN_MODULE_NAMESPACE}/backends/src/${NAME})
  install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/../../${NAME} DESTINATION ${INSTALL_INCLUDE_DIR}/${PRALIN_MODULE_NAMESPACE}/backends/)
endfunction()
