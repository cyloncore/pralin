# - Try to find ZBar
# This will define
#
#  ZBar_FOUND - 
#  ZBar_LIBRARY_DIR - 
#  ZBar_INCLUDE_DIR - 
#  ZBar_LIBRARIES - 
#

find_package(PkgConfig)
pkg_check_modules(PC_ZBar QUIET zbar)
set(ZBar_DEFINITIONS ${PC_ZBar_CFLAGS_OTHER})
find_library(ZBar_LIBRARIES NAMES zbar 
             HINTS ${PC_ZBar_LIBDIR} ${PC_ZBar_LIBRARY_DIRS} )
find_path(ZBar_INCLUDE_DIR Decoder.h
          HINTS ${PC_ZBar_INCLUDEDIR} ${PC_ZBar_INCLUDE_DIRS}
          PATH_SUFFIXES zbar )
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ZBar  DEFAULT_MSG  ZBar_LIBRARIES ZBar_INCLUDE_DIR)

add_library(zbar INTERFACE IMPORTED)
set_target_properties(zbar PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${ZBar_INCLUDE_DIR}"
  INTERFACE_LINK_LIBRARIES ${ZBar_LIBRARIES}
)
