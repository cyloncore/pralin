# - Try to find sentencepiece
# This will define
#
#  sentencepiece_FOUND - 
#  sentencepiece_LIBRARY_DIR - 
#  sentencepiece_INCLUDE_DIR - 
#  sentencepiece_LIBRARIES - 
#
# And a sentencepiece targe

find_package(PkgConfig)
pkg_check_modules(PC_sentencepiece QUIET sentencepiece)
set(sentencepiece_DEFINITIONS ${PC_sentencepiece_CFLAGS_OTHER})

find_library(sentencepiece_LIBRARIES NAMES sentencepiece 
             HINTS ${PC_sentencepiece_LIBDIR} ${PC_sentencepiece_LIBRARY_DIRS} )
find_path(sentencepiece_INCLUDE_DIR sentencepiece_processor.h
          HINTS ${PC_sentencepiece_INCLUDEDIR} ${PC_sentencepiece_INCLUDE_DIRS}
          PATH_SUFFIXES tf )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(sentencepiece  DEFAULT_MSG  sentencepiece_LIBRARIES sentencepiece_INCLUDE_DIR)

add_library(sentencepiece INTERFACE IMPORTED)
set_target_properties(sentencepiece PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${sentencepiece_INCLUDE_DIR}"
  INTERFACE_LINK_LIBRARIES ${sentencepiece_LIBRARIES}
)

