#################################################################################
# Find Darknet library
#
# DARKNET_FOUND       = if the library found
# DARKNET_LIBRARY     = full path to the library
# DARKNET_INCLUDE_DIR = where to find the library headers
#
# a target `Darknet` is also created, that can be used with target_linklibraries
# 
#################################################################################


FIND_PATH(DARKNET_INCLUDE_DIR darknet.h
    DOC "Path to Darknet library include directory")

FIND_LIBRARY(DARKNET_LIBRARY
    NAMES darknet
    DOC "Path to Darknet library file")


# Handle the QUIETLY and REQUIRED arguments and set DARKNET_FOUND to TRUE
# if all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Darknet DEFAULT_MSG DARKNET_LIBRARY DARKNET_INCLUDE_DIR)

IF(DARKNET_FOUND)
  add_library(Darknet INTERFACE IMPORTED)
  set_target_properties(Darknet PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${DARKNET_INCLUDE_DIR}"
    INTERFACE_LINK_LIBRARIES ${DARKNET_LIBRARY}
  )
ENDIF()
