# Copyright (c) 2017-present, Facebook, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
# Taken from Caffe2

# - Try to find cuDNN
#
# The following variables are optionally searched for defaults
#  CuDNN_ROOT_DIR:            Base directory where all cuDNN components are found
#
# The following are set after configuration is done:
#  CuDNN_FOUND
#  CuDNN_INCLUDE_DIRS
#  CuDNN_LIBRARIES
#  CuDNN_LIBRARY_DIRS

include(FindPackageHandleStandardArgs)

set(CuDNN_ROOT_DIR "" CACHE PATH "Folder contains NVIDIA cuDNN")

find_path(CuDNN_INCLUDE_DIR cudnn.h
    HINTS ${CuDNN_ROOT_DIR} ${CUDA_TOOLKIT_ROOT_DIR}
    PATH_SUFFIXES cuda/include include)

find_library(CuDNN_LIBRARY cudnn
    HINTS ${CuDNN_ROOT_DIR} ${CUDA_TOOLKIT_ROOT_DIR}
    PATH_SUFFIXES lib lib64 cuda/lib cuda/lib64 lib/x64)

find_package_handle_standard_args(
    CUDNN DEFAULT_MSG CuDNN_INCLUDE_DIR CuDNN_LIBRARY)

if(CuDNN_FOUND)
    # get cuDNN version
  file(READ ${CuDNN_INCLUDE_DIR}/cudnn.h CuDNN_HEADER_CONTENTS)
    string(REGEX MATCH "define CuDNN_MAJOR * +([0-9]+)"
                 CuDNN_VERSION_MAJOR "${CuDNN_HEADER_CONTENTS}")
    string(REGEX REPLACE "define CuDNN_MAJOR * +([0-9]+)" "\\1"
                 CuDNN_VERSION_MAJOR "${CuDNN_VERSION_MAJOR}")
    string(REGEX MATCH "define CuDNN_MINOR * +([0-9]+)"
                 CuDNN_VERSION_MINOR "${CuDNN_HEADER_CONTENTS}")
    string(REGEX REPLACE "define CuDNN_MINOR * +([0-9]+)" "\\1"
                 CuDNN_VERSION_MINOR "${CuDNN_VERSION_MINOR}")
    string(REGEX MATCH "define CuDNN_PATCHLEVEL * +([0-9]+)"
                 CuDNN_VERSION_PATCH "${CuDNN_HEADER_CONTENTS}")
    string(REGEX REPLACE "define CuDNN_PATCHLEVEL * +([0-9]+)" "\\1"
                 CuDNN_VERSION_PATCH "${CuDNN_VERSION_PATCH}")
  # Assemble cuDNN version
  if(NOT CuDNN_VERSION_MAJOR)
    set(CuDNN_VERSION "?")
  else()
    set(CuDNN_VERSION "${CuDNN_VERSION_MAJOR}.${CuDNN_VERSION_MINOR}.${CuDNN_VERSION_PATCH}")
  endif()

  set(CuDNN_INCLUDE_DIRS ${CuDNN_INCLUDE_DIR})
  set(CuDNN_LIBRARIES ${CuDNN_LIBRARY})
  message(STATUS "Found cuDNN: v${CuDNN_VERSION}  (include: ${CuDNN_INCLUDE_DIR}, library: ${CuDNN_LIBRARY})")
  mark_as_advanced(CuDNN_ROOT_DIR CuDNN_LIBRARY CuDNN_INCLUDE_DIR)
endif()
