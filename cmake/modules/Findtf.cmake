# - Try to find tf
# This will define
#
#  TF_FOUND - 
#  TF_LIBRARY_DIR - 
#  TF_INCLUDE_DIR - 
#  TF_LIBRARIES - 
#

find_package(PkgConfig)
pkg_check_modules(PC_TF QUIET tf)
set(TF_DEFINITIONS ${PC_TF_CFLAGS_OTHER})
find_library(TF_LIBRARIES NAMES tf 
             HINTS ${PC_TF_LIBDIR} ${PC_TF_LIBRARY_DIRS} )
find_path(TF_INCLUDE_DIR tf/transform_listener.h
          HINTS ${PC_TF_INCLUDEDIR} ${PC_TF_INCLUDE_DIRS}
          PATH_SUFFIXES tf )
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(TF  DEFAULT_MSG  TF_LIBRARIES TF_INCLUDE_DIR)

add_library(ros::tf INTERFACE IMPORTED)
set_target_properties(ros::tf PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${TF_INCLUDE_DIR}"
  INTERFACE_LINK_LIBRARIES ${TF_LIBRARIES}
)

