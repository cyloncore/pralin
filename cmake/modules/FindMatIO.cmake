# - Find MatIO
# Find the Mat File I/O includes and library
# This module defines
#  MATIO_INCLUDE_DIR, where to find matio.h
#  MATIO_LIBRARIES, the libraries needed to use matio.
#  MATIO_FOUND, If false, do not try to use matio.


# Copyright (c) 2015, Cyrille Berger, <cyrille.berger@liu.se>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.


# use pkg-config to get the directories and then use these values
# in the FIND_PATH() and FIND_LIBRARY() calls

find_path(MATIO_INCLUDE_DIR matio.h
   PATHS
)

find_library(MATIO_LIBRARIES NAMES matio
   PATHS
)

if(MATIO_INCLUDE_DIR)
   set(MatIO_FOUND TRUE)
else()
   set(MatIO_FOUND FALSE)
endif()

if(NOT MatIO_FOUND)
   if(NOT MATIO_FIND_QUIETLY)
      if(MATIO_FIND_REQUIRED)
         message(FATAL_ERROR "Required package Mat File I/O Runtime NOT found")
      else(MATIO_FIND_REQUIRED)
         message(STATUS "Mat File I/O Runtime NOT found")
      endif(MATIO_FIND_REQUIRED)
   endif(NOT MATIO_FIND_QUIETLY)
endif(NOT MATIO_FOUND)

mark_as_advanced(MATIO_INCLUDE_DIR MATIO_LIBRARIES MATIO_FOUND)

if(MatIO_FOUND)

  if(NOT TARGET MatIO)
    add_library(MatIO INTERFACE IMPORTED)
  endif()
  set_target_properties(MatIO PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${MATIO_INCLUDE_DIR}"
    INTERFACE_LINK_LIBRARIES "${MATIO_LIBRARIES}"
  )

endif()