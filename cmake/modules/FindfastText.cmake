#################################################################################
# Find Facebook fastText library
#
# FASTTEXT_FOUND       = if the library found
# FASTTEXT_LIBRARY     = full path to the library
# FASTTEXT_INCLUDE_DIR = where to find the library headers
#
# a target `fastText` is also created, that can be used with target_linklibraries
# 
#################################################################################


FIND_PATH(FASTTEXT_INCLUDE_DIR fasttext/fasttext.h
    DOC "Path to fastText library include directory")

FIND_LIBRARY(FASTTEXT_LIBRARY
    NAMES fasttext
    DOC "Path to fastText library file")


# Handle the QUIETLY and REQUIRED arguments and set FASTTEXT_FOUND to TRUE
# if all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(fastText DEFAULT_MSG FASTTEXT_LIBRARY FASTTEXT_INCLUDE_DIR)

IF(FASTTEXT_FOUND)
  add_library(fastText INTERFACE IMPORTED)
  set_target_properties(fastText PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${FASTTEXT_INCLUDE_DIR}"
    INTERFACE_LINK_LIBRARIES ${FASTTEXT_LIBRARY}
  )
ENDIF()
