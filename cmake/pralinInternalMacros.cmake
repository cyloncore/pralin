
function(pralin_add_rspec_test)
  cmake_parse_arguments(pralin_add_rspec_test "" "NAME;COMMAND" "" ${ARGN})

  if(PRALIN_TESTS_OUTPUT_JUNIT)
    set(RSPEC_EXTRA_ARGS --format RspecJunitFormatter --out ${PRALIN_TESTS_JUNIT_OUTPUT_DIRECTORY}/${pralin_add_rspec_test_NAME}.xml)
  endif()

  add_test(NAME ${pralin_add_rspec_test_NAME} COMMAND ${CMAKE_COMMAND} -E env PRALIN_BUILD_DEFINITIONS_DIRECTORY=${PRALIN_BUILD_DEFINITIONS_DIRECTORY} RUBYLIB=${CMAKE_SOURCE_DIR}/bindings/ruby:${CMAKE_BINARY_DIR}/bindings/ruby /usr/bin/env rspec ${pralin_add_rspec_test_COMMAND} ${RSPEC_EXTRA_ARGS} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

endfunction()

function(pralin_add_py_test)
  cmake_parse_arguments(pralin_add_py_test "" "NAME;COMMAND" "" ${ARGN})

  if(PRALIN_TESTS_OUTPUT_JUNIT)
    set(PYTEST_EXTRA_ARGS -m pytest --junitxml ${PRALIN_TESTS_JUNIT_OUTPUT_DIRECTORY}/${pralin_add_py_test_NAME}.xml)
  endif()

  add_test(NAME ${pralin_add_py_test_NAME} COMMAND ${CMAKE_COMMAND} -E env PRALIN_BUILD_DEFINITIONS_DIRECTORY=${PRALIN_BUILD_DEFINITIONS_DIRECTORY} ${Python_EXECUTABLE} ${PYTEST_EXTRA_ARGS} ${pralin_add_py_test_COMMAND} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

endfunction()

function(pralin_make_absolute OUTPUT_VAR DIRECTORY)
  foreach(p ${ARGN})
    file(REAL_PATH ${p} p_rp BASE_DIRECTORY ${DIRECTORY} EXPAND_TILDE)
    list(APPEND ${OUTPUT_VAR} ${p_rp})
  endforeach()
  set(${OUTPUT_VAR} ${${OUTPUT_VAR}} PARENT_SCOPE)
endfunction()

function(pralin_add_headers_to_doxygen )
  pralin_make_absolute(TMP ${CMAKE_CURRENT_SOURCE_DIR} ${ARGN})
  list(APPEND PRALIN_DOXYGEN_INPUT ${TMP})
  set(PRALIN_DOXYGEN_INPUT ${PRALIN_DOXYGEN_INPUT} CACHE INTERNAL "" FORCE)
endfunction()

function(pralin_install_headers DESTINATION)
  pralin_add_headers_to_doxygen(${ARGN})
  install(FILES ${ARGN} DESTINATION ${DESTINATION})
endfunction()
