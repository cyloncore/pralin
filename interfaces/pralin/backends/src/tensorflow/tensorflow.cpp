#include "tensorflow.h"

// #include "tensorflow/cc/ops/standard_ops.h"

#include <math_ops.h>

using namespace pralin::backends::tensorflow;

struct image_backend::data
{
  ::tensorflow::Tensor tensor;
};

image_backend::image_backend(const ::tensorflow::Tensor& _mat) : d(new data) { d->tensor = _mat; }

image_backend::~image_backend() { delete d; }

std::size_t image_backend::get_width() const { return d->tensor.dim_size(1); }

std::size_t image_backend::get_width_step() const
{
  return get_width() * get_channels() * values::scalar::size(get_channel_type());
}

std::size_t image_backend::get_height() const { return d->tensor.dim_size(2); }

std::size_t image_backend::get_channels() const { return d->tensor.dim_size(3); }

uint8_t* image_backend::get_data() const
{
  return reinterpret_cast<uint8_t*>(const_cast<char*>(d->tensor.tensor_data().data()));
}

pralin::values::image::abstract_backend* image_backend::cast(pralin::values::scalar_type _ct,
                                                             double _scale_factor) const
{
#if 0
  std::vector<std::pair<string, tensorflow::Tensor>> inputs = {
      {"input", input},
  };
  
  
//   ::tensorflow::Tensor dst(shape,  );
  tensorflow::GraphDef graph;
  tensorflow::ops::Cast cast(scope, d->tensor);
  
  std::unique_ptr<tensorflow::Session> session(
      tensorflow::NewSession(tensorflow::SessionOptions()));
  TF_RETURN_IF_ERROR(session->Create(graph));
  TF_RETURN_IF_ERROR(session->Run({inputs}, {"dim"}, {}, out_tensors));
  
  return new image_backend( );
  return Status::OK();
#endif
  return nullptr;
}

#if 0
pralin::values::scalar_type image_backend::get_channel_type() const

::tensorflow::Tensor image_backend::get_tensor() const;
int image_backend::channel_type(pralin::values::scalar_type _ct);
values::image image_backend::create(const ::tensorflow::Tensor& _math);
::tensorflow::Tensor image_backend::get_tensor(const values::image& _image);

#endif

#if 0
Mat frame;
frame=imread("./cara.jpg");
Tensor inputImg(tensorflow::DT_UINT8, tensorflow::TensorShape({1,frame.rows,frame.cols,3}));
uint8_t *p = inputImg.flat<tensorflow::uint8>().data();
Mat cameraImg(frame.rows, frame.cols, CV_8UC3, p);
frame.convertTo(cameraImg, CV_8UC3);
#endif
