#pragma once

#include <tensorflow/core/framework/tensor.h>

#include <pralin/values/image>

namespace pralin::backends::tensorflow
{
  class image_backend : public pralin::values::image::abstract_backend
  {
  public:
    image_backend(const ::tensorflow::Tensor& _mat);
    virtual ~image_backend();
    std::size_t get_width() const override;
    std::size_t get_width_step() const override;
    std::size_t get_height() const override;
    std::size_t get_channels() const override;
    uint8_t* get_data() const override;
    pralin::values::image::abstract_backend* cast(pralin::values::scalar_type _ct,
                                                  double _scale_factor) const override;
    pralin::values::scalar_type get_channel_type() const override;
    // pralin::values::tensor to_tensor() const override;

    ::tensorflow::Tensor get_tensor() const;
  public:
    static ::tensorflow::DataType data_type(pralin::values::scalar_type _ct);
    static values::image create(const ::tensorflow::Tensor& _math);
    static ::tensorflow::Tensor get_tensor(const values::image& _image);
  private:
    struct data;
    data* const d;
  };
} // namespace pralin::backends::tensorflow
