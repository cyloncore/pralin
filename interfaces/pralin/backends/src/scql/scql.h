#include <pralin/forward>
#include <scQL/Forward.h>

namespace pralin::backends::scql
{

  ::pralin::values::image get_image(const scQL::Core::ResultValue& _value);
  ::pralin::values::lidar_scan get_lidar_scan(const scQL::Core::ResultValue& _value);
  ::pralin::values::point_cloud get_lidar3d_scan(const scQL::Core::ResultValue& _value);
  ::pralin::values::point_cloud get_point_cloud(const scQL::Core::ResultValue& _value);

} // namespace pralin::backends::scql
