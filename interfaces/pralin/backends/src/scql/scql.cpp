#include "scql.h"

#include <scQL/Core/ResultValue.h>
#include <scQL/Data/Image.h>
#include <scQL/Data/Lidar3DScan.h>
#include <scQL/Data/LidarScan.h>
#include <scQL/Data/PointCloud.h>

#include <pralin/backends/knowl>

namespace pralin::backends::scql
{
  namespace details
  {
    template<typename _PV_, typename _B_, typename _SD_>
    _PV_ get_value(const scQL::Core::ResultValue& _value)
    {
      auto const [success, value, errorMessage] = _value.value<_SD_>();
      if(success)
      {
        _PV_ pc = _B_::create(value.value());
        return pc;
      }
      else
      {
        throw pralin::exception("Failed to get value: {}", errorMessage);
      }
    }
  } // namespace details
  ::pralin::values::image get_image(const scQL::Core::ResultValue& _value)
  {
    return details::get_value<::pralin::values::image, pralin::backends::knowl::image_backend,
                              scQL::Data::Image>(_value);
  }
  ::pralin::values::lidar_scan get_lidar_scan(const scQL::Core::ResultValue& _value)
  {
    return details::get_value<::pralin::values::lidar_scan,
                              pralin::backends::knowl::lidar_scan_backend, scQL::Data::LidarScan>(
      _value);
  }
  ::pralin::values::point_cloud get_lidar3d_scan(const scQL::Core::ResultValue& _value)
  {
    return details::get_value<::pralin::values::point_cloud,
                              pralin::backends::knowl::lidar3d_scan_backend,
                              scQL::Data::Lidar3DScan>(_value);
  }
  ::pralin::values::point_cloud get_point_cloud(const scQL::Core::ResultValue& _value)
  {
    return details::get_value<::pralin::values::point_cloud,
                              pralin::backends::knowl::point_cloud_backend, scQL::Data::PointCloud>(
      _value);
  }

} // namespace pralin::backends::scql
