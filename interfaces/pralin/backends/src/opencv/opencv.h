#pragma once

#include <opencv2/core/mat.hpp>

#include <pralin/values/image>

namespace pralin::backends::opencv
{
  class image_backend : public pralin::values::image::abstract_backend
  {
  public:
    image_backend(const cv::Mat& _mat, const pralin::values::image::colorspace& _cs);
    virtual ~image_backend();
    std::size_t get_width() const override;
    std::size_t get_width_step() const override;
    std::size_t get_height() const override;
    std::size_t get_channels() const override;
    uint8_t* get_data() const override;
    pralin::values::image::colorspace get_colorspace() const override;
    pralin::values::image::abstract_backend* cast(pralin::values::scalar_type _ct,
                                                  double _scale_factor) const override;
    pralin::values::scalar_type get_channel_type() const override;
    // pralin::values::tensor to_tensor() const override;

    cv::Mat get_mat() const;
    cv::_InputArray get_input_array() const;
    cv::_OutputArray get_output_array() const;
  public:
    static int channel_type(pralin::values::scalar_type _ct);
    static values::image create(const pralin::values::meta_data& _meta_data, const cv::Mat& _mat,
                                const pralin::values::image::colorspace& _cs);
    static cv::Mat get_mat(const values::image& _image);
    static cv::_InputArray get_input_array(const values::image& _image);
    static cv::_OutputArray get_output_array(const values::image& _image);
  private:
    struct data;
    data* const d;
  };
} // namespace pralin::backends::opencv
