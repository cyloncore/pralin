#include "opencv.h"

#include <pralin/logging>

using namespace pralin::backends::opencv;

struct image_backend::data
{
  cv::Mat mat;
  pralin::values::image::colorspace cs;
};

image_backend::image_backend(const cv::Mat& _mat, const pralin::values::image::colorspace& _cs)
    : d(new data)
{
  d->mat = _mat;
  d->cs = _cs;
}

image_backend::~image_backend() { delete d; }

std::size_t image_backend::get_width() const { return d->mat.cols; }

std::size_t image_backend::get_width_step() const { return d->mat.step; }

std::size_t image_backend::get_height() const { return d->mat.rows; }

std::size_t image_backend::get_channels() const { return d->mat.channels(); }

pralin::values::image::colorspace image_backend::get_colorspace() const { return d->cs; }

uint8_t* image_backend::get_data() const { return d->mat.ptr(); }

cv::Mat image_backend::get_mat() const { return d->mat; }

cv::_InputArray image_backend::get_input_array() const { return d->mat; }

cv::_OutputArray image_backend::get_output_array() const { return d->mat; }

pralin::values::image::abstract_backend* image_backend::cast(pralin::values::scalar_type _ct,
                                                             double _scale_factor) const
{
  cv::Mat o;
  d->mat.convertTo(o, channel_type(_ct), _scale_factor);
  return new image_backend(o, d->cs);
}

pralin::values::scalar_type image_backend::get_channel_type() const
{
  switch(d->mat.depth())
  {
  case CV_8U:
    return pralin::values::scalar_type::uint_8;
  case CV_8S:
    return pralin::values::scalar_type::int_8;
  case CV_16U:
    return pralin::values::scalar_type::uint_16;
  case CV_16S:
    return pralin::values::scalar_type::int_16;
  case CV_32S:
    return pralin::values::scalar_type::int_32;
  case CV_32F:
    return pralin::values::scalar_type::float_32;
  case CV_64F:
    return pralin::values::scalar_type::float_64;
  }
  pralin_fatal("unknown opencv channel type");
}

int image_backend::channel_type(pralin::values::scalar_type _ct)
{
  switch(_ct)
  {
  case pralin::values::scalar_type::uint_8:
    return CV_8U;
  case pralin::values::scalar_type::int_8:
    return CV_8S;
  case pralin::values::scalar_type::uint_16:
    return CV_16U;
  case pralin::values::scalar_type::int_16:
    return CV_16S;
  case pralin::values::scalar_type::uint_32:
    pralin_fatal("uint32 is not available in OpenCV");
  case pralin::values::scalar_type::int_32:
    return CV_32S;
  case pralin::values::scalar_type::float_32:
    return CV_32F;
  case pralin::values::scalar_type::float_64:
    return CV_64F;
  case pralin::values::scalar_type::int_64:
  case pralin::values::scalar_type::uint_64:
    pralin_fatal("64bits integer channels are not defined in OpenCV");
  }
  pralin_fatal("unknown pralin channel type");
}

pralin::values::image image_backend::create(const pralin::values::meta_data& _meta_data,
                                            const cv::Mat& _mat,
                                            const pralin::values::image::colorspace& _cs)
{
  return pralin::values::image(_meta_data, new image_backend(_mat, _cs));
}

cv::Mat image_backend::get_mat(const values::image& _image)
{
  const image_backend* ib = dynamic_cast<const image_backend*>(_image.get_backend());
  if(ib)
  {
    return ib->get_mat();
  }
  else
  {
    int type;
    switch(_image.get_channel_type())
    {
    case pralin::values::scalar_type::uint_8:
      type = CV_8UC(_image.get_channels());
      break;
    case pralin::values::scalar_type::int_8:
      type = CV_8SC(_image.get_channels());
      break;
    case pralin::values::scalar_type::uint_16:
      type = CV_16UC(_image.get_channels());
      break;
    case pralin::values::scalar_type::int_16:
      type = CV_16SC(_image.get_channels());
      break;
    case pralin::values::scalar_type::uint_32:
      pralin_fatal("uint32 is not available in OpenCV");
    case pralin::values::scalar_type::int_32:
      type = CV_32SC(_image.get_channels());
      break;
    case pralin::values::scalar_type::float_32:
      type = CV_32FC(_image.get_channels());
      break;
    case pralin::values::scalar_type::float_64:
      type = CV_64FC(_image.get_channels());
      break;
    case pralin::values::scalar_type::int_64:
    case pralin::values::scalar_type::uint_64:
      pralin_fatal("64bits integer channels are not defined in OpenCV");
    }
    return cv::Mat(_image.get_height(), _image.get_width(), type,
                   const_cast<void*>((void*)_image.get_data()), _image.get_width_step());
  }
}

cv::_InputArray image_backend::get_input_array(const values::image& _image)
{
  const image_backend* ib = dynamic_cast<const image_backend*>(_image.get_backend());
  if(ib)
  {
    return ib->get_input_array();
  }
  else
  {
    return get_mat(_image);
  }
}

cv::_OutputArray image_backend::get_output_array(const values::image& _image)
{
  const image_backend* ib = dynamic_cast<const image_backend*>(_image.get_backend());
  if(ib)
  {
    return ib->get_output_array();
  }
  else
  {
    return get_mat(_image);
  }
}
