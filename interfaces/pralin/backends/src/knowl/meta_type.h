#include <pralin/meta_type>

#include <knowCore/Value.h>

namespace pralin::backends::knowl
{
  knowCore::ValueHash convert(const any_value_map& _map);
}

namespace pralin
{
  template<>
  struct meta_type::getter_implementation<knowCore::Value>
  {
    static meta_type get()
    {
      static meta_type mt = meta_type::create<knowCore::Value>();
      return mt;
    }
  };
  template<>
  class any_value_converter<knowCore::Value, std::string>
  {
  public:
    static knowCore::Value convert_from(const std::string& _value)
    {
      return knowCore::Value::fromValue(QString::fromStdString(_value));
    }
  };
  template<>
  struct any_value_converters_initial_list<knowCore::Value> : details::pack<std::string>
  {
  };

} // namespace pralin
