#include <cres_qt>

template<typename _T_, typename... _TArgs_>
inline void rv_try_assign(_T_* _value, const cres_qresult<_T_>& _result, const char* _format,
                          const _TArgs_&... _args)
{
  if(_result.is_successful())
  {
    *_value = _result.value();
  }
  else
  {
    throw pralin::exception(
      std::vformat(_format, std::make_format_args(_args..., _result.error())));
  }
}

template<typename _T_, typename... _TArgs_>
inline _T_ rv_try_get(const cres_qresult<_T_>& _result, const char* _format,
                      const _TArgs_&... _args)
{
  if(_result.is_successful())
  {
    return _result.get_value();
  }
  else
  {
    throw pralin::exception(
      std::vformat(_format, std::make_format_args(_args..., _result.get_error())));
  }
}

template<typename _T_, typename... _TArgs_>
inline void rv_try_void(const cres_qresult<_T_>& _result, const char* _format,
                        const _TArgs_&... _args)
{
  if(not _result.is_successful())
  {
    throw pralin::exception(
      std::vformat(_format, std::make_format_args(_args..., _result.get_error())));
  }
}
