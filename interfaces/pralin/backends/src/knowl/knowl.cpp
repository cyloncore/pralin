#include "knowl.h"

#include <knowCore/ValueHash.h>

knowCore::ValueHash pralin::backends::knowl::convert(const any_value_map& _map)
{
  knowCore::ValueHash vh;
  for(auto const& [k, v] : _map)
  {
    vh.insert(QString::fromStdString(k), v.to_value<knowCore::Value>());
  }
  return vh;
}
