#include "knowl.h"

using namespace pralin::backends::knowl;
namespace values = pralin::values;

struct image_backend::data
{
  knowValues::Values::Image image;
};

image_backend::image_backend(const knowValues::Values::Image& _image) : d(new data{_image}) {}

image_backend::~image_backend() { delete d; }

std::size_t image_backend::get_width() const { return d->image->image().width(); }

std::size_t image_backend::get_width_step() const
{
  return d->image->image().width() * d->image->image().pixelSize();
}

std::size_t image_backend::get_height() const { return d->image->image().height(); }

std::size_t image_backend::get_channels() const { return d->image->image().channels(); }

uint8_t* image_backend::get_data() const { return d->image->image().dataPtr(); }

values::scalar_type image_backend::get_channel_type() const
{
  using kType = knowCore::Image::Type;
  using scalar_type = values::scalar_type;
  switch(d->image->image().type())
  {
  case kType::UnsignedInteger8:
    return scalar_type::uint_8;
  case kType::Integer8:
    return scalar_type::int_8;
  case kType::UnsignedInteger16:
    return scalar_type::uint_16;
  case kType::Integer16:
    return scalar_type::int_16;
  case kType::UnsignedInteger32:
    return scalar_type::uint_32;
  case kType::Integer32:
    return scalar_type::int_32;
  case kType::Float32:
    return scalar_type::float_32;
  case kType::Float64:
    return scalar_type::float_64;
  }
  throw pralin::exception("Unsupported channel type.");
}

values::image::colorspace image_backend::get_colorspace() const
{
  using kColorSpace = knowCore::Image::ColorSpace;
  switch(d->image->image().colorSpace())
  {
  case kColorSpace::RGB:
    return values::image::colorspace::rgb();
  case kColorSpace::BGR:
    return values::image::colorspace::bgr();
  case kColorSpace::Unknown:
    return values::image::colorspace();
    // also update image_backend::get_value if more colorspaces are added to knowCore::Image
  }
  throw pralin::exception("Unsupported colorspace.");
}

namespace
{
  knowCore::Image::Type st_to_kit(values::scalar_type _st)
  {
    using scalar_type = values::scalar_type;
    switch(_st)
    {
    case scalar_type::uint_8:
      return knowCore::Image::Type::UnsignedInteger8;
    case scalar_type::uint_16:
      return knowCore::Image::Type::UnsignedInteger16;
    case scalar_type::uint_32:
      return knowCore::Image::Type::UnsignedInteger32;
    case scalar_type::uint_64:
      throw pralin::exception(
        "64 bits unsigned integer images are not supported by knowCore::Image");
    case scalar_type::int_8:
      return knowCore::Image::Type::Integer8;
    case scalar_type::int_16:
      return knowCore::Image::Type::Integer16;
    case scalar_type::int_32:
      return knowCore::Image::Type::Integer32;
    case scalar_type::int_64:
      throw pralin::exception("64 bits integer images are not supported by knowCore::Image");
    case scalar_type::float_32:
      return knowCore::Image::Type::Float32;
    case scalar_type::float_64:
      return knowCore::Image::Type::Float64;
    }
    throw pralin::exception("Unsupported scalar type.");
  }
} // namespace

values::image::abstract_backend* image_backend::cast(values::scalar_type _ct,
                                                     double _scale_factor) const
{
  knowCore::Image ni = d->image->image().convert(st_to_kit(_ct));

  utilities::scalar_array_adaptor adaptor(_ct, ni.dataPtr(),
                                          ni.width() * ni.channels() * ni.height());
  adaptor *= _scale_factor;

  return new image_backend(knowValues::Values::Image::create()
                             .setImage(ni)
                             .setPose(d->image->pose())
                             .setTimestamp(d->image->timestamp())
                             .setFrameId(d->image->frameId()));
}

knowValues::Values::Image image_backend::get_image() const { return d->image; }

values::image image_backend::create(const knowValues::Values::Image& _image)
{
  return values::image{values::meta_data::create()
                         .set_pose(convert(_image->pose()))
                         .set_timestamp(convert(_image->timestamp()))
                         .set_frame_id(_image->frameId().toStdString()),
                       new image_backend(_image)};
}

values::image image_backend::create(const knowCore::Value& _msg)
{
  auto const [success, value, errorMessage] = _msg.value<knowValues::Values::Image>();
  if(success)
  {
    return create(value.value());
  }
  else
  {
    throw pralin::exception("Cannot convert {} to an image frame value: {}", _msg, errorMessage);
  }
}

knowValues::Values::Image image_backend::get_value(const values::image& _image)
{
  const image_backend* lsb = dynamic_cast<const image_backend*>(_image.get_backend());
  if(lsb)
  {
    return lsb->get_image();
  }
  else
  {
    knowCore::Image::ColorSpace cs = knowCore::Image::ColorSpace::Unknown;
    if(_image.get_colorspace().is_rgb())
    {
      cs = knowCore::Image::ColorSpace::RGB;
      if(_image.get_channels() != 3)
      {
        throw pralin::exception("Invalid number of channels for RGB, expected 3 got {}.",
                                _image.get_channels());
      }
    }
    else if(_image.get_colorspace().is_bgr())
    {
      cs = knowCore::Image::ColorSpace::BGR;
      if(_image.get_channels() != 3)
      {
        throw pralin::exception("Invalid number of channels for BGR, expected 3 got {}.",
                                _image.get_channels());
      }
    }
    knowCore::Image::Type type = st_to_kit(_image.get_channel_type());
    knowCore::Image img;
    if(cs != knowCore::Image::ColorSpace::Unknown)
    {
      img = knowCore::Image(_image.get_width(), _image.get_height(), cs, type);
    }
    else
    {
      img = knowCore::Image(_image.get_width(), _image.get_height(), _image.get_channels(), type);
    }
    std::copy(_image.get_data(), _image.get_data() + _image.get_width_step() * _image.get_height(),
              img.dataPtr());
    return knowValues::Values::Image::create()
      .setImage(img)
      .setPose(convert(_image.get_meta_data().get_pose()))
      .setFrameId(QString::fromStdString(_image.get_meta_data().get_frame_id()))
      .setTimestamp(knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(
        _image.get_meta_data().get_timestamp()));
  }
}
