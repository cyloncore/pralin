#include "point_cloud_private.h"

using namespace pralin::backends::knowl;

struct point_cloud_backend::data : point_cloud_private<knowValues::Values::PointCloud>
{
};

point_cloud_backend::point_cloud_backend(const knowValues::Values::PointCloud& _frame)
    : d(new data{_frame, _frame->data()})
{
}

point_cloud_backend::~point_cloud_backend() { delete d; }

const uint8_t* point_cloud_backend::get_point(std::size_t _index) const
{
  return d->get_point(_index);
}

std::size_t point_cloud_backend::get_count() const { return d->get_count(); }

void point_cloud_backend::copy_to(uint8_t* _data) const { d->copy_to(_data); }

std::size_t point_cloud_backend::get_point_size() const { return d->get_point_size(); }

knowValues::Values::PointCloud point_cloud_backend::get_point_cloud() const { return d->value; }

pralin::values::point_cloud point_cloud_backend::create(const knowValues::Values::PointCloud& _pc)
{
  return data::create(_pc);
}

pralin::values::point_cloud point_cloud_backend::create(const knowCore::Value& _msg)
{
  return data::create(_msg);
}

knowValues::Values::PointCloud point_cloud_backend::get_value(const values::point_cloud& _pc)
{
  return data::get_value(_pc);
}
