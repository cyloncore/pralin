#include "knowl.h"

using namespace pralin::backends::knowl;

struct lidar_scan_backend::data
{
  knowValues::Values::LidarScan frame;
  const QVector<float> ranges, intensities;
};

lidar_scan_backend::lidar_scan_backend(const knowValues::Values::LidarScan& _frame)
    : d(new data{_frame, _frame->ranges(), _frame->intensities()})
{
}

lidar_scan_backend::~lidar_scan_backend() { delete d; }

double lidar_scan_backend::get_range_min() const { return d->frame->rangeMin(); }

double lidar_scan_backend::get_range_max() const { return d->frame->rangeMax(); }

double lidar_scan_backend::get_angle_min() const { return d->frame->angleMin(); }

double lidar_scan_backend::get_angle_max() const { return d->frame->angleMax(); }

double lidar_scan_backend::get_angle_increment() const { return d->frame->angleIncrement(); }

pralin::utilities::scalar_const_array_adaptor lidar_scan_backend::get_ranges() const
{
  return {d->ranges.constData(), (unsigned int)(d->ranges.size())};
}

pralin::utilities::scalar_const_array_adaptor lidar_scan_backend::get_intensities() const
{
  return {d->intensities.constData(), (unsigned int)(d->intensities.size())};
}

pralin::values::scalar_unit lidar_scan_backend::get_scalar_unit() const
{
  return pralin::values::scalar_unit::meter;
}

knowValues::Values::LidarScan lidar_scan_backend::get_scan() const { return d->frame; }

pralin::values::lidar_scan lidar_scan_backend::create(const knowValues::Values::LidarScan& _msg)
{
  return pralin::values::lidar_scan{pralin::values::meta_data::create()
                                      .set_pose(convert(_msg->pose()))
                                      .set_timestamp(convert(_msg->timestamp()))
                                      .set_frame_id(_msg->frameId().toStdString()),
                                    new lidar_scan_backend(_msg)};
}

pralin::values::lidar_scan lidar_scan_backend::create(const knowCore::Value& _msg)
{
  auto const [success, value, errorMessage] = _msg.value<knowValues::Values::LidarScan>();
  if(success)
  {
    return create(value.value());
  }
  else
  {
    throw pralin::exception("Cannot convert {} to a lidar frame value: {}", _msg, errorMessage);
  }
}

knowValues::Values::LidarScan lidar_scan_backend::get_value(const values::lidar_scan& _scan)
{
  const lidar_scan_backend* lsb = dynamic_cast<const lidar_scan_backend*>(_scan.get_backend());
  if(lsb)
  {
    return lsb->get_scan();
  }
  else
  {
    return knowValues::Values::LidarScan::create()
      .setAngleIncrement(_scan.get_angle_increment())
      .setAngleMax(_scan.get_angle_max())
      .setAngleMin(_scan.get_angle_min())
      .setIntensities(convert(_scan.get_intensities()))
      .setPose(convert(_scan.get_meta_data().get_pose()))
      .setRangeMax(_scan.get_range_max())
      .setRangeMin(_scan.get_range_min())
      .setRanges(convert(_scan.get_ranges()))
      .setTimeIncrement(0)
      .setFrameId(QString::fromStdString(_scan.get_meta_data().get_frame_id()))
      .setTimestamp(knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(
        _scan.get_meta_data().get_timestamp()));
  }
}
