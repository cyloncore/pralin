#include "knowl.h"

namespace pralin::backends::knowl
{
  namespace details
  {
    template<typename _T_>
    struct point_cloud_private_helper;
    template<>
    struct point_cloud_private_helper<knowValues::Values::Lidar3DScan>
    {
      using backend = lidar3d_scan_backend;
      static knowValues::Values::Lidar3DScan get_kl_value(const lidar3d_scan_backend* _t)
      {
        return _t->get_scan();
      }
      static knowValues::Values::Lidar3DScan::Builder
        set_pose_cs(knowValues::Values::Lidar3DScan::Builder _builder, uint32_t,
                    const knowGIS::GeoPose& _pose)
      {
        return _builder.setPose(_pose);
      }
      static knowValues::Values::Lidar3DScan::Builder
        set_geometry(knowValues::Values::Lidar3DScan::Builder _builder,
                     const pralin::values::meta_data&)
      {
        return _builder;
      }
      static knowGIS::GeoPose get_pose(const knowValues::Values::Lidar3DScan& _scan)
      {
        return _scan->pose();
      }
    };
    template<>
    struct point_cloud_private_helper<knowValues::Values::PointCloud>
    {
      using backend = point_cloud_backend;
      static knowValues::Values::PointCloud get_kl_value(const point_cloud_backend* _t)
      {
        return _t->get_point_cloud();
      }
      static knowValues::Values::PointCloud::Builder
        set_pose_cs(knowValues::Values::PointCloud::Builder _builder, uint32_t _srid,
                    const knowGIS::GeoPose& _pose)
      {
        if(_srid > 0)
        {
          _builder.setCoordinateSystem(Cartography::CoordinateSystem(_srid));
        }
        return _builder.setTransformation(_pose);
      }
      static knowValues::Values::PointCloud::Builder
        set_geometry(knowValues::Values::PointCloud::Builder _builder,
                     const pralin::values::meta_data& _meta_data)
      {
        if(_meta_data.has<std::string>("geometry"))
        {
          std::string geometry_str = _meta_data.get<std::string>("geometry");
          auto const& [s, go, m]
            = knowGIS::GeometryObject::fromWKT(QString::fromStdString(geometry_str));
          if(s)
          {
            _builder.setGeometry(go.value());
          }
          else
          {
            throw pralin::exception("Failed to parse geometry '{}' with error '{}'.", geometry_str,
                                    m);
          }
        }
        return _builder;
      }
      static knowGIS::GeoPose get_pose(const knowValues::Values::PointCloud& _scan)
      {
        return _scan->transformation();
      }
    };
  } // namespace details
  template<typename _T_>
  struct point_cloud_private
  {
    _T_ value;
    QByteArray data;
    using helper = details::point_cloud_private_helper<_T_>;
    using backend = typename helper::backend;

    const uint8_t* get_point(std::size_t _index) const
    {
      return reinterpret_cast<const uint8_t*>(data.data()) + _index * get_point_size();
    }

    std::size_t get_count() const { return value->pointsCount(); }

    void copy_to(uint8_t* _data) const { std::copy(data.begin(), data.end(), _data); }

    std::size_t get_point_size() const { return value->data().size() / value->pointsCount(); }

    static pralin::values::point_cloud create(const _T_& _pc)
    {
      pralin::values::point_cloud::definition_builder db;
      for(const knowValues::Values::Definitions::PointCloudField* kl_field : _pc->fields())
      {
        pralin::values::scalar_type data_type = pralin::values::scalar_type::uint_8;
        switch(kl_field->dataType())
        {
        case knowValues::Values::Definitions::PointCloudField::DataType::Integer8:
          data_type = pralin::values::scalar_type::int_8;
          break;
        case knowValues::Values::Definitions::PointCloudField::DataType::Integer16:
          data_type = pralin::values::scalar_type::int_16;
          break;
        case knowValues::Values::Definitions::PointCloudField::DataType::Integer32:
          data_type = pralin::values::scalar_type::int_32;
          break;
        case knowValues::Values::Definitions::PointCloudField::DataType::Integer64:
          data_type = pralin::values::scalar_type::int_64;
          break;
        case knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger8:
          data_type = pralin::values::scalar_type::uint_8;
          break;
        case knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger16:
          data_type = pralin::values::scalar_type::uint_16;
          break;
        case knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger32:
          data_type = pralin::values::scalar_type::uint_32;
          break;
        case knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger64:
          data_type = pralin::values::scalar_type::uint_64;
          break;
        case knowValues::Values::Definitions::PointCloudField::DataType::Float32:
          data_type = pralin::values::scalar_type::float_32;
          break;
        case knowValues::Values::Definitions::PointCloudField::DataType::Float64:
          data_type = pralin::values::scalar_type::float_64;
          break;
        }
        db.add(kl_field->name().toStdString(), data_type, 1);
      }
      knowCore::NanoSeconds tns = _pc->timestamp().template toEpoch<knowCore::NanoSeconds>();
      uint32_t srid = _pc->pointsCoordinateSystem().srid();
      return pralin::values::point_cloud(pralin::values::meta_data::create()
                                           .set_pose(convert(helper::get_pose(_pc)))
                                           .set_srid(srid)
                                           .set_timestamp(convert(tns)),
                                         db, new backend(_pc));
    }

    static pralin::values::point_cloud create(const knowCore::Value& _msg)
    {
      auto const [success, value, errorMessage] = _msg.value<_T_>();
      if(success)
      {
        return create(value.value());
      }
      else
      {
        throw pralin::exception("Cannot convert {} to a point cloud value: {}", _msg, errorMessage);
      }
    }

    static _T_ get_value(const values::point_cloud& _pc)
    {
      const backend* pcb = dynamic_cast<const backend*>(_pc.get_backend());
      if(pcb)
      {
        return helper::get_kl_value(pcb);
      }
      else
      {
        QList<knowValues::Values::Definitions::PointCloudField*> fields;
        for(const pralin::values::point_cloud::field& field : _pc.get_fields())
        {
          knowValues::Values::Definitions::PointCloudField::DataType data_type;
          switch(field.get_scalar_type())
          {
          case pralin::values::scalar_type::int_8:
            data_type = knowValues::Values::Definitions::PointCloudField::DataType::Integer8;
            break;
          case pralin::values::scalar_type::int_16:
            data_type = knowValues::Values::Definitions::PointCloudField::DataType::Integer16;
            break;
          case pralin::values::scalar_type::int_32:
            data_type = knowValues::Values::Definitions::PointCloudField::DataType::Integer32;
            break;
          case pralin::values::scalar_type::int_64:
            data_type = knowValues::Values::Definitions::PointCloudField::DataType::Integer64;
            break;
          case pralin::values::scalar_type::uint_8:
            data_type
              = knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger8;
            break;
          case pralin::values::scalar_type::uint_16:
            data_type
              = knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger16;
            break;
          case pralin::values::scalar_type::uint_32:
            data_type
              = knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger32;
            break;
          case pralin::values::scalar_type::uint_64:
            data_type
              = knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger64;
            break;
          case pralin::values::scalar_type::float_32:
            data_type = knowValues::Values::Definitions::PointCloudField::DataType::Float32;
            break;
          case pralin::values::scalar_type::float_64:
            data_type = knowValues::Values::Definitions::PointCloudField::DataType::Float64;
            break;
          }
          fields.append(knowValues::Values::Definitions::PointCloudField::create()
                          .setCount(field.get_count())
                          .setDataType(data_type)
                          .setName(QString::fromStdString(field.get_name()))
                          .setOffset(field.get_offset()));
        }
        QByteArray data;
        data.resize(_pc.get_point_size() * _pc.get_count());
        _pc.copy_to(reinterpret_cast<uint8_t*>(data.data()));
        return helper::set_geometry(helper::set_pose_cs(_T_::create(),
                                                        _pc.get_meta_data().get_srid(),
                                                        convert(_pc.get_meta_data().get_pose())),
                                    _pc.get_meta_data())
          .setTimestamp(knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(
            _pc.get_meta_data().get_timestamp()))
          .setData(data)
          .setFields(fields)
          .setPointsCount(_pc.get_count());
      }
    }
  };

} // namespace pralin::backends::knowl
