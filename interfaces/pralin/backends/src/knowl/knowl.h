#include "meta_type.h"

#include <knowValues/Values.h>

#include <pralin/values/image>
#include <pralin/values/lidar_scan>
#include <pralin/values/point_cloud>

#include "return_value.h"

namespace pralin::backends::knowl
{
  inline uint64_t convert(const knowCore::Timestamp& _ts)
  {
    knowCore::NanoSeconds tns = _ts.template toEpoch<knowCore::NanoSeconds>();
    return rv_try_get(tns.count().toInt64(), "Failed to convert timestamp {} to int64: {}", _ts);
  }
  inline const pralin::values::geometry::pose convert(const knowGIS::GeoPose& _geo_pose)
  {
    return values::geometry::pose::create_geo_pose(
      _geo_pose.position().longitude(), _geo_pose.position().latitude(),
      _geo_pose.position().altitude(),
      {_geo_pose.orientation().x(), _geo_pose.orientation().y(), _geo_pose.orientation().z(),
       _geo_pose.orientation().w()});
  }
  inline knowGIS::GeoPose convert(const pralin::values::geometry::pose& _pose)
  {
    if(_pose.is_valid())
    {
      return knowGIS::GeoPose(
        knowGIS::GeoPoint::from(knowGIS::Point(_pose.get_x(), _pose.get_y(), _pose.get_z(),
                                               Cartography::CoordinateSystem(_pose.get_srid()))),
        {_pose.get_rotation()[0], _pose.get_rotation()[1], _pose.get_rotation()[2],
         _pose.get_rotation()[3]});
    }
    else
    {
      return knowGIS::GeoPose();
    }
  }
  inline QVector<float> convert(const pralin::utilities::scalar_const_array_adaptor& _input)
  {
    QVector<float> v;
    v.resize(_input.size());
    _input.copy_to(v.data());
    return v;
  }
  class image_backend : public pralin::values::image::abstract_backend
  {
  public:
    image_backend(const knowValues::Values::Image& _image);
    virtual ~image_backend();
    std::size_t get_width() const override;
    std::size_t get_width_step() const override;
    std::size_t get_height() const override;
    std::size_t get_channels() const override;
    uint8_t* get_data() const override;
    values::scalar_type get_channel_type() const override;
    values::image::colorspace get_colorspace() const override;
    abstract_backend* cast(values::scalar_type _ct, double _scale_factor) const override;

    knowValues::Values::Image get_image() const;
    // pralin::values::tensor to_tensor() const override;
  public:
    static values::image create(const knowValues::Values::Image& _msg);
    static values::image create(const knowCore::Value& _msg);
    static knowValues::Values::Image get_value(const values::image& _image);
  private:
    struct data;
    data* const d;
  };
  class lidar_scan_backend : public pralin::values::lidar_scan::abstract_backend
  {
  public:
    lidar_scan_backend(const knowValues::Values::LidarScan& _frame);
    virtual ~lidar_scan_backend();
    double get_range_min() const override;
    double get_range_max() const override;
    double get_angle_min() const override;
    double get_angle_max() const override;
    double get_angle_increment() const override;
    utilities::scalar_const_array_adaptor get_ranges() const override;
    utilities::scalar_const_array_adaptor get_intensities() const override;
    pralin::values::scalar_unit get_scalar_unit() const override;

    knowValues::Values::LidarScan get_scan() const;
  public:
    static values::lidar_scan create(const knowValues::Values::LidarScan& _msg);
    static values::lidar_scan create(const knowCore::Value& _msg);
    static knowValues::Values::LidarScan get_value(const values::lidar_scan& _image);
  private:
    struct data;
    data* const d;
  };
  class lidar3d_scan_backend : public pralin::values::point_cloud::abstract_backend
  {
  public:
    lidar3d_scan_backend(const knowValues::Values::Lidar3DScan& _point_cloud);
    virtual ~lidar3d_scan_backend();
    const uint8_t* get_point(std::size_t _index) const override;
    std::size_t get_count() const override;
    void copy_to(uint8_t* _data) const override;
    std::size_t get_point_size() const override;

    knowValues::Values::Lidar3DScan get_scan() const;
  public:
    static values::point_cloud create(const knowValues::Values::Lidar3DScan& _msg);
    static values::point_cloud create(const knowCore::Value& _msg);
    static knowValues::Values::Lidar3DScan get_value(const values::point_cloud& _image);
  private:
    struct data;
    data* const d;
  };
  class point_cloud_backend : public pralin::values::point_cloud::abstract_backend
  {
  public:
    point_cloud_backend(const knowValues::Values::PointCloud& _point_cloud);
    virtual ~point_cloud_backend();
    const uint8_t* get_point(std::size_t _index) const override;
    std::size_t get_count() const override;
    void copy_to(uint8_t* _data) const override;
    std::size_t get_point_size() const override;

    knowValues::Values::PointCloud get_point_cloud() const;
  public:
    static values::point_cloud create(const knowValues::Values::PointCloud& _msg);
    static values::point_cloud create(const knowCore::Value& _msg);
    static knowValues::Values::PointCloud get_value(const values::point_cloud& _image);
  private:
    struct data;
    data* const d;
  };
} // namespace pralin::backends::knowl
