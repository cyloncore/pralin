#include "pcl.h"

using namespace pralin::backends::pcl;

struct point_cloud_backend::data
{
  ::pcl::PCLPointCloud2::Ptr cloud;
};

point_cloud_backend::point_cloud_backend(::pcl::PCLPointCloud2::Ptr _point_cloud)
    : d(new data{_point_cloud})
{
}

point_cloud_backend::~point_cloud_backend() { delete d; }

const uint8_t* point_cloud_backend::get_point(std::size_t _index) const
{
  return d->cloud->data.data() + _index * d->cloud->point_step;
}

std::size_t point_cloud_backend::get_count() const { return d->cloud->height * d->cloud->width; }

std::size_t point_cloud_backend::get_point_size() const { return d->cloud->point_step; }

void point_cloud_backend::copy_to(uint8_t* _data) const
{
  std::copy(d->cloud->data.begin(), d->cloud->data.end(), _data);
}

const ::pcl::PCLPointCloud2::ConstPtr point_cloud_backend::get_point_cloud() const
{
  return d->cloud;
}

::pcl::PCLPointCloud2::Ptr point_cloud_backend::get_point_cloud() { return d->cloud; }

pralin::values::point_cloud point_cloud_backend::create(::pcl::PCLPointCloud2::Ptr _point_cloud,
                                                        const values::meta_data& _meta_data)
{
  values::point_cloud::definition_builder db;

  for(const ::pcl::PCLPointField& pcl_field : _point_cloud->fields)
  {
    values::scalar_type pcl_field_scalar_type = values::scalar_type::int_8;
    using pclPFT = ::pcl::PCLPointField::PointFieldTypes;
    switch(pcl_field.datatype)
    {
    case pclPFT::INT8:
      pcl_field_scalar_type = values::scalar_type::int_8;
      break;
    case pclPFT::UINT8:
      pcl_field_scalar_type = values::scalar_type::uint_8;
      break;
    case pclPFT::INT16:
      pcl_field_scalar_type = values::scalar_type::int_16;
      break;
    case pclPFT::UINT16:
      pcl_field_scalar_type = values::scalar_type::uint_16;
      break;
    case pclPFT::INT32:
      pcl_field_scalar_type = values::scalar_type::int_32;
      break;
    case pclPFT::UINT32:
      pcl_field_scalar_type = values::scalar_type::uint_32;
      break;
    case pclPFT::FLOAT32:
      pcl_field_scalar_type = values::scalar_type::float_32;
      break;
    case pclPFT::FLOAT64:
      pcl_field_scalar_type = values::scalar_type::float_64;
      break;
    }

    db.add(pcl_field.name, pcl_field_scalar_type, pcl_field.count);
  }

  return values::point_cloud(values::meta_data::create()
                               .set(_meta_data)
                               .set_frame_id(_point_cloud->header.frame_id)
                               .set_timestamp(_point_cloud->header.stamp),
                             db, new point_cloud_backend(_point_cloud));
}

::pcl::PCLPointCloud2::ConstPtr
  point_cloud_backend::get_point_cloud(const values::point_cloud& _point_cloud)
{
  const point_cloud_backend* pcb
    = dynamic_cast<const point_cloud_backend*>(_point_cloud.get_backend());
  if(pcb)
  {
    return pcb->get_point_cloud();
  }
  else
  {
    ::pcl::PCLPointCloud2::Ptr pc2(new ::pcl::PCLPointCloud2);
    pc2->header.frame_id = _point_cloud.get_meta_data().get_frame_id();
    pc2->header.stamp = _point_cloud.get_meta_data().get_timestamp();

    for(const values::point_cloud::field& pr_field : _point_cloud.get_fields())
    {
      ::pcl::PCLPointField pcl_field;
      pcl_field.name = pr_field.get_name();
      pcl_field.offset = pr_field.get_offset();
      pcl_field.count = pr_field.get_count();
      using pclPFT = ::pcl::PCLPointField::PointFieldTypes;
      switch(pr_field.get_scalar_type())
      {
      case values::scalar_type::int_8:
        pcl_field.datatype = pclPFT::INT8;
        break;
      case values::scalar_type::uint_8:
        pcl_field.datatype = pclPFT::UINT8;
        break;
      case values::scalar_type::int_16:
        pcl_field.datatype = pclPFT::INT16;
        break;
      case values::scalar_type::uint_16:
        pcl_field.datatype = pclPFT::UINT16;
        break;
      case values::scalar_type::int_32:
        pcl_field.datatype = pclPFT::INT32;
        break;
      case values::scalar_type::uint_32:
        pcl_field.datatype = pclPFT::UINT32;
        break;
      case values::scalar_type::float_32:
        pcl_field.datatype = pclPFT::FLOAT32;
        break;
      case values::scalar_type::float_64:
        pcl_field.datatype = pclPFT::FLOAT64;
        break;
      case values::scalar_type::int_64:
      case values::scalar_type::uint_64:
        pralin_fatal("int64 not supported in PCL");
      }
      pc2->fields.push_back(pcl_field);
    }

    pc2->width = _point_cloud.get_count();
    pc2->height = 1;
    pc2->point_step = _point_cloud.get_point_size();
    pc2->row_step = _point_cloud.get_point_size() * _point_cloud.get_count();
    pc2->data.resize(pc2->row_step);
    _point_cloud.copy_to(pc2->data.data());

    return pc2;
  }
}
