#pragma once

#include <pcl/PCLPointCloud2.h>

#include <pralin/values/point_cloud>

namespace pralin::backends::pcl
{
  class point_cloud_backend : public pralin::values::point_cloud::abstract_backend
  {
  public:
  public:
    point_cloud_backend(::pcl::PCLPointCloud2::Ptr _point_cloud);
    virtual ~point_cloud_backend();

    const uint8_t* get_point(std::size_t _index) const final;
    std::size_t get_count() const final;
    void copy_to(uint8_t* _data) const final;
    std::size_t get_point_size() const final;

    const ::pcl::PCLPointCloud2::ConstPtr get_point_cloud() const;
    ::pcl::PCLPointCloud2::Ptr get_point_cloud();
  public:
    static values::point_cloud create(::pcl::PCLPointCloud2::Ptr _point_cloud,
                                      const values::meta_data& _meta_data);
    static ::pcl::PCLPointCloud2::ConstPtr get_point_cloud(const values::point_cloud& _point_cloud);
  private:
    struct data;
    data* const d;
  };
} // namespace pralin::backends::pcl
