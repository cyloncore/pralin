#pragma once

#include <torch/script.h>

#include <pralin/values/image>

namespace pralin::backends::torch
{
  class image_backend : public pralin::values::image::abstract_backend
  {
  public:
    image_backend(const ::torch::Tensor& _mat,
                  const pralin::values::image::colorspace& _colorspace);
    virtual ~image_backend();
    pralin::values::image::colorspace get_colorspace() const override;
    std::size_t get_width() const override;
    std::size_t get_width_step() const override;
    std::size_t get_height() const override;
    std::size_t get_channels() const override;
    uint8_t* get_data() const override;
    pralin::values::image::abstract_backend* cast(pralin::values::scalar_type _ct,
                                                  double _scale_factor) const override;
    pralin::values::scalar_type get_channel_type() const override;
    // pralin::values::tensor to_tensor() const override;

    ::torch::Tensor get_tensor() const;
    static ::torch::ScalarType scalar_type(pralin::values::scalar_type _ct);
    static ::torch::Tensor get_tensor(const values::image& _image);
  private:
    struct data;
    data* const d;
  };
} // namespace pralin::backends::torch
