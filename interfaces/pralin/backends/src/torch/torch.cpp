#include "torch.h"

#include "config.h"

using namespace pralin::backends::torch;

struct image_backend::data
{
  ::torch::Tensor mat;
  pralin::values::image::colorspace cs;
};

image_backend::image_backend(const ::torch::Tensor& _mat,
                             const pralin::values::image::colorspace& _cs)
    : d(new data)
{
  d->mat = _mat;
  d->cs = _cs;
  pralin_assert(_mat.dim() >= 2);
}

image_backend::~image_backend() { delete d; }

pralin::values::image::colorspace image_backend::get_colorspace() const { return d->cs; }

std::size_t image_backend::get_width() const { return d->mat.sizes()[0]; }

std::size_t image_backend::get_width_step() const { return d->mat.stride(1); }

std::size_t image_backend::get_height() const { return d->mat.sizes()[1]; }

std::size_t image_backend::get_channels() const
{
  return d->mat.dim() == 2 ? 1 : d->mat.sizes()[3];
}

uint8_t* image_backend::get_data() const { return reinterpret_cast<uint8_t*>(d->mat.data_ptr()); }

pralin::values::image::abstract_backend* image_backend::cast(pralin::values::scalar_type _ct,
                                                             double _scale_factor) const
{
  return new image_backend(d->mat.toType(scalar_type(_ct)).mul(_scale_factor), d->cs);
}

pralin::values::scalar_type image_backend::get_channel_type() const
{
  using pt = pralin::values::scalar_type;
  switch(d->mat.scalar_type())
  {
  case ::torch::ScalarType::Byte:
  case ::torch::ScalarType::QUInt8:
    return pt::uint_8;
  case ::torch::ScalarType::Char:
  case ::torch::ScalarType::QInt8:
    return pt::int_8;
  case ::torch::ScalarType::Short:
    return pt::int_16;
  case ::torch::ScalarType::Int:
  case ::torch::ScalarType::QInt32:
    return pt::int_32;
  case ::torch::ScalarType::Long:
    return pt::int_64;
  case ::torch::ScalarType::Float:
    return pt::float_32;
  case ::torch::ScalarType::Double:
    return pt::float_64;
  case ::torch::ScalarType::NumOptions:
  case ::torch::ScalarType::Undefined:
  case ::torch::ScalarType::Half:
  case ::torch::ScalarType::ComplexHalf:
  case ::torch::ScalarType::ComplexFloat:
  case ::torch::ScalarType::ComplexDouble:
  case ::torch::ScalarType::Bool:
  case ::torch::ScalarType::BFloat16:
#ifndef PRALIN_TORCH_LESS_THAN_2_0_0
  case ::torch::ScalarType::QUInt2x4:
#endif
  case ::torch::ScalarType::QUInt4x2:
    throw exception("Unsupported torch scalar type.");
  }
  pralin_fatal("internal error");
}

torch::Tensor image_backend::get_tensor() const { return d->mat; }

::torch::ScalarType image_backend::scalar_type(pralin::values::scalar_type _ct)
{
  using pt = pralin::values::scalar_type;
  switch(_ct)
  {
  case pt::uint_8:
    return ::torch::ScalarType::Byte;
  case pt::int_8:
    return ::torch::ScalarType::Char;
  case pt::int_16:
  case pt::uint_16:
    return ::torch::ScalarType::Short;
  case pt::int_32:
  case pt::uint_32:
    return ::torch::ScalarType::Int;
  case pt::int_64:
  case pt::uint_64:
    return ::torch::ScalarType::Long;
  case pt::float_32:
    return ::torch::ScalarType::Float;
  case pt::float_64:
    return ::torch::ScalarType::Double;
  }
  pralin_fatal("internal error");
}

::torch::Tensor image_backend::get_tensor(const values::image& _image)
{
  const image_backend* ib = dynamic_cast<const image_backend*>(_image.get_backend());
  if(ib)
  {
    return ib->get_tensor();
  }
  else
  {
    return ::torch::from_blob(const_cast<uint8_t*>(_image.get_data()),
                              {static_cast<int64_t>(_image.get_height()),
                               static_cast<int64_t>(_image.get_width()),
                               static_cast<int64_t>(_image.get_channels())},
                              scalar_type(_image.get_channel_type()))
      .clone();
  }
}
