#include <Eigen/Geometry>
#include <pralin/values/geometry>

namespace pralin::backends::eigen
{
  inline Eigen::Vector3d to_vector(const values::geometry::point& _point)
  {
    return {_point.get_x(), _point.get_y(), _point.get_z()};
  }
  inline Eigen::Quaterniond to_quaternion(const values::geometry::pose& _pose)
  {
    return Eigen::Quaterniond(_pose.get_rotation().data().data());
  }
  inline values::geometry::point to_point(const Eigen::Vector3d& _vector, int _srid)
  {
    return {_vector.x(), _vector.y(), _vector.z(), _srid};
  }
  inline values::geometry::pose to_pose(const Eigen::Vector3d& _vector,
                                        const Eigen::Quaterniond& _rotation, int _srid)
  {
    return values::geometry::pose{_vector.x(),
                                  _vector.y(),
                                  _vector.z(),
                                  {_rotation.x(), _rotation.y(), _rotation.z(), _rotation.w()},
                                  _srid};
  }
  template<typename _TType_, int _Rows_, int _Cols_>
  inline values::matrix<_TType_, _Rows_, _Cols_>
    to_matrix(const Eigen::Matrix<_TType_, _Rows_, _Cols_>& _em)
  {
    return values::matrix<_TType_, _Rows_, _Cols_>(_em.data(), _em.data() + _Rows_ * _Cols_);
  }
  template<typename _TType_, typename _EType_, int _Rows_, int _Cols_>
  inline values::matrix<_TType_, _Rows_, _Cols_>
    to_matrix(const Eigen::Matrix<_EType_, _Rows_, _Cols_>& _em)
  {
    return values::matrix<_TType_, _Rows_, _Cols_>(_em.data(), _em.data() + _Rows_ * _Cols_);
  }
  template<typename _TType_, std::size_t _Rows_, std::size_t _Cols_>
  inline Eigen::Matrix<_TType_, _Rows_, _Cols_>
    to_matrix(const values::matrix<_TType_, _Rows_, _Cols_>& _vm)
  {
    return _vm.data().data();
  }
  template<typename _EType_, typename _TType_, std::size_t _Rows_, std::size_t _Cols_>
  inline auto to_matrix(const values::matrix<_TType_, _Rows_, _Cols_>& _em)
  {
    return Eigen::Matrix<_TType_, _Rows_, _Cols_>(_em.data().data()).template cast<_EType_>();
  }
  // values::geometry::transformation to_transformation(const Eigen::Matrix4d& _matrix)
  // {
  //   values::geometry::transformation tr;
  // }
  class transformation_backend : public pralin::values::geometry::transformation::abstract_backend
  {
  public:
    transformation_backend(const Eigen::Affine3d& _affine_3d);
    virtual ~transformation_backend();
    values::geometry::point transform(const values::geometry::point& _point) const override
    {
      return to_point(m_transformation * to_vector(_point), _point.get_srid());
    }
    values::geometry::pose transform(const values::geometry::pose& _pose) const override
    {
      return to_pose(
        m_transformation * to_vector(_pose),
        Eigen::Quaterniond((m_transformation.rotation() * to_quaternion(_pose)).matrix()),
        _pose.get_srid());
    }
  private:
    Eigen::Affine3d m_transformation;
  };
} // namespace pralin::backends::eigen
