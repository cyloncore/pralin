#include "pralin_llama.h"

#include <pralin/resources>

#include "llama_inference.h"

using namespace pralin::algorithms::ggml::llama;

struct chat::data
{
};

chat::chat() : d(nullptr) {}

chat::~chat() { delete d; }

void chat::process(const std::string& _prompt, const any_value& _input_context,
                   std::string* _answer, any_value* _output_context) const
{
  using llama_inference_sp = std::shared_ptr<llama_inference>;
  llama_inference_sp inf;
  if(_input_context.is_valid() and _input_context.is_of_type<llama_inference_sp>())
  {
    inf = _input_context.to_value<llama_inference_sp>();
  }
  else
  {
    std::string filename
      = resources::get(get_parameters().get_module("ggml_llama"), get_parameters().get_filename(),
                       get_parameters().get_source());
    llama_inference::parameters p;
    p.n_threads = get_parameters().get_threads();
    inf = std::make_shared<llama_inference>(p);
    inf->load_model(filename);
  }

  *_answer = inf->run(_prompt);
  *_output_context = inf;
}

void chat::init() {}

struct inference::data
{
};

inference::inference() : d(nullptr) {}

inference::~inference() { delete d; }

void inference::process(const std::string& _prompt, std::string* _answer) const
{
  std::string filename
    = resources::get(get_parameters().get_module("ggml_llama"), get_parameters().get_filename(),
                     get_parameters().get_source());
  llama_inference::parameters p;
  p.n_threads = get_parameters().get_threads();
  llama_inference li(p); // TODO find a way to reset the context rather than reload
  li.load_model(filename);
  *_answer = li.run(_prompt);
}

void inference::init() {}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("ggml/llama", description("Integration of llama implementation by ggml."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::ggml::llama::chat, "ggml/llama", "chat", pralin::algorithm_priority::cpu,
  description(
    "Chat using the LLAMA network.",
    "Take a text `prompt` as input, and return the answer from the chat bot. `context` should be "
    "looped over, if it is invalid, the algorithm will initialise the context."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::ggml::llama::inference, "ggml/llama", "inference",
  pralin::algorithm_priority::cpu,
  description("Single-use inference using LLAMA network",
              "This algorithm take an input text prompt and return the inference."))
