#pragma once

#include <pralin/definitions/ml>
#include <pralin/definitions/nlp>
#include <pralin/implementation>

namespace pralin::algorithms::ggml::llama
{
  PRALIN_PARAMETERS_EXTENDS(llama, definitions::ml::model_parameters, PRALIN_PARAMETER(int, 1),
                            threads);
  class chat : public implementation<chat, definitions::nlp::chat, llama_parameters>
  {
    friend implementation_t;
    chat();
    ~chat();
  public:
    void process(const std::string& _prompt, const any_value& _input_context, std::string* _answer,
                 any_value* _output_context) const final;
  private:
    void init();
    struct data;
    data* d;
  };
  class inference : public implementation<inference, definitions::nlp::transform, llama_parameters>
  {
    friend implementation_t;
    inference();
    ~inference();
  public:
    void process(const std::string& _prompt, std::string* _answer) const final;
  private:
    void init();
    struct data;
    data* d;
  };
} // namespace pralin::algorithms::ggml::llama
