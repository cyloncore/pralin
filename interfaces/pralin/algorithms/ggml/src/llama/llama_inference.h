/***
 * Based on libjustlm
 * Copyright (c) 2023 niansa
 * License: MIT
 * Original source: https://gitlab.com/niansa/libjustlm/
 */

#include <cstring>

#include "ggml.h"
#include "llama.h"

#include "common/grammar-parser.h"

#include <pralin/exception>

namespace pralin::algorithms::ggml::llama
{
  class llama_inference
  {
    PRALIN_META_INFO(llama_inference)
  public:
    struct parameters
    {
      int seed = 0;           // RNG seed
      unsigned n_threads = 0; // Amount of threads to use, immutable after Inference was constructed
      unsigned n_ctx = 2024;  // Context size
      unsigned n_ctx_window_top_bar
        = 0;                // Top bar of context window. Must be smaller than context size
      unsigned n_batch = 8; // Batch size
      unsigned n_repeat_last = 0;
      unsigned n_eos_ignores = 0;

      float scroll_keep = 0.0f; // 0.4f to keep 40% of context below top bar when scrolling; 0.0f to
                                // remove everything after top bar

      unsigned top_k = 40;
      float top_p = 0.9f;
      float temp = 0.72f;
      float mirostat_learning_rate = 0.1f;  // mirostat specific
      float mirostat_target_entropy = 5.0f; // mirostat specific
      float repeat_penalty = 1.0f;

      unsigned n_gpu_layers = 38;
      bool use_mlock = true;   // llama specific
      int prefer_mirostat = 0; // Use given mirostat version if available (see
                               // is_mirostat_available()); llama specific
    };
  private:
    struct state
    {
      llama_context* ctx = nullptr;
      llama_model* model = nullptr;
      llama_grammar* grammar = nullptr;
      bool grammar_override_temp;
      grammar_parser::parse_state parsed_grammar;
      std::string prompt; // Mostly here for easy "debugging"
      std::vector<int> tokens;
      unsigned n_ctx;
    };

    state* get_state() { return m_state; }
    const state* get_state() const { return m_state; }

    void init(const std::string& weights_path)
    {
      // Allocate state
      m_state = new state;

      // Get llama parameters
      llama_context_params lparams = llama_context_default_params();
      lparams.seed = m_params.seed;
      lparams.n_ctx = m_params.n_ctx = m_params.n_ctx > 0 ? m_params.n_ctx : 2024;
      lparams.n_threads = m_params.n_threads;
      // lparams.n_threads_batch = m_params.n_threads;  TODO: Is this sane?

      // Get model parameters
      llama_model_params mparams = llama_model_default_params();
      mparams.use_mlock = m_params.use_mlock;
      mparams.n_gpu_layers = m_params.n_gpu_layers;

      // Load model
      m_state->model = llama_load_model_from_file(weights_path.c_str(), mparams);
      if(!m_state->model)
      {
        throw pralin::exception("Failed to initialize llama model from file");
      }

      // Create context
      m_state->ctx = llama_new_context_with_model(m_state->model, lparams);
      if(!m_state->ctx)
      {
        throw pralin::exception("Failed to initialize llama context from model");
      }

      // Initialize some variables
      m_state->n_ctx = llama_n_ctx(m_state->ctx);
    }

    // This function reduces the size of our tokens vector according to some parameters
    // All tokens will be evaluated if scrolling was needed and true will be returned
    bool window_scroll()
    {
      // Check that we actually need to scroll
      if(m_state->tokens.size() <= m_state->n_ctx)
      {
        // Nope
        return false;
      }
      // Start scrolling
      if(m_params.scroll_keep > 0.0f)
      {
        // "Scroll" down the context window...
        unsigned keep_count = float(m_state->tokens.size() - m_params.n_ctx_window_top_bar)
                              * 0.4f; // We keep about 40%
        // Get vector of tokens to keep
        std::vector<int> tokens_in_view(m_state->tokens.end() - keep_count, m_state->tokens.end());
        // Cut down tokens vector size
        m_state->tokens.resize(m_params.n_ctx_window_top_bar + keep_count);
        // Overwrite tokens after top bar with tokens in view
        std::memcpy(m_state->tokens.data() + m_params.n_ctx_window_top_bar, tokens_in_view.data(),
                    tokens_in_view.size() * sizeof(int));
      }
      else
      {
        // Cut down tokens vector size to top bar
        m_state->tokens.resize(m_params.n_ctx_window_top_bar);
      }
      // Evaluate tokens
      return evaluate_tokens(0);
    }

    bool evaluate_tokens(size_t starting_offset)
    {
      // Evaluate tokens in batches
      unsigned it;
      for(it = starting_offset;; it += m_params.n_batch)
      {
        if(it + m_params.n_batch >= ssize_t(m_state->tokens.size()))
          break;

        // Evaluate
        const auto batch
          = llama_batch_get_one(m_state->tokens.data() + it, m_params.n_batch, it, 0);
        if(llama_decode(m_state->ctx, batch))
        {
          throw exception("Failed to evaluate tokens in batches.");
        }
      }

      // Evaluate remaining tokens
      if(it < m_state->tokens.size())
      {
        for(; it != m_state->tokens.size(); it++)
        {
          const auto batch = llama_batch_get_one(m_state->tokens.data() + it, 1, it, 0);
          if(llama_decode(m_state->ctx, batch))
          {
            throw pralin::exception("Failed to evaluate individual tokens");
          }
        }
      }
      return true;
    }

    int accept_token(int t)
    {
      if(m_state->grammar)
      {
        llama_grammar_accept_token(m_state->ctx, m_state->grammar, t);
      }
      return t;
    }

    int llama_sample_top_p_top_k()
    {
      float* logits = llama_get_logits(m_state->ctx);
      int n_vocab = llama_n_vocab(m_state->model);
      // Populate initial list of all candidates
      std::vector<llama_token_data> candidates;
      candidates.reserve(n_vocab);
      for(int token_id = 0; token_id < n_vocab; token_id++)
      {
        candidates.emplace_back(llama_token_data{token_id, logits[token_id], 0.0f});
      }
      llama_token_data_array candidates_p = {candidates.data(), candidates.size(), false};
      // Sample repeat penalty
      std::size_t n_repeat_last
        = std::min<std::size_t>(m_state->tokens.size(), m_params.n_repeat_last);
      llama_sample_repetition_penalties(
        m_state->ctx, &candidates_p,
        m_params.n_repeat_last ? (m_state->tokens.data() + m_state->tokens.size() - n_repeat_last)
                               : nullptr,
        n_repeat_last, m_params.repeat_penalty, 1.0f, 1.0f); // Might be wrong
      // Grammar sampling
      if(m_state->grammar)
      {
        llama_sample_grammar(m_state->ctx, &candidates_p, m_state->grammar);
      }
      if(!(m_state->grammar && m_state->grammar_override_temp)
         && (m_params.temp > 0.01f || m_params.temp < -0.01f))
      {
        // Temperature sampling
        switch(m_params.prefer_mirostat)
        {
        case 0:
        {
          llama_sample_top_k(m_state->ctx, &candidates_p, m_params.top_k, 1);
          llama_sample_tail_free(m_state->ctx, &candidates_p, 1.0f, 1);
          llama_sample_typical(m_state->ctx, &candidates_p, 1.0f, 1);
          llama_sample_top_p(m_state->ctx, &candidates_p, m_params.top_p, 1);
          llama_sample_temp(m_state->ctx, &candidates_p, m_params.temp);
          return accept_token(llama_sample_token(m_state->ctx, &candidates_p));
        }
        case 1:
        {
          float mirostat_mu = 2.0f * m_params.mirostat_target_entropy;
          const int mirostat_m = 100;
          llama_sample_temp(m_state->ctx, &candidates_p, m_params.temp);
          return accept_token(llama_sample_token_mirostat(
            m_state->ctx, &candidates_p, m_params.mirostat_target_entropy,
            m_params.mirostat_learning_rate, mirostat_m, &mirostat_mu));
        }
        case 2:
        {
          float mirostat_mu = 2.0f * m_params.mirostat_target_entropy;
          llama_sample_temp(m_state->ctx, &candidates_p, m_params.temp);
          return accept_token(llama_sample_token_mirostat_v2(
            m_state->ctx, &candidates_p, m_params.mirostat_target_entropy,
            m_params.mirostat_learning_rate, &mirostat_mu));
        }
        default:
          throw pralin::exception("Invalid mirostat version {}", m_params.prefer_mirostat);
        }
      }
      else
      {
        // Greedy sampling
        return accept_token(llama_sample_token(m_state->ctx, &candidates_p));
      }
    }
  public:
    llama_inference(const parameters& p) : m_params(p) {}
    ~llama_inference()
    {
      if(m_state)
      {
        if(m_state->ctx)
          llama_free(m_state->ctx);
        delete m_state;
      }
    }

    void load_model(const std::string& weights_path) { init(weights_path); }

    bool append(const std::string& prompt)
    {
      // Check if prompt was empty
      const bool was_empty = m_state->prompt.empty();

      // Append to current prompt
      m_state->prompt.append(prompt);

      // Resize buffer for tokens
      const std::size_t old_token_count = m_state->tokens.size();
      m_state->tokens.resize(old_token_count + m_state->prompt.size());

      // Run tokenizer
      const auto token_count = llama_tokenize(
        m_state->model, prompt.c_str(), prompt.size(), m_state->tokens.data() + old_token_count,
        m_state->tokens.size() - old_token_count, was_empty, false);
      m_state->tokens.resize(old_token_count + token_count);

      // Make sure token limit isn't being hit
      if(window_scroll())
      {
        // That function already has evaluated our tokens since scrolling was needed
        return true;
      }

      // Evaluate new tokens
      return evaluate_tokens(old_token_count);
    }

    std::string run(std::string_view end)
    {
      std::string fres;

      // Loop until done
      bool abort = false;
      unsigned eos_count = 0;
      size_t last_size = 0;
      while(!abort && (end.empty() || fres.find(end) == fres.npos))
      {
        last_size = fres.size();
        // Sample top p and top k
        int id;
        try
        {
          id = llama_sample_top_p_top_k();
        }
        catch(const std::exception& e)
        {
          throw pralin::exception(e.what());
        }

        if(id == llama_token_eos(m_state->model))
        {
          if(eos_count++ == m_params.n_eos_ignores)
          {
            abort = true;
            continue;
          }
          m_state->tokens.push_back(0);
          llama_tokenize(m_state->model, "\n", 1, &m_state->tokens.back(), 1, false, false);
          id = m_state->tokens.back();
        }
        else
        {
          // Add token
          m_state->tokens.push_back(id);
        }

        // Make sure token limit isn't hit
        window_scroll();

        // Get token as string
        std::string str(14, ' ');
        str.resize(llama_token_to_piece(m_state->model, id, str.data(), 14, true));

        // Append string to function result
        m_state->prompt.append(str);
        fres.append(str);

        // Evaluate token
        //  TODO: Respect batch size
        const auto batch = llama_batch_get_one(m_state->tokens.data() + m_state->tokens.size() - 1,
                                               1, m_state->tokens.size() - 1, 0);
        if(llama_decode(m_state->ctx, batch))
        {
          throw pralin::exception("Failed to evaluate new tokens");
        }
      }

      // Create final string  TODO: Could be optimized
      if(!abort && fres.size() > end.size())
      {
        fres = std::string(fres.data(), last_size);
      }

      // Return final string
      return fres;
    }

    unsigned get_context_size() const { return get_state()->tokens.size(); }

    bool load_grammar(const std::string& src, bool override_temperature)
    {
      m_state->parsed_grammar = grammar_parser::parse(src.c_str());
      if(m_state->parsed_grammar.rules.empty())
      {
        throw exception("Failed to parse grammar (or no rules)");
      }

      auto rules = m_state->parsed_grammar.c_rules();
      m_state->grammar = llama_grammar_init(rules.data(), rules.size(),
                                            m_state->parsed_grammar.symbol_ids.at("root"));
      if(!m_state->grammar)
      {
        throw exception("Failed to generate llama grammar");
      }

      m_state->grammar_override_temp = override_temperature;

      return true;
    }

    const std::string& get_prompt() const { return m_state->prompt; }
  private:
    state* m_state = nullptr;
    parameters m_params;
  };
} // namespace pralin::algorithms::ggml::llama
