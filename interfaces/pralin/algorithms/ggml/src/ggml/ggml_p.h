#include <ggml/ggml.h>

namespace pralin::algorithms::ggml
{
  static void log_callback_default(ggml_log_level level, const char* text, void* user_data);
}
