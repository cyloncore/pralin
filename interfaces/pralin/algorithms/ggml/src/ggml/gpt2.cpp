#include "gpt2_p.h"

#include <pralin/resources>

inference::inference() : d(new data) {}

inference::~inference() {}

void inference::process(const std::string& _prompt, std::string* _answer) const
{
  int n_past = 0;

  int64_t t_sample_us = 0;
  int64_t t_predict_us = 0;

  std::vector<float> logits;

  // tokenize the prompt
  std::vector<gpt_vocab::id> embd_inp = ::gpt_tokenize(d->vocab, _prompt);

  int32_t n_predict = std::min(d->params.n_predict, d->model.hparams.n_ctx - (int)embd_inp.size());

  // submit the input prompt token-by-token
  // this reduces the memory usage during inference, at the cost of a bit of speed at the beginning
  std::vector<gpt_vocab::id> embd;

  *_answer = std::string();

  for(size_t i = embd.size(); i < embd_inp.size() + n_predict; i++)
  {
    // predict
    if(embd.size() > 0)
    {
      if(!d->gpt2_eval(d->model, d->sched, n_past, embd, logits))
      {
        throw pralin::exception("Failed to predict\n");
      }
    }

    n_past += embd.size();
    embd.clear();

    if(i >= embd_inp.size())
    {
      // sample next token
      const int top_k = d->params.top_k;
      const float top_p = d->params.top_p;
      const float temp = d->params.temp;

      const int n_vocab = d->model.hparams.n_vocab;

      gpt_vocab::id id = gpt_sample_top_k_top_p(d->vocab, logits.data() + (logits.size() - n_vocab),
                                                top_k, top_p, temp, d->rng);

      // add it to the context
      embd.push_back(id);
    }
    else
    {
      // if here, it means we are still processing the input prompt
      for(size_t k = i; k < embd_inp.size(); k++)
      {
        embd.push_back(embd_inp[k]);
        if(int32_t(embd.size()) >= d->params.n_batch)
        {
          break;
        }
      }
      i += embd.size() - 1;
    }

    // display text
    for(auto id : embd)
    {
      *_answer += d->vocab.id_to_token[id];
    }
    fflush(stdout);

    // end of text token
    if(embd.back() == 50256)
    {
      break;
    }
  }
}

void inference::init()
{
  d->params.n_predict = get_parameters().get_predictions_count();
  d->params.model = resources::get(get_parameters().get_module("ggml_gpt2"),
                                   get_parameters().get_filename(), get_parameters().get_source());
  d->params.seed = std::chrono::system_clock::now().time_since_epoch().count();
  d->rng = std::mt19937(d->params.seed);

  if(!d->gpt2_model_load(d->params.model, d->model, d->vocab, d->params))
  {
    throw pralin::exception("Failed to load model from '{}'.", d->params.model);
  }

  {
    // initialize the scheduler
    d->sched = ggml_backend_sched_new(d->model.backends.data(), NULL, d->model.backends.size(),
                                      GPT2_MAX_NODES, false);

    // create the worst case graph for memory usage estimation
    int n_tokens = std::min(d->model.hparams.n_ctx, d->params.n_batch);
    int n_past = d->model.hparams.n_ctx - n_tokens;
    struct ggml_cgraph* gf
      = d->gpt2_graph(d->model, n_past, std::vector<gpt_vocab::id>(n_tokens, 0));

    ggml_backend_sched_reserve(d->sched, gf);

    // compute the required memory
    size_t mem_size = 0;
    for(size_t i = 0; i < d->model.backends.size(); i++)
    {
      size_t size = ggml_backend_sched_get_buffer_size(d->sched, d->model.backends[i]);
      if(size > 0)
      {
        mem_size += size;
        pralin_info("compute buffer size = {} MB.", ggml_backend_name(d->model.backends[i]),
                    size / 1024.0 / 1024.0);
      }
    }

    pralin_info("Total compute buffer size: {} MB.", mem_size / 1024.0 / 1024.0);
  }
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("ggml/gpt2", description("Integration of gpt2 implementation by ggml."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::ggml::gpt2::inference, "ggml/gpt2", "inference",
  pralin::algorithm_priority::cpu,
  description("Single-use inference using GPT2 network",
              "This algorithm take an input text prompt and return the inference."))
