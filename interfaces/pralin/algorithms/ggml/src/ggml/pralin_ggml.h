#pragma once

#include <pralin/definitions/ml>
#include <pralin/definitions/nlp>
#include <pralin/implementation>

namespace pralin::algorithms::ggml::gpt2
{
  PRALIN_PARAMETERS_EXTENDS(gpt2, definitions::ml::model_parameters, PRALIN_PARAMETER(int, 200),
                            predictions_count);
  class inference : public implementation<inference, definitions::nlp::transform, gpt2_parameters>
  {
    friend implementation_t;
    inference();
    ~inference();
  public:
    void process(const std::string& _prompt, std::string* _answer) const final;
  private:
    void init();
    struct data;
    data* d;
  };
} // namespace pralin::algorithms::ggml::gpt2
