#include "ggml_p.h"

#include <pralin/logging>

static void pralin::algorithms::ggml::log_callback_default(ggml_log_level level, const char* text,
                                                           void* user_data)
{
  (void)user_data;
  switch(level)
  {
  case GGML_LOG_LEVEL_ERROR:
    pralin_error(text);
    break;
  case GGML_LOG_LEVEL_WARN:
    pralin_warning(text);
    break;
  case GGML_LOG_LEVEL_INFO:
    pralin_info(text);
    break;
  }
}
