#pragma once

#include <pralin/definitions/image>
#include <pralin/implementation>

namespace pralin::algorithms::opencv::dnn
{
  PRALIN_PARAMETERS(object_detection, PRALIN_PARAMETER(float, 0.2), threshold);

  class object_detection_darknet
      : public implementation<object_detection_darknet, definitions::image::object_detection,
                              object_detection_parameters>
  {
    friend implementation_t;
  public:
    object_detection_darknet();
    ~object_detection_darknet();
    void process(const values::image& _input,
                 values::tagged_polygons_vector<values::class_tag>* _output) const final;
  private:
    void init(const std::string& _prefix, const std::string& _cfg, const std::string& _weights,
              const std::string& _label);
  private:
    struct data;
    data* d;
  };
} // namespace pralin::algorithms::opencv::dnn
