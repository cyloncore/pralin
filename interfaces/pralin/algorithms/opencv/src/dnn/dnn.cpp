#include <pralin/algorithms/opencv/dnn>

#include <fstream>

#include <opencv2/dnn.hpp>
#include <pralin/backends/opencv>
#include <pralin/resources>
#include <pralin/values/class_tag>
#include <pralin/values/polygon>
#include <pralin/values/tagged>

using namespace pralin::algorithms::opencv::dnn;

struct object_detection_darknet::data
{
  ~data()
  {
    // TODO free net/names
  }
  cv::dnn::Net net;
  std::vector<std::string> names;
};

object_detection_darknet::object_detection_darknet() : d(new data) {}

void object_detection_darknet::init(const std::string& _prefix, const std::string& _cfg,
                                    const std::string& _weights, const std::string& _labels)
{
  d->net = cv::dnn::readNetFromDarknet(pralin::resources::get(_prefix, "cfg", _cfg),
                                       pralin::resources::get(_prefix, "weights", _weights));

  std::ifstream names_file;
  names_file.open(pralin::resources::get(_prefix, "names", _labels));
  std::string line;
  while(std::getline(names_file, line))
  {
    d->names.push_back(line);
  }

  if(d->net.getLayer(d->net.getUnconnectedOutLayers()[0])->type != "Region")
  {
    throw pralin::exception("Invalid darknet network, it should be of type Region, but got '{}'",
                            d->net.getLayer(d->net.getUnconnectedOutLayers()[0])->type);
  }
}

object_detection_darknet::~object_detection_darknet() { delete d; }

void object_detection_darknet::process(
  const pralin::values::image& _input,
  pralin::values::tagged_polygons_vector<pralin::values::class_tag>* _output) const
{
  std::vector<cv::Mat> outs;
  try
  {
    cv::Mat blob = cv::dnn::blobFromImage(pralin::backends::opencv::image_backend::get_mat(_input),
                                          pralin::values::image::scale_factor(_input));
    d->net.setInput(blob);

    d->net.forward(outs, d->net.getUnconnectedOutLayersNames());
  }
  catch(cv::Exception& ex)
  {
    // re-throw as pralin::exception
    throw pralin::exception("Exception occured in OpenCV: {}", ex.what());
  }
  for(size_t i = 0; i < outs.size(); ++i)
  {
    // Network produces output blob with a shape NxC where N is a number of
    // detected objects and C is a number of classes + 4 where the first 4
    // numbers are [center_x, center_y, width, height]
    float* data = (float*)outs[i].data;
    for(int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
    {
      cv::Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
      cv::Point classIdPoint;
      double confidence;
      cv::minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
      if(confidence > get_parameters().get_threshold())
      {
        float centerX = data[0] * _input.get_width();
        float centerY = data[1] * _input.get_height();
        float width = data[2] * _input.get_width();
        float height = data[3] * _input.get_height();

        values::class_tag ct{confidence, d->names[classIdPoint.x]};
        values::polygon r
          = values::polygon::create_from_centered_rect(centerX, centerY, width, height);
        _output->push_back({ct, r});
      }
    }
  }
}

#include <pralin/algorithms_registry>
using namespace std::string_literals;

PRALIN_REGISTER_MODULE("opencv/dnn", description("Integration of OpenCV DNN module."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::opencv::dnn::object_detection_darknet, "opencv/dnn", "yolo.v3",
  (uint32_t)pralin::algorithm_priority::cpu + 10, description("YOLO v3 algorithm using OpenCV"),
  "pralin.darknet.yolo.v3"s,
  "https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/yolov3.cfg"s,
  "https://pjreddie.com/media/files/yolov3.weights"s,
  "https://raw.githubusercontent.com/pjreddie/darknet/master/data/coco.names"s)
