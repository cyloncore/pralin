#include <pralin/algorithms/opencv/imgproc>

#include <opencv2/imgproc.hpp>

#include <pralin/backends/opencv>

using namespace pralin::algorithms::opencv::imgproc;

void to_grayscale::process(const values::image& _input, values::image* _output) const
{
  switch(_input.get_channels())
  {
  case 1:
    *_output = _input;
    break;
  case 3:
  case 4:
  {
    cv::Mat colorMat = pralin::backends::opencv::image_backend::get_mat(_input);
    cv::Mat grayMat;
    int code = -1;
    switch(_input.get_colorspace().get_type())
    {
    case pralin::values::image::colorspace::type::bgr:
      code = cv::COLOR_BGR2GRAY;
      break;
    case pralin::values::image::colorspace::type::bgra:
      code = cv::COLOR_BGRA2GRAY;
      break;
    case pralin::values::image::colorspace::type::rgb:
      code = cv::COLOR_RGB2GRAY;
      break;
    case pralin::values::image::colorspace::type::rgba:
      code = cv::COLOR_RGBA2GRAY;
      break;
    default:
      throw pralin::exception("Unsupported colorspace.");
      return;
    }
    cv::cvtColor(colorMat, grayMat, code);
    *_output = pralin::backends::opencv::image_backend::create(
      _input.get_meta_data(), grayMat, pralin::values::image::colorspace::grayscale());
    break;
  }
  default:
    throw pralin::exception("Unsupported number of channels {}", _input.get_channels());
  }
}

void crop::process(const values::image& _input, values::image* _output) const
{
  cv::Mat inputMat = pralin::backends::opencv::image_backend::get_mat(_input);
  *_output = pralin::backends::opencv::image_backend::create(
    _input.get_meta_data(),
    inputMat(cv::Rect(get_parameters().get_x(), get_parameters().get_y(),
                      get_parameters().get_width(), get_parameters().get_height())),
    _input.get_colorspace());
}

void resize::process(const values::image& _input, values::image* _output) const
{
  cv::Mat inputMat = pralin::backends::opencv::image_backend::get_mat(_input);
  cv::Mat outputMat;
  cv::Size new_size;
  double fx = 0.0;
  double fy = 0.0;
  int interpolation = cv::INTER_LINEAR;

  if(get_parameters().has_width() or get_parameters().has_height())
  {
    new_size = cv::Size(_input.get_width(), _input.get_height());
  }
  if(get_parameters().has_scale())
  {
    fx = get_parameters().get_scale();
    fy = get_parameters().get_scale();
  }
  else if(get_parameters().has_scale_x() or get_parameters().has_scale_y())
  {
    fx = get_parameters().get_scale_x(1.0);
    fy = get_parameters().get_scale_y(1.0);
  }

  cv::resize(inputMat, outputMat, new_size, fx, fy, interpolation);
  *_output = pralin::backends::opencv::image_backend::create(_input.get_meta_data(), outputMat,
                                                             _input.get_colorspace());
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_ALGORITHM(pralin::algorithms::opencv::imgproc::to_grayscale, "opencv/imgproc",
                          "to_grayscale", pralin::algorithm_priority::cpu,
                          description("Convert an image to grayscale."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::opencv::imgproc::crop, "opencv/imgproc", "crop",
                          pralin::algorithm_priority::cpu,
                          description("Crop an image.",
                                      "Crop an image to `x`, `y`, `width`, `height`"))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::opencv::imgproc::resize, "opencv/imgproc", "resize",
  pralin::algorithm_priority::cpu,
  description(
    "Resize an image using interpolation.",
    "Resize `input`, either using fixed size (`width` or `height`), or relative (`scale`, "
    "`scale_x` , `scale_y`). All parameters are optionals, but at least one need to be set, or the "
    "algorithm will trigger an exception. If scale is only specified for one dimension, it will "))
