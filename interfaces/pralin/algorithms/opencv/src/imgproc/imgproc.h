#pragma once

#include <pralin/definitions/image>
#include <pralin/implementation>

namespace pralin::algorithms::opencv::imgproc
{
  class to_grayscale : public implementation<to_grayscale, definitions::image::filter>
  {
  public:
    to_grayscale() = default;
    ~to_grayscale() = default;
    void process(const values::image& _input, values::image* _output) const final;
  };
  PRALIN_PARAMETERS(crop, PRALIN_PARAMETER(uint64_t, 0), x, PRALIN_PARAMETER(uint64_t, 0), y,
                    PRALIN_PARAMETER(uint64_t, 0), width, PRALIN_PARAMETER(uint64_t, 0), height);

  class crop : public implementation<crop, definitions::image::filter, crop_parameters>
  {
  public:
    crop() = default;
    ~crop() = default;
    void process(const values::image& _input, values::image* _output) const final;
  };

  enum interpolations
  {
    linear
  };

  PRALIN_PARAMETERS(resize, PRALIN_OPTIONAL_PARAMETER(uint64_t), width,
                    PRALIN_OPTIONAL_PARAMETER(uint64_t), height, PRALIN_OPTIONAL_PARAMETER(double),
                    scale, PRALIN_OPTIONAL_PARAMETER(double), scale_x,
                    PRALIN_OPTIONAL_PARAMETER(double), scale_y,
                    PRALIN_PARAMETER(interpolations, interpolations::linear), interpolation);

  class resize : public implementation<resize, definitions::image::filter, resize_parameters>
  {
  public:
    resize() = default;
    ~resize() = default;
    void process(const values::image& _input, values::image* _output) const final;
  };

} // namespace pralin::algorithms::opencv::imgproc

template<>
struct pralin::meta_type::getter_implementation<pralin::algorithms::opencv::imgproc::interpolations>
{
  static meta_type get()
  {
    static meta_type mt = meta_type::create<pralin::algorithms::opencv::imgproc::interpolations>();
    return mt;
  }
};