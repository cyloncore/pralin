#pragma once

#include <pralin/definitions/image>
#include <pralin/implementation>

namespace pralin::algorithms::opencv::highgui
{
  PRALIN_PARAMETERS(viewer, PRALIN_PARAMETER(std::string, "notitle"), title,
                    PRALIN_PARAMETER(int, 1), wait_delay);

  class imshow : public implementation<imshow, definitions::image::viewer, viewer_parameters>
  {
  public:
    imshow() = default;
    void process(const values::image& _image) const final;
  };

  class imshow_object_detections
      : public implementation<imshow_object_detections, definitions::image::object_detection_viewer,
                              viewer_parameters>
  {
  public:
    imshow_object_detections() = default;
    void process(const values::image& _image,
                 const values::tagged_polygons_vector<values::class_tag>& _rectangles) const final;
  };
} // namespace pralin::algorithms::opencv::highgui
