#include <pralin/algorithms/opencv/highgui>

#include <pralin/background_actions_manager>

#include <iostream>
#include <list>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <pralin/backends/opencv>
#include <pralin/values/class_tag>
#include <pralin/values/polygon>
#include <pralin/values/tagged>

using namespace pralin::algorithms::opencv::highgui;

void imshow::process(const values::image& _image) const
{
  viewer_parameters vp = get_parameters();
  pralin::background_actions_manager::enqueue<void>(
    [vp, _image]()
    {
      cv::imshow(vp.get_title(), pralin::backends::opencv::image_backend::get_mat(_image));
      cv::waitKey(vp.get_wait_delay());
    });
}

void imshow_object_detections::process(
  const values::image& _image,
  const values::tagged_polygons_vector<values::class_tag>& _rectangles) const
{
  cv::Mat frame = pralin::backends::opencv::image_backend::get_mat(_image).clone();

  for(const values::tagged_polygon<values::class_tag>& r : _rectangles)
  {
    cv::Scalar color(rand() % 255, rand() % 255, rand() % 255);

    values::rect_interface ri(r.value);

    double left = ri.left();
    double top = ri.top();
    double right = ri.right();
    double bottom = ri.bottom();

    cv::rectangle(frame, cv::Point(left, top), cv::Point(right, bottom), color, 2);

    std::ostringstream out;
    out.precision(2);
    out << r.tag.confidence;
    std::string label = r.tag.label + " " + out.str();

    int baseLine;
    cv::Size labelSize = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);

    top = std::max(top, (double)labelSize.height);
    cv::rectangle(frame, cv::Point(left, top - labelSize.height),
                  cv::Point(left + labelSize.width, top + baseLine), color, cv::FILLED);
    cv::putText(frame, label, cv::Point(left, top), cv::FONT_HERSHEY_SIMPLEX, 0.5,
                cv::Scalar::all(255) - color);
  }

  viewer_parameters vp = get_parameters();
  pralin::background_actions_manager::enqueue<void>(
    [vp, frame]()
    {
      cv::imshow(vp.get_title(), frame);
      cv::waitKey(vp.get_wait_delay());
    });
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("opencv/highgui",
                       description("Integration of OpenCV Highgui module, for displaying images."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::opencv::highgui::imshow, "opencv/highgui", "imshow",
                          pralin::algorithm_priority::cpu, description("Show image in a viewer."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::opencv::highgui::imshow_object_detections,
                          "opencv/highgui", "imshow_object_detections",
                          pralin::algorithm_priority::cpu,
                          description("Show result of an object detection algorithm in a viewer."))
