#pragma once

#include <pralin/definitions/image>
#include <pralin/implementation>

namespace pralin::algorithms::opencv::imgcodecs
{
  class imread : public implementation<imread, definitions::image::reader>
  {
  public:
    imread() = default;
    void process(const std::string& _filename, values::image* _output) const final;
  };
  class imwrite : public implementation<imwrite, definitions::image::writter>
  {
  public:
    imwrite() = default;
    void process(const std::string& _filename, const values::image& _input) const final;
  };
} // namespace pralin::algorithms::opencv::imgcodecs
