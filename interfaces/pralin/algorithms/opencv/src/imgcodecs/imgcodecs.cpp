#include <pralin/algorithms/opencv/imgcodecs>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include <pralin/backends/opencv>

using namespace pralin::algorithms::opencv::imgcodecs;

void imread::process(const std::string& _filename, values::image* _output) const
{
  cv::Mat mat = cv::imread(_filename, cv::IMREAD_UNCHANGED);
  pralin::values::image::colorspace cs;
  switch(mat.channels())
  {
  case 1:
    cs = pralin::values::image::colorspace::grayscale();
    break;
  case 3:
    cs = pralin::values::image::colorspace::bgr();
    break;
  case 4:
    cs = pralin::values::image::colorspace::bgra();
    break;
  }
  *_output
    = pralin::backends::opencv::image_backend::create(pralin::values::meta_data::empty(), mat, cs);
}

void imwrite::process(const std::string& _filename, const values::image& _input) const
{
  cv::Mat inputMat = pralin::backends::opencv::image_backend::get_mat(_input);
  if(_input.get_colorspace().is_rgb())
  {
    cv::Mat swapMat;
    cv::cvtColor(inputMat, swapMat, cv::COLOR_RGB2BGR);
    inputMat = swapMat;
  }
  else if(_input.get_colorspace().is_rgba())
  {
    cv::Mat swapMat;
    cv::cvtColor(inputMat, swapMat, cv::COLOR_RGBA2BGRA);
    inputMat = swapMat;
  }
  cv::imwrite(_filename, inputMat);
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE(
  "opencv/imgcodecs",
  description("Integration of OpenCV Imgcodecs module, for input/output operation on images."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::opencv::imgcodecs::imread, "opencv/imgcodecs",
                          "imread", pralin::algorithm_priority::cpu,
                          description("Read an image from a file.",
                                      "Read an image from `filename`."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::opencv::imgcodecs::imwrite, "opencv/imgcodecs",
                          "imwrite", pralin::algorithm_priority::cpu,
                          description("Write an image to a file.", "Write `image` to `filename`."))
