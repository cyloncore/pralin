#include <pralin/definitions/ml>
#include <pralin/definitions/nlp>
#include <pralin/implementation>

namespace pralin::algorithms::fasttext
{
  PRALIN_PARAMETERS_EXTENDS(classification, definitions::ml::model_parameters,
                            PRALIN_PARAMETER(double, 0.5), threshold);

  class classification
      : public implementation<classification, ::pralin::definitions::nlp::classification,
                              classification_parameters>
  {
    friend implementation_t;
  public:
    classification();
    ~classification();
    void process(const std::string& _string, pralin::values::class_vector* _detections) const final;
  private:
    void init(const std::string& _model);
    void init();
    struct data;
    data* d;
  };
} // namespace pralin::algorithms::fasttext
