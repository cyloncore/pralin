#include "fasttext.h"

#include <fasttext/fasttext.h>

#include <sstream>

#include <pralin/resources>
#include <pralin/string_p.h>
#include <pralin/values/class_tag>

using namespace pralin::algorithms::fasttext;

struct classification::data
{
  ::fasttext::FastText ft;
};

classification::classification() : d(new data) {}

classification::~classification() {}

void classification::init(const std::string& _model)
{
  try
  {
    d->ft.loadModel(_model);
  }
  catch(const std::invalid_argument& _ia)
  {
    throw pralin::exception("While loading fastText model: '{}'", _ia.what());
  }
}

void classification::init()
{
  init(pralin::resources::get(get_parameters().get_module("fasttext"),
                              get_parameters().get_filename(), get_parameters().get_source()));
}

void classification::process(const std::string& _string,
                             pralin::values::class_vector* _detections) const
{
  std::vector<std::pair<::fasttext::real, std::string>> predictions;
  std::string str = _string;
  pralin::string::replace_all(str, "\n", "");
  std::stringstream in(str);
  d->ft.predictLine(in, predictions, -1, get_parameters().get_threshold());

  for(const std::pair<::fasttext::real, std::string>& p : predictions)
  {
    _detections->push_back(pralin::values::class_tag{p.first, p.second});
  }
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("darknet",
                       description("Integration of fasttext library, for text classification."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::fasttext::classification, "fasttext",
                          "classification", pralin::algorithm_priority::cpu,
                          description("Classify text using fasttext library."))
