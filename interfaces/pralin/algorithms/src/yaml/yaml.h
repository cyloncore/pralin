#pragma once

#include <pralin/definitions/io>
#include <pralin/implementation>

namespace pralin::algorithms::yaml
{
  class parser : public implementation<parser, definitions::io::parser<any_value>>
  {
    friend implementation_t;
  public:
    parser();
    ~parser();
    void process(const std::string& _input, const std::optional<values::meta_data>& _meta_data,
                 any_value* _value) const final;
  private:
    struct data;
    data* const d;
  };
} // namespace pralin::algorithms::yaml
