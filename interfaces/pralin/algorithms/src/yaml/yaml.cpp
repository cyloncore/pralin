#include "yaml.h"

#include <regex>

#include <yaml-cpp/yaml.h>

#include <pralin/exception>
#include <pralin/values/meta_data>

using namespace pralin::algorithms::yaml;

namespace
{
  static std::regex is_integer_regex{"^\\d+$"};
  static std::regex is_double_regex{"^[-+]?\\d*\\.?\\d+([eE][-+]?\\d+)?$"};

  pralin::any_value node_to_value(const YAML::Node& _node)
  {
    switch(_node.Type())
    {
    case YAML::NodeType::Undefined:
    case YAML::NodeType::Null:
      return pralin::any_value();
    case YAML::NodeType::Map:
    {
      pralin::any_value_map vm;
      for(auto it = _node.begin(); it != _node.end(); ++it)
      {
        vm[it->first.Scalar()] = node_to_value(it->second);
      }
      return vm;
    }
    case YAML::NodeType::Sequence:
    {
      pralin::values_vector vv;
      for(std::size_t i = 0; i < _node.size(); ++i)
      {
        vv.push_back(node_to_value(_node[i]));
      }
      return vv;
    }
    case YAML::NodeType::Scalar:
    {
      std::string scalar = _node.Scalar();
      if(_node.Tag() == "tag:yaml.org,2002:str" or _node.Tag() == "!")
      {
        return scalar;
      }
      else if(scalar == "true")
      {
        return true;
      }
      else if(scalar == "false")
      {
        return false;
      }
      else if(scalar == "null")
      {
        return pralin::any_value();
      }
      else if(std::regex_search(scalar, is_integer_regex))
      {
        return std::stoi(scalar);
      }
      else if(std::regex_search(scalar, is_double_regex))
      {
        return std::stod(scalar);
      }
      else
      {
        return scalar;
      }
    }
    }
    throw pralin::exception("Unsupported YAML type: {}", int(_node.Type()));
  }
} // namespace

parser::parser() : d(nullptr) {}

parser::~parser() {}

void parser::process(const std::string& _input, const std::optional<values::meta_data>& _meta_data,
                     any_value* _value) const
{
  YAML::Node node;
  try
  {
    node = YAML::Load(_input);
  }
  catch(const YAML::ParserException& pe)
  {
    throw exception("Parse error in YAML file: {}", pe.what());
  }

  *_value = node_to_value(node);
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("yaml",
                       description("Integration with yaml library, for parsing YAML strings."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::yaml::parser, "yaml", "parser",
                          pralin::algorithm_priority::cpu, description("Parse a YAML string."))
