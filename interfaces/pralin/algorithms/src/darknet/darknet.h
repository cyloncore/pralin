#pragma once

#include <pralin/implementation>

#include <pralin/definitions/image>

namespace pralin::algorithms::darknet
{
  PRALIN_PARAMETERS(object_detection, PRALIN_PARAMETER(float, 0.2), threshold,
                    PRALIN_PARAMETER(float, 0.5), hierarchical_threshold,
                    PRALIN_PARAMETER(float, 0.4), non_maximal_suppression);

  class object_detection
      : public implementation<object_detection, definitions::image::object_detection,
                              object_detection_parameters>
  {
    friend implementation_t;
  public:
    object_detection();
    ~object_detection();
    void process(const values::image& _input,
                 values::tagged_polygons_vector<values::class_tag>* _output) const final;
  private:
    void init(const std::string& _prefix, const std::string& _cfg, const std::string& _weights,
              const std::string& _names);
  private:
    struct data;
    data* d;
  };
} // namespace pralin::algorithms::darknet
