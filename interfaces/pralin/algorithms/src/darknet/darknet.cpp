#include <pralin/algorithms/darknet>

#include <iostream>

#include <darknet.hpp>

#include <pralin/backends/opencv>

#include <pralin/resources>

#include <pralin/values/class_tag>
#include <pralin/values/image>
#include <pralin/values/polygon>
#include <pralin/values/tagged>

using namespace pralin::algorithms::darknet;

struct object_detection::data
{
  Darknet::NetworkPtr net = nullptr;
};

object_detection::object_detection() : d(new data) {}

void object_detection::init(const std::string& _prefix, const std::string& _cfg,
                            const std::string& _weights, const std::string& _names)
{
  d->net
    = Darknet::load_neural_network(pralin::resources::get(_prefix, "network.cfg", _cfg),
                                   pralin::resources::get(_prefix, "network.names", _names),
                                   pralin::resources::get(_prefix, "network.weights", _weights));
  Darknet::set_detection_threshold(d->net, get_parameters().get_threshold());
}

object_detection::~object_detection() { delete d; }

void object_detection::process(const values::image& _input,
                               values::tagged_polygons_vector<values::class_tag>* _output) const
{
  cv::Mat input_mat = pralin::backends::opencv::image_backend::get_mat(_input);
  Darknet::Predictions predictions = Darknet::predict(d->net, input_mat);

  for(const Darknet::Prediction prediction : predictions)
  {
    if(prediction.best_class >= 0)
    {
      auto it = prediction.prob.find(prediction.best_class);
      if(it != prediction.prob.end())
      {
        values::class_tag ct{it->second, Darknet::get_class_names(d->net)[prediction.best_class]};
        cv::Rect pr = prediction.rect;
        values::polygon r = values::polygon::create_rect(pr.x, pr.y, pr.width, pr.height);
        _output->push_back({ct, r});
      }
    }
  }
}

#include <pralin/algorithms_registry>
using namespace std::string_literals;

namespace pralin::algorithms::darknet
{
  pralin::algorithm_priority algorithm_priority()
  {
    // FIXME detect if Darknet was built with CPU or GPU
    return pralin::algorithm_priority::cpu;
  }
} // namespace pralin::algorithms::darknet

PRALIN_REGISTER_MODULE("darknet",
                       description("Integration of darknet library, mainly for yolo v3 algorithm."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::darknet::object_detection, "darknet", "yolo.v3",
  pralin::algorithms::darknet::algorithm_priority(), description("YOLO v3 algorithm using darknet"),
  "pralin.darknet.yolo.v3"s,
  "https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/yolov3.cfg"s,
  "https://pjreddie.com/media/files/yolov3.weights"s,
  "https://raw.githubusercontent.com/pjreddie/darknet/master/data/coco.names"s)

PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::darknet::object_detection, "darknet", "yolo.v3-tiny",
  pralin::algorithms::darknet::algorithm_priority(), description("YOLO v3 algorithm using darknet"),
  "pralin.darknet.yolo.v3-tiny"s,
  "https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/yolov3-tiny.cfg"s,
  "https://pjreddie.com/media/files/yolov3-tiny.weights"s,
  "https://raw.githubusercontent.com/pjreddie/darknet/master/data/coco.names"s)
