#pragma once

#include <pralin/definitions/image>
#include <pralin/implementation>

namespace pralin::algorithms::zbar
{
  PRALIN_PARAMETERS(scanner, PRALIN_PARAMETER(std::string, ""), config);

  class scanner
      : public implementation<scanner, definitions::image::object_detection, scanner_parameters>
  {
    friend implementation_t;
  public:
    scanner();
    ~scanner();
    void process(const values::image& _input,
                 values::tagged_polygons_vector<values::class_tag>* _output) const final;
  private:
    void init();
  private:
    struct data;
    data* const d;
  };
} // namespace pralin::algorithms::zbar
