#include <pralin/algorithms/zbar>

#include <iostream>

#include <zbar.h>

#include <pralin/values/class_tag>
#include <pralin/values/image>
#include <pralin/values/polygon>
#include <pralin/values/tagged>

using namespace pralin::algorithms::zbar;

struct scanner::data
{
  ::zbar::ImageScanner imageScanner;
};

scanner::scanner() : d(new data) {}

scanner::~scanner() { delete d; }

void scanner::init()
{
  d->imageScanner.set_config(::zbar::ZBAR_NONE, ::zbar::ZBAR_CFG_ENABLE, 1);
  if(get_parameters().get_config().size() > 0)
  {
    d->imageScanner.set_config(get_parameters().get_config());
  }
}

void scanner::process(const values::image& _input,
                      values::tagged_polygons_vector<values::class_tag>* _output) const
{
  values::image source = _input.cast<uint8_t>();

  std::string image_format;
  switch(source.get_channels())
  {
  case 1:
    image_format = "Y800";
    break;
  default:
    std::cerr << "Unsupported number of channels in zbar scanner (convert to grayscale first): "
              << source.get_channels() << std::endl;
    return;
  }

  ::zbar::Image img(source.get_width(), source.get_height(), image_format, source.get_data(),
                    source.get_size());

  // scan the image for barcodes
  /*int n =*/d->imageScanner.scan(img);

  // extract results
  for(::zbar::Image::SymbolIterator symbol = img.symbol_begin(); symbol != img.symbol_end();
      ++symbol)
  {
    std::vector<values::point> points;
    ::zbar::Symbol sym = *symbol;
    for(::zbar::Symbol::PointIterator point = sym.point_begin();
        point
        != ::zbar::Symbol::PointIterator(&sym,
                                         -1) /*symbol->point_end() <- crashes with zbar 0.10*/;
        ++point)
    {
      double x = (*point).x;
      double y = (*point).y;
      points.push_back({x, y});
    }
    values::class_tag ct{1.0, symbol->get_data()};
    values::polygon r = values::polygon(_input.get_meta_data(), points);
    _output->push_back({ct, r});
  }
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE(
  "zbar", description("Integration with zbar library, for reading QR and Bar Codes in images."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::zbar::scanner, "zbar", "bar_scanner",
                          pralin::algorithm_priority::cpu, description("Scan for QR codes."))
