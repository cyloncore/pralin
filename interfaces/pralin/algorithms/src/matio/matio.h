#pragma once

#include <pralin/definitions/tensor>
#include <pralin/implementation>

namespace pralin::algorithms::matio
{
  PRALIN_PARAMETERS(reader, PRALIN_PARAMETER(std::string, required), variable_name);

  class reader : public implementation<reader, definitions::tensor::reader, reader_parameters>
  {
    friend implementation_t;
    reader() = default;
  public:
    void process(const std::string& _filename, values::tensor* _image) const final;
  };

} // namespace pralin::algorithms::matio
