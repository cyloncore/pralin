#include "matio.h"

#include <matio.h>

#include <pralin/values/tensor>

using namespace pralin::algorithms::matio;

void reader::process(const std::string& _filename, values::tensor* _tensor) const
{
  mat_t* matfp = Mat_Open(_filename.c_str(), MAT_ACC_RDONLY);
  if(not matfp)
  {
    throw pralin::exception("Failed to open '{}' as a mat file.", _filename);
  }

  matvar_t* matvar = Mat_VarRead(matfp, get_parameters().get_variable_name().c_str());
  if(not matvar)
  {
    Mat_Close(matfp);
    throw pralin::exception("No variable '{}' in file '{}'.", get_parameters().get_variable_name(),
                            _filename);
  }

  std::vector<std::size_t> dimensions;
  for(int i = 0; i < matvar->rank; ++i)
  {
    dimensions.push_back(matvar->dims[i]);
  }

  pralin::values::scalar_type st;
  switch(matvar->data_type)
  {
  case MAT_T_INT8:
    st = pralin::values::scalar_type::int_8;
    break;
  case MAT_T_UINT8:
    st = pralin::values::scalar_type::uint_8;
    break;
  case MAT_T_INT16:
    st = pralin::values::scalar_type::int_16;
    break;
  case MAT_T_UINT16:
    st = pralin::values::scalar_type::uint_16;
    break;
  case MAT_T_UINT32:
    st = pralin::values::scalar_type::int_32;
    break;
  case MAT_T_INT32:
    st = pralin::values::scalar_type::uint_32;
    break;
  case MAT_T_INT64:
    st = pralin::values::scalar_type::int_64;
    break;
  case MAT_T_UINT64:
    st = pralin::values::scalar_type::uint_64;
    break;
  case MAT_T_SINGLE:
    st = pralin::values::scalar_type::float_32;
    break;
  case MAT_T_DOUBLE:
    st = pralin::values::scalar_type::float_64;
    break;
  default:
    throw pralin::exception("Unsupported data type: '{}' for variable '{}' in file '{}'.",
                            matvar->data_type, get_parameters().get_variable_name(), _filename);
  }

  *_tensor = values::tensor::create_default(
    values::meta_data::empty(), values::tensor::creation_flag::from_column_major,
    reinterpret_cast<uint8_t*>(matvar->data), dimensions, st);

  Mat_VarFree(matvar);
  Mat_Close(matfp);
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("matio", description("Integration with matio library, for input/output of "
                                            "tensors in MAT (matlab/octave) file format."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::matio::reader, "matio", "reader", pralin::algorithm_priority::cpu,
  description("Reader for MAT (matlab/octave) matrix file.",
              "Output the tensor contained in the variable (`variable_name`) from the MAT file "
              "given by the input `filename`."))
