#include <knowCore/Messages.h>

#include <scQL/Core/QueryResult.h>
#include <scQL/Core/QueryResultValueIterator.h>
#include <scQL/Core/Uris/scql.h>
#include <scQL/Data/Value.h>
#include <scQL/Remote/Client.h>

#include <pralin/definitions/operators>
#include <pralin/definitions/query>
#include <pralin/implementation>
#include <pralin/values/iterator>
#include <pralin/values/point_cloud>

#include <pralin/backends/knowl>
#include <pralin/backends/scql>

namespace pralin::algorithms::scql
{
  struct connection_handle
  {
    PRALIN_META_INFO(connection_handle)
  public:
    std::shared_ptr<scQL::Remote::Client> client;
  };
  PRALIN_PARAMETERS(create_connection_handle, PRALIN_PARAMETER(std::string, std::string()), library,
                    PRALIN_PARAMETER(std::string, std::string()), name,
                    PRALIN_PARAMETER(std::string, std::string("{}")), options);

  class create_connection_handle
      : public implementation<create_connection_handle,
                              ::pralin::definitions::operators::create<any_value>,
                              create_connection_handle_parameters>
  {
  public:
    create_connection_handle() = default;
    ~create_connection_handle() = default;
    void process(pralin::any_value* _connection_handle) const final;
  };
#define __PRALIN_GET_PRALIN_TYPE(__KV_Value__)                                                     \
  typename query_frame_details::traits<__KV_Value__>::pralin_value

  namespace query_frame_details
  {
    template<typename _knowl_value_>
    struct traits;

    template<>
    struct traits<knowValues::Values::Image>
    {
      static QString frame_uri() { return scQL::Core::Uris::scql_types::image; }
      using pralin_value = pralin::values::image;
      using knowl_backend = pralin::backends::knowl::image_backend;
      static ::pralin::values::image get_value(const scQL::Core::ResultValue& _value)
      {
        return pralin::backends::scql::get_image(_value);
      }
    };
    template<>
    struct traits<knowValues::Values::LidarScan>
    {
      static QString frame_uri() { return scQL::Core::Uris::scql_types::lidar_scan; }
      using pralin_value = pralin::values::lidar_scan;
      using knowl_backend = pralin::backends::knowl::lidar_scan_backend;
      static ::pralin::values::lidar_scan get_value(const scQL::Core::ResultValue& _value)
      {
        return pralin::backends::scql::get_lidar_scan(_value);
      }
    };
    template<>
    struct traits<knowValues::Values::Lidar3DScan>
    {
      static QString frame_uri() { return scQL::Core::Uris::scql_types::lidar3d_scan; }
      using pralin_value = pralin::values::point_cloud;
      using knowl_backend = pralin::backends::knowl::lidar3d_scan_backend;
      static ::pralin::values::point_cloud get_value(const scQL::Core::ResultValue& _value)
      {
        return pralin::backends::scql::get_lidar3d_scan(_value);
      }
    };
    template<>
    struct traits<knowValues::Values::PointCloud>
    {
      static knowCore::Uri frame_uri() { return scQL::Core::Uris::scql_types::point_cloud; }
      using pralin_value = pralin::values::point_cloud;
      using knowl_backend = pralin::backends::knowl::point_cloud_backend;
      static ::pralin::values::point_cloud get_value(const scQL::Core::ResultValue& _value)
      {
        return pralin::backends::scql::get_point_cloud(_value);
      }
    };
    template<typename _KV_Value_>
    struct iteraror
        : public values::iterator<__PRALIN_GET_PRALIN_TYPE(_KV_Value_)>::abstract_backend
    {
      using traits = query_frame_details::traits<_KV_Value_>;
      iteraror(const scQL::Core::QueryResultValueIterator& _frameIterator)
          : m_iterator(_frameIterator)
      {
        m_has_next = m_iterator.next();
        if(not m_has_next)
        {
          pralin_warning("No value returned by query.");
        }
      }
      traits::pralin_value next() final
      {
        typename traits::pralin_value value = traits::get_value(m_iterator.resultValue(0));
        if(not m_has_next)
        {
          pralin_warning("Calling next on a finished iterator!");
        }
        m_has_next = m_iterator.next();
        return value;
      }
      bool has_next() const final { return m_has_next; }
    public:
      scQL::Core::QueryResultValueIterator m_iterator;
      bool m_has_next;
    };
  } // namespace query_frame_details

  template<typename _KV_Value_>
  class query_value : public implementation<
                        query_value<_KV_Value_>,
                        ::pralin::definitions::query::select<__PRALIN_GET_PRALIN_TYPE(_KV_Value_)>,
                        ::pralin::definitions::query::query_default_parameters>
  {
    using traits = query_frame_details::traits<_KV_Value_>;
    using pralin_value = typename traits::pralin_value;
  public:
    query_value() = default;
    ~query_value() = default;
    void process(const pralin::any_value& _connection,
                 values::iterator<pralin_value>* _iterator) const final
    {
      connection_handle c = _connection.to_value<connection_handle>();

      scQL::Core::QueryResult qr = c.client->execute(
        "SELECT <%frameuri> AS scan WHERE scan.pose inside "
        "%area^^<http://www.opengis.net/ont/geosparql#Geometry>",
        knowCore::ValueHash()
          .insert("%frameuri", traits::frame_uri())
          .insert("%area", QString::fromStdString(this->get_parameters().get_query())));
      qr.waitForFinished();
      if(qr.status() == scQL::Core::QueryResult::Status::Finished)
      {
        *_iterator = values::iterator<pralin_value>(
          std::make_shared<query_frame_details::iteraror<_KV_Value_>>(
            scQL::Core::QueryResultValueIterator(qr)));
      }
      else
      {
        throw pralin::exception("Error while querying frames: {}", qr.messages().toString());
      }
    }
  };
#undef __PRALIN_GET_PRALIN_TYPE
} // namespace pralin::algorithms::scql
