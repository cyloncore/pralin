#include "scql.h"

#include <QJsonDocument>

#include <scQL/Remote/TransportLibrary.h>

using namespace pralin::algorithms::scql;

void create_connection_handle::process(pralin::any_value* _connection_handle) const
{
  QJsonParseError error;
  QJsonDocument doc
    = QJsonDocument::fromJson(QByteArray::fromStdString(get_parameters().get_options()), &error);

  if(doc.isNull())
  {
    throw pralin::exception("Failed to parse options: '{}' with error: {}",
                            get_parameters().get_options(), error.errorString());
  }

  QVariantMap options = doc.toVariant().toMap();

  auto const [success, transport, errorMessage] = scQL::Remote::TransportLibrary::createTransport(
    QString::fromStdString(get_parameters().get_library()),
    QString::fromStdString(get_parameters().get_name()), options);
  if(success)
  {
    *_connection_handle
      = pralin::any_value(connection_handle{std::make_shared<scQL::Remote::Client>(transport)});
  }
  else
  {
    throw pralin::exception("Failed to create transport {}", errorMessage);
  }
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("scql", description("Integration with scQL library, for active queries."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::scql::create_connection_handle, "scql",
                          "create_connection_handle", pralin::algorithm_priority::cpu,
                          description("Create a connection to a remote scQL query engine."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::scql::query_value<knowValues::Values::Image>, "scql",
                          "query_image", pralin::algorithm_priority::cpu,
                          description("Query for image using scQL."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::scql::query_value<knowValues::Values::LidarScan>,
                          "scql", "query_lidar_scan", pralin::algorithm_priority::cpu,
                          description("Query for lidar scan using scQL."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::scql::query_value<knowValues::Values::Lidar3DScan>,
                          "scql", "query_lidar3d_scan", pralin::algorithm_priority::cpu,
                          description("Query for lidar 3D scan using scQL."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::scql::query_value<knowValues::Values::PointCloud>,
                          "scql", "query_point_cloud", pralin::algorithm_priority::cpu,
                          description("Query for a point cloud using scQL."))
