#include <pralin/definitions/geometry>
#include <pralin/implementation>

namespace pralin::algorithms::proj
{
  PRALIN_PARAMETERS(create_transformation_provider,
                    PRALIN_PARAMETER(uint32_t, std::numeric_limits<uint32_t>::max()), srid,
                    PRALIN_PARAMETER(bool, false), swap);
  class create_transformation_provider
      : public implementation<create_transformation_provider,
                              ::pralin::definitions::geometry::create_transformation_provider,
                              create_transformation_provider_parameters>
  {
    friend implementation_t;
  public:
    create_transformation_provider() = default;
    ~create_transformation_provider() = default;
    void process(values::geometry::transformation_provider* _transformation_provider) const final;
  };
} // namespace pralin::algorithms::proj
