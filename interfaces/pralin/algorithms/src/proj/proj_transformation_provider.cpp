#include "proj.h"

#include <proj.h>

#include <pralin/values/geometry>
#include <pralin/values/meta_data>

#include <pralin/backends/eigen>

namespace pralin::algorithms::proj
{
  class transformation_backend : public pralin::values::geometry::transformation::abstract_backend
  {
  public:
    transformation_backend(int _src_srid, int _dst_srid, std::optional<Eigen::Affine3d> _pre,
                           std::optional<Eigen::Affine3d> _post, bool _swap)
        : m_pj_context(proj_context_create()), m_out_srid(_dst_srid), m_pre(_pre), m_post(_post)
    {
      pralin_debug_vn(_src_srid, _dst_srid);
      m_P_src = proj_create(m_pj_context, ("EPSG:" + std::to_string(_src_srid)).c_str());
      m_P_dst = proj_create(m_pj_context, ("EPSG:" + std::to_string(_dst_srid)).c_str());

      m_P = proj_create_crs_to_crs_from_pj(m_pj_context, m_P_src, m_P_dst, NULL, NULL);
      if(_swap)
      {
        PJ* nP = proj_normalize_for_visualization(PJ_DEFAULT_CTX, m_P);
        std::swap(nP, m_P);
        proj_destroy(nP);
      }
    }
    virtual ~transformation_backend()
    {
      proj_destroy(m_P);
      proj_destroy(m_P_src);
      proj_destroy(m_P_dst);

      proj_context_destroy(m_pj_context);
    }
    values::geometry::point transform(const values::geometry::point& _point) const override
    {
      Eigen::Vector3d pv = backends::eigen::to_vector(_point);

      if(m_pre)
      {
        pv = *m_pre * pv;
      }

      PJ_COORD c_in;
      c_in.xyzt.x = pv.x();
      c_in.xyzt.y = pv.y();
      c_in.xyzt.z = pv.z();
      c_in.xyzt.t = HUGE_VAL;

      PJ_COORD c_out = proj_trans(m_P, PJ_FWD, c_in);

      pv << c_out.xyzt.x, c_out.xyzt.y, c_out.xyzt.z;
      if(m_post)
      {
        pv = *m_post * pv;
      }

      return backends::eigen::to_point(pv, m_out_srid);
    }
    values::geometry::pose transform(const values::geometry::pose& _pose) const override
    {
      Eigen::Vector3d pv = backends::eigen::to_vector(_pose);
      Eigen::Quaterniond rot = backends::eigen::to_quaternion(_pose);

      if(m_pre)
      {
        pv = *m_pre * pv;
        rot = m_pre->rotation() * rot;
      }

      PJ_COORD c_in;
      c_in.xyzt.x = pv.x();
      c_in.xyzt.y = pv.y();
      c_in.xyzt.z = pv.z();
      c_in.xyzt.t = HUGE_VAL;

      PJ_COORD c_out = proj_trans(m_P, PJ_FWD, c_in);

      pv << c_out.xyzt.x, c_out.xyzt.y, c_out.xyzt.z;
      if(m_post)
      {
        pv = *m_post * pv;
        rot = m_post->rotation() * rot;
      }

      return backends::eigen::to_pose(pv, rot, m_out_srid);
    }
  private:
    PJ* m_P_src;
    PJ* m_P_dst;
    PJ* m_P;
    PJ_CONTEXT* m_pj_context;
    int m_out_srid;
    std::optional<Eigen::Affine3d> m_pre, m_post;
  };
  class proj_transformation_provider_backend
      : public pralin::values::geometry::transformation_provider::abstract_backend
  {
    using pose = pralin::values::geometry::pose;
  public:
    proj_transformation_provider_backend(uint32_t _srid, bool _swap)
        : m_pj_context(proj_context_create()), m_srid(_srid), m_swap(_swap)
    {
    }
    virtual ~proj_transformation_provider_backend() { proj_context_destroy(m_pj_context); }
  protected:
    pose transform(const pose& _pose, uint32_t _srid) const
    {
      if(_pose.get_srid() == _srid)
      {
        return _pose;
      }
      PJ* P_src = proj_create(m_pj_context, ("EPSG:" + std::to_string(_pose.get_srid())).c_str());
      PJ* P_dst = proj_create(m_pj_context, ("EPSG:" + std::to_string(_srid)).c_str());

      PJ* P = proj_create_crs_to_crs_from_pj(m_pj_context, P_src, P_dst, NULL, NULL);
      if(m_swap)
      {
        PJ* nP = proj_normalize_for_visualization(PJ_DEFAULT_CTX, P);
        std::swap(nP, P);
        proj_destroy(nP);
      }

      PJ_COORD c_in;
      c_in.xyzt.x = _pose.get_x();
      c_in.xyzt.y = _pose.get_y();
      c_in.xyzt.z = _pose.get_z();
      c_in.xyzt.t = HUGE_VAL;

      PJ_COORD c_out = proj_trans(P, PJ_FWD, c_in);

      pose np = pose(c_out.xyzt.x, c_out.xyzt.y, c_out.xyzt.z, _pose.get_rotation(), _srid);

      proj_destroy(P);
      proj_destroy(P_src);
      proj_destroy(P_dst);

      return np;
    }
  public:
    pralin::values::geometry::transformation
      get_transformation(const pralin::values::meta_data& _src_meta_data,
                         const pralin::values::meta_data& _dst_meta_data) const override
    {

      int src_srid = m_srid;
      int dst_srid = m_srid;
      std::optional<Eigen::Affine3d> pre;
      std::optional<Eigen::Affine3d> post;

      if(_src_meta_data.has_srid())
      {
        src_srid = _src_meta_data.get_srid();
      }
      if(_dst_meta_data.has_srid())
      {
        dst_srid = _dst_meta_data.get_srid();
      }
      if(_src_meta_data.has_pose())
      {
        pose src_pose = transform(_src_meta_data.get_pose(), src_srid);
        pre = Eigen::Translation3d(backends::eigen::to_vector(src_pose))
              * backends::eigen::to_quaternion(src_pose);
      }
      if(_dst_meta_data.has_pose())
      {
        pose dst_pose = transform(_dst_meta_data.get_pose(), dst_srid);
        post = (Eigen::Translation3d(backends::eigen::to_vector(dst_pose))
                * backends::eigen::to_quaternion(dst_pose))
                 .inverse();
      }
      return values::geometry::transformation(
        new transformation_backend(src_srid, dst_srid, pre, post, m_swap));
    }
  private:
    PJ_CONTEXT* m_pj_context;
    uint32_t m_srid;
    bool m_swap;
  };
} // namespace pralin::algorithms::proj

using namespace pralin::algorithms::proj;

void create_transformation_provider::process(
  values::geometry::transformation_provider* _transformation_provider) const
{
  *_transformation_provider
    = values::geometry::transformation_provider(new proj_transformation_provider_backend(
      get_parameters().get_srid(), get_parameters().get_swap()));
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE(
  "proj", description("Integration with proj library, for geographic transformations."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::proj::create_transformation_provider, "proj",
                          "create_transformation_provider", pralin::algorithm_priority::cpu,
                          description("Compute a transformation provider."))
