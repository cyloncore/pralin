#pragma once

#include <pralin/implementation>
#include <pralin/values/meta_data>

#include <pralin/definitions/operators>

namespace pralin::algorithms::html
{
  class html_to_text
      : public implementation<html_to_text,
                              definitions::operators::transform<std::string, std::string>>
  {
    friend implementation_t;
    html_to_text() = default;
    ~html_to_text() = default;
  public:
    void process(const std::string& _value, std::string* _output) const final;
  };
} // namespace pralin::algorithms::html
