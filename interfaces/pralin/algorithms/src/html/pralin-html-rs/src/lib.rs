#[cxx::bridge]
mod ffi
{
  extern "Rust" {
    fn html_to_text(input: String) -> Result<String>;
  }
}

fn html_to_text(input: String) -> Result<String, html2text::Error>
{
  html2text::from_read(input.as_bytes(), 120)
}
