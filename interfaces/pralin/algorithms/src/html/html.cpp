#include "html.h"

#include "pralin_html_rs_cxx/lib.h"
#include "rust/cxx.h"

using namespace pralin::algorithms::html;

void html_to_text::process(const std::string& _value, std::string* _output) const
{
  try
  {
    ::rust::String result = ::html_to_text(::rust::String(_value));
    *_output = (std::string)result;
  }
  catch(const rust::Error& _err)
  {
    throw pralin::exception("An error occured in html_to_text: {}", _err.what());
  }
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("pralin/html", description("Algorithms related to handling HTML."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::html::html_to_text, "pralin/html", "html_to_text",
                          pralin::algorithm_priority::cpu,
                          description("Render HTML to a text representation."))
