#pragma once

#include <pralin/definitions/image>
#include <pralin/implementation>

namespace pralin::algorithms::tensorflow
{
  //   PRALIN_PARAMETERS(object_detection, PRALIN_PARAMETER(float, 0.2), threshold);

  class object_detection
      : public implementation<
          object_detection, definitions::image::object_detection /*, object_detection_parameters*/>
  {
    friend implementation_t;
    void init(const std::string& _prefix, const std::string& _graph, const std::string& _label);
  public:
    object_detection();
    ~object_detection();
    void process(const values::image& _input,
                 values::tagged_polygons_vector<values::class_tag>* _output) const;
  private:
    struct data;
    data* d;
  };
} // namespace pralin::algorithms::tensorflow
