#include <pralin/algorithms/tensorflow>

#include <tensorflow/core/public/session.h>

#include <pralin/resources.h>

namespace pralin::algorithms::tensorflow::details
{
  struct tensorflow_interface
  {
    std::unique_ptr<::tensorflow::Session> session;
    bool load_graph(const std::string& _filename);
  };

  struct tensorflow_classification_interface : public tensorflow_interface
  {
    std::map<int, std::string> label_map;
    bool load_label_map(const std::string& _filename);
  };

} // namespace pralin::algorithms::tensorflow::details

using namespace pralin::algorithms::tensorflow;

bool details::tensorflow_interface::load_graph(const std::string& _filename)
{
  pralin_fatal("details::tensorflow_interface::load_graph");
}

bool details::tensorflow_classification_interface::load_label_map(const std::string& _filename)
{
  pralin_fatal("details::tensorflow_interface::load_label_map");
}

struct object_detection::data
{
  ~data()
  {
    // TODO free net/names
  }
  details::tensorflow_classification_interface interface;
};

object_detection::object_detection() : d(new data) {}

void object_detection::init(const std::string& _prefix, const std::string& _graph,
                            const std::string& _label)
{
  if(not d->interface.load_graph(pralin::resources::get(_prefix, "graph", _graph))
     or not d->interface.load_label_map(pralin::resources::get(_prefix, "label_map", _label)))
  {
    throw pralin::exception("Faiked to load model");
  }
}

object_detection::~object_detection() { delete d; }

#include <pralin/algorithms_registry>
using namespace std::string_literals;

PRALIN_REGISTER_MODULE("tensorflow",
                       description("Integration with tensorflow library, for deep learning."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::tensorflow::object_detection, "tensorflow", "mobilenet-v1-egohands", 50,
  description("Detect objects using mobilenet."), "pralin.tensorflow.mobilenet.v1.egohands"s,
  "https://github.com/lysukhin/tensorflow-object-detection-cpp/raw/master/demo/ssd_mobilenet_v1_egohands/frozen_inference_graph.pb"s,
  "https://raw.githubusercontent.com/lysukhin/tensorflow-object-detection-cpp/master/demo/ssd_mobilenet_v1_egohands/labels_map.pbtxt"s)
