#include <pralin/algorithms/torch>

#include <iostream>

#include <torch/nn/functional/upsampling.h>
#include <torch/script.h>

#include <pralin/backends/torch>
#include <pralin/resources>
#include <pralin/values/class_tag>
#include <pralin/values/image>
#include <pralin/values/polygon>
#include <pralin/values/tagged>

namespace pralin::algorithms::torch::details
{
  struct torch_interface
  {
    ::torch::jit::script::Module module;
    bool load_script(const std::string& _filename);
  };

  struct torch_classification_interface : public torch_interface
  {
    std::vector<std::string> labels;
    bool load_label_map(const std::string& _filename);
  };

} // namespace pralin::algorithms::torch::details

using namespace pralin::algorithms::torch;

bool details::torch_interface::load_script(const std::string& _filename)
{
  try
  {
    module = ::torch::jit::load(_filename);
    return true;
  }
  catch(const c10::Error& e)
  {
    std::cerr << "error loading the model: " << e.msg() << std::endl;
    return -1;
  }
}

bool details::torch_classification_interface::load_label_map(const std::string& _filename)
{
  std::ifstream f(_filename);
  std::string name = "";
  while(std::getline(f, name))
  {
    labels.push_back(name);
  }
  return true;
}

struct object_detection::data
{
  ~data()
  {
    // TODO free net/names
  }
  details::torch_classification_interface interface;
};

object_detection::object_detection() : d(new data) {}

void object_detection::init(const std::string& _prefix, const std::string& _script,
                            const std::string& _label)
{
  if(not d->interface.load_script(pralin::resources::get(_prefix, "script", _script))
     or not d->interface.load_label_map(pralin::resources::get(_prefix, "label_map", _label)))
  {
    throw pralin::exception("Failed to load script.");
  }
}

object_detection::~object_detection() { delete d; }

namespace
{
  std::vector<torch::Tensor> non_max_suppression(torch::Tensor preds, float score_thresh = 0.5,
                                                 float iou_thresh = 0.5)
  {
    std::vector<torch::Tensor> output;
    for(int i = 0; i < preds.sizes()[0]; ++i)
    {
      torch::Tensor pred = preds.select(0, i);

      // Filter by scores
      torch::Tensor scores
        = pred.select(1, 4) * std::get<0>(torch::max(pred.slice(1, 5, pred.sizes()[1]), 1));
      pred = torch::index_select(pred, 0, torch::nonzero(scores > score_thresh).select(1, 0));
      if(pred.sizes()[0] == 0)
        continue;

      // (center_x, center_y, w, h) to (left, top, right, bottom)
      pred.select(1, 0) = pred.select(1, 0) - pred.select(1, 2) / 2;
      pred.select(1, 1) = pred.select(1, 1) - pred.select(1, 3) / 2;
      pred.select(1, 2) = pred.select(1, 0) + pred.select(1, 2);
      pred.select(1, 3) = pred.select(1, 1) + pred.select(1, 3);

      // Computing scores and classes
      std::tuple<torch::Tensor, torch::Tensor> max_tuple
        = torch::max(pred.slice(1, 5, pred.sizes()[1]), 1);
      pred.select(1, 4) = pred.select(1, 4) * std::get<0>(max_tuple);
      pred.select(1, 5) = std::get<1>(max_tuple);

      torch::Tensor dets = pred.slice(1, 0, 6);

      torch::Tensor keep = torch::empty({dets.sizes()[0]});
      torch::Tensor areas
        = (dets.select(1, 3) - dets.select(1, 1)) * (dets.select(1, 2) - dets.select(1, 0));
      std::tuple<torch::Tensor, torch::Tensor> indexes_tuple = torch::sort(dets.select(1, 4), 0, 1);
      torch::Tensor v = std::get<0>(indexes_tuple);
      torch::Tensor indexes = std::get<1>(indexes_tuple);
      int count = 0;
      while(indexes.sizes()[0] > 0)
      {
        keep[count] = (indexes[0].item().toInt());
        count += 1;

        // Computing overlaps
        torch::Tensor lefts = torch::empty(indexes.sizes()[0] - 1);
        torch::Tensor tops = torch::empty(indexes.sizes()[0] - 1);
        torch::Tensor rights = torch::empty(indexes.sizes()[0] - 1);
        torch::Tensor bottoms = torch::empty(indexes.sizes()[0] - 1);
        torch::Tensor widths = torch::empty(indexes.sizes()[0] - 1);
        torch::Tensor heights = torch::empty(indexes.sizes()[0] - 1);
        for(int i = 0; i < indexes.sizes()[0] - 1; ++i)
        {
          lefts[i] = std::max(dets[indexes[0]][0].item().toFloat(),
                              dets[indexes[i + 1]][0].item().toFloat());
          tops[i] = std::max(dets[indexes[0]][1].item().toFloat(),
                             dets[indexes[i + 1]][1].item().toFloat());
          rights[i] = std::min(dets[indexes[0]][2].item().toFloat(),
                               dets[indexes[i + 1]][2].item().toFloat());
          bottoms[i] = std::min(dets[indexes[0]][3].item().toFloat(),
                                dets[indexes[i + 1]][3].item().toFloat());
          widths[i] = std::max(float(0), rights[i].item().toFloat() - lefts[i].item().toFloat());
          heights[i] = std::max(float(0), bottoms[i].item().toFloat() - tops[i].item().toFloat());
        }
        torch::Tensor overlaps = widths * heights;

        // FIlter by IOUs
        torch::Tensor ious
          = overlaps
            / (areas.select(0, indexes[0].item().toInt())
               + torch::index_select(areas, 0, indexes.slice(0, 1, indexes.sizes()[0])) - overlaps);
        indexes
          = torch::index_select(indexes, 0, torch::nonzero(ious <= iou_thresh).select(1, 0) + 1);
      }
      keep = keep.toType(torch::kInt64);
      output.push_back(torch::index_select(dets, 0, keep.slice(0, 0, count)));
    }
    return output;
  }
} // namespace

void object_detection::process(const values::image& _input,
                               values::tagged_polygons_vector<values::class_tag>* _output) const
{
  ::torch::Tensor tensor = pralin::backends::torch::image_backend::get_tensor(
    _input.cast<float>(values::image::scale_factor(_input)));

  tensor = tensor.permute({2, 0, 1});
  tensor = tensor.unsqueeze(0);
  tensor
    = ::torch::nn::functional::interpolate(tensor, ::torch::nn::functional::InterpolateFuncOptions()
                                                     .size(std::vector<int64_t>{384, 640})
                                                     .mode(::torch::kBilinear));

  // Forward prediction
  ::torch::Tensor preds;
  try
  {
    preds = d->interface.module.forward({tensor}).toTuple()->elements()[0].toTensor();
  }
  catch(const std::runtime_error& _err)
  {
    throw pralin::exception("An error occured when runnning torch object_detection: '{}'",
                            _err.what());
  }

  // Export results to bounding boxes
  std::vector<::torch::Tensor> dets = non_max_suppression(preds, 0.4, 0.5);

  if(dets.size() > 0)
  {
    ::torch::Tensor res = dets.front();
    for(int i = 0; i < res.sizes()[0]; ++i)
    {
      float left = res[i][0].item().toFloat() * _input.get_width() / 640;
      float top = res[i][1].item().toFloat() * _input.get_height() / 384;
      float right = res[i][2].item().toFloat() * _input.get_width() / 640;
      float bottom = res[i][3].item().toFloat() * _input.get_height() / 384;
      float score = res[i][4].item().toFloat();
      int classID = res[i][5].item().toInt();

      values::class_tag ct{score, d->interface.labels[classID]};
      values::polygon r = values::polygon::create_rect(left, top, right - left, bottom - top);
      _output->push_back({ct, r});
      pralin_debug_vn(r, ct);
    }
  }
}

#include <pralin/algorithms_registry>
using namespace std::string_literals;

namespace pralin::algorithms::torch
{
  pralin::algorithm_priority algorithm_priority()
  {
    // TODO detect for GPU availability in torch
    return pralin::algorithm_priority::cpu;
  }
} // namespace pralin::algorithms::torch

PRALIN_REGISTER_MODULE("tensorflow",
                       description("Integration with torch library, for deep learning."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::torch::object_detection, "torch", "yolo.v5", algorithm_priority(),
  description("Detect objects using YOLO v5."), "pralin.torch.yolo.v5"s,
  "https://github.com/Nebula4869/YOLOv5-LibTorch/raw/master/yolov5s.torchscript.pt"s,
  "https://raw.githubusercontent.com/Nebula4869/YOLOv5-LibTorch/master/coco.names"s)
