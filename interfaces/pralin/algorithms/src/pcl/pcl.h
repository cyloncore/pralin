#pragma once

#include <pralin/definitions/point_cloud>
#include <pralin/implementation>

namespace pralin::algorithms::pcl
{
  PRALIN_PARAMETERS(downsample, PRALIN_PARAMETER(double, 0.0), x, PRALIN_PARAMETER(double, 0.0), y,
                    PRALIN_PARAMETER(double, 0.0), z);
  /**
   * Apply a transformation on a point cloud (translation and rotation)
   */
  class downsample
      : public implementation<downsample, definitions::point_cloud::filter, downsample_parameters>
  {
  public:
    downsample() = default;
    ~downsample() = default;
    void process(const values::point_cloud& _input, values::point_cloud* _output) const final;
  };
  /**
   */
  class combine : public implementation<combine, definitions::point_cloud::combine>
  {
  public:
    combine() = default;
    ~combine() = default;
    void process(
      const values::point_cloud& _input_a, const values::point_cloud& _input_b,
      const std::optional<values::geometry::transformation_provider>& _transformation_provider,
      values::point_cloud* _output) const final;
  };
  /**
   * Save a point cloud to a binary PCD file
   */
  class pcd_writter : public implementation<pcd_writter, definitions::point_cloud::writter>
  {
  public:
    pcd_writter() = default;
    ~pcd_writter() = default;
    void process(const std::string& _filename, const values::point_cloud& _point_cloud) const final;
  };

  /**
   * Read a point cloud from a binary PCD file
   */
  class pcd_reader : public implementation<pcd_reader, definitions::point_cloud::reader>
  {
  public:
    pcd_reader() = default;
    ~pcd_reader() = default;
    void process(const std::string& _filename, values::point_cloud* _point_cloud) const final;
  };

  class icp : public implementation<icp, definitions::point_cloud::registration>
  {
  public:
    icp() = default;
    ~icp() = default;
    void process(const values::point_cloud& _ref, const values::point_cloud& _tr,
                 const values::geometry::linear_transformation& _input_transformation,
                 values::geometry::linear_transformation* _output_transformation) const final;
  };

  class transformation_estimation_2D
      : public implementation<transformation_estimation_2D, definitions::point_cloud::registration>
  {
  public:
    transformation_estimation_2D() = default;
    ~transformation_estimation_2D() = default;
    void process(const values::point_cloud& _ref, const values::point_cloud& _tr,
                 const values::geometry::linear_transformation& _input_transformation,
                 values::geometry::linear_transformation* _output_transformation) const final;
  };
} // namespace pralin::algorithms::pcl
