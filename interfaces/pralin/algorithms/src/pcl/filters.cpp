#include <pralin/algorithms/pcl>

#include <pcl/common/transforms.h>
#include <pcl/filters/crop_hull.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/point_cloud.h>

#include <pralin/backends/pcl>
#include <pralin/values/polygon>

#include <pralin/backends/eigen>

using namespace pralin::algorithms::pcl;

void downsample::process(const values::point_cloud& _input, values::point_cloud* _output) const
{
  ::pcl::PCLPointCloud2::ConstPtr input
    = pralin::backends::pcl::point_cloud_backend::get_point_cloud(_input);
  ::pcl::PCLPointCloud2::Ptr transformed_cloud(new ::pcl::PCLPointCloud2());

  ::pcl::VoxelGrid<::pcl::PCLPointCloud2> sor;
  sor.setLeafSize(get_parameters().get_x(), get_parameters().get_y(), get_parameters().get_z());
  sor.setInputCloud(input);
  sor.filter(*transformed_cloud);
  transformed_cloud->header = input->header;

  *_output
    = pralin::backends::pcl::point_cloud_backend::create(transformed_cloud, _input.get_meta_data());
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_ALGORITHM(pralin::algorithms::pcl::downsample, "pcl", "downsample",
                          pralin::algorithm_priority::cpu, description("Downsample a point cloud."))
