#include <pralin/algorithms/pcl>

#include <pcl/conversions.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/transformation_estimation_2D.h>
#include <pralin/backends/eigen>

#include <pralin/backends/pcl>

using namespace pralin::algorithms::pcl;

typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

void print4x4Matrix(const Eigen::Matrix4d& matrix)
{
  printf("Rotation matrix :\n");
  printf("    | %6.3f %6.3f %6.3f | \n", matrix(0, 0), matrix(0, 1), matrix(0, 2));
  printf("R = | %6.3f %6.3f %6.3f | \n", matrix(1, 0), matrix(1, 1), matrix(1, 2));
  printf("    | %6.3f %6.3f %6.3f | \n", matrix(2, 0), matrix(2, 1), matrix(2, 2));
  printf("Translation vector :\n");
  printf("t = < %6.3f, %6.3f, %6.3f >\n\n", matrix(0, 3), matrix(1, 3), matrix(2, 3));
}

void icp::process(const values::point_cloud& _ref, const values::point_cloud& _target,
                  const values::geometry::linear_transformation&,
                  values::geometry::linear_transformation* _output_transformation) const
{
  PointCloudT::Ptr ref(new PointCloudT);
  PointCloudT::Ptr tr(new PointCloudT);
  ::pcl::fromPCLPointCloud2(*pralin::backends::pcl::point_cloud_backend::get_point_cloud(_ref),
                            *ref);
  ::pcl::fromPCLPointCloud2(*pralin::backends::pcl::point_cloud_backend::get_point_cloud(_target),
                            *tr);
  int iterations = 100;
  ::pcl::IterativeClosestPoint<PointT, PointT> icp;
  icp.setMaximumIterations(iterations);
  icp.setInputSource(ref);
  icp.setInputTarget(tr);
  icp.align(*ref);

  if(icp.hasConverged())
  {
    std::cout << "\nICP has converged, score is " << icp.getFitnessScore() << std::endl;
    std::cout << "\nICP transformation " << iterations << " : cloud_icp -> cloud_in" << std::endl;
    Eigen::Matrix4d transformation_matrix = icp.getFinalTransformation().cast<double>();
    *_output_transformation = pralin::backends::eigen::to_matrix(transformation_matrix);
  }
  else
  {
    throw pralin::exception("\nICP has not converged.\n");
  }
}

void transformation_estimation_2D::process(
  const values::point_cloud& _ref, const values::point_cloud& _target,
  const values::geometry::linear_transformation&,
  values::geometry::linear_transformation* _output_transformation) const
{
  PointCloudT::Ptr ref(new PointCloudT);
  PointCloudT::Ptr tr(new PointCloudT);
  ::pcl::fromPCLPointCloud2(*pralin::backends::pcl::point_cloud_backend::get_point_cloud(_ref),
                            *ref);
  ::pcl::fromPCLPointCloud2(*pralin::backends::pcl::point_cloud_backend::get_point_cloud(_target),
                            *tr);
  ::pcl::registration::TransformationEstimation2D<PointT, PointT> te;
  Eigen::Matrix4f transformation_matrix = Eigen::Matrix4f::Identity();
  te.estimateRigidTransformation(*ref, *tr, transformation_matrix);
  *_output_transformation = pralin::backends::eigen::to_matrix<double>(transformation_matrix);
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_ALGORITHM(pralin::algorithms::pcl::icp, "pcl", "icp",
                          pralin::algorithm_priority::cpu,
                          description("Compute transformation between two point clouds using ICP."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::pcl::transformation_estimation_2D, "pcl",
                          "transformation_estimation_2D", pralin::algorithm_priority::cpu,
                          description("Estimate 2D transformation between two point clouds."))
