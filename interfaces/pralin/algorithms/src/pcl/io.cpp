#include <pralin/algorithms/pcl>

#include <pcl/io/pcd_io.h>
#include <pralin/backends/pcl>

using namespace pralin::algorithms::pcl;

void pcd_writter::process(const std::string& _filename,
                          const values::point_cloud& _point_cloud) const
{
  ::pcl::PCLPointCloud2::ConstPtr input
    = pralin::backends::pcl::point_cloud_backend::get_point_cloud(_point_cloud);

  ::pcl::PCDWriter writter;
  writter.writeBinary(_filename, *input);
}

void pcd_reader::process(const std::string& _filename, values::point_cloud* _point_cloud) const
{
  ::pcl::PCLPointCloud2::Ptr output_cloud(new ::pcl::PCLPointCloud2());

  ::pcl::PCDReader reader;
  if(reader.read(_filename, *output_cloud) == 0)
  {
    *_point_cloud = pralin::backends::pcl::point_cloud_backend::create(
      output_cloud, ::pralin::values::meta_data::create().set_filename(_filename));
  }
  else
  {
    throw pralin::exception("Failed to read file {}", _filename);
  }
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_ALGORITHM(pralin::algorithms::pcl::pcd_writter, "pcl", "pcd_writter",
                          pralin::algorithm_priority::cpu,
                          description("Save a point cloud to a PCD file."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::pcl::pcd_reader, "pcl", "pcd_reader",
                          pralin::algorithm_priority::cpu,
                          description("Read a point cloud from a PCD file."))
