#include <pralin/algorithms/pcl>

#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>

#include <implementations/pralin/algorithms/point_cloud>
#include <pralin/backends/eigen>
#include <pralin/backends/pcl>
#include <pralin/values/geometry>

// #include <pralin/po>

using namespace pralin::algorithms::pcl;

void combine::process(
  const values::point_cloud& _input_a, const values::point_cloud& _input_b,
  const std::optional<values::geometry::transformation_provider>& _transformation_provider,
  values::point_cloud* _output) const
{
  if(_input_a.is_valid())
  {
    ::pcl::PCLPointCloud2::ConstPtr input_a_cloud
      = pralin::backends::pcl::point_cloud_backend::get_point_cloud(_input_a);
    ::pcl::PCLPointCloud2::Ptr input_b_cloud(new ::pcl::PCLPointCloud2(
      *pralin::backends::pcl::point_cloud_backend::get_point_cloud(_input_b)));

    if(_transformation_provider)
    {
      values::geometry::transformation transfo
        = _transformation_provider->get_transformation(_input_b, _input_a.get_meta_data());

      pralin::algorithms::point_cloud::transform(
        input_b_cloud->data.data(), input_b_cloud->width * input_b_cloud->height,
        input_b_cloud->point_step, _input_b.get_fields(), transfo);
      input_b_cloud->header.frame_id = _input_a.get_meta_data().get_frame_id();
    }
    ::pcl::PCLPointCloud2::concatenate(*input_b_cloud, *input_a_cloud);
    *_output
      = pralin::backends::pcl::point_cloud_backend::create(input_b_cloud, _input_a.get_meta_data());
  }
  else
  {
    *_output = _input_b;
  }
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("pcl",
                       description("Integration with PCL library, for point clouds manipulation."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::pcl::combine, "pcl", "combine",
                          pralin::algorithm_priority::cpu,
                          description("Combine two point clouds together."))
