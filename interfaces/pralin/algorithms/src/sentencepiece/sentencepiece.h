#include <pralin/definitions/ml>
#include <pralin/definitions/nlp>
#include <pralin/implementation>

namespace pralin::algorithms::sentencepiece
{
  /**
   * @ingroup pralin_sentencepiece
   *
   * This class tokenize a document into a set of integer ids, corresponding to each tokens.
   *
   * The model is loaded from the parameters \ref pralin::definitions::ml::model_parameters.
   */
  class tokenize_to_ids
      : public implementation<tokenize_to_ids, ::pralin::definitions::nlp::tokenize_to_ids,
                              pralin::definitions::ml::model_parameters>
  {
    friend implementation_t;
  public:
    tokenize_to_ids();
    ~tokenize_to_ids();
    void process(const std::string& _string, pralin::values::tensor* _ids) const final;
  private:
    void init();
    struct data;
    data* d;
  };
  /**
   * @ingroup pralin_sentencepiece
   *
   * This class tokenize a document into a set of strings, corresponding to each tokens.
   *
   * The model is loaded from the parameters \ref pralin::definitions::ml::model_parameters.
   */
  class tokenize_to_strings
      : public implementation<tokenize_to_strings, ::pralin::definitions::nlp::tokenize_to_strings,
                              pralin::definitions::ml::model_parameters>
  {
    friend implementation_t;
  public:
    tokenize_to_strings();
    ~tokenize_to_strings();
    void process(const std::string& _string, std::vector<std::string>* _tokens) const final;
  private:
    void init();
    struct data;
    data* d;
  };
} // namespace pralin::algorithms::sentencepiece
