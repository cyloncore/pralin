#include "sentencepiece.h"

#include <sentencepiece_processor.h>

#include <sstream>

#include <pralin/resources>
#include <pralin/values/tensor>

using namespace pralin::algorithms::sentencepiece;

struct tokenize_to_ids::data
{
  ::sentencepiece::SentencePieceProcessor processor;
};

tokenize_to_ids::tokenize_to_ids() : d(new data) {}

tokenize_to_ids::~tokenize_to_ids() {}

void tokenize_to_ids::init()
{
  const auto status = d->processor.Load(
    pralin::resources::get(get_parameters().get_module("sentencepiece"),
                           get_parameters().get_filename(), get_parameters().get_source()));
  if(not status.ok())
  {
    throw pralin::exception("Failed to load 'sentencepiece' model: {}", status.ToString());
  }
}

void tokenize_to_ids::process(const std::string& _string, pralin::values::tensor* _ids) const
{
  std::vector<int> ids;
  d->processor.Encode(_string, &ids);
  *_ids = pralin::values::tensor::create_default(pralin::values::meta_data::empty(),
                                                 values::tensor::creation_flag::copy, ids.data(),
                                                 ids.size());
}

struct tokenize_to_strings::data
{
  ::sentencepiece::SentencePieceProcessor processor;
};

tokenize_to_strings::tokenize_to_strings() : d(new data) {}

tokenize_to_strings::~tokenize_to_strings() {}

void tokenize_to_strings::init()
{
  const auto status = d->processor.Load(
    pralin::resources::get(get_parameters().get_module("sentencepiece"),
                           get_parameters().get_filename(), get_parameters().get_source()));
  if(not status.ok())
  {
    throw pralin::exception("Failed to load 'sentencepiece' model: {}", status.ToString());
  }
}

void tokenize_to_strings::process(const std::string& _string,
                                  std::vector<std::string>* _tokens) const
{
  d->processor.Encode(_string, _tokens);
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE(
  "pcl", description("Integration with sentencepiece library, for string tokenization."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::sentencepiece::tokenize_to_ids, "sentencepiece",
                          "tokenize_to_ids", pralin::algorithm_priority::cpu,
                          description("Tokenize to ids using sentencepiece algorithm."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::sentencepiece::tokenize_to_strings, "sentencepiece",
                          "tokenize_to_strings", pralin::algorithm_priority::cpu,
                          description("Tokenize to strings using sentencepiece algorithm."))
