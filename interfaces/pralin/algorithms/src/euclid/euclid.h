#include <pralin/definitions/geometry>
#include <pralin/implementation>
#include <pralin/parameters>

#include <pralin/values/polygon>

namespace pralin::algorithms::euclid
{
  class wkt_to_polygon
      : public implementation<wkt_to_polygon, ::pralin::definitions::geometry::polygon_parser>
  {
    friend implementation_t;
  public:
    wkt_to_polygon() = default;
    ~wkt_to_polygon() = default;
    void process(const std::string& _wkt, const std::optional<values::meta_data>& _meta_data,
                 values::polygon* _polygon) const final;
  };
} // namespace pralin::algorithms::euclid
