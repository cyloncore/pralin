#include "euclid.h"

#include <euclid/geos>
#include <euclid/io>

using namespace pralin::algorithms::euclid;

void wkt_to_polygon::process(const std::string& _wkt,
                             const std::optional<values::meta_data>& _meta_data,
                             values::polygon* _polygon) const
{
  int srid = 4326;
  // Check if a SRID is specified
  if(_meta_data and _meta_data->has_srid())
  {
    srid = _meta_data->get_srid();
  }
  std::string wkt = _wkt;
  if(_wkt.starts_with("SRID="))
  {
    int pos = _wkt.find(';');
    if(pos > 0)
    {
      srid = std::stoi(_wkt.substr(5, pos - 5));
      wkt = _wkt.substr(pos + 1);
    }
    else
    {
      throw pralin::exception("Invalid wkt, missing ';' in '{}'", _wkt);
    }
  }

  ::euclid::geos::polygon polygon = ::euclid::io::from_wkt<::euclid::geos::polygon>(wkt);

  if(polygon.is_null())
  {
    throw pralin::exception("Failed to parse polygon in string '{}'", _wkt);
  }
  else
  {
    std::vector<values::point> points;
    ::euclid::geos::linear_ring lr = polygon.exterior_ring();
    for(auto it = lr.points().begin(); it != lr.points().end(); ++it)
    {
      ::euclid::simple::point pt = *it;
      std::cout << pt.x() << " " << pt.y() << std::endl;
      points.push_back({pt.x(), pt.y()});
    }
    *_polygon = values::polygon(values::meta_data::create().set(_meta_data).set_srid(srid), points);
  }
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE("euclid",
                       description("Integration of euclid library, for geometry manipulation."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::euclid::wkt_to_polygon, "euclid", "wkt_to_polygon",
                          pralin::algorithm_priority::cpu,
                          description("Parse a WKT string into a polygon."))
