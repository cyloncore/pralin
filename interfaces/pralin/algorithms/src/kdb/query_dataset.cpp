#include "kdb.h"

#include <pralin/algorithms_registry>

PRALIN_REGISTER_ALGORITHM(pralin::algorithms::kdb::query_dataset<__PRALIN_LIST(
                            pralin::values::image, pralin::backends::knowl::image_backend)>,
                          "kdb/sensing", "query_image_dataset", pralin::algorithm_priority::cpu,
                          description("Query images from a dataset."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::kdb::query_dataset<
    __PRALIN_LIST(pralin::values::lidar_scan, pralin::backends::knowl::lidar_scan_backend)>,
  "kdb/sensing", "query_lidar_scan_dataset", pralin::algorithm_priority::cpu,
  description("Query lidar scan from a dataset."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::kdb::query_dataset<
    __PRALIN_LIST(pralin::values::point_cloud, pralin::backends::knowl::lidar3d_scan_backend)>,
  "kdb/sensing", "query_lidar3d_scan_dataset", pralin::algorithm_priority::cpu,
  description("Query lidar 3d scan from a dataset."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::kdb::query_dataset<
    __PRALIN_LIST(pralin::values::point_cloud, pralin::backends::knowl::point_cloud_backend)>,
  "kdb/sensing", "query_point_cloud_dataset", pralin::algorithm_priority::cpu,
  description("Query point clouds from a dataset."))
