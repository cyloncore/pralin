#include "kdb.h"

#include <QDir>

#include <kDB/Repository/Store.h>

using namespace pralin::algorithms::kdb;

void create_connection_handle::process(pralin::any_value* _connection_handle) const
{
  kDB::Repository::Store store(QString::fromStdString(get_parameters().get_host()),
                               get_parameters().get_port());
  store.autoSelectPort();
  kDB::Repository::Connection connection = store.createConnection();

  rv_try_void(connection.connect(), "Failed to connect: {}");

  *_connection_handle = pralin::any_value(connection_handle{connection});
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE(
  "kdb", description("Integration with kDB Repository library, for interfacing with a kDB Store."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::kdb::create_connection_handle, "kdb",
                          "create_connection_handle", pralin::algorithm_priority::cpu,
                          description("Create a connection to a kDB Store."))
