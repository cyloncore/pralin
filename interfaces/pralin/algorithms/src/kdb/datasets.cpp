#include "kdb.h"

#include <knowCore/Uris/askcore_graph.h>

#include <kDBDatasets/Collection.h>

#include <pralin/backends/knowl>

using namespace pralin::algorithms::kdb;

void dataset_get::process(const pralin::any_value& _connection,
                          kDBDatasets::Dataset* _dataset) const
{
  kDB::Repository::Connection c = _connection.to_value<connection_handle>().connection;
  kDBDatasets::Collection collection = kDBDatasets::Collection::allDatasets(c);
  *_dataset = rv_try_get(collection.dataset(QString::fromStdString(get_parameters().get_query())),
                         "Failure to get dataset {} with error {}", get_parameters().get_query());
}

void dataset_create::process(const pralin::any_value& _connection,
                             const std::optional<kDBDatasets::Dataset>& _reference_dataset,
                             kDBDatasets::Dataset* _created_dataset) const
{

  kDB::Repository::Connection c = _connection.to_value<connection_handle>().connection;
  kDBDatasets::Collection collection = kDBDatasets::Collection::allDatasets(c);

  knowCore::Uri datasetUri(QString::fromStdString(get_parameters().get_uri()));

  knowCore::Uri type;
  knowGIS::GeometryObject go;

  if(_reference_dataset)
  {
    type = _reference_dataset->type();
    go = rv_try_get(_reference_dataset->geometry(), "{}");
  }
  else
  {
    if(not get_parameters().has_type())
    {
      throw exception("Type is required to create a dataset.");
    }
    if(not get_parameters().has_geometry())
    {
      throw exception("Geometry is required to create a dataset.");
    }
  }
  if(get_parameters().has_type())
  {
    type = QString::fromStdString(get_parameters().get_type());
  }
  if(get_parameters().has_geometry())
  {
    QString wkt_string = QString::fromStdString(get_parameters().get_geometry());
    go = rv_try_get(knowGIS::GeometryObject::fromWKT(wkt_string),
                    "While parsing WKT string {}, got error: {}.", wkt_string);
  }

  cres_qresult<kDBDatasets::Dataset> dataset_rv
    = kDBDatasets::Collection::allDatasets(c).dataset(datasetUri);

  if(dataset_rv.is_successful())
  {
    throw exception("Dataset {} already exists.", datasetUri);
  }
  else
  {
    knowCore::Uri collection_uri = QString::fromStdString(get_parameters().get_collection(
      QString(knowCore::Uris::askcore_graph::private_datasets).toStdString()));
    kDBDatasets::Collection collection
      = rv_try_get(kDBDatasets::Collection::get(c, collection_uri),
                   "Failed to retrieve {} collection with error: {}.", collection_uri);
    kDBDatasets::Dataset dataset = rv_try_get(
      collection.createDataset(type, go, {}, datasetUri),
      "When creating dataset {} of type {} and geometry {}, got error: {}.", datasetUri, type, go);
  }
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE(
  "kdb/datasets", description("Integration with kDB datasets library, for datasets manipulation."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::kdb::dataset_get, "kdb/datasets", "get",
                          pralin::algorithm_priority::cpu,
                          description("Retrieve a dataset from a collection."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::kdb::dataset_create, "kdb/datasets", "create",
  pralin::algorithm_priority::cpu,
  description(
    "Create a dataset.",
    "Using the kDB `connection`, create a new dataset metainformation. If `reference_dataset` is "
    "given as input, it will create a derivative dataset from that dataset, re-using data type and "
    "geometry. If `type` or `geometry` is specified as parameter, it will take precendence over "
    "the `reference_dataset`. If the `reference_dataset` is not given as input, `type` and "
    "`geometry` are mandatory parameters. `collections` is an optional list of collection where "
    "the dataset should be added, if the list is not given, the dataset is added to the private "
    "collection."))
