#include "kdb.h"

#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TripleStore.h>

#include <pralin/values/class_tag>

#include <kDBSensing/SalientRegions/Collection.h>
#include <kDBSensing/SalientRegions/SalientRegion.h>

using namespace pralin::algorithms::kdb::salient_region;

void add::process(const pralin::any_value& _handle, const values::geometry::pose& _pose,
                  const uint64_t& _timestamp, const values::class_vector& _classes,
                  const any_value_map& _properties) const
{
  kDB::Repository::Connection c = _handle.to_value<connection_handle>().connection;

  kDBSensing::SalientRegions::Collection collection
    = rv_try_get(kDBSensing::SalientRegions::Collection::get(
                   c, QString::fromStdString(get_parameters().get_triple_store_uri())),
                 "{}");

  knowCore::UriList classes;
  for(const values::class_tag& ct : _classes)
  {
    classes.append(QString::fromStdString(ct.label));
  }
  rv_try_void(collection.createSalientRegion(
                pralin::backends::knowl::convert(_pose).position(),
                knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(_timestamp), classes,
                pralin::backends::knowl::convert(_properties)),
              "{}");
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE(
  "kdb/sensing/salient_region",
  description("Integration with kDB Sensing library, for manipulating salient regions."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::kdb::salient_region::add,
                          "kdb/sensing/salient_region", "add", pralin::algorithm_priority::cpu,
                          description("Add a salient region to a dataset."))
