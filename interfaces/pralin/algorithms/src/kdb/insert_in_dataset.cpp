#include "kdb.h"

#include <knowCore/Uris/askcore_graph.h>

#include <kDB/Repository/QueryConnectionInfo.h>
#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDBPointClouds/PointCloud.h>
#include <kDBSensing/CameraFrame.h>
#include <kDBSensing/CameraInfo.h>

#include <pralin/backends/knowl>

using namespace pralin::algorithms::kdb;

template<>
void insert_in_dataset<pralin::values::point_cloud>::process(
  const pralin::any_value& _connection, const pralin::values::point_cloud& _value) const
{
  pralin_debug("Start insertion");
  kDB::Repository::Connection c = _connection.to_value<connection_handle>().connection;
  knowCore::Uri dataset_uri = QString::fromStdString(this->get_parameters().get_query());

  knowDBC::Query q
    = c.createSQLQuery("DELETE FROM \"pointclouds\" WHERE \"dataseturi\" = %dataset_uri");
  q.bindValue("%dataset_uri", QString(dataset_uri));
  knowDBC::Result r = q.execute();
  if(not r)
  {
    throw pralin::exception("Failed to remove point cloud from dataset with error: {}", r.error());
  }

  knowValues::Values::PointCloud kl_pc
    = pralin::backends::knowl::point_cloud_backend::get_value(_value);
  if(not kl_pc->geometry().isValid())
  {
    pralin_warning("Geometry is not valid for point cloud, this can cause problems.");
  }

  rv_try_void(kDBPointClouds::PointCloudRecord::create(c, dataset_uri, kl_pc),
              "Failed to insert point cloud: {}");
}

template<>
void insert_in_dataset<pralin::values::image>::process(const pralin::any_value& _connection,
                                                       const pralin::values::image& _value) const
{
  kDB::Repository::Connection c = _connection.to_value<connection_handle>().connection;
  knowCore::Uri dataset_uri = QString::fromStdString(this->get_parameters().get_query());

  knowValues::Values::Image img = pralin::backends::knowl::image_backend::get_value(_value);
  knowCore::Image img_data = img->image();

  cres_qresult<kDBSensing::CameraFrameRecord> rv = kDBSensing::CameraFrameRecord::create(
    c, "", dataset_uri, QString::fromStdString(_value.get_meta_data().get_frame_id()),
    img->timestamp(), kDBSensing::CameraInfoRecord(), img_data.width(), img_data.height(),
    img_data.pixelSize() * img_data.width(), img_data.encoding(), QString("none"),
    img_data.toArray(), QJsonValue());

  rv_try_void(rv, "Failed to insert image: {}");
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE(
  "kdb/sensing",
  description("Integration with kDB Sensing library, for sensor frame manipulation."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::kdb::insert_in_dataset<pralin::values::point_cloud>,
                          "kdb/sensing", "insert_point_cloud_in_dataset",
                          pralin::algorithm_priority::cpu,
                          description("Insert a point cloud in a dataset."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::kdb::insert_in_dataset<pralin::values::image>,
                          "kdb/sensing", "insert_image_in_dataset", pralin::algorithm_priority::cpu,
                          description("Insert an image in a dataset."))
