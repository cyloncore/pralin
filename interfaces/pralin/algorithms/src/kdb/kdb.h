#include <kDB/Repository/Connection.h>

#include <kDBDatasets/Collection.h>
#include <kDBDatasets/DataInterfaceRegistry.h>
#include <kDBDatasets/Dataset.h>

#include <kDBSensing/Forward.h>

#include <pralin/definitions/operators>
#include <pralin/definitions/query>
#include <pralin/implementation>
#include <pralin/values/iterator>

#include <pralin/backends/knowl>

template<>
struct pralin::meta_type::getter_implementation<kDBDatasets::Dataset>
{
  static meta_type get()
  {
    static meta_type mt = meta_type::create<kDBDatasets::Dataset>();
    return mt;
  }
};

namespace pralin::algorithms::kdb
{
  // TODO move into values?
  template<typename _T_>
  struct shared_ptr : public std::shared_ptr<_T_>
  {
    PRALIN_META_INFO(shared_ptr<_T_>)
  public:
    using std::shared_ptr<_T_>::shared_ptr;
  };
  template<typename _Tp, typename... _Args>
  inline shared_ptr<_Tp> make_shared(_Args&&... __args)
  {
    return shared_ptr<_Tp>(new _Tp(__args...));
  }

  struct connection_handle
  {
    PRALIN_META_INFO(connection_handle)
  public:
    kDB::Repository::Connection connection;
  };
  PRALIN_PARAMETERS(create_connection_handle, PRALIN_PARAMETER(std::string, std::string()), host,
                    PRALIN_PARAMETER(int, -1), port);

  class create_connection_handle
      : public implementation<create_connection_handle,
                              ::pralin::definitions::operators::create<any_value>,
                              create_connection_handle_parameters>
  {
    friend implementation_t;
    create_connection_handle() = default;
    ~create_connection_handle() = default;
  public:
    void process(pralin::any_value* _connection_handle) const final;
  };
  // algorithms for handling datasets metadata

  class dataset_get
      : public implementation<dataset_get,
                              ::pralin::definitions::query::select_one<kDBDatasets::Dataset>,
                              ::pralin::definitions::query::query_default_parameters>
  {
    friend implementation_t;
    dataset_get() = default;
    ~dataset_get() = default;
  public:
    void process(const pralin::any_value& _connection, kDBDatasets::Dataset* _dataset) const final;
  };

  PRALIN_PARAMETERS(dataset_create, PRALIN_PARAMETER(std::string, ""), uri,
                    PRALIN_OPTIONAL_PARAMETER(std::string), type,
                    PRALIN_OPTIONAL_PARAMETER(std::string), geometry,
                    PRALIN_OPTIONAL_PARAMETER(std::string), collection);

  class dataset_create
      : public implementation<dataset_create,
                              ::pralin::algorithm_definition<
                                "kdb/dataset_create", input<pralin::any_value, "connection">,
                                input<std::optional<kDBDatasets::Dataset>, "source_dataset">,
                                output<kDBDatasets::Dataset, "created_dataset">>,
                              dataset_create_parameters>
  {
    friend implementation_t;
    dataset_create() = default;
    ~dataset_create() = default;
  public:
    void process(const pralin::any_value& _connection,
                 const std::optional<kDBDatasets::Dataset>& _reference_dataset,
                 kDBDatasets::Dataset* _created_dataset) const final;
  };

  // algorithms for querying
  template<typename _TValue_, typename _knowl_backend_>
  class query_dataset
      : public implementation<query_dataset<_TValue_, _knowl_backend_>,
                              ::pralin::definitions::query::select<_TValue_>,
                              ::pralin::definitions::query::query_default_parameters>
  {
    friend implementation<query_dataset<_TValue_, _knowl_backend_>,
                          ::pralin::definitions::query::select<_TValue_>,
                          ::pralin::definitions::query::query_default_parameters>;
    query_dataset() = default;
    ~query_dataset() = default;
  public:
    void process(const pralin::any_value& _connection,
                 values::iterator<_TValue_>* _iterator) const final;
  };
  template<typename _TValue_>
  class insert_in_dataset
      : public implementation<insert_in_dataset<_TValue_>,
                              ::pralin::definitions::query::insert<_TValue_>,
                              ::pralin::definitions::query::query_default_parameters>
  {
    friend implementation<insert_in_dataset<_TValue_>,
                          ::pralin::definitions::query::insert<_TValue_>,
                          ::pralin::definitions::query::query_default_parameters>;
    insert_in_dataset() = default;
    ~insert_in_dataset() = default;
  public:
    void process(const pralin::any_value& _connection, const _TValue_& _value) const final;
  };
  namespace query_dataset_details
  {
    template<typename _TValue_, typename _knowl_backend_>
    struct iteraror : public values::iterator<_TValue_>::abstract_backend
    {
      iteraror(const kDBDatasets::ValueIterator& _frameIterator) : m_iterator(_frameIterator) {}
      _TValue_ next() final
      {
        auto const [success, value, errorMessage] = m_iterator.next();
        if(success)
        {
          return _knowl_backend_::create(value.value());
        }
        else
        {
          throw pralin::exception("Invalid value when iterating dataset: {}", errorMessage);
          return _TValue_();
        }
      }
      bool has_next() const final { return m_iterator.hasNext(); }
    public:
      kDBDatasets::ValueIterator m_iterator;
    };
  } // namespace query_dataset_details
  template<typename _TValue_, typename _knowl_backend_>
  void
    query_dataset<_TValue_, _knowl_backend_>::process(const pralin::any_value& _connection,
                                                      values::iterator<_TValue_>* _iterator) const
  {
    kDB::Repository::Connection c = _connection.to_value<connection_handle>().connection;
    cres_qresult<kDBDatasets::Dataset> ds = kDBDatasets::Collection::allDatasets(c).dataset(
      QString::fromStdString(this->get_parameters().get_query()));
    if(ds.is_successful())
    {
      auto const& [success, it, errorMessage]
        = kDBDatasets::DataInterfaceRegistry::createValueIterator(c, ds.get_value());
      if(success)
      {
        *_iterator = values::iterator<_TValue_>(
          std::make_shared<query_dataset_details::iteraror<_TValue_, _knowl_backend_>>(it.value()));
      }
      else
      {
        throw pralin::exception("Error while querying dataset: {}", errorMessage);
      }
    }
    else
    {
      throw pralin::exception("Error while retrieving dataset {} with error: {}",
                              this->get_parameters().get_query(), ds.get_error());
    }
  }

  namespace salient_region
  {
    PRALIN_PARAMETERS(add, PRALIN_PARAMETER(std::string, ""), triple_store_uri);
    using add_definition
      = algorithm_definition<"kdb/salient_region_builder/add", input<any_value, "connection">,
                             input<values::geometry::pose, "pose">, input<uint64_t, "timestamp">,
                             input<values::class_vector, "classes">,
                             input<any_value_map, "properties">>;

    class add : public implementation<add, add_definition, add_parameters>
    {
      friend implementation_t;
      add() = default;
      ~add() = default;
    public:
      void process(const any_value& _connection, const values::geometry::pose& _pose,
                   const uint64_t& _timestamp, const values::class_vector& _classes,
                   const any_value_map& _properties) const final;
    };
  } // namespace salient_region

} // namespace pralin::algorithms::kdb
