#include "word2vec.h"

#include "word2vec.hpp"

#include <pralin/resources>
#include <pralin/values/tensor>

using namespace pralin::algorithms::word2vec;

struct word2vec::data
{
  std::unique_ptr<w2v::w2vModel_t> model;
};

word2vec::word2vec() : d(new data) {}

word2vec::~word2vec() {}

void word2vec::process(const std::string& _input, values::tensor* _output) const
{
  if(not d->model)
  {
    d->model = std::make_unique<w2v::w2vModel_t>();
    std::string filename;
    if(get_parameters().get_url().empty())
    {
      filename = get_parameters().get_filename();
    }
    else
    {
      filename
        = resources::get("word2vec", get_parameters().get_filename(), get_parameters().get_url());
    }
    if(not d->model->load(filename))
    {
      throw exception("Failed to load word2vec model from '{}'", filename);
    }
  }
  const w2v::vector_t* vec = d->model->vector(_input);

  *_output = values::tensor::create_default(values::meta_data::empty(),
                                            values::tensor::creation_flag::copy,
                                            const_cast<float*>(vec->data()), vec->size());
}

struct doc2vec::data
{
  std::unique_ptr<w2v::w2vModel_t> model;
};

doc2vec::doc2vec() : d(new data) {}

doc2vec::~doc2vec() {}

void doc2vec::process(const std::string& _input, values::tensor* _output) const
{
  if(not d->model)
  {
    d->model = std::make_unique<w2v::w2vModel_t>();
    std::string filename;
    if(get_parameters().get_url().empty())
    {
      filename = get_parameters().get_filename();
    }
    else
    {
      filename
        = resources::get("word2vec", get_parameters().get_filename(), get_parameters().get_url());
    }
    if(not d->model->load(filename))
    {
      throw exception("Failed to load word2vec model from '{}'", filename);
    }
  }
  w2v::doc2vec_t vec(d->model, _input);

  *_output = values::tensor::create_default(
    values::meta_data::empty(), values::tensor::creation_flag::copy, vec.data(), vec.size());
}
#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE(
  "word2vec",
  description("Integration with word2vec library, for vectorization of words and documents."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::word2vec::word2vec, "word2vec", "word2vec",
                          pralin::algorithm_priority::cpu,
                          description("Convert a word to a vector representation."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::word2vec::doc2vec, "word2vec", "doc2vec",
                          pralin::algorithm_priority::cpu,
                          description("Convert a document to a vector representation."))
