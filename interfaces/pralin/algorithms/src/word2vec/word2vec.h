#pragma once

#include <pralin/definitions/nlp>
#include <pralin/implementation>

namespace pralin::algorithms::word2vec
{
  PRALIN_PARAMETERS(word2vec, PRALIN_PARAMETER(std::string, ""), filename,
                    PRALIN_PARAMETER(std::string, ""), url);

  class word2vec
      : public implementation<word2vec, definitions::nlp::word_to_vec, word2vec_parameters>
  {
    friend implementation_t;
  public:
    word2vec();
    ~word2vec();
    void process(const std::string& _input, values::tensor* _output) const final;
  private:
    struct data;
    std::shared_ptr<data> d;
  };
  class doc2vec : public implementation<doc2vec, definitions::nlp::doc_to_vec, word2vec_parameters>
  {
    friend implementation_t;
  public:
    doc2vec();
    ~doc2vec();
    void process(const std::string& _input, values::tensor* _output) const final;
  private:
    struct data;
    std::shared_ptr<data> d;
  };
} // namespace pralin::algorithms::word2vec
