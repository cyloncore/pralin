#include <pralin/algorithms/libsegments>

#include <iostream>

#include <libsegments/Segment.hpp>
#include <libsegments/SegmentsDetector.hpp>
#include <libsegments/evaluation/StatisticalComparison.hpp>

#include <pralin/values/image>
#include <pralin/values/polygon>

#include <pralin/backends/opencv>

using namespace pralin::algorithms::libsegments;

struct detector::data
{
  ::libsegments::SegmentsDetector* detector = nullptr;
};

detector::detector() : d(new data) {}

detector::~detector() { delete d; }

void detector::init(const std::string& _detector)
{
  if(_detector == "chaining")
  {
    d->detector = ::libsegments::SegmentsDetector::create(
      ::libsegments::SegmentsDetector::Algorithm::Chaining);
  }
  else if(_detector == "lsd")
  {
    d->detector
      = ::libsegments::SegmentsDetector::create(::libsegments::SegmentsDetector::Algorithm::LSD);
  }
  else if(_detector == "elsed")
  {
    d->detector
      = ::libsegments::SegmentsDetector::create(::libsegments::SegmentsDetector::Algorithm::ELSED);
  }
  else if(_detector == "dseg")
  {
    d->detector
      = ::libsegments::SegmentsDetector::create(::libsegments::SegmentsDetector::Algorithm::DSeg);
  }
  else if(_detector == "hdseg")
  {
    d->detector
      = ::libsegments::SegmentsDetector::create(::libsegments::SegmentsDetector::Algorithm::HDSeg);
  }
  else if(_detector == "hough")
  {
    d->detector
      = ::libsegments::SegmentsDetector::create(::libsegments::SegmentsDetector::Algorithm::Hough);
  }
  else
  {
    throw pralin::exception("Unknown detector '{}'", _detector);
  }
}

void detector::init() { init(get_parameters().get_detector()); }

void detector::process(const values::image& _input, values::segment_vector* _output) const
{
  std::vector<::libsegments::Segment> segments
    = d->detector->detectSegments(::pralin::backends::opencv::image_backend::get_mat(_input));

  _output->clear();

  for(::libsegments::Segment s : segments)
  {
    _output->push_back({values::point{s.x1, s.y1}, values::point{s.x2, s.y2}});
  }
}

void statistical_comparison::process(const values::segment_vector& _ground_truth,
                                     const values::segment_vector& _segments,
                                     any_value_map* _results) const
{
  using MatchMode = ::libsegments::evaluation::StatisticalComparison::MatchMode;
  MatchMode mode;
  if(get_parameters().get_mode() == "DoubleMatch")
  {
    mode = MatchMode::DoubleMatch;
  }
  else if(get_parameters().get_mode() == "StructuralHungarian")
  {
    mode = MatchMode::StructuralHungarian;
  }
  else if(get_parameters().get_mode() == "AreaDistanceHungarian")
  {
    mode = MatchMode::AreaDistanceHungarian;
  }
  else if(get_parameters().get_mode() == "sap")
  {
    mode = MatchMode::sap;
  }
  else
  {
    throw pralin::exception("Unknown statistical comparison mode: '{}'",
                            get_parameters().get_mode());
  }

  std::vector<::libsegments::Segment> gt;
  for(const values::segment& s : _ground_truth)
  {
    gt.push_back({s.first.x, s.first.y, s.second.x, s.second.y});
  }
  std::vector<::libsegments::Segment> segs;
  for(const values::segment& s : _segments)
  {
    segs.push_back({s.first.x, s.first.y, s.second.x, s.second.y});
  }

  ::libsegments::evaluation::StatisticalComparison comp(gt, segs, mode,
                                                        get_parameters().get_sap_threshold());

  switch(mode)
  {
  case MatchMode::DoubleMatch:
    (*_results)["splited"] = comp.splitedSegments().size();
    (*_results)["threshold"] = get_parameters().get_sap_threshold();
    break;
  case MatchMode::sap:
    (*_results)["score"] = comp.sapScore();
    (*_results)["threshold"] = get_parameters().get_sap_threshold();
    break;
  }
  (*_results)["distanceMean"] = comp.distanceMean();
  (*_results)["distanceStdDev"] = comp.distanceStdDev();
  (*_results)["elsedPrecision"] = comp.elsedPrecision();
  (*_results)["elsedRecall"] = comp.elsedRecall();
  (*_results)["elsedIntersectionOverUnion"] = comp.elsedIntersectionOverUnion();
  (*_results)["unmatched"] = comp.unmatched();
}

#include <pralin/algorithms_registry>

using namespace std::string_literals;

PRALIN_REGISTER_MODULE(
  "libsegments",
  description("Integration with libsegments library, for detection of line segments in images."))
PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::libsegments::detector, "libsegments", "detector",
  pralin::algorithm_priority::cpu,
  description("Image segments in an image.",
              "This algorithm can use any of the algorithms, which is selected at run time by the "
              "parameter 'detector' which can take the following values: 'chaining', 'lsd', "
              "'elsed', 'dseg', 'hdseg' and 'hough'."))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::libsegments::detector, "libsegments", "chaining",
                          pralin::algorithm_priority::cpu,
                          description("Image segments in an image using chaining algorithm."),
                          "chaining"s)
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::libsegments::detector, "libsegments", "lsd",
                          pralin::algorithm_priority::cpu,
                          description("Image segments in an image using LSD algorithm."), "lsd"s)
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::libsegments::detector, "libsegments", "elsed",
                          pralin::algorithm_priority::cpu,
                          description("Image segments in an image using ELSED algorithm."),
                          "elsed"s)
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::libsegments::detector, "libsegments", "dseg",
                          pralin::algorithm_priority::cpu,
                          description("Image segments in an image using DSEG algorithm."), "dseg"s)
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::libsegments::detector, "libsegments", "hdseg",
                          pralin::algorithm_priority::cpu,
                          description("Image segments in an image using HDSEG algorithm."),
                          "hdseg"s)
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::libsegments::detector, "libsegments", "hough",
                          pralin::algorithm_priority::cpu,
                          description("Image segments in an image using hough algorithm."),
                          "hough"s)

PRALIN_REGISTER_ALGORITHM(
  pralin::algorithms::libsegments::statistical_comparison, "libsegments", "statistical_comparison",
  pralin::algorithm_priority::cpu,
  description("Computate statistics comparison between two segment detection results."), )
