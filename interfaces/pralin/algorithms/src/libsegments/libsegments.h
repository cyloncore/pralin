#pragma once

#include <pralin/definitions/image>
#include <pralin/implementation>

namespace pralin::algorithms::libsegments
{
  PRALIN_PARAMETERS(detector, PRALIN_PARAMETER(std::string, ""), detector);

  class detector
      : public implementation<detector, definitions::image::segment_detector, detector_parameters>
  {
    friend implementation_t;
    detector();
    ~detector();
  public:
    void process(const values::image& _input, values::segment_vector* _output) const final;
  private:
    void init(const std::string& _detector);
    void init();
  private:
    struct data;
    data* const d;
  };

  PRALIN_PARAMETERS(statistical_comparison, PRALIN_PARAMETER(std::string, ""), mode,
                    PRALIN_PARAMETER(double, 5.0), sap_threshold);

  class statistical_comparison
      : public implementation<statistical_comparison,
                              definitions::operators::binary<values::segment_vector, any_value_map>,
                              statistical_comparison_parameters>
  {
    friend implementation_t;
    statistical_comparison() = default;
    ;
    ~statistical_comparison() = default;
  public:
    void process(const values::segment_vector& _ground_truth,
                 const values::segment_vector& _segments, any_value_map* _results) const final;
  };

} // namespace pralin::algorithms::libsegments
