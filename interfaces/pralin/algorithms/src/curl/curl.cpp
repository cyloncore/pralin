#include "curl.h"

#include <sstream>

#include <curl/curl.h>

#include <pralin/values/class_tag>
#include <pralin/values/image>

#include <pralin/algorithms/image>

#include "json.hpp"

using namespace pralin::algorithms::curl;

namespace
{
  static int writer(char* data, size_t size, size_t nmemb, std::string* writerData)
  {
    if(writerData == NULL)
    {
      return 0;
    }

    writerData->append(data, size * nmemb);

    return size * nmemb;
  }
} // namespace

struct remote_classification::data
{
  CURL* curl;
};

remote_classification::remote_classification() : d(new data)
{
  curl_global_init(CURL_GLOBAL_ALL);
  d->curl = curl_easy_init();
}

remote_classification::~remote_classification()
{
  if(d->curl)
  {
    curl_easy_cleanup(d->curl);
  }
  delete d;
}

void remote_classification::process(const values::image& _input,
                                    values::class_vector* _ouptut) const
{
  if(d->curl)
  {
    // Compress to jpeg

    pralin::algorithms::image::jpg_encoder encoder;

    auto jpeg_buffer = std::shared_ptr<std::ostringstream>(new std::ostringstream);
    encoder.process(jpeg_buffer, _input);
    std::string jpeg_data = jpeg_buffer->str();

    curl_easy_setopt(d->curl, CURLOPT_URL, get_parameters().get_server().c_str());

    curl_mime* mime = curl_mime_init(d->curl);
    curl_mimepart* part = curl_mime_addpart(mime);
    curl_mime_name(part, "imagefile");
    curl_mime_filename(part, "image.jpeg");
    curl_mime_data(part, jpeg_data.c_str(), jpeg_data.size());

    curl_easy_setopt(d->curl, CURLOPT_MIMEPOST, mime);
    curl_easy_setopt(d->curl, CURLOPT_WRITEFUNCTION, writer);
    std::string buffer;
    curl_easy_setopt(d->curl, CURLOPT_WRITEDATA, &buffer);

    CURLcode res = curl_easy_perform(d->curl);
    if(res != CURLE_OK)
    {
      throw pralin::exception("Faield to send image to '{}' with error '{}'",
                              get_parameters().get_server(), curl_easy_strerror(res));
    }
    else
    {
      using json = nlohmann::json;
      json data = json::parse(buffer);
      _ouptut->push_back({1.0, data["ans"]});
    }
  }
}

#include <pralin/algorithms_registry>

PRALIN_REGISTER_MODULE(
  "curl", description("Interface with the curl library for integration of remote services"))
PRALIN_REGISTER_ALGORITHM(pralin::algorithms::curl::remote_classification, "curl",
                          "remote_classification", (uint32_t)pralin::algorithm_priority::cpu - 10,
                          description("Send an image to a remote server for classification.",
                                      "Send the `input` image to the server specified by `server` "
                                      "to get a classification result as a vector of class."))
