#pragma once

#include <pralin/definitions/image>
#include <pralin/implementation>

namespace pralin::algorithms::curl
{
  enum Format
  {
    JPEG
  };
}

namespace pralin
{
  template<>
  struct meta_type::getter_implementation<pralin::algorithms::curl::Format>
  {
    static meta_type get()
    {
      static meta_type mt = meta_type::create<pralin::algorithms::curl::Format>();
      return mt;
    }
  };
} // namespace pralin

namespace pralin::algorithms::curl
{
  PRALIN_PARAMETERS(remote_classification, PRALIN_PARAMETER(std::string, std::string()), server,
                    PRALIN_PARAMETER(Format, Format::JPEG), format);

  class remote_classification
      : public implementation<remote_classification, definitions::image::classification,
                              remote_classification_parameters>
  {
    friend implementation_t;
  public:
    remote_classification();
    ~remote_classification();
    void process(const values::image& _input, values::class_vector* _output) const final;
  private:
    struct data;
    data* d;
  };
} // namespace pralin::algorithms::curl
