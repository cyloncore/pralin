/**
 * @defgroup pralin pralin
 * @{
 *
 * This namespace contains the core classes for the pralin algorithm interfaces.
 */
namespace pralin
{
}

/// @}

/**
 * @ingroup pralin_values
 *
 * This namespace defines values that are used in algorithm, such as, image, scalar, point cloud...
 */
namespace pralin::values
{
}

/**
 * @ingroup pralin_definitions
 *
 * This namespace contains the definitions for the pralin algorithms.
 */
namespace pralin::definitions
{
}

/**
 * @ingroup pralin_algorithms
 *
 * This namespace contains the implementation or interface to other libraries.
 */
namespace pralin::algorithms
{
}

/**
 * @ingroup pralin_compose
 *
 * This namespace contains the classes related to the pralin/compose library.
 */
namespace pralin::compose
{
}