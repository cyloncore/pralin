This module allows to use algorithms that have been trained by [sentencepiece](https://github.com/google/sentencepiece). The list of classes for the sentencepiece are in the group \ref pralin_sentencepiece.

To use this module, the first is to train a model using `spm_train`, for instance:

```bash
spm_train --input=shakespeare.txt --model_prefix=sentencepiece --vocab_size=8000 --character_coverage=1.0
```

More information about training can be found in the [sentencepiece documentation](https://github.com/google/sentencepiece).

It can then be used with:

```cpp

#include 

```
