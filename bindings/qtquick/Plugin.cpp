#include "Plugin.h"

#include "Singleton.h"

#include <qqml.h>

void Plugin::registerTypes(const char* uri)
{
  Q_ASSERT(uri == QStringLiteral("Pralin"));
  qmlRegisterSingletonType<Singleton>(uri, 2, 0, "Pralin", [](QQmlEngine*, QJSEngine*) -> QObject*
                                      { return new Singleton; });
  // qmlRegisterSingletonInstance(uri, 2, 0, "Pralin", new Singleton);
}
