import QtQuick 2.0

import Pralin 2.0

Loader
{
  id: _root_
  property string type
  property var value

  function __component_for_type(_type)
  {
    if(_type == "pralin::values::scalar" || _type == Pralin.stringTypename)
    {
      return Qt.createComponent("Label.qml")
    }
    console.log("No display component for type: " + _type)
    return Qt.createComponent("Unknown.qml")
  }
  sourceComponent: __component_for_type(_root_.type)
  onLoaded:
  {
  }
}
