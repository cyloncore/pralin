import QtQuick 2.0

import Pralin 2.0

Loader
{
  id: _root_
  property string type
  property var value

  function __component_for_type(_type)
  {
    if(_type == "pralin::values::scalar")
    {
      return Qt.createComponent("NumberEditor.qml")
    }
    else if(_type == Pralin.stringTypename)
    {
      return Qt.createComponent("StringEditor.qml")
    }
    return Qt.createComponent("Unknown.qml")
  }
  sourceComponent: __component_for_type(_root_.type)
  onLoaded:
  {
    _root_.value = Qt.binding(function() { return _root_.item.value })
  }
}
