#pragma once

#include <QQmlExtensionPlugin>

class Plugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "com.cycloncore.pralin.compose")
public:
  void registerTypes(const char* uri);
};
