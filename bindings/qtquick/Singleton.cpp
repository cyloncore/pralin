#include "Singleton.h"

#include <pralin/algorithms_registry>

Singleton::Singleton(QObject* _parent) : QObject(_parent) {}

Singleton::~Singleton() {}

void Singleton::loadInstalledDefinitions()
{
  pralin::algorithms_registry::load_default_definitions();
}

QString Singleton::stringTypename() const
{
  return QString::fromStdString(pralin::meta_type::get<std::string>().get_name());
}
