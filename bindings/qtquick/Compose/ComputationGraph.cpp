#include "ComputationGraph.h"

#include <QVariantHash>

#include <pralin/compose/computation_graph>
#include <pralin/values/scalar>

struct ComputationGraph::Private
{
  QString definition;
  pralin::compose::computation_graph cg;
  QString lastError;
  QVariantList inputs, outputs;
  bool running = false;
};

ComputationGraph::ComputationGraph(QObject* parent) : QObject(parent), d(new Private) {}

ComputationGraph::~ComputationGraph() {}

QString ComputationGraph::definition() const { return d->definition; }

void ComputationGraph::setDefinition(const QString& _string)
{
  if(d->definition == _string)
    return;
  d->lastError.clear();
  d->definition = _string;
  try
  {
    d->cg.load_from_string(d->definition.toStdString());
  }
  catch(const pralin::exception& _ex)
  {
    d->lastError = QString::fromStdString(_ex.what());
  }
  d->inputs.clear();
  std::fill_n(std::back_inserter(d->inputs), d->cg.get_input_infos().size(), QVariant());
  emit(definitionChanged());
  emit(lastErrorChanged());
}

QString ComputationGraph::lastError() const { return d->lastError; }

namespace
{
  struct scalar_to_var
  {
    template<typename _T_>
      requires std::is_floating_point_v<_T_>
    void operator()(_T_ _v)
    {
      var = _v;
    }
    template<typename _T_>
      requires std::is_signed_v<_T_> and std::is_integral_v<_T_>
    void operator()(_T_ _v)
    {
      var = qint64(_v);
    }
    template<typename _T_>
      requires std::is_unsigned_v<_T_> and std::is_integral_v<_T_>
    void operator()(_T_ _v)
    {
      var = quint64(_v);
    }
    QVariant& var;
  };
} // namespace

void ComputationGraph::process()
{
  pralin::values_vector inputs;
  for(const QVariant& var : d->inputs)
  {
    switch(var.userType())
    {
    case QMetaType::UInt:
      inputs.push_back(pralin::values::scalar(var.toUInt()));
      break;
    case QMetaType::Int:
      inputs.push_back(pralin::values::scalar(var.toInt()));
      break;
    case QMetaType::ULongLong:
      inputs.push_back(pralin::values::scalar((uint64_t)var.toULongLong()));
      break;
    case QMetaType::LongLong:
      inputs.push_back(pralin::values::scalar((int64_t)var.toLongLong()));
      break;
    case QMetaType::Double:
      inputs.push_back(pralin::values::scalar(var.toDouble()));
      break;
    case QMetaType::QString:
      inputs.push_back(var.toString().toStdString());
      break;
    default:
      d->lastError = "Can't convert " + var.toString() + " to pralin value.";
      emit(lastErrorChanged());
      return;
    }
  }
  pralin::values_ptr_vector outputs = d->cg.create_output_values();
  d->lastError.clear();
  try
  {
    d->running = true;
    emit(runningChanged());
    d->cg.process(inputs, outputs);
    d->running = false;
    emit(runningChanged());
    d->outputs.clear();
    for(const pralin::any_value_ptr& o_ptr : outputs)
    {
      QVariant var;
      pralin::any_value o = o_ptr.dereference();
      if(o.is_convertible<pralin::values::scalar>())
      {
        pralin::values::scalar s = o.to_value<pralin::values::scalar>();
        s.visit(scalar_to_var{var});
      }
      else if(o.is_convertible<std::string>())
      {
        var = QString::fromStdString(o.to_value<std::string>());
      }
      d->outputs.push_back(var);
    }
    emit(outputsChanged());
  }
  catch(const pralin::exception& _ex)
  {
    d->lastError = QString::fromStdString(_ex.what());
  }
  emit(lastErrorChanged());
}

QVariantList ComputationGraph::inputs() const { return d->inputs; }

void ComputationGraph::setInputs(const QVariantList& _list)
{
  d->inputs = _list;
  emit(inputsChanged());
}

QVariantList ComputationGraph::outputs() const { return d->outputs; }

QVariantList ComputationGraph::inputsInfos() const
{
  QVariantList l;
  for(const pralin::input_info& ii : d->cg.get_input_infos())
  {
    l.append(QVariantMap{{"name", QString::fromStdString(ii.name)},
                         {"type", QString::fromStdString(ii.type.get_name())}});
  }
  return l;
}

QVariantList ComputationGraph::outputsInfos() const
{
  QVariantList l;
  for(const pralin::output_info& oi : d->cg.get_output_infos())
  {
    l.append(QVariantMap{{"name", QString::fromStdString(oi.name)},
                         {"type", QString::fromStdString(oi.type.get_name())}});
  }
  return l;
}

bool ComputationGraph::running() const { return d->running; }

#include "moc_ComputationGraph.cpp"
