#include "Plugin.h"

#include "ComputationGraph.h"

#include <qqml.h>

void Plugin::registerTypes(const char* uri)
{
  Q_ASSERT(uri == QStringLiteral("Pralin.Compose"));
  qmlRegisterType<ComputationGraph>(uri, 2, 0, "ComputationGraph");
}
