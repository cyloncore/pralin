#ifndef COMPUTATIONGRAPH_H
#define COMPUTATIONGRAPH_H

#include <QObject>

class ComputationGraph : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY(ComputationGraph)
  Q_PROPERTY(QString definition READ definition WRITE setDefinition NOTIFY definitionChanged)
  Q_PROPERTY(QString lastError READ lastError NOTIFY lastErrorChanged)
  Q_PROPERTY(QVariantList inputs READ inputs WRITE setInputs NOTIFY inputsChanged)
  Q_PROPERTY(QVariantList outputs READ outputs NOTIFY outputsChanged)
  Q_PROPERTY(QVariantList inputsInfos READ inputsInfos NOTIFY definitionChanged)
  Q_PROPERTY(QVariantList outputsInfos READ outputsInfos NOTIFY definitionChanged)
  Q_PROPERTY(bool running READ running NOTIFY runningChanged)
public:
  ComputationGraph(QObject* parent = 0);
  ~ComputationGraph();
public:
  QString definition() const;
  void setDefinition(const QString& _string);
  QString lastError() const;
  QVariantList inputs() const;
  void setInputs(const QVariantList& _list);
  QVariantList outputs() const;
  QVariantList inputsInfos() const;
  QVariantList outputsInfos() const;
  bool running() const;
public:
  Q_INVOKABLE void process();
signals:
  void definitionChanged();
  void lastErrorChanged();
  void inputsChanged();
  void outputsChanged();
  void runningChanged();
private:
  struct Private;
  Private* const d;
};

#endif // COMPUTATIONGRAPH_H
