#pragma once

#include <QObject>

class Singleton : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString stringTypename READ stringTypename CONSTANT)
public:
  Singleton(QObject* _parent = nullptr);
  ~Singleton();
  Q_INVOKABLE void loadInstalledDefinitions();
  QString stringTypename() const;
};