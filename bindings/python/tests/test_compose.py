#!/usr/bin/env python3

import unittest as ut

import pralin
import pralin.compose

pralin.AlgorithmsRegistry.load_default_definitions()

class TestCompose(ut.TestCase):
  def test_addition(self):
    cg = pralin.compose.ComputationGraph()
    cg.load_from_string("""
compose:
    inputs:
      - a
      - b
    outputs:
      c: add[0]
    process:
      - pralin/arithmetic/addition:
          id: add
          inputs: [a, b]""")
    sc = pralin.Output()
    cg.process(1, 2, sc)
    self.assertEqual(sc.value(), 3)

if __name__ == '__main__':
  ut.main()
