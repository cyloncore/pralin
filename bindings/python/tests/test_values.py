#!/usr/bin/env python3

import unittest as ut

import pralin
import pralin.values
import numpy as np

pralin.AlgorithmsRegistry.load_default_definitions()

class TestOutput(ut.TestCase):
  def test_addition(self):
    algo  = pralin.AlgorithmsRegistry.create("pralin/arithmetic", "addition")
    self.assertNotEqual(algo, None)
    result = pralin.Output()
    algo.process(1, 2, result)
    self.assertEqual(result.value(), 3)

class TestTensor(ut.TestCase):
  def test_creation(self):
    t = pralin.values.Tensor()
    self.assertEqual(t.get_dimensions(), [])
  def test_process_creation(self):
    nl = pralin.values.Tensor()
    algo  = pralin.AlgorithmsRegistry.create("pralin/testing", "create_tensor_serie")
    self.assertNotEqual(algo, None)
    algo.process(nl)
    self.assertEqual(nl.get_dimensions(), [5])
    self.assertEqual(nl.as_list(), [0.0, 1.0, 2.0, 3.0, 4.0])

  def test_numpy(self):
    nl = pralin.values.Tensor()
    algo  = pralin.AlgorithmsRegistry.create("pralin/testing", "create_tensor_serie")
    self.assertNotEqual(algo, None)
    algo.process(nl)
    na = np.array(nl, copy=False)
    self.assertEqual(na.tolist(), [0.0, 1.0, 2.0, 3.0, 4.0])

if __name__ == '__main__':
  ut.main()
