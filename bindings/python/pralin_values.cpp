#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <pralin/version>

#include <pralin/values/image>
#include <pralin/values/polygon>
#include <pralin/values/tensor>

#include "types.h"

namespace py = pybind11;

namespace
{
  std::string get_buffer_format(pralin::values::scalar_type _st)
  {
    using st = pralin::values::scalar_type;
    switch(_st)
    {
    case st::uint_8:
      return py::format_descriptor<uint8_t>::format();
    case st::uint_16:
      return py::format_descriptor<uint16_t>::format();
    case st::uint_32:
      return py::format_descriptor<uint32_t>::format();
    case st::uint_64:
      return py::format_descriptor<uint64_t>::format();
    case st::int_8:
      return py::format_descriptor<int8_t>::format();
    case st::int_16:
      return py::format_descriptor<int16_t>::format();
    case st::int_32:
      return py::format_descriptor<int32_t>::format();
    case st::int_64:
      return py::format_descriptor<int64_t>::format();
    case st::float_32:
      return py::format_descriptor<float>::format();
    case st::float_64:
      return py::format_descriptor<double>::format();
    };
    throw pralin::exception("Invalid scalar_type '{}'!", int(_st));
  }
  pralin::values::scalar_type get_scalar_type(const std::string& _format)
  {
    using st = pralin::values::scalar_type;
    if(_format == py::format_descriptor<uint8_t>::format())
    {
      return st::uint_8;
    }
    else if(_format == py::format_descriptor<uint16_t>::format())
    {
      return st::uint_16;
    }
    else if(_format == py::format_descriptor<uint32_t>::format())
    {
      return st::uint_32;
    }
    else if(_format == py::format_descriptor<uint64_t>::format())
    {
      return st::uint_64;
    }
    else if(_format == py::format_descriptor<int8_t>::format())
    {
      return st::int_8;
    }
    else if(_format == py::format_descriptor<int16_t>::format())
    {
      return st::int_16;
    }
    else if(_format == py::format_descriptor<int32_t>::format())
    {
      return st::int_32;
    }
    else if(_format == py::format_descriptor<int64_t>::format())
    {
      return st::int_64;
    }
    else if(_format == py::format_descriptor<float>::format())
    {
      return st::float_32;
    }
    else if(_format == py::format_descriptor<double>::format())
    {
      return st::float_64;
    }
    throw pralin::exception("Unsupported format '{}'!", _format);
  }
} // namespace

PYBIND11_MODULE(values, m)
{
  m.doc() = R"pbdoc(
    pralin.values
    -------------

    .. currentmodule:: pralin.values

    .. autosummary::
      :toctree: _generate

      computation_graph
  )pbdoc";
  py::class_<pralin::values::meta_data>(m, "MetaData")
    .def_static("empty", &pralin::values::meta_data::empty);

  // Tensor
  py::class_<pralin::values::tensor>(m, "Tensor", py::buffer_protocol())
    .def(py::init())
    .def_static("create_default",
                [](const pralin::values::meta_data& _meta_data, py::buffer b)
                {
                  py::buffer_info info = b.request();

                  std::vector<std::size_t> dimensions;
                  std::copy(info.shape.begin(), info.shape.end(), std::back_inserter(dimensions));
                  return pralin::values::tensor::create_default(
                    _meta_data, pralin::values::tensor::creation_flag::copy,
                    reinterpret_cast<uint8_t*>(info.ptr), dimensions, get_scalar_type(info.format));
                })
    .def_buffer(
      [](pralin::values::tensor& _tensor)
      {
        return py::buffer_info(
          _tensor.get_data(),                                     /* Pointer to buffer */
          pralin::values::scalar_size(_tensor.get_scalar_type()), /* Size of one scalar */
          get_buffer_format(_tensor.get_scalar_type()), /* Python struct-style format descriptor */
          _tensor.get_dimensions().size(),              /* Number of dimensions */
          _tensor.get_dimensions(),                     /* Buffer dimensions */
          _tensor.get_strides()                         /* Strides (in bytes) for each index */
        );
      })
    .def("get_dimensions", &pralin::values::tensor::get_dimensions)
    .def("as_list",
         [](pralin::values::tensor* _tensor)
         {
           py::list l;
           for(std::size_t i = 0; i < _tensor->get_count(); ++i)
           {
             l.append(_tensor->get_value_at(i));
           }
           return l;
         });

  // Image
  auto imageC
    = py::class_<pralin::values::image>(m, "Image", py::buffer_protocol())
        .def(py::init())
        .def_static(
          "create_default",
          pralin::overload<const pralin::values::tensor&, const pralin::values::image::colorspace&>(
            &pralin::values::image::create_default))
        .def_buffer(
          [](pralin::values::image& _image)
          {
            uint8_t csize = ::pralin::values::scalar_size(_image.get_channel_type());
            return py::buffer_info(
              _image.get_data(), /* Pointer to buffer */
              csize,             /* Size of one scalar */
              get_buffer_format(
                _image.get_channel_type()), /* Python struct-style format descriptor */
              _image.get_channels(),        /* Number of dimensions */
              std::vector<ssize_t>{_image.get_height(), _image.get_width(),
                                   _image.get_channels()}, /* Buffer dimensions */
              std::vector<ssize_t>{_image.get_width_step(), csize * _image.get_channels(), csize}
              /* Strides (in bytes) for each index */
            );
          })
        .def("get_width", &pralin::values::image::get_width)
        .def("get_height", &pralin::values::image::get_height)
    // .def("to_tensor", &pralin::values::image::to_tensor)
    ;
  py::class_<pralin::values::image::colorspace>(imageC, "Colorspace")
    .def(py::init())
    .def_static("rgb", &pralin::values::image::colorspace::rgb);
  m.attr("__version__") = PRALIN_VERSION_STRING;

  // Polygon
  py::class_<pralin::values::point>(m, "Point")
    .def(py::init())
    .def_readwrite("x", &pralin::values::point::x)
    .def_readwrite("y", &pralin::values::point::y);
  py::class_<pralin::values::segment>(m, "Segment")
    .def(py::init())
    .def(py::init([](double _x1, double _y1, double _x2, double _y2)
                  { return pralin::values::segment({{_x1, _y1}, {_x2, _y2}}); }))
    .def_readwrite("first", &pralin::values::segment::first)
    .def_readwrite("second", &pralin::values::segment::second);
}