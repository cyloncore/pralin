#include <pybind11/pybind11.h>

#include <pralin/values/scalar>

namespace pybind11::detail
{
  template<>
  struct type_caster<pralin::values::scalar>
  {
  public:
    PYBIND11_TYPE_CASTER(pralin::values::scalar, const_name("pralin::values::scalar"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      if(int_::check_(src))
      {
        value = pybind11::cast<long>(src);
        return true;
      }
      else if(float_::check_(src))
      {
        value = pybind11::cast<double>(src);
        return true;
      }
      return false;
    }

    template<typename _T_>
    static object sc_to_py(const pralin::values::scalar& _scalar)
    {
      return pybind11::cast(_scalar.cast<_T_>());
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(pralin::values::scalar src, return_value_policy /* policy */,
                       handle /* parent */)
    {
      using nlt = pralin::values::scalar_type;
      switch(src.get_type())
      {
      case nlt::uint_8:
        return sc_to_py<uint8_t>(src).release();
      case nlt::uint_16:
        return sc_to_py<uint16_t>(src).release();
      case nlt::uint_32:
        return sc_to_py<uint32_t>(src).release();
      case nlt::uint_64:
        return sc_to_py<uint64_t>(src).release();
      case nlt::int_8:
        return sc_to_py<int8_t>(src).release();
      case nlt::int_16:
        return sc_to_py<int16_t>(src).release();
      case nlt::int_32:
        return sc_to_py<int32_t>(src).release();
      case nlt::int_64:
        return sc_to_py<int64_t>(src).release();
      case nlt::float_32:
        return sc_to_py<float>(src).release();
      case nlt::float_64:
        return sc_to_py<double>(src).release();
      }
      throw pralin::exception("Unknown scalar type.");
    }
  };
} // namespace pybind11::detail