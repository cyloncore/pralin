#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <pralin/version>

#include <pralin/values/scalar>
#include <pralin/values/tensor>

#include <pralin/compose/computation_graph>

namespace py = pybind11;

PYBIND11_MODULE(compose, m)
{
  m.doc() = R"pbdoc(
    pralin_compose
    --------------

    .. currentmodule:: pralin.compose

    .. autosummary::
      :toctree: _generate

      computation_graph
  )pbdoc";
  py::class_<pralin::compose::computation_graph, pralin::abstract_algorithm>(m, "ComputationGraph")
    .def(py::init())
    .def("load_from_file", &pralin::compose::computation_graph::load_from_file, py::arg("filename"),
         py::arg("parameters") = pralin::any_value_map())
    .def("load_from_string", &pralin::compose::computation_graph::load_from_string,
         py::arg("definition"), py::arg("parameters") = pralin::any_value_map());

  m.attr("__version__") = PRALIN_VERSION_STRING;
}