#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <pralin/global>
#include <pralin/version>

#include <pralin/algorithm_instance>
#include <pralin/algorithms_registry>

#include <pralin/values/class_tag>
#include <pralin/values/image>
#include <pralin/values/point_cloud>
#include <pralin/values/polygon>
#include <pralin/values/scalar>
#include <pralin/values/tagged>
#include <pralin/values/tensor>

#include "types.h"

namespace py = pybind11;

namespace
{
  template<typename _T_>
  bool is_of_type(PyTypeObject* srctype)
  {
    return srctype == py::detail::get_type_info(typeid(_T_), true)->type;
  }
} // namespace

namespace pybind11::detail
{
  template<>
  struct type_caster<pralin::any_value>
  {
  public:
    PYBIND11_TYPE_CASTER(pralin::any_value, const_name("pralin::any_value"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      if(py::int_::check_(src))
      {
        value = pralin::values::scalar(py::cast<long>(src));
        return true;
      }
      else if(py::float_::check_(src))
      {
        value = pralin::values::scalar(py::cast<double>(src));
        return true;
      }
      else if(py::str::check_(src))
      {
        value = py::cast<std::string>(src);
        return true;
      }
      else if(py::list::check_(src))
      {
        py::list list = py::cast<py::list>(src);
        std::vector<pralin::any_value> values;
        for(long i = 0; i < list.size(); ++i)
        {
          values.push_back(py::cast<pralin::any_value>(list[i]));
        }
        value = values;
        return true;
      }
      else
      {
        PyTypeObject* srctype = Py_TYPE(src.ptr());
        if(is_of_type<pralin::values::tensor>(srctype))
        {
          value = py::cast<pralin::values::tensor*>(src);
          return true;
        }
        else if(is_of_type<pralin::values::image>(srctype))
        {
          value = py::cast<pralin::values::image*>(src);
          return true;
        }
        else if(is_of_type<pralin::values::segment>(srctype))
        {
          value = py::cast<pralin::values::segment*>(src);
          return true;
        }

        return false;
      }
    }

    template<typename _T_>
    static handle any_value_cast(pralin::any_value const& _value)
    {
      if(_value.is_of_type<_T_>())
      {
        return py::cast(_value.to_value<_T_>()).release();
      }
      throw pralin::exception("Cannot cast any_value of type {} to python.",
                              _value.get_meta_type().get_name());
    }

    template<typename _T_, typename _T2_, typename... _TOther_>
    static handle any_value_cast(pralin::any_value const& _value)
    {
      if(_value.is_of_type<_T_>())
      {
        return py::cast(_value.to_value<_T_>()).release();
      }
      return any_value_cast<_T2_, _TOther_...>(_value);
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(pralin::any_value src, return_value_policy /* policy */, handle /* parent */)
    {
      return any_value_cast<
        std::string, pralin::values::class_tag, pralin::values::image, pralin::values::point_cloud,
        pralin::values::polygon, pralin::values::scalar, pralin::values::tensor,
        pralin::values::segment,
        pralin::values::tagged<pralin::values::class_tag, pralin::values::polygon>, double,
        std::size_t, int>(src);
    }
  };
  template<>
  struct type_caster<pralin::any_value_ptr>
  {
  public:
    PYBIND11_TYPE_CASTER(pralin::any_value_ptr, const_name("pralin::any_value_ptr"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      PyTypeObject* srctype = Py_TYPE(src.ptr());
      if(is_of_type<pralin::values::tensor>(srctype))
      {
        value = py::cast<pralin::values::tensor*>(src);
        return true;
      }
      else if(is_of_type<pralin::values::image>(srctype))
      {
        value = py::cast<pralin::values::image*>(src);
        return true;
      }
      else if(is_of_type<pralin::values::segment>(srctype))
      {
        value = py::cast<pralin::values::segment*>(src);
        return true;
      }
      return false;
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(pralin::any_value_ptr /*src*/, return_value_policy /* policy */,
                       handle /* parent */)
    {
      throw std::runtime_error("any_value_ptr to python: not implemented");
    }
  };
} // namespace pybind11::detail

namespace
{
  struct output
  {
    pralin::any_value value;
  };

  std::string get_buffer_format(pralin::values::scalar_type _st)
  {
    using st = pralin::values::scalar_type;
    switch(_st)
    {
    case st::uint_8:
      return py::format_descriptor<uint8_t>::format();
    case st::uint_16:
      return py::format_descriptor<uint16_t>::format();
    case st::uint_32:
      return py::format_descriptor<uint32_t>::format();
    case st::uint_64:
      return py::format_descriptor<uint64_t>::format();
    case st::int_8:
      return py::format_descriptor<int8_t>::format();
    case st::int_16:
      return py::format_descriptor<int16_t>::format();
    case st::int_32:
      return py::format_descriptor<int32_t>::format();
    case st::int_64:
      return py::format_descriptor<int64_t>::format();
    case st::float_32:
      return py::format_descriptor<float>::format();
    case st::float_64:
      return py::format_descriptor<double>::format();
    };
    throw pralin::exception("Invalid scalar_type {}", int(_st));
  }
} // namespace

PYBIND11_MODULE(pralin_, m)
{
  m.doc() = R"pbdoc(
    pralin
    ------

    .. currentmodule:: pralin

    .. autosummary::
      :toctree: _generate

      algorithm_instance
  )pbdoc";
  //--- output
  py::class_<output>(m, "Output")
    .def(py::init())
    .def("value", [](output* _self) { return _self->value; });
  //--- pralin::abstract_algorithm
  py::class_<pralin::abstract_algorithm>(m, "AbstractAlgorithm")
    .def("process",
         [](pralin::abstract_algorithm* _self_aa, const py::args& _args)
         {
           std::vector<pralin::input_info> input_infos = _self_aa->get_input_infos();
           std::vector<pralin::output_info> output_infos = _self_aa->get_output_infos();
           if(_args.size() != int(input_infos.size() + output_infos.size()))
           {
             throw pralin::exception("Invalid number of arguments, got {} expected {}",
                                     _args.size(), input_infos.size() + output_infos.size());
           }
           // Inputs
           pralin::values_vector input;
           for(std::size_t i = 0; i < input_infos.size(); ++i)
           {
             pralin::any_value iv = py::cast<pralin::any_value>(_args[i]);
             if(iv.is_convertible(input_infos[i].type))
             {
               input.push_back(iv);
             }
             else if(iv.is_pointer_to(input_infos[i].type))
             {
               input.push_back(pralin::any_value_ptr(iv).dereference());
             }
             else
             {
               throw pralin::exception("Expected type '{}' got '{}' at input '{}' position {}",
                                       input_infos[i].type.get_name(),
                                       iv.get_meta_type().get_name(), input_infos[i].name, i);
             }
           }
           pralin::values_ptr_vector outputs;
           for(std::size_t i = 0; i < output_infos.size(); ++i)
           {
             py::object val_out_i = _args[i + input_infos.size()];
             if(PyList_Check(val_out_i.ptr()) or PyDict_Check(val_out_i.ptr())
                or is_of_type<output>(Py_TYPE(val_out_i.ptr())))
             {
               outputs.push_back(output_infos[i].type.create_any_value_pointer());
             }
             else
             {
               pralin::any_value_ptr ov = py::cast<pralin::any_value_ptr>(val_out_i);
               if(not ov.dereference().is_convertible(output_infos[i].type))
               {
                 throw pralin::exception("Expected type '{}' got '{}' at output '{}' position {}",
                                         output_infos[i].type.get_name(),
                                         ov.get_meta_type().get_name(), output_infos[i].name, i);
               }
               outputs.push_back(ov);
             }
           }
           _self_aa->process(input, outputs);
           for(std::size_t i = 0; i < output_infos.size(); ++i)
           {
             py::object val_out_i = _args[i + input_infos.size()];
             if(PyList_Check(val_out_i.ptr()))
             {
               py::list list = py::cast<py::list>(val_out_i);
               pralin::values::any_value_iterator iterator
                 = outputs[i].dereference().to_value<pralin::values::any_value_iterator>();
               while(iterator.has_next())
               {
                 list.append(iterator.next());
               }
             }
             else if(PyDict_Check(val_out_i.ptr()))
             {
               using key_conv = py::detail::make_caster<std::string>;
               using value_conv = py::detail::make_caster<pralin::any_value>;

               py::dict dict = py::cast<py::dict>(val_out_i);
               pralin::any_value_map map
                 = outputs[i].dereference().to_value<pralin::any_value_map>();
               for(const auto& [k, v] : map)
               {
                 auto key = reinterpret_steal<py::object>(
                   key_conv::cast(k, py::return_value_policy::automatic_reference, dict));
                 auto value = reinterpret_steal<py::object>(
                   value_conv::cast(v, py::return_value_policy::automatic_reference, dict));

                 dict[key] = value;
               }
             }
             else if(is_of_type<output>(Py_TYPE(val_out_i.ptr())))
             {
               py::cast<output*>(val_out_i)->value = outputs[i].dereference();
             }
           }
         });
  //--- pralin::algorithm_instance
  py::class_<pralin::algorithm_instance, pralin::abstract_algorithm>(m, "Algorithm");
  //--- pralin::algorithms_registry
  py::class_<pralin::algorithms_registry,
             std::unique_ptr<pralin::algorithms_registry, py::nodelete>>(m, "AlgorithmsRegistry")
    .def_static("load_default_definitions", &pralin::algorithms_registry::load_default_definitions)
    .def_static("load_definitions_from", &pralin::algorithms_registry::load_definitions_from)
    .def_static(
      "create",
      pralin::overload_r_c<pralin::algorithm_instance, pralin::algorithms_registry,
                           const std::string&, const std::string&, const pralin::any_value_map&>(
        &pralin::algorithms_registry::create),
      py::arg("module"), py::arg("algorithm"), py::arg("parameters") = pralin::any_value_map())
    .def_static(
      "create_any",
      pralin::overload_r_c<pralin::algorithm_instance, pralin::algorithms_registry,
                           const std::string&, const std::string&, const pralin::any_value_map&>(
        &pralin::algorithms_registry::create_any),
      py::arg("definition"), py::arg("algorithm"), py::arg("parameters") = pralin::any_value_map());

  m.attr("__version__") = PRALIN_VERSION_STRING;
}