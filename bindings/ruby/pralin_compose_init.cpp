#include "types.h"

#include <pralin/config_p.h>
#include <pralin/global>

#include <pralin/algorithm_instance>
#include <pralin/algorithms_registry>
#include <pralin/any_value>
#include <pralin/resources>

#include <pralin/values/class_tag>
#include <pralin/values/image>
#include <pralin/values/point_cloud>
#include <pralin/values/polygon>
#include <pralin/values/tagged>
#include <pralin/values/tensor>

#include <pralin/compose/computation_graph>

extern "C" void Init_pralin_compose_bindings_ruby()
{
  Rice::Module rb_mPralin = Rice::define_module("Pralin");

  Rice::Module rb_mPralinCompose = Rice::define_module_under(rb_mPralin, "Compose");
  // BEGIN pralin::compose::ComputationGraph
  Rice::define_class_under<pralin::compose::computation_graph, pralin::abstract_algorithm>(
    rb_mPralinCompose, "ComputationGraph")
    .define_constructor(Rice::Constructor<pralin::compose::computation_graph>())
    .define_method("load_from_file", &pralin::compose::computation_graph::load_from_file,
                   Rice::Arg("filename"), Rice::Arg("parameters") = pralin::any_value_map())
    .define_method("load_from_string", &pralin::compose::computation_graph::load_from_string,
                   Rice::Arg("definition"), Rice::Arg("parameters") = pralin::any_value_map());
  // END pralin::compose::ComputationGraph
}
