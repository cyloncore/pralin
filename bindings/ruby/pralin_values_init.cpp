#include "types.h"

#include <pralin/config_p.h>
#include <pralin/global>

#include <pralin/values/class_tag>
#include <pralin/values/image>
#include <pralin/values/point_cloud>
#include <pralin/values/polygon>
#include <pralin/values/scalar>
#include <pralin/values/tagged>
#include <pralin/values/tensor>

namespace
{
  struct output
  {
    pralin::any_value value;
  };

  Rice::Data_Type<pralin::values::tensor> rb_cTensor;
  Rice::Data_Type<pralin::values::image> rb_cImage;
  Rice::Data_Type<pralin::values::polygon> rb_cPolygon;
  Rice::Data_Type<pralin::values::point_cloud> rb_cPointCloud;
  Rice::Data_Type<pralin::values::point_cloud_builder> rb_cPointCloudBuilder;
  Rice::Data_Type<pralin::values::geometry::linear_transformation> rb_cLinearTransformation;
} // namespace

extern "C" void Init_pralin_values_bindings_ruby()
{
  Rice::Module rb_mPralin = Rice::define_module("Pralin");
  Rice::Module rb_mPralinValues = Rice::define_module_under(rb_mPralin, "Values");

  // BEGIN pralin::values::class_tag
  Rice::define_class_under<pralin::values::class_tag>(rb_mPralinValues, "ClassTag")
    .define_constructor(Rice::Constructor<pralin::values::class_tag>())
    .define_method("confidence", [](pralin::values::class_tag* _self) { return _self->confidence; })
    .define_method("confidence=", [](pralin::values::class_tag* _self, double _confidence)
                   { _self->confidence = _confidence; })
    .define_method("label", [](pralin::values::class_tag* _self) { return _self->label; })
    .define_method("label=", [](pralin::values::class_tag* _self, std::string _label)
                   { _self->label = _label; })
    .define_method("to_s",
                   [](pralin::values::class_tag* _self) { return std::format("{}", *_self); });
  // END pralin::values::class_tag
  // BEGIN pralin::values::point
  Rice::define_class_under<pralin::values::point>(rb_mPralinValues, "Point")
    .define_constructor(Rice::Constructor<pralin::values::point>())
    .define_method("x", [](const pralin::values::point& _self) { return _self.x; })
    .define_method("y", [](const pralin::values::point& _self) { return _self.y; })
    .define_method("to_s",
                   [](const pralin::values::point& _self) { return std::format("{}", _self); });
  // END pralin::values::point
  // BEGIN pralin::values::polygon
  rb_cPolygon = Rice::define_class_under<pralin::values::polygon>(rb_mPralinValues, "Polygon")
                  .define_constructor(Rice::Constructor<pralin::values::polygon>())
                  .define_method("vertices", &pralin::values::polygon::vertices)
                  .define_method("to_s", [](const pralin::values::polygon& _self)
                                 { return std::format("{}", _self); });
  // END pralin::values::polygon
  // BEGIN pralin::values::geometry::linear_transformation
  rb_cLinearTransformation
    = Rice::define_class_under<pralin::values::geometry::linear_transformation>(
        rb_mPralinValues, "LinearTransformation")
        .define_constructor(Rice::Constructor<pralin::values::geometry::linear_transformation>())
        .define_method("matrix", &pralin::values::geometry::linear_transformation::get_matrix);
  // END pralin::values::geometry::linear_transformation
  // BEGIN pralin::image
  rb_cImage = Rice::define_class_under<pralin::values::image>(rb_mPralinValues, "Image")
                .define_constructor(Rice::Constructor<pralin::values::image>())
                .define_method("width", &pralin::values::image::get_width)
                .define_method("height", &pralin::values::image::get_height);
  // END pralin::image
  // BEGIN pralin::tensor
  rb_cTensor = Rice::define_class_under<pralin::values::tensor>(rb_mPralinValues, "Tensor")
                 .define_constructor(Rice::Constructor<pralin::values::tensor>())
                 .define_method("dimensions", &pralin::values::tensor::get_dimensions)
                 .define_method("to_a",
                                [](const pralin::values::tensor* _tensor)
                                {
                                  Rice::Array array;
                                  for(std::size_t idx = 0; idx < _tensor->get_count(); ++idx)
                                  {
                                    array.push(_tensor->get_value_at(idx));
                                  }
                                  return array;
                                });
  // END pralin::tensor
  // BEGIN pralin::point_cloud
  rb_cPointCloud
    = Rice::define_class_under<pralin::values::point_cloud>(rb_mPralinValues, "PointCloud")
        .define_constructor(Rice::Constructor<pralin::values::point_cloud>());
  // END pralin::point_cloud
  // BEGIN pralin::point_cloud_builder
  rb_cPointCloudBuilder
    = Rice::define_class_under<pralin::values::point_cloud_builder>(rb_mPralinValues,
                                                                    "PointCloudBuilder")
        .define_singleton_function(
          "create_default", [](const std::vector<std::string>& _names)
          { return pralin::values::point_cloud_builder::create_default<float>(_names); })
        .define_method(
          "add_point",
          [](pralin::values::point_cloud_builder* _self, const std::vector<float>& _points)
          {
            // TODO Check number of elements
            _self->add_point((const uint8_t*)_points.data());
          })
        .define_method("to_point_cloud", &pralin::values::point_cloud_builder::to_point_cloud);
  // END pralin::point_cloud_builder
}
