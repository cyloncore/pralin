#!/usr/bin/env rspec

require 'pralin-rspec'
require 'pralin/compose'

RSpec.describe Pralin::Compose do
  it "can run an addition with inputs and output" do
    cg = Pralin::Compose::ComputationGraph.new
    cg.load_from_string <<-COMPOSITION
compose:
    inputs:
      - a
      - b
    outputs:
      c: add[0]
    process:
      - pralin/arithmetic/addition:
          id: add
          inputs: [a, b]
COMPOSITION
    sc = Pralin::Output.new
    cg.process(1, 2, sc)
    expect(sc.value).to eq(3)
  end
end
