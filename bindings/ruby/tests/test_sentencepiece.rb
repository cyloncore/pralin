#!/usr/bin/env rspec

require 'pralin-rspec'

RSpec.describe "s" do
  it "can tokenize to ids" do
    tokenize_to_ids = Pralin::AlgorithmsRegistry.create("sentencepiece", "tokenize_to_ids",
      { "module" => "test_data",
        "filename" => "shakespeare.model",
        "source" => "https://gitlab.com/cyloncore/pralin_data/-/raw/stable/models/sentencepiece/shakespeare.model"
      });
    results = Pralin::Values::Tensor.new
    tokenize_to_ids.process("The castle of the king is on the hill?", results)
    expect(results.dimensions.to_a).to eq([10])
    expect(results.to_a).to eq([45, 1717, 13, 7, 107, 24, 65, 7, 3771, 17])
  end
  it "can classify cooking text" do
    tokenize_to_strings = Pralin::AlgorithmsRegistry.create("sentencepiece", "tokenize_to_strings",
      { "module" => "test_data",
        "filename" => "shakespeare.model",
        "source" => "https://gitlab.com/cyloncore/pralin_data/-/raw/stable/models/sentencepiece/shakespeare.model"
      });
    results = []
    tokenize_to_strings.process("The castle of the king is on the hill?", results)
    expect(results.length).to eq(10)
    expect(results).to eq(["▁The", "▁castle", "▁of", "▁the", "▁king", "▁is", "▁on", "▁the", "▁hill", "?"])
  end
end
