#!/usr/bin/env rspec

require 'pralin-rspec'

def test_show_image(img, title)
  image_viewer = Pralin::AlgorithmsRegistry.create_any("pralin/image/viewer", "imshow", {"title" => title});
  image_viewer.process(img)
  sleep(30)
end

def load_image(filename)
    image_reader = Pralin::AlgorithmsRegistry.create("opencv/imgcodecs", "imread");
    result = Pralin::Values::Image.new()
    local_filename = Pralin::Resources::get "test_data", filename, "https://gitlab.com/cyloncore/pralin_data/-/raw/stable/images/#{filename}"
    image_reader.process(local_filename, result)
    return result
end

RSpec.describe "imread" do
  it "can be created" do
    image_reader = Pralin::AlgorithmsRegistry.create("opencv/imgcodecs", "imread");
    expect(image_reader).not_to be_nil    
  end
  it "can load image 'horses.jpg'" do
    result = load_image('horses.jpg')
    expect(result.width()).to eq(773)
    expect(result.height()).to eq(512)
  end
end

RSpec.describe "object_detection" do
  it "can run yolov3 on 'dog.jpg'" do
    yolov3 = Pralin::AlgorithmsRegistry.create("opencv/dnn", "yolo.v3");
    expect(yolov3).not_to be_nil
    result = []
    yolov3.process(load_image("dog.jpg"), result)
  end
end
