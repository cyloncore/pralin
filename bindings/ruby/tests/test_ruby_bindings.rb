#!/usr/bin/env rspec

require 'pralin-rspec'

RSpec.describe Pralin::Values::Image do
  it "can be created empty" do
    nl = Pralin::Values::Image.new()
    expect(nl.width()).to eq(0)
    expect(nl.height()).to eq(0)
  end
end

RSpec.describe Pralin::AlgorithmsRegistry do
  it "has not an unamed algorithm" do
    expect { Pralin::AlgorithmsRegistry.create("", "") }.to raise_error()
  end
  it "has the arithmetic addition algorithm" do
    algo  = Pralin::AlgorithmsRegistry.create("pralin/arithmetic", "addition", Hash.new())
    expect(algo).not_to be_nil
    algo  = Pralin::AlgorithmsRegistry.create("pralin/arithmetic", "addition")
    expect(algo).not_to be_nil
    algo  = Pralin::AlgorithmsRegistry.create_any("pralin/operators/binary", "addition", Hash.new())
    expect(algo).not_to be_nil
    algo  = Pralin::AlgorithmsRegistry.create_any("pralin/operators/binary", "addition")
    expect(algo).not_to be_nil
  end
end

RSpec.describe "pralin/arithmetic/addition" do
  it "can compute 1+2=3" do
    algo  = Pralin::AlgorithmsRegistry.create("pralin/arithmetic", "addition")
    expect(algo).not_to be_nil
    result = Pralin::Output.new()
    algo.process(1, 2, result)
    expect(result.value()).to eq(3)
  end
end

RSpec.describe Pralin::Values::Tensor do
  it "can be created empty" do
    nl = Pralin::Values::Tensor.new()
    expect(nl.dimensions.to_a).to eq([])
  end
  it "can be processed" do
    nl = Pralin::Values::Tensor.new()
    algo  = Pralin::AlgorithmsRegistry.create("pralin/testing", "create_tensor_serie")
    algo.process(nl)
    expect(nl.dimensions.to_a).to eq([5])
    expect(nl.to_a).to eq([0.0, 1.0, 2.0, 3.0, 4.0])
  end
end
