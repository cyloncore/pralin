#!/usr/bin/env rspec

require 'pralin-rspec'

RSpec.describe "ggml/bert" do
  it "can compute embeddings" do
    classification = Pralin::AlgorithmsRegistry.create("ggml/bert", "embeddings",
      { "module" => "test_data",
        "filename" => "ggml-all-MiniLM-L6-v2-q4_0.bin",
        "source" => "https://gitlab.com/cyloncore/pralin_data/-/raw/stable/models/ggml/all-MiniLM-L6-v2-q4_0.bin"
      });
    results = Pralin::Values::Tensor.new
    classification.process("Why not put knives in the dishwasher?", results)
    expect(results.dimensions.to_a).to eq([384])
    expect(results.to_a.slice(0, 5)).to eq([-0.06938486546278, 0.01590554416179657, -0.00032962459954433143, -0.00397432129830122, -0.003179576713591814])
  end
end

RSpec.describe "ggml" do
  it "can run gpt2" do
    classification = Pralin::AlgorithmsRegistry.create("ggml/gpt2", "inference",
      { "module" => "test_data",
        "filename" => "ggml-model-gpt-2-117M.bin",
        "source" => "https://huggingface.co/ggerganov/ggml/resolve/main/ggml-model-gpt-2-117M.bin",
        "predictions_count" => 20
      });
    answer = Pralin::Output.new
    classification.process("Why not put knives in the dishwasher?", answer)
    expect(answer.value).to start_with("Why not put knives in the dishwasher?")
  end
end
