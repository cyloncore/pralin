#!/usr/bin/env rspec

require 'pralin-rspec'

RSpec.describe "fasttext" do
  it "can classify cooking text" do
    classification = Pralin::AlgorithmsRegistry.create("fasttext", "classification",
      { "module" => "test_data",
        "filename" => "model_cooking.bin",
        "source" => "https://gitlab.com/cyloncore/pralin_data/-/raw/stable/models/fasttext/model_cooking.bin"
      });
    results = []
    classification.process("Why not put knives in the dishwasher?", results)
    expect(results.length).to eq(1)
    expect(results[0].label).to eq('__label__knives')
    expect(results[0].confidence).to eq(0.7021743655204773)
  end
end
