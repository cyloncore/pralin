#!/usr/bin/env rspec

require 'pralin-rspec'

RSpec.describe "html" do
  it "can convert html to text" do
    html_to_text = Pralin::AlgorithmsRegistry.create("pralin/html", "html_to_text");
    result = Pralin::Output.new()
    html_to_text.process("<b>Hello world!</b>", result)
    expect(result.value()).to eq("Hello world!\n")
  end
end
