#!/usr/bin/env rspec

require 'pralin-rspec'

def load_image(filename)
    image_reader = Pralin::AlgorithmsRegistry.create_any("pralin/image/reader", "imread");
    result = Pralin::Values::Image.new()
    local_filename = Pralin::Resources::get "test_data", filename, "https://gitlab.com/cyloncore/pralin_data/-/raw/stable/images/#{filename}"
    image_reader.process(local_filename, result)
    return result
end

RSpec.describe "object_detection" do
  it "can run yolov5 on 'dog.jpg'" do
    yolov5 = Pralin::AlgorithmsRegistry.create("torch", "yolo.v5");
    expect(yolov5).not_to be_nil
    result = []
    yolov5.process(load_image("dog.jpg"), result)
    expect(result.size).to eq(3)

    result.each() do |ir|
      case ir["tag"].label
      when "bicycle"
        expect(ir["tag"].confidence).to be_within(0.01).of(0.8181410431861877)
        expect(ir["value"].vertices[0].x).to be_within(0.01).of(137.7516632080078)
        expect(ir["value"].vertices[0].y).to be_within(0.01).of(121.71610260009766)
        expect(ir["value"].vertices[1].x).to be_within(0.01).of(588.2476959228516)
        expect(ir["value"].vertices[1].y).to be_within(0.01).of(433.70108795166016)
      when "car"
        expect(ir["tag"].confidence).to be_within(0.01).of(0.7189828753471375)
        expect(ir["value"].vertices[0].x).to be_within(0.01).of(469.3439025878906)
        expect(ir["value"].vertices[0].y).to be_within(0.01).of(78.58585357666016)
        expect(ir["value"].vertices[1].x).to be_within(0.01).of(689.0192260742188)
        expect(ir["value"].vertices[1].y).to be_within(0.01).of(169.9859619140625)
      when "dog"
        expect(ir["tag"].confidence).to be_within(0.01).of(0.6560696959495544)
        expect(ir["value"].vertices[0].x).to be_within(0.01).of(130.03958129882812)
        expect(ir["value"].vertices[0].y).to be_within(0.01).of(223.174072265625)
        expect(ir["value"].vertices[1].x).to be_within(0.01).of(310.87255859375)
        expect(ir["value"].vertices[1].y).to be_within(0.01).of(537.6622924804688)
      end
    end
  end
end
