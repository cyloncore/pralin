#!/usr/bin/env rspec

require 'pralin-rspec'

RSpec.describe "word2vec" do
  it "can vectorize word" do
    classification = Pralin::AlgorithmsRegistry.create("word2vec", "word2vec",
      { "filename" => "shakespeare.w2v",
        "url" => "https://gitlab.com/cyloncore/pralin_data/-/raw/stable/models/word2vec/shakespeare.w2v"});
    vec = Pralin::Values::Tensor.new()
    classification.process("castle", vec)
    expect(vec.dimensions.to_a).to eq([500])
    expect(vec.to_a.slice(0, 5)).to eq([0.353473424911499, 0.5411036014556885, 0.26852697134017944, -0.4422858953475952, 1.1312720775604248])
  end
  it "can vectorize document" do
    classification = Pralin::AlgorithmsRegistry.create("word2vec", "doc2vec",
      { "filename" => "shakespeare.w2v",
        "url" => "https://gitlab.com/cyloncore/pralin_data/-/raw/stable/models/word2vec/shakespeare.w2v"});
    vec = Pralin::Values::Tensor.new()
    classification.process("The castle of the king.", vec)
    expect(vec.dimensions.to_a).to eq([500])
    expect(vec.to_a.slice(0, 5)).to eq([0.29154878854751587, 0.629813015460968, 0.30790597200393677, -0.5023677349090576, 1.236695647239685])
  end
end
