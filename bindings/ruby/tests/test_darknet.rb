#!/usr/bin/env rspec

require 'pralin-rspec'

def load_image(filename)
    image_reader = Pralin::AlgorithmsRegistry.create_any("pralin/image/reader", "imread");
    result = Pralin::Values::Image.new()
    local_filename = Pralin::Resources::get "test_data", filename, "https://gitlab.com/cyloncore/pralin_data/-/raw/stable/images/#{filename}"
    image_reader.process(local_filename, result)
    return result
end

RSpec.describe "object_detection" do
  it "can run yolov3-tiny on 'dog.jpg'" do
    yolov3 = Pralin::AlgorithmsRegistry.create("darknet", "yolo.v3-tiny", {"threshold" => 0.5});
    expect(yolov3).not_to be_nil
    result = []
    yolov3.process(load_image("dog.jpg"), result)
    expect(result.size).to eq(3)

    result.each() do |ir|
      case ir["tag"].label
      when "car"
        if ir["tag"].confidence < 0.7
          expect(ir["tag"].confidence).to be_within(0.01).of(0.6524531841278076)
          expect(ir["value"].vertices[0].x).to be_within(0.01).of(528.4180870056152)
          expect(ir["value"].vertices[0].y).to be_within(0.01).of(32.1198844909668)
          expect(ir["value"].vertices[1].x).to be_within(0.01).of(627.4959754943848)
          expect(ir["value"].vertices[1].y).to be_within(0.01).of(116.6805305480957)
        else
          expect(ir["tag"].confidence).to be_within(0.01).of(0.7252074480056763)
          expect(ir["value"].vertices[0].x).to be_within(0.01).of(467)
          expect(ir["value"].vertices[0].y).to be_within(0.01).of(83)
          expect(ir["value"].vertices[1].x).to be_within(0.01).of(687)
          expect(ir["value"].vertices[1].y).to be_within(0.01).of(172)
        end
      when "dog"
        expect(ir["tag"].confidence).to be_within(0.01).of(0.828930139541626)
        expect(ir["value"].vertices[0].x).to be_within(0.01).of(124)
        expect(ir["value"].vertices[0].y).to be_within(0.01).of(219)
        expect(ir["value"].vertices[1].x).to be_within(0.01).of(382)
        expect(ir["value"].vertices[1].y).to be_within(0.01).of(519)
      end
    end
  end
end
