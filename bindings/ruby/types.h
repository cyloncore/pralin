#include <rice/rice.hpp>
#include <rice/stl.hpp>

// Cleanup some ruby defines that clashes with fmt
#undef isfinite
#undef int128_t
#undef uint128_t

#include <pralin/values/scalar>

namespace rice_pralin
{
  struct output
  {
    pralin::any_value value;
  };

  template<typename _T_>
  class From_Ruby_Ptr
  {
  public:
    using From_Ruby_Ptr_T = From_Ruby_Ptr<_T_>;
    _T_* convert(VALUE value)
    {
      this->converted_ = Rice::detail::From_Ruby<_T_>().convert(value);
      return &this->converted_;
    }
  private:
    pralin::any_value converted_;
  };

  template<typename _T_>
  class From_Ruby_Ref
  {
  public:
    using From_Ruby_Ref_T = From_Ruby_Ref<_T_>;
    From_Ruby_Ref() = default;

    explicit From_Ruby_Ref(Rice::Arg* arg) : arg_(arg) {}

    _T_& convert(VALUE value)
    {
      if(value == Qnil && this->arg_ && this->arg_->hasDefaultValue())
      {
        return this->arg_->defaultValue<_T_>();
      }
      else
      {
        this->converted_ = Rice::detail::From_Ruby<_T_>().convert(value);
        return this->converted_;
      }
    }
  private:
    Rice::Arg* arg_ = nullptr;
    _T_ converted_;
  };
} // namespace rice_pralin

namespace Rice::detail
{
  // pralin::values::scalar
  template<>
  struct Type<pralin::values::scalar>
  {
    static bool verify() { return true; }
  };

  template<>
  class From_Ruby<pralin::values::scalar>
  {
  public:
    From_Ruby() = default;

    explicit From_Ruby(Arg* arg) : arg_(arg) {}

    pralin::values::scalar convert(VALUE value)
    {
      switch(rb_type(value))
      {
      case RUBY_T_NIL:
        if(this->arg_ && this->arg_->hasDefaultValue())
        {
          return this->arg_->template defaultValue<pralin::values::scalar>();
        }
        return pralin::values::scalar();
      case RUBY_T_FIXNUM:
        return Rice::detail::From_Ruby<int64_t>().convert(value);
      case RUBY_T_FLOAT:
        return Rice::detail::From_Ruby<double>().convert(value);
      default:
      {
        std::stringstream stream;
        stream << "Cannot use " << detail::protect(rb_obj_classname, value) << " (rb_type = 0x"
               << std::hex << rb_type(value) << ") " << " to construct pralin::values::scalar";
        throw std::invalid_argument(stream.str().c_str());
      }
      }
    }
  private:
    Arg* arg_ = nullptr;
  };

  template<>
  struct From_Ruby<pralin::values::scalar*>
      : public rice_pralin::From_Ruby_Ptr<pralin::values::scalar>
  {
    using rice_pralin::From_Ruby_Ptr<pralin::values::scalar>::From_Ruby_Ptr;
  };

  template<>
  struct From_Ruby<pralin::values::scalar&>
      : public rice_pralin::From_Ruby_Ref<pralin::values::scalar>
  {
    using rice_pralin::From_Ruby_Ref<pralin::values::scalar>::From_Ruby_Ref;
  };

  template<>
  class To_Ruby<pralin::values::scalar>
  {
    template<typename _T_>
    VALUE sc_to_ruby(pralin::values::scalar const& _scalar)
    {
      return Rice::detail::To_Ruby<_T_>().convert(_scalar.cast<_T_>());
    }
  public:
    VALUE convert(pralin::values::scalar const& x)
    {
      using nlt = pralin::values::scalar_type;
      switch(x.get_type())
      {
      case nlt::uint_8:
        return sc_to_ruby<uint8_t>(x);
      case nlt::uint_16:
        return sc_to_ruby<uint16_t>(x);
      case nlt::uint_32:
        return sc_to_ruby<uint32_t>(x);
      case nlt::uint_64:
        return sc_to_ruby<uint64_t>(x);
      case nlt::int_8:
        return sc_to_ruby<int8_t>(x);
      case nlt::int_16:
        return sc_to_ruby<int16_t>(x);
      case nlt::int_32:
        return sc_to_ruby<int32_t>(x);
      case nlt::int_64:
        return sc_to_ruby<int64_t>(x);
      case nlt::float_32:
        return sc_to_ruby<float>(x);
      case nlt::float_64:
        return sc_to_ruby<double>(x);
      }
      return Qnil;
    }
  };
  // pralin::values::tagged
  template<typename _TTag_, typename _TTValue_>
  struct Type<pralin::values::tagged<_TTag_, _TTValue_>>
  {
    static bool verify() { return Type<_TTag_>::verify() and Type<_TTValue_>::verify(); }
  };

  template<typename _TTag_, typename _TTValue_>
  class From_Ruby<pralin::values::tagged<_TTag_, _TTValue_>>
  {
  public:
    From_Ruby() = default;

    explicit From_Ruby(Arg* arg) : arg_(arg) {}

    pralin::values::tagged<_TTag_, _TTValue_> convert(VALUE value)
    {
      pralin::any_value_map map = From_Ruby<pralin::any_value_map>().convert(value);
      auto it_tag = map.find("tag");
      auto it_value = map.find("value");
      if(it_tag == map.end())
      {
        throw std::invalid_argument("Missing 'tag' key in tagged definition.");
      }
      if(it_value == map.end())
      {
        throw std::invalid_argument("Missing 'value' key in tagged definition.");
      }
      pralin::values::tagged<_TTag_, _TTValue_> tagged_value;
      tagged_value.tag = it_tag->second.to_value<_TTag_>();
      tagged_value.value = it_value->second.to_value<_TTValue_>();
      return tagged_value;
    }
  private:
    Arg* arg_ = nullptr;
  };

  template<typename _TTag_, typename _TTValue_>
  struct From_Ruby<pralin::values::tagged<_TTag_, _TTValue_>*>
      : public rice_pralin::From_Ruby_Ptr<pralin::values::tagged<_TTag_, _TTValue_>>
  {
    using rice_pralin::From_Ruby_Ptr<pralin::values::tagged<_TTag_, _TTValue_>>::From_Ruby_Ptr;
  };

  template<typename _TTag_, typename _TTValue_>
  struct From_Ruby<pralin::values::tagged<_TTag_, _TTValue_>&>
      : public rice_pralin::From_Ruby_Ref<pralin::values::tagged<_TTag_, _TTValue_>>
  {
    using rice_pralin::From_Ruby_Ref<pralin::values::tagged<_TTag_, _TTValue_>>::From_Ruby_Ref;
  };

  template<typename _TTag_, typename _TTValue_>
  class To_Ruby<pralin::values::tagged<_TTag_, _TTValue_>>
  {
  public:
    VALUE convert(pralin::values::tagged<_TTag_, _TTValue_> const& x)
    {
      pralin::any_value_map map;
      map["tag"] = x.tag;
      map["value"] = x.value;
      return To_Ruby<pralin::any_value_map>().convert(map);
    }
  };
  // pralin::values::matrix
  template<typename _TType_, std::size_t _Rows_, std::size_t _Cols_>
  struct Type<pralin::values::matrix<_TType_, _Rows_, _Cols_>>
  {
    static bool verify() { return Type<_TType_>::verify(); }
  };

  template<typename _TType_, std::size_t _Rows_, std::size_t _Cols_>
  pralin::values::matrix<_TType_, _Rows_, _Cols_> matrixFromArray(VALUE value)
  {
    long length = protect(rb_array_len, value);
    pralin::values::matrix<_TType_, _Rows_, _Cols_> result;
    if(length != _Rows_ * _Cols_)
    {
      throw Exception(rb_eTypeError, "Wrong number of elements, expected %i, got %i.",
                      _Rows_ * _Cols_, length);
    }
    for(std::size_t i = 0; i < _Rows_ * _Cols_; i++)
    {
      VALUE element = protect(rb_ary_entry, value, i);
      result.data()[i] = From_Ruby<_TType_>().convert(element);
    }

    return result;
  }

  template<typename _TType_, std::size_t _Rows_, std::size_t _Cols_>
  class From_Ruby<pralin::values::matrix<_TType_, _Rows_, _Cols_>>
  {
  public:
    From_Ruby() = default;

    explicit From_Ruby(Arg* arg) : arg_(arg) {}

    pralin::values::matrix<_TType_, _Rows_, _Cols_> convert(VALUE value)
    {
      switch(rb_type(value))
      {
      case T_ARRAY:
      {
        // If this an Ruby array and the vector type is copyable
        if constexpr(std::is_default_constructible_v<_TType_>)
        {
          return matrixFromArray<_TType_, _Rows_, _Cols_>(value);
        }
      }
      case T_NIL:
      {
        if(this->arg_ && this->arg_->hasDefaultValue())
        {
          return this->arg_
            ->template defaultValue<pralin::values::matrix<_TType_, _Rows_, _Cols_>>();
        }
      }
      default:
      {
        throw Exception(rb_eTypeError, "wrong argument type %s (expected %s)",
                        detail::protect(rb_obj_classname, value), "pralin::values::matrix");
      }
      }
    }
  private:
    Arg* arg_ = nullptr;
  };

  template<typename _TType_, std::size_t _Rows_, std::size_t _Cols_>
  struct From_Ruby<pralin::values::matrix<_TType_, _Rows_, _Cols_>*>
      : public rice_pralin::From_Ruby_Ptr<pralin::values::matrix<_TType_, _Rows_, _Cols_>>
  {
    using rice_pralin::From_Ruby_Ptr<
      pralin::values::matrix<_TType_, _Rows_, _Cols_>>::From_Ruby_Ptr;
  };

  template<typename _TType_, std::size_t _Rows_, std::size_t _Cols_>
  struct From_Ruby<pralin::values::matrix<_TType_, _Rows_, _Cols_>&>
      : public rice_pralin::From_Ruby_Ref<pralin::values::matrix<_TType_, _Rows_, _Cols_>>
  {
    using rice_pralin::From_Ruby_Ref<
      pralin::values::matrix<_TType_, _Rows_, _Cols_>>::From_Ruby_Ref;
  };

  template<typename _TType_, std::size_t _Rows_, std::size_t _Cols_>
  class To_Ruby<pralin::values::matrix<_TType_, _Rows_, _Cols_>>
  {
  public:
    VALUE convert(pralin::values::matrix<_TType_, _Rows_, _Cols_> const& x)
    {
      Rice::Array array;
      for(_TType_ v : x)
      {
        array.push(v);
      }
      return array.value();
    }
  };
  // pralin::any_value
  template<>
  struct Type<pralin::any_value>
  {
    static bool verify() { return true; }
  };
  template<typename _T_>
  VALUE any_valueToVALUE_(pralin::any_value const& _value)
  {
    if(_value.is_of_type<_T_>())
    {
      return To_Ruby<_T_>().convert(_value.to_value<_T_>());
    }
    return Qnil;
  }

  template<typename _T_, typename _T2_, typename... _TOther_>
  VALUE any_valueToVALUE_(pralin::any_value const& _value)
  {
    if(_value.is_of_type<_T_>())
    {
      return To_Ruby<_T_>().convert(_value.to_value<_T_>());
    }
    return any_valueToVALUE_<_T2_, _TOther_...>(_value);
  }

  VALUE any_valueToVALUE(pralin::any_value const& _value)
  {
    VALUE val = any_valueToVALUE_<
      std::string, pralin::values::class_tag, pralin::values::image, pralin::values::point_cloud,
      pralin::values::polygon, pralin::values::scalar, pralin::values::tensor,
      pralin::values::tagged<pralin::values::class_tag, pralin::values::polygon>>(_value);
    if(val == Qnil)
    {
      throw std::runtime_error(
        std::format("Cannot convert value of type {} to ruby", _value.get_meta_type().get_name()));
    }
    else
    {
      return val;
    }
  }

  template<>
  class To_Ruby<pralin::any_value>
  {
  public:
    VALUE convert(pralin::any_value const& x) { return any_valueToVALUE(x); }
  };

  template<>
  class To_Ruby<pralin::any_value&>
  {
  public:
    VALUE convert(pralin::any_value const& x) { return any_valueToVALUE(x); }
  };

  template<>
  class From_Ruby<pralin::any_value>
  {
  public:
    From_Ruby() = default;

    explicit From_Ruby(Arg* arg) : arg_(arg) {}

    pralin::any_value convert(VALUE value)
    {
      if(value == Qnil && this->arg_ && this->arg_->hasDefaultValue())
      {
        return this->arg_->defaultValue<pralin::any_value>();
      }
      else
      {
        switch(rb_type(value))
        {
        case RUBY_T_FIXNUM:
          return pralin::values::scalar(From_Ruby<int64_t>().convert(value));
          break;
        case RUBY_T_FLOAT:
          return pralin::values::scalar(From_Ruby<double>().convert(value));
          break;
        case RUBY_T_STRING:
          return From_Ruby<std::string>().convert(value);
          break;
        case RUBY_T_ARRAY:
        {
          Rice::Array array(value);
          std::vector<pralin::any_value> values;
          for(long i = 0; i < array.size(); ++i)
          {
            values.push_back(From_Ruby<pralin::any_value>().convert(array[i].value()));
          }
          return values;
        }
        case RUBY_T_DATA:
        {
          VALUE class_of_value = rb_class_of(value);
          if(class_of_value == Rice::Data_Type<pralin::values::image>::klass().value())
          {
            return From_Ruby<pralin::values::image*>().convert(value);
          }
          if(class_of_value == Rice::Data_Type<pralin::values::tensor>::klass().value())
          {
            return From_Ruby<pralin::values::tensor*>().convert(value);
          }
          if(class_of_value == Rice::Data_Type<pralin::values::point_cloud>::klass().value())
          {
            return From_Ruby<pralin::values::point_cloud*>().convert(value);
          }
          if(class_of_value
             == Rice::Data_Type<pralin::values::point_cloud_builder>::klass().value())
          {
            return From_Ruby<pralin::values::point_cloud_builder*>().convert(value);
          }
          if(class_of_value
             == Rice::Data_Type<pralin::values::geometry::linear_transformation>::klass().value())
          {
            return From_Ruby<pralin::values::geometry::linear_transformation*>().convert(value);
          }
          if(class_of_value == Rice::Data_Type<rice_pralin::output>::klass().value())
          {
            return From_Ruby<rice_pralin::output*>().convert(value)->value;
          }
        }
          [[fallthrough]];
        default:
        {
          std::stringstream stream;
          stream << "Cannot convert " << detail::protect(rb_obj_classname, value)
                 << " (rb_type = 0x" << std::hex << rb_type(value) << ") "
                 << " to pralin::any_value";
          throw std::invalid_argument(stream.str().c_str());
        }
        }
      }
    }
  private:
    Arg* arg_ = nullptr;
  };

  template<>
  struct From_Ruby<pralin::any_value*> : public rice_pralin::From_Ruby_Ptr<pralin::any_value>
  {
    using From_Ruby_Ptr::From_Ruby_Ptr;
  };

  template<>
  struct From_Ruby<pralin::any_value&> : public rice_pralin::From_Ruby_Ref<pralin::any_value>
  {
    using From_Ruby_Ref::From_Ruby_Ref;
  };

  // pralin::any_value_ptr
  template<>
  struct Type<pralin::any_value_ptr>
  {
    static bool verify() { return true; }
  };

  VALUE any_valueToVALUE(pralin::any_value_ptr const&)
  {
    throw std::runtime_error("any_value_ptr to VALUE: not implemented");
  }

  template<>
  class To_Ruby<pralin::any_value_ptr>
  {
  public:
    VALUE convert(pralin::any_value_ptr const& x) { return any_valueToVALUE(x); }
  };

  template<>
  class To_Ruby<pralin::any_value_ptr&>
  {
  public:
    VALUE convert(pralin::any_value_ptr const& x) { return any_valueToVALUE(x); }
  };

  template<>
  class From_Ruby<pralin::any_value_ptr>
  {
  public:
    From_Ruby() = default;

    explicit From_Ruby(Arg* arg) : arg_(arg) {}

    pralin::any_value_ptr convert(VALUE value)
    {
      if(value == Qnil && this->arg_ && this->arg_->hasDefaultValue())
      {
        return this->arg_->defaultValue<pralin::any_value_ptr>();
      }
      else
      {
        switch(rb_type(value))
        {
        case RUBY_T_DATA:
        {
          VALUE class_of_value = rb_class_of(value);
          if(class_of_value == Rice::Data_Type<pralin::values::image>::klass().value())
          {
            return From_Ruby<pralin::values::image*>().convert(value);
          }
          if(class_of_value == Rice::Data_Type<pralin::values::tensor>::klass().value())
          {
            return From_Ruby<pralin::values::tensor*>().convert(value);
          }
          if(class_of_value == Rice::Data_Type<pralin::values::point_cloud>::klass().value())
          {
            return From_Ruby<pralin::values::point_cloud*>().convert(value);
          }
          if(class_of_value
             == Rice::Data_Type<pralin::values::point_cloud_builder>::klass().value())
          {
            return From_Ruby<pralin::values::point_cloud_builder*>().convert(value);
          }
          if(class_of_value
             == Rice::Data_Type<pralin::values::geometry::linear_transformation>::klass().value())
          {
            return From_Ruby<pralin::values::geometry::linear_transformation*>().convert(value);
          }
        }
          [[fallthrough]];
        default:
        {
          std::stringstream stream;
          stream << "Cannot convert " << detail::protect(rb_obj_classname, value)
                 << " (rb_type = 0x" << std::hex << rb_type(value) << ") "
                 << " to pralin::any_value_ptr";
          throw std::invalid_argument(stream.str().c_str());
        }
        }
      }
    }
  private:
    Arg* arg_ = nullptr;
  };

  template<>
  struct From_Ruby<pralin::any_value_ptr*>
      : public rice_pralin::From_Ruby_Ptr<pralin::any_value_ptr>
  {
    using From_Ruby_Ptr::From_Ruby_Ptr;
  };

  template<>
  struct From_Ruby<pralin::any_value_ptr&>
      : public rice_pralin::From_Ruby_Ref<pralin::any_value_ptr>
  {
    using From_Ruby_Ref::From_Ruby_Ref;
  };
} // namespace Rice::detail
