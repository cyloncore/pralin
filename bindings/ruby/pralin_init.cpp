#include "types.h"

#include <pralin/config_p.h>
#include <pralin/global>

#include <pralin/algorithm_instance>
#include <pralin/algorithms_registry>
#include <pralin/any_value>
#include <pralin/resources>

#include <pralin/values/class_tag>
#include <pralin/values/image>
#include <pralin/values/point_cloud>
#include <pralin/values/polygon>
#include <pralin/values/tagged>
#include <pralin/values/tensor>

namespace
{
  Rice::Data_Type<rice_pralin::output> rb_cOutput;
}

namespace
{
  VALUE algorithm_process(int argc, VALUE* argv, VALUE self)
  {
    pralin::abstract_algorithm* self_aa = Rice::detail::cpp_protect(
      [&] { return Rice::detail::From_Ruby<pralin::abstract_algorithm*>().convert(self); });

    std::vector<pralin::input_info> input_infos = self_aa->get_input_infos();
    std::vector<pralin::output_info> output_infos = self_aa->get_output_infos();
    if(argc != int(input_infos.size() + output_infos.size()))
    {
      rb_exc_raise(
        rb_exc_new2(rb_eArgError, std::format("Invalid number of arguments, got {} expected {}",
                                              argc, input_infos.size() + output_infos.size())
                                    .c_str()));
      return Qnil;
    }
    pralin::values_vector inputs;
    for(std::size_t i = 0; i < input_infos.size(); ++i)
    {
      pralin::any_value iv = Rice::detail::cpp_protect(
        [&] { return Rice::detail::From_Ruby<pralin::any_value>().convert(argv[i]); });
      if(input_infos[i].type == pralin::any_value::get_class_meta_type()
         or iv.is_convertible(input_infos[i].type))
      {
        inputs.push_back(iv);
      }
      else if(iv.is_pointer_to(input_infos[i].type))
      {
        inputs.push_back(pralin::any_value_ptr(iv).dereference());
      }
      else
      {
        rb_exc_raise(rb_exc_new2(
          rb_eArgError, std::format("Expected type '{}' got '{}' at input '{}' position {}",
                                    input_infos[i].type.get_name(), iv.get_meta_type().get_name(),
                                    input_infos[i].name, i)
                          .c_str()));
        return Qnil;
      }
    }
    pralin::values_ptr_vector outputs;
    for(std::size_t i = 0; i < output_infos.size(); ++i)
    {
      VALUE val_out_i = argv[i + input_infos.size()];
      if(rb_type(val_out_i) == RUBY_T_ARRAY or rb_class_of(val_out_i) == rb_cOutput.klass().value())
      {
        outputs.push_back(output_infos[i].type.create_any_value_pointer());
      }
      else
      {
        pralin::any_value_ptr ov = Rice::detail::cpp_protect(
          [&] { return Rice::detail::From_Ruby<pralin::any_value_ptr>().convert(val_out_i); });
        if(not ov.dereference().is_convertible(output_infos[i].type))
        {
          rb_exc_raise(rb_exc_new2(
            rb_eArgError, std::format("Expected type '{}' got '{}' at output '{}' position {}",
                                      output_infos[i].type.get_name(),
                                      ov.get_meta_type().get_name(), output_infos[i].name, i)
                            .c_str()));
          return Qnil;
        }
        outputs.push_back(ov);
      }
    }
    try
    {
      self_aa->process(inputs, outputs);
    }
    catch(const pralin::exception& ex)
    {
      rb_exc_raise(rb_exc_new_cstr(rb_eStandardError, ex.what()));
      return Qnil;
    }
    // Update outputs
    for(std::size_t i = 0; i < output_infos.size(); ++i)
    {
      VALUE val_out_i = argv[i + input_infos.size()];
      if(rb_type(val_out_i) == RUBY_T_ARRAY)
      {
        Rice::Array val_out_i_array(val_out_i);
        Rice::detail::cpp_protect(
          [&]
          {
            pralin::values::any_value_iterator iterator
              = outputs[i].dereference().to_value<pralin::values::any_value_iterator>();
            while(iterator.has_next())
            {
              val_out_i_array.push(iterator.next());
            }
          });
      }
      else if(rb_class_of(val_out_i) == rb_cOutput.klass().value())
      {
        Rice::detail::From_Ruby<rice_pralin::output*>().convert(val_out_i)->value
          = outputs[i].dereference();
      }
    }
    return Qnil;
  }

} // namespace

extern "C" void Init_pralin_bindings_ruby()
{
  Rice::Module rb_mPralin = Rice::define_module("Pralin");
  Rice::Module rb_mPralinValues = Rice::define_module_under(rb_mPralin, "Values");

  // BEGIN output
  Rice::Data_Type<rice_pralin::output> rb_cOutput
    = Rice::define_class_under<rice_pralin::output>(rb_mPralin, "Output")
        .define_constructor(Rice::Constructor<rice_pralin::output>())
        .define_method("value", [](rice_pralin::output* _self) { return _self->value; });
  // END output
  // BEGIN pralin::abstract_algorithm
  Rice::Data_Type<pralin::abstract_algorithm> rb_cAlgorithm
    = Rice::define_class_under<pralin::abstract_algorithm>(rb_mPralin, "AbstractAlgorithm");
  rb_define_method(rb_cAlgorithm.value(), "process", (RUBY_METHOD_FUNC)&algorithm_process, -1);
  // END pralin::abstract_algorithm
  // BEGIN pralin::algorithm_instance
  Rice::define_class_under<pralin::algorithm_instance, pralin::abstract_algorithm>(rb_mPralin,
                                                                                   "Algorithm");
  // END pralin::algorithm_instance
  // BEGIN pralin::algorithms_registry
  Rice::Data_Type<pralin::algorithms_registry> rb_cAlgorithmsRegistry
    = Rice::define_class_under<pralin::algorithms_registry>(rb_mPralin, "AlgorithmsRegistry")
        .define_singleton_function("load_default_definitions",
                                   &pralin::algorithms_registry::load_default_definitions)
        .define_singleton_function("load_definitions_from",
                                   &pralin::algorithms_registry::load_definitions_from)
        .define_singleton_function(
          "create",
          pralin::overload_r_c<pralin::algorithm_instance, pralin::algorithms_registry,
                               const std::string&, const std::string&,
                               const pralin::any_value_map&>(&pralin::algorithms_registry::create),
          Rice::Arg("module"), Rice::Arg("algorithm"),
          Rice::Arg("parameters") = pralin::any_value_map())
        .define_singleton_function(
          "create_any",
          pralin::overload_r_c<pralin::algorithm_instance, pralin::algorithms_registry,
                               const std::string&, const std::string&,
                               const pralin::any_value_map&>(
            &pralin::algorithms_registry::create_any),
          Rice::Arg("definition"), Rice::Arg("algorithm"),
          Rice::Arg("parameters") = pralin::any_value_map());
  // END pralin::algorithms_registry
  // BEGIN pralin::resources
  Rice::Data_Type<pralin::resources> rb_cResources
    = Rice::define_class_under<pralin::resources>(rb_mPralin, "Resources")
        .define_singleton_function("get", &pralin::resources::get);
  // END pralin::resources
}
