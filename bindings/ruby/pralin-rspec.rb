require 'rspec'
require 'pralin/values'

RSpec.configure do |config|
  config.before(:all) { }
  config.after(:all) { }
  config.before(:suite) do

    if ENV.key?('PRALIN_BUILD_DEFINITIONS_DIRECTORY')
      Pralin::AlgorithmsRegistry.load_definitions_from(ENV['PRALIN_BUILD_DEFINITIONS_DIRECTORY'], false)
    else
      puts "WARNING: Using load installed definitions, behavior might differ from 'make test'. Set PRALIN_BUILD_DEFINITIONS_DIRECTORY environment variable."
      Pralin::AlgorithmsRegistry.load_default_definitions()
    end
  end
  config.after(:suite) { }
end
