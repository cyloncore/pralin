#include "algorithm_registry_monitor_p.h"

using namespace pralin;

abstract_algorithm_registry_monitor* abstract_algorithm_registry_monitor::s_monitor = nullptr;

abstract_algorithm_registry_monitor::~abstract_algorithm_registry_monitor() {}

abstract_algorithm_registry_monitor* abstract_algorithm_registry_monitor::current_monitor()
{
  return s_monitor;
}

void abstract_algorithm_registry_monitor::set_current_monitor(
  abstract_algorithm_registry_monitor* _monitor)
{
  delete s_monitor;
  s_monitor = _monitor;
}
