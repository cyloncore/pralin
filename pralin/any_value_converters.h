#ifndef _PRALIN_ANY_VALUE_CONVERTERS_H_
#define _PRALIN_ANY_VALUE_CONVERTERS_H_

#include "values/scalar"

namespace pralin
{
  template<typename _TTo_, typename _TFrom_, typename _Enable_ = void>
  class any_value_converter;

  template<typename _T_>
  class any_value_converter<_T_, _T_*, std::enable_if_t<not std::is_pointer_v<_T_>>>
  {
  public:
    static _T_ convert_from(const _T_* _value) { return *_value; }
  };
  template<typename _T_>
  class any_value_converter<_T_, std::optional<_T_>>
  {
  public:
    static _T_ convert_from(const std::optional<_T_>& _value) { return *_value; }
  };
  template<typename _T_>
  class any_value_converter<std::optional<_T_>, _T_>
  {
  public:
    static std::optional<_T_> convert_from(const _T_& _value) { return _value; }
  };

  template<typename _T_, typename _Enable_ = void>
  struct any_value_converters_initial_list;
  namespace details
  {
    template<typename _T_, typename _TPack_>
    struct initialize_converters;
    template<typename _T_, typename _T1_, typename... _TOther_>
    struct initialize_converters<_T_, pack<_T1_, _TOther_...>>
    {
      template<template<typename> class _ci_, typename _C_>
      static void fill(_C_* _container)
      {
        (*_container)[typeid(_T1_)] = new _ci_<_T1_>;
        initialize_converters<_T_, pack<_TOther_...>>::template fill<_ci_>(_container);
      }
    };
    template<typename _T_, typename... _TOther_>
    struct initialize_converters<_T_, pack<_T_, _TOther_...>>
    {
      template<template<typename> class _ci_, typename _C_>
      static void fill(_C_* _container)
      {
        initialize_converters<_T_, pack<_TOther_...>>::template fill<_ci_>(_container);
      }
    };
    template<typename _T_>
    struct initialize_converters<_T_, pack<>>
    {
      template<template<typename> class _ci_, typename _C_>
      static void fill(_C_*)
      {
      }
    };

    class any_value_converters_interface
    {
    protected:
      static void* get_data(const std::type_index& _type, const std::function<void*()>& _creator,
                            const std::function<void(void*)>& _deleter);
    };

    template<typename _T_>
    concept has_dereference_converter
      = (not std::is_pointer_v<_T_> and std::is_copy_constructible_v<_T_>);
    template<typename _T_, template<typename> class _ci_, typename _C_>
      requires has_dereference_converter<_T_>
    void initialize_dereference_converter(_C_* _container)
    {
      (*_container)[typeid(_T_*)] = new _ci_<_T_*>;
    };
    template<typename _T_, template<typename> class _ci_, typename _C_>
      requires(not has_dereference_converter<_T_>)
    void initialize_dereference_converter(_C_* _container) {};

    template<typename _T_>
    concept has_optional_converter
      = (not std::is_pointer_v<_T_> and std::is_copy_constructible_v<_T_>);

    template<typename _T_>
    struct initialize_optional_converter
    {
      template<template<typename> class _ci_, typename _C_>
        requires has_optional_converter<_T_>
      static void fill(_C_* _container)
      {
        (*_container)[typeid(std::optional<_T_>)] = new _ci_<std::optional<_T_>>();
      }
      template<template<typename> class _ci_, typename _C_>
        requires(not has_optional_converter<_T_>)
      static void fill(_C_* _container)
      {
      }
    };
    template<typename _T_>
    struct initialize_optional_converter<std::optional<_T_>>
    {
      template<template<typename> class _ci_, typename _C_>
        requires has_optional_converter<_T_>
      static void fill(_C_* _container)
      {
        (*_container)[typeid(_T_)] = new _ci_<_T_>();
      }
      template<template<typename> class _ci_, typename _C_>
        requires(not has_optional_converter<_T_>)
      static void fill(_C_* _container)
      {
      }
    };
  } // namespace details
  /**
   * @ingroup pralin
   * Base class for any value converters, mainly for runtime check on types.
   */
  template<typename _T_>
  class any_value_converters : details::any_value_converters_interface
  {
  private:
    struct abstract_converter
    {
      virtual ~abstract_converter() {}
      virtual _T_ convert_from(const any_value& _value) const = 0;
    };
    template<typename _TFrom_>
      requires(not std::is_same_v<_T_, _TFrom_>)
    struct converter_implementation : public abstract_converter
    {
      _T_ convert_from(const any_value& _value) const final
      {
        return any_value_converter<_T_, _TFrom_>::convert_from(_value.to_value<_TFrom_>());
      }
    };
    struct data
    {
      std::unordered_map<std::type_index, abstract_converter*> converters;
    };
    any_value_converters()
        : d(reinterpret_cast<data*>(get_data(
            typeid(_T_), []() -> void* { return new data; },
            [](void* _ptr) { delete reinterpret_cast<data*>(_ptr); })))
    {
      details::initialize_converters<_T_, typename any_value_converters_initial_list<_T_>::pack_t>::
        template fill<converter_implementation>(&d->converters);
      details::initialize_dereference_converter<_T_, converter_implementation>(&d->converters);
      details::initialize_optional_converter<_T_>::template fill<converter_implementation>(
        &d->converters);
    }
  public:
    ~any_value_converters() {}
    _T_ convert_from(const any_value& _value) const
    {
      auto it = d->converters.find(_value.get_meta_type().get_index());
      if(it != d->converters.end())
      {
        return it->second->convert_from(_value);
      }
      else
      {
        pralin_info("There are {} converters for {}:", d->converters.size(),
                    meta_type::get<_T_>().get_name());
        for(auto it = d->converters.begin(); it != d->converters.end(); ++it)
        {
          pralin_info("From {}: ", details::demangle(it->first.name()));
        }

        throw exception("Cannot convert value {} of type {} to type {}", _value,
                        _value.get_meta_type().get_name(), meta_type::get<_T_>().get_name());
      }
    }
    bool can_convert_from(const meta_type& _meta_type) const
    {
      return d->converters.find(_meta_type.get_index()) != d->converters.end();
    }
    template<typename _TFrom_>
      requires(not std::is_same_v<_T_, _TFrom_>)
    void register_converter()
    {
      d->converters[typeid(_TFrom_)] = new converter_implementation<_TFrom_>();
    }
    static any_value_converters<_T_> get()
    {
      static any_value_converters<_T_> c;
      return c;
    }
  private:
    // private data
    data* d;
  };

  // Scalars
  template<typename _T2_>
  class any_value_converter<values::scalar, _T2_, std::enable_if_t<values::is_scalar_type<_T2_>>>
  {
  public:
    static values::scalar convert_from(const _T2_& _value) { return _value; }
  };
  template<typename _T1_, typename _T2_>
  class any_value_converter<
    _T1_, _T2_, std::enable_if_t<values::is_scalar_type<_T1_> and values::is_scalar_type<_T2_>>>
  {
  public:
    static _T1_ convert_from(const _T2_& _value) { return _value; }
  };
  template<typename _T1_>
  class any_value_converter<_T1_, values::scalar, std::enable_if_t<values::is_scalar_type<_T1_>>>
  {
  public:
    static _T1_ convert_from(const values::scalar& _value) { return _value.cast<_T1_>(); }
  };
  template<>
  struct any_value_converters_initial_list<values::scalar>
      : details::pack<int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t, uint32_t, uint64_t,
                      float, double>
  {
  };
  template<typename _T_>
  struct any_value_converters_initial_list<_T_, std::enable_if_t<values::is_scalar_type<_T_>>>
      : details::pack<values::scalar, int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t,
                      uint32_t, uint64_t, float, double>
  {
  };

  // Vectors
  template<typename _T_>
  class any_value_converter<values_vector, std::vector<_T_>>
  {
  public:
    static values_vector convert_from(const std::vector<_T_>& _value)
    {
      values_vector v;
      std::copy(_value.begin(), _value.end(), std::back_inserter(v));
      return v;
    }
  };
  template<typename _T_>
  class any_value_converter<std::vector<_T_>, values_vector>
  {
  public:
    static std::vector<_T_> convert_from(const values_vector& _value)
    {
      std::vector<_T_> v;
      std::transform(_value.begin(), _value.end(), std::back_inserter(v),
                     [](const any_value& _v) { return _v.to_value<_T_>(); });
      return v;
    }
  };
  template<typename _T_>
  struct any_value_converters_initial_list<std::vector<_T_>> : details::pack<values_vector>
  {
  };

  // unordered_map
  template<typename _T_>
  class any_value_converter<any_value_map, std::unordered_map<std::string, _T_>>
  {
  public:
    static any_value_map convert_from(const std::unordered_map<std::string, _T_>& _value)
    {
      any_value_map v;
      std::transform(_value.begin(), _value.end(), std::inserter(v, v.begin()),
                     [](const std::pair<std::string, any_value>& _v)
                     { return std::make_pair(_v.first, _v.second); });
      return v;
    }
  };
  template<typename _T_>
  class any_value_converter<std::unordered_map<std::string, _T_>, any_value_map>
  {
  public:
    static std::unordered_map<std::string, _T_> convert_from(const any_value_map& _value)
    {
      std::unordered_map<std::string, _T_> v;
      std::transform(_value.begin(), _value.end(), std::inserter(v, v.begin()),
                     [](const std::pair<std::string, any_value>& _v)
                     { return std::make_pair(_v.first, _v.second.to_value<_T_>()); });
      return v;
    }
  };
  template<typename _T_>
  struct any_value_converters_initial_list<std::unordered_map<std::string, _T_>>
      : details::pack<any_value_map>
  {
  };

  // Default
  template<typename _T_, typename _Enabled_>
  struct any_value_converters_initial_list : details::pack<>
  {
  };

} // namespace pralin

#endif
