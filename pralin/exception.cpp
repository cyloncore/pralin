#include "exception"

using namespace pralin;

struct exception::data
{
  std::string str;
};

exception::exception(const char* _text) : d(new data{_text}) {}

exception::exception(const std::string& _text) : d(new data{_text}) {}

exception::exception(exception&& _rhs) : d(_rhs.d) { const_cast<data*&>(_rhs.d) = nullptr; }

exception::exception(const exception& _rhs) : d(new data(*_rhs.d)) {}

exception& exception::operator=(const exception& _rhs)
{
  *d = *_rhs.d;
  return *this;
}

exception::~exception() { delete d; }

const char* exception::what() const throw() { return d->str.c_str(); }
