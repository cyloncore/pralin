#include "documentation_p.h"

using namespace pralin::documentation;
using namespace pralin::documentation::details;

input_output_parameter::input_output_parameter() : input_output_parameter(new data) {}

input_output_parameter::input_output_parameter(data* _d) : d(_d) {}

std::string input_output_parameter::get_name() const { return d->name; }

std::string input_output_parameter::get_type() const { return d->type; }

parameter::parameter() : input_output_parameter(new data) {}

std::string parameter::get_default_value() const { return D()->default_value; }

bool parameter::is_required() const { return D()->is_required; }

const parameter::data* parameter::D() const { return static_cast<const data*>(d.get()); }

parameter::data* parameter::D() { return static_cast<data*>(d.get()); }

algorithm_module::algorithm_module(const std::shared_ptr<data>& _d) : d(_d) {}

std::string algorithm_module::get_name() const { return d->name; }

std::string algorithm_module::get_short_description() const
{
  return d->short_description ? d->short_description : std::string();
}

std::string algorithm_module::get_description() const
{
  return d->description ? d->description : std::string();
}

const algorithm::data* algorithm::D() const { return static_cast<const data*>(d.get()); }

algorithm::data* algorithm::D() { return static_cast<data*>(d.get()); }

algorithm::algorithm() : details::algorithm_module(std::make_shared<data>()) {}

algorithm::algorithm(const algorithm& _rhs) : details::algorithm_module(_rhs.d) {}

algorithm& algorithm::operator=(const algorithm& _rhs)
{
  d = _rhs.d;
  return *this;
}

algorithm::~algorithm() {}

std::string algorithm::get_module() const { return D()->module; }

std::string algorithm::get_fullname() const { return D()->module + "/" + D()->name; }

std::vector<input> algorithm::get_inputs() const { return D()->inputs; }

std::vector<output> algorithm::get_outputs() const { return D()->outputs; }

std::vector<parameter> algorithm::get_parameters() const { return D()->parameters; }

const module::data* module::D() const { return static_cast<const data*>(d.get()); }

module::data* module::D() { return static_cast<data*>(d.get()); }

module::module() :details::algorithm_module(std::make_shared<data>()) {}

module::module(const module& _rhs) :details::algorithm_module(_rhs.d) {}

module& module::operator=(const module& _rhs)
{
  d = _rhs.d;
  return *this;
}

module::~module() {}

std::map<std::string, algorithm> module::get_algorithms() { return D()->algorithms; }

bool module::has_algorithm(const std::string& _name) const
{
  return D()->algorithms.find(_name) != D()->algorithms.end();
}

algorithm module::get_algorithm(const std::string& _name) const
{
  return D()->algorithms.find(_name)->second;
}

algorithm_builder::algorithm_builder(const std::string& _module, const std::string& _algorithm)
{
  m_algorithm.D()->module = _module;
  m_algorithm.D()->name = _algorithm;
}

void algorithm_builder::set_short_description(const char* _short_description)
{
  m_algorithm.D()->short_description = _short_description;
}

void algorithm_builder::set_description(const char* _description)
{
  m_algorithm.D()->description = _description;
}

void algorithm_builder::add_input(const std::string& _name, const std::string& _type)
{
  input i;
  i.d->name = _name;
  i.d->type = _type;
  m_algorithm.D()->inputs.push_back(i);
}

void algorithm_builder::add_output(const std::string& _name, const std::string& _type)
{
  output o;
  o.d->name = _name;
  o.d->type = _type;
  m_algorithm.D()->outputs.push_back(o);
}

void algorithm_builder::add_parameter(const std::string& _name, const std::string& _type,
                                      const std::string& _default_value, bool _is_required)
{
  parameter p;
  p.d->name = _name;
  p.d->type = _type;
  p.D()->default_value = _default_value;
  p.D()->is_required = _is_required;
  m_algorithm.D()->parameters.push_back(p);
}
