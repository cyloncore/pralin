#include <pralin/tests/catch.h>

#include "config.h"

#include <pralin/algorithms_registry>
#include <pralin/compose/computation_graph>
#include <pralin/values/iterator>
#include <pralin/values/scalar>

using computation_graph = pralin::compose::computation_graph;

#include <pralin/implementation>

namespace test_ops
{
  PRALIN_PARAMETERS(test_parameters, PRALIN_PARAMETER(int, 90), integer,
                    PRALIN_PARAMETER(double, 90.0), floating_point,
                    PRALIN_PARAMETER(std::string, "hello"), string);
  class test_parameters
      : public pralin::implementation<
          test_parameters,
          pralin::algorithm_definition<"test_ops/test_parameters",
                                       pralin::output<pralin::values::scalar, "integer">,
                                       pralin::output<pralin::values::scalar, "floating_point">,
                                       pralin::output<std::string, "text">>,
          test_parameters_parameters>
  {
  public:
    test_parameters() {}
    void process(pralin::values::scalar* _integer, pralin::values::scalar* _floating_point,
                 std::string* _string) const final
    {
      *_integer = get_parameters().get_integer();
      *_floating_point = get_parameters().get_floating_point();
      *_string = get_parameters().get_string();
    }
  };
} // namespace test_ops

PRALIN_REGISTER_ALGORITHM(test_ops::test_parameters, "test_ops", "test_parameters",
                          pralin::algorithm_priority::cpu, description("test for parameters"))

TEST_CASE("simple", "[compose]")
{
  pralin::algorithms_registry::load_default_definitions();
  computation_graph cg;
  cg.load_from_file(COMPOSE_TEST_DATA_DIR "/test1.yaml");
  pralin::values_ptr_vector outputs = cg.create_output_values();
  REQUIRE(outputs.size() == 1);
  CHECK(outputs[0].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  cg.process({}, outputs);
  pralin::values::scalar v = outputs[0].dereference().to_value<pralin::values::scalar>();
  CHECK(v.cast<int>() == 3);
}

TEST_CASE("values", "[compose]")
{
  pralin::algorithms_registry::load_default_definitions();
  computation_graph cg;
  cg.load_from_file(COMPOSE_TEST_DATA_DIR "/test2.yaml");
  pralin::values_ptr_vector outputs = cg.create_output_values();
  REQUIRE(outputs.size() == 1);
  CHECK(outputs[0].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  cg.process({}, outputs);
  pralin::values::scalar v = outputs[0].dereference().to_value<pralin::values::scalar>();
  CHECK(v.cast<int>() == 5);
}

TEST_CASE("inputs", "[compose]")
{
  pralin::algorithms_registry::load_default_definitions();
  computation_graph cg;
  cg.load_from_file(COMPOSE_TEST_DATA_DIR "/test3.yaml");
  pralin::values_ptr_vector outputs = cg.create_output_values();
  REQUIRE(outputs.size() == 1);
  CHECK(outputs[0].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  cg.process({pralin::values::scalar(3)}, outputs);
  pralin::values::scalar v = outputs[0].dereference().to_value<pralin::values::scalar>();
  CHECK(v.cast<int>() == 3);
}

TEST_CASE("parallel", "[compose]")
{
  pralin::algorithms_registry::load_default_definitions();
  computation_graph cg;
  cg.load_from_file(COMPOSE_TEST_DATA_DIR "/test4.yaml");
  pralin::values_ptr_vector outputs = cg.create_output_values();
  REQUIRE(outputs.size() == 2);
  CHECK(outputs[0].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  CHECK(outputs[1].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  cg.process({pralin::values::scalar(3), pralin::values::scalar(5)}, outputs);
  pralin::values::scalar v1 = outputs[0].dereference().to_value<pralin::values::scalar>();
  CHECK(v1.cast<int>() == 3);
  pralin::values::scalar v2 = outputs[1].dereference().to_value<pralin::values::scalar>();
  CHECK(v2.cast<int>() == 5);
}

TEST_CASE("parameters", "[compose]")
{
  pralin::algorithms_registry::load_default_definitions();
  computation_graph cg;
  cg.load_from_file(COMPOSE_TEST_DATA_DIR "/test5.yaml");
  pralin::values_ptr_vector outputs = cg.create_output_values();
  REQUIRE(outputs.size() == 3);
  CHECK(outputs[0].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  CHECK(outputs[1].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  CHECK(outputs[2].get_meta_type() == pralin::meta_type::get<std::string>());
  cg.process({}, outputs);
  pralin::values::scalar v1 = outputs[0].dereference().to_value<pralin::values::scalar>();
  CHECK(v1.cast<int>() == 45);
  pralin::values::scalar v2 = outputs[1].dereference().to_value<pralin::values::scalar>();
  CHECK(v2.cast<int>() == 82);
  std::string v3 = outputs[2].dereference().to_value<std::string>();
  CHECK(v3 == "tp[2]");
}

TEST_CASE("conditional second form", "[compose]")
{
  using namespace std::literals::string_literals;
  pralin::algorithms_registry::load_default_definitions();
  computation_graph cg;
  cg.load_from_file(COMPOSE_TEST_DATA_DIR "/test6.yaml");
  pralin::values_ptr_vector outputs = cg.create_output_values();
  REQUIRE(outputs.size() == 1);
  CHECK(outputs[0].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  cg.process({"mode_1"s}, outputs);
  pralin::values::scalar v1 = outputs[0].dereference().to_value<pralin::values::scalar>();
  CHECK(v1.cast<int>() == 3);
  cg.process({"mode_2"s}, outputs);
  v1 = outputs[0].dereference().to_value<pralin::values::scalar>();
  CHECK(v1.cast<int>() == 6);
  cg.process({"mode_3"s}, outputs);
  v1 = outputs[0].dereference().to_value<pralin::values::scalar>();
  CHECK(v1.cast<int>() == 0);
}

TEST_CASE("foreach", "[compose]")
{
  using namespace std::literals::string_literals;
  pralin::algorithms_registry::load_default_definitions();
  computation_graph cg;
  cg.load_from_file(COMPOSE_TEST_DATA_DIR "/test7.yaml");
  pralin::values_ptr_vector outputs = cg.create_output_values();
  REQUIRE(outputs.size() == 1);
  CHECK(outputs[0].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  cg.process({pralin::values::create_iterator(std::vector<int>{1, 3, 4})}, outputs);
  pralin::values::scalar v1 = outputs[0].dereference().to_value<pralin::values::scalar>();
  CHECK(v1.cast<int>() == 8);

  cg.process({std::vector<int>{2, 5, 4}}, outputs);
  v1 = outputs[0].dereference().to_value<pralin::values::scalar>();
  CHECK(v1.cast<int>() == 11);
}

TEST_CASE("list and maps", "[compose]")
{
  using namespace std::literals::string_literals;
  pralin::algorithms_registry::load_default_definitions();
  computation_graph cg;
  cg.load_from_file(COMPOSE_TEST_DATA_DIR "/test8.yaml");
  pralin::values_ptr_vector outputs = cg.create_output_values();
  REQUIRE(outputs.size() == 2);
  CHECK(outputs[0].get_meta_type() == pralin::meta_type::get<pralin::values_vector>());
  CHECK(outputs[1].get_meta_type() == pralin::meta_type::get<pralin::any_value_map>());
  cg.process({}, outputs);
  std::vector<int> v1 = outputs[0].dereference().to_value<std::vector<int>>();
  CHECK(v1 == std::vector<int>{3, 5});
  std::unordered_map<std::string, int> v2
    = outputs[1].dereference().to_value<std::unordered_map<std::string, int>>();
  CHECK(v2["first"] == 3);
  CHECK(v2["second"] == 5);
}

TEST_CASE("graph parameters", "[compose]")
{
  using namespace std::literals::string_literals;
  pralin::algorithms_registry::load_default_definitions();
  computation_graph cg;
  pralin::any_value_map parameters;
  parameters["param_1"] = 4;
  cg.load_from_file(COMPOSE_TEST_DATA_DIR "/test9.yaml", parameters);
  pralin::values_ptr_vector outputs = cg.create_output_values();
  REQUIRE(outputs.size() == 2);
  CHECK(outputs[0].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  CHECK(outputs[1].get_meta_type() == pralin::meta_type::get<pralin::values::scalar>());
  cg.process({}, outputs);
  CHECK(outputs[0].dereference().to_value<int>() == 3);
  CHECK(outputs[1].dereference().to_value<int>() == 6);
}

TEST_CASE("templtae", "[compose]")
{
  using namespace std::literals::string_literals;
  pralin::algorithms_registry::load_default_definitions();
  computation_graph cg;
  pralin::any_value_map parameters;
  parameters["param_0"] = std::string("z: ");
  cg.load_from_file(COMPOSE_TEST_DATA_DIR "/test10.yaml", parameters);
  pralin::values_ptr_vector outputs = cg.create_output_values();
  REQUIRE(outputs.size() == 1);
  CHECK(outputs[0].get_meta_type() == pralin::meta_type::get<std::string>());
  cg.process({pralin::values::create_iterator(std::vector<int>{1, 3, 4})}, outputs);
  std::string v1 = outputs[0].dereference().to_value<std::string>();
  CHECK(v1 == "z: 1->1\nz: 3->4\nz: 4->8\n");
}