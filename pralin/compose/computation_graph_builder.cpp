#include "computation_graph_builder"

#include <functional>

#include <pralin/config_p.h>
#include <pralin/exception>
#include <pralin/logging>

#include "computation_nodes/conditional.h"
#include "computation_nodes/for_each.h"
#include "computation_nodes/operation.h"
#include "computation_nodes/repeat_while.h"
#include "computation_nodes/sequential.h"
#include "computation_nodes/string_template.h"
#include "expression"

#ifdef PRALIN_HAS_TASKS_MACHINE
#include "computation_nodes/parallel.h"
#endif

using namespace pralin::compose;

struct computation_graph_builder::data
{
  std::list<std::function<void(computation_node*)>> containers;
  computation_node* root = nullptr;

  void add_node(computation_node* _node)
  {
    if(root)
    {
      if(containers.empty())
        throw exception("root node is set, but it has been closed for further addition");
      containers.back()(_node);
    }
    else
    {
      root = _node;
    }
  }
};

computation_graph_builder::computation_graph_builder() : d(new data) {}

computation_graph_builder::~computation_graph_builder() {}

computation_node* computation_graph_builder::finish_graph()
{
  if(not d->root)
  {
    throw exception("Null graph.");
  }
  if(not d->containers.empty())
  {
    throw exception("Incomplete graph.");
  }
  return d->root;
}

void computation_graph_builder::start_sequential()
{
  computation_nodes::sequential* seq = new computation_nodes::sequential;
  d->add_node(seq);
  d->containers.push_back([seq](computation_node* n) { seq->add_node(n); });
}

void computation_graph_builder::finish_sequential() { d->containers.pop_back(); }

void computation_graph_builder::start_parallel()
{
#ifdef PRALIN_HAS_TASKS_MACHINE
  computation_nodes::parallel* par = new computation_nodes::parallel;
#else
  pralin_warning("pralin was built without tasks_machine, parallel blocks are run sequentially.");
  computation_nodes::sequential* par = new computation_nodes::sequential;
#endif
  d->add_node(par);
  d->containers.push_back([par](computation_node* n) { par->add_node(n); });
}

void computation_graph_builder::finish_parallel() { d->containers.pop_back(); }

void computation_graph_builder::start_conditional(const expression& _cond_var)
{
  computation_nodes::conditional* cond = new computation_nodes::conditional{_cond_var};
  d->add_node(cond);
  d->containers.push_back([cond](computation_node* n) { cond->add_node(n); });
}

void computation_graph_builder::start_conditional(const expression& _on_var,
                                                  const expression& _equals_var)
{
  computation_nodes::conditional* cond = new computation_nodes::conditional{_on_var, _equals_var};
  d->add_node(cond);
  d->containers.push_back([cond](computation_node* n) { cond->add_node(n); });
}

void computation_graph_builder::finish_conditional() { d->containers.pop_back(); }

void computation_graph_builder::start_repeat_while(const expression& _cond_var)
{
  computation_nodes::repeat_while* seq = new computation_nodes::repeat_while{_cond_var};
  d->add_node(seq);
  d->containers.push_back([seq](computation_node* n) { seq->add_node(n); });
}

void computation_graph_builder::finish_repeat_while() { d->containers.pop_back(); }

void computation_graph_builder::start_for_each(const std::string& _value_var,
                                               const expression& _iteartor_var)
{
  computation_nodes::for_each* seq = new computation_nodes::for_each{_value_var, _iteartor_var};
  d->add_node(seq);
  d->containers.push_back([seq](computation_node* n) { seq->add_node(n); });
}

void computation_graph_builder::finish_for_each() { d->containers.pop_back(); }

void computation_graph_builder::create_string_template(const std::string& _id,
                                                       const std::string& _template,
                                                       const std::vector<expression>& _expressions)
{
  d->add_node(new computation_nodes::string_template(_id, _template, _expressions));
}

void computation_graph_builder::create_operation(const std::string& _id,
                                                 const algorithm_instance& _algorithm,
                                                 const std::vector<expression>& _inputs)
{
  d->add_node(new computation_nodes::operation(_id, _algorithm, _inputs));
}
