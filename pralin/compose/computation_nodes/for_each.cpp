#include "for_each.h"

#include <pralin/values/iterator>

#include <pralin/compose/expression>

#include "sequential_executor.h"
#include "utils.h"

using namespace pralin::compose::computation_nodes;

struct for_each::data
{
  std::string value_state;
  expression iterator_state;
  std::vector<computation_node*> nodes;
};

for_each::for_each(const std::string& _value_state, const expression& _iterator_state) : d(new data)
{
  d->value_state = _value_state;
  d->iterator_state = _iterator_state;
}

for_each::~for_each() { delete_all(d->nodes); }

void for_each::process(const execution_context& _context, const state& _input_states,
                       state* _output_states) const
{
  sequential_executor se{d->nodes, _context, _input_states, _output_states};

  values::any_value_iterator it
    = d->iterator_state.get_value(_input_states, "for_each").to_value<values::any_value_iterator>();

  while(it.has_next())
  {
    se.current_state[d->value_state] = it.next();
    se.execute_once();
  }
  se.finalise();
}

void for_each::add_node(::pralin::compose::computation_node* _node) { d->nodes.push_back(_node); }
