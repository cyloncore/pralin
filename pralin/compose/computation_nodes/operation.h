#include <pralin/compose/computation_node>
#include <pralin/implementation>
#include <pralin/parameters>

namespace pralin::compose::computation_nodes
{
  /**
   * @ingroup pralin_compose
   * Execute an algorithm and update the state.
   */
  class operation : public implementation<operation, ::pralin::compose::computation_node>
  {
    friend class pralin::compose::computation_graph_builder;
  public:
    /**
     * Takes ownership of the \ref _algorithm.
     */
    operation(const std::string& _id, const algorithm_instance& _algorithm,
              const std::vector<expression>& _inputs);
    ~operation();
    void process(const execution_context& _context, const state& _input_states,
                 state* _output_states) const final;
  private:
    struct data;
    data* d;
  };
} // namespace pralin::compose::computation_nodes
