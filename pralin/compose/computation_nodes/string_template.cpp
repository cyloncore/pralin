#include "string_template.h"

#include <pralin/compose/execution_context>
#include <pralin/compose/expression>

using any_value = pralin::any_value;

using namespace pralin::compose::computation_nodes;

struct string_template::data
{
  std::string id, template_str;
  std::vector<expression> expressions;
};

string_template::string_template(const std::string& _id, const std::string& _template,
                                 const std::vector<expression>& _expressions)
    : d(new data{_id, _template, _expressions})
{
}

string_template::~string_template() { delete d; }

namespace
{
  template<typename F, size_t... Is>
  auto gen_tuple_impl(F func, std::index_sequence<Is...>)
  {
    return std::make_tuple(func(Is)...);
  }

  template<size_t N, typename F>
  auto gen_tuple(F func)
  {
    return gen_tuple_impl(func, std::make_index_sequence<N>{});
  }

  constexpr std::size_t MAX_N = 100;
  template<std::size_t _N_>
  std::string format_template(const std::string& _format_str,
                              const std::vector<any_value>& _input_values)
  {
    if(_N_ == _input_values.size())
    {
      auto q = [](auto&&... args) { return std::make_format_args(args...); };
      return std::vformat(
        _format_str,
        std::apply(q, gen_tuple<_N_>([&_input_values](std::size_t idx)
                                     { return pralin::any_value(_input_values[idx]); })));
    }
    else
    {
      return format_template<_N_ + 1>(_format_str, _input_values);
    }
  }
  template<>
  std::string format_template<0>(const std::string& _format_str,
                                 const std::vector<any_value>& _input_values)
  {
    if(_input_values.empty())
    {
      return _format_str;
    }
    else
    {
      return format_template<1>(_format_str, _input_values);
      throw pralin::exception("Unsupported number of arguments: ", _input_values);
    }
  }
  template<>
  std::string format_template<MAX_N>(const std::string& _format_str,
                                     const std::vector<any_value>& _input_values)
  {
    throw pralin::exception("Unsupported number of arguments: {}", _input_values.size());
  }

} // namespace

void string_template::process(const execution_context& _context, const state& _input_states,
                              state* _output_states) const
{
  std::vector<any_value> input_values;
  for(std::size_t idx = 0; idx < d->expressions.size(); ++idx)
  {
    input_values.push_back(d->expressions[idx].get_value(_input_states, d->id));
  }
  std::string r;
  try
  {
    r = format_template<0>(d->template_str, input_values);
  }
  catch(const std::format_error& _fe)
  {
    throw pralin::exception("Failed to format '{}' with '{}', error is: '{}'", d->template_str,
                            input_values, _fe.what());
  }

  std::vector<std::string> mappings
    = _context.get_states_mappings()[std::format("{}[{}]", d->id, 0)];
  for(const std::string& v : mappings)
  {
    (*_output_states)[v] = r;
  }
}