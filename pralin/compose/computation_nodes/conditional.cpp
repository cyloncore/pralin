#include "conditional.h"

#include <pralin/compose/expression>

#include "sequential_executor.h"
#include "utils.h"

using namespace pralin::compose::computation_nodes;

struct conditional::data
{
  expression cond_state, on_state, equals_state;
  std::vector<computation_node*> nodes;
};

conditional::conditional(const expression& _cond_state) : d(new data)
{
  d->cond_state = _cond_state;
}

conditional::conditional(const expression& _on_state, const expression& _equals_state) : d(new data)
{
  d->on_state = _on_state;
  d->equals_state = _equals_state;
}

conditional::~conditional() { delete_all(d->nodes); }

void conditional::process(const execution_context& _context, const state& _input_states,
                          state* _output_states) const
{
  bool run = false;
  if(d->cond_state.is_valid())
  {
    run = utils::evaluate_to_boolean(d->cond_state.get_value(_input_states, "conditional/cond"));
  }
  else
  {
    run = (d->on_state.get_value(_input_states, "conditonal/on")
           == d->equals_state.get_value(_input_states, "conditional/equals"));
  }

  if(run)
  {
    sequential_executor se{d->nodes, _context, _input_states, _output_states};
    se.execute_once();
    se.finalise();
  }
}

void conditional::add_node(::pralin::compose::computation_node* _node)
{
  d->nodes.push_back(_node);
}
