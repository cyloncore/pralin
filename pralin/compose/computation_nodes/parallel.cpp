#include "parallel.h"

#include <mutex>

#include <tasks_machine/queue>
#include <tasks_machine/queued_task>
#include <tasks_machine/queued_task_status>

#include <pralin/compose/execution_context>

using namespace pralin::compose::computation_nodes;

struct parallel::data
{
  std::vector<computation_node*> nodes;

  static tasks_machine::queue* get_queue()
  {
    static tasks_machine::queue q;
    return &q;
  }
};

parallel::parallel() : d(new data) {}

parallel::~parallel() { delete_all(d->nodes); }

void parallel::process(const execution_context& _context, const state& _input_states,
                       state* _output_states) const
{
  execution_context context = _context.context_without_full_state();
  std::mutex m;
  std::vector<tasks_machine::queued_task_status> qts;
  for(computation_node* n : d->nodes)
  {
    qts.push_back(data::get_queue()->enqueue_task(tasks_machine::queued_task::create_single_run(
      [n, &context, &_input_states, &_output_states, &m]()
      {
        state update;
        n->process(context, _input_states, &update);
        std::unique_lock<std::mutex> l{m};
        for(auto it = update.begin(); it != update.end(); ++it)
        {
          (*_output_states)[it->first] = it->second;
        }
        return true;
      },
      [] { return true; })));
  }
  for(const tasks_machine::queued_task_status& status : qts)
  {
    status.wait_for_finished();
  }
}

void parallel::add_node(::pralin::compose::computation_node* _node) { d->nodes.push_back(_node); }
