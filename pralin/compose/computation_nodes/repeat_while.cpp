#include "repeat_while.h"

#include <pralin/compose/expression>

#include "sequential_executor.h"
#include "utils.h"

using namespace pralin::compose::computation_nodes;

struct repeat_while::data
{
  expression cond_state;
  std::vector<computation_node*> nodes;
};

repeat_while::repeat_while(const expression& _cond_state) : d(new data)
{
  d->cond_state = _cond_state;
}

repeat_while::~repeat_while() { delete_all(d->nodes); }

void repeat_while::process(const execution_context& _context, const state& _input_states,
                           state* _output_states) const
{
  sequential_executor se{d->nodes, _context, _input_states, _output_states};

  while((
    [&se, this]()
    {
      return utils::evaluate_to_boolean(d->cond_state.get_value(se.current_state, "repeat_while"));
    })())
  {
    se.execute_once();
  }
  se.finalise();
}

void repeat_while::add_node(::pralin::compose::computation_node* _node)
{
  d->nodes.push_back(_node);
}
