#include <pralin/compose/computation_node>
#include <pralin/implementation>
#include <pralin/parameters>

namespace pralin::compose::computation_nodes
{
  /**
   * @ingroup pralin_compose
   * Execute statements in loop as long as a given condition is true (invalid is considered false).
   */
  class conditional : public implementation<conditional, ::pralin::compose::computation_node>
  {
    friend class pralin::compose::computation_graph_builder;
  public:
    conditional(const expression& _cond_sate);
    conditional(const expression& _on_state, const expression& _equals_state);
    ~conditional();
    void process(const execution_context& _context, const state& _input_states,
                 state* _output_states) const final;
  private:
    void add_node(::pralin::compose::computation_node* _node);
    struct data;
    data* d;
  };
} // namespace pralin::compose::computation_nodes
