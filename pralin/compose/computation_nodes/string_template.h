#include <pralin/compose/computation_node>
#include <pralin/implementation>
#include <pralin/parameters>

namespace pralin::compose::computation_nodes
{
  /**
   * @ingroup pralin_compose
   * Allow to create string using template.
   */
  class string_template
      : public implementation<string_template, ::pralin::compose::computation_node>
  {
    friend class pralin::compose::computation_graph_builder;
  public:
    string_template(const std::string& _id, const std::string& _template,
                    const std::vector<expression>& _expressions);
    ~string_template();
    void process(const execution_context& _context, const state& _input_states,
                 state* _output_states) const final;
  private:
    struct data;
    data* d;
  };
} // namespace pralin::compose::computation_nodes