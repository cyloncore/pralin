#include <pralin/compose/forward>

#include <pralin/compose/execution_context>

namespace pralin::compose::computation_nodes
{
  struct sequential_executor
  {
    void execute_once()
    {
      for(computation_node* n : nodes)
      {
        if(context.can_output_full_state())
        {
          n->process(context, current_state, &current_state);
        }
        else
        {
          state update;
          n->process(context, current_state, &update);
          for(auto it = update.begin(); it != update.end(); ++it)
          {
            current_state[it->first] = it->second;
            (*_output_states)[it->first] = it->second;
          }
        }
      }
    }
    void finalise()
    {
      if(context.can_output_full_state())
      {
        std::swap(*_output_states, current_state);
      }
    }
    const std::vector<computation_node*>& nodes;
    const execution_context& context;
    state current_state;
    state* _output_states;
  };

} // namespace pralin::compose::computation_nodes
