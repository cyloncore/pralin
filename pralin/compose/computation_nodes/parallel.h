#include <pralin/compose/computation_node>
#include <pralin/implementation>
#include <pralin/parameters>

namespace pralin::compose::computation_nodes
{
  /**
   * @ingroup pralin_compose
   * Execute a sequence of operations, respecting the order.
   */
  class parallel : public implementation<parallel, ::pralin::compose::computation_node>
  {
    friend class pralin::compose::computation_graph_builder;
  public:
    parallel();
    ~parallel();
    void process(const execution_context& _context, const state& _input_states,
                 state* _output_states) const final;
  private:
    void add_node(::pralin::compose::computation_node* _node);
    struct data;
    data* d;
  };
} // namespace pralin::compose::computation_nodes
