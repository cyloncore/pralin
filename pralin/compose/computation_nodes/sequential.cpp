#include "sequential.h"

#include "sequential_executor.h"

using namespace pralin::compose::computation_nodes;

struct sequential::data
{
  std::vector<computation_node*> nodes;
};

sequential::sequential() : d(new data) {}

sequential::~sequential() { delete_all(d->nodes); }

void sequential::process(const execution_context& _context, const state& _input_states,
                         state* _output_states) const
{
  sequential_executor se{d->nodes, _context, _input_states, _output_states};
  se.execute_once();
  se.finalise();
}

void sequential::add_node(::pralin::compose::computation_node* _node) { d->nodes.push_back(_node); }
