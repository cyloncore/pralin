#include <pralin/exception>
#include <pralin/values/scalar>

namespace pralin::compose::computation_nodes::utils
{
  inline bool evaluate_to_boolean(const any_value& _value)
  {
    if(_value.is_of_type<bool>())
    {
      return _value.to_value<bool>();
    }
    else if(_value.get_meta_type().is_scalar())
    {
      throw "wip";
    }
    else if(_value.is_valid())
    {
      throw exception("Invalid value, expected bool.");
    }
    else
    {
      return false;
    }
  }
} // namespace pralin::compose::computation_nodes::utils