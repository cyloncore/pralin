#include <pralin/compose/computation_node>
#include <pralin/implementation>
#include <pralin/parameters>

namespace pralin::compose::computation_nodes
{
  /**
   * @ingroup pralin_compose
   * For each element of an iterator.
   */
  class for_each : public implementation<for_each, ::pralin::compose::computation_node>
  {
    friend class pralin::compose::computation_graph_builder;
  public:
    for_each(const std::string& _value_state, const expression& _cond_sate);
    ~for_each();
    void process(const execution_context& _context, const state& _input_states,
                 state* _output_states) const final;
  private:
    void add_node(::pralin::compose::computation_node* _node);
    struct data;
    data* d;
  };
} // namespace pralin::compose::computation_nodes
