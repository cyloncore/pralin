#include "operation.h"

#include <pralin/algorithm_instance>
#include <pralin/exception>
#include <pralin/values/scalar>

#include <pralin/compose/execution_context>
#include <pralin/compose/expression>

using namespace pralin::compose::computation_nodes;

struct operation::data
{
  std::string id;
  std::vector<expression> inputs;
  algorithm_instance algorithm;
};

operation::operation(const std::string& _id, const algorithm_instance& _algorithm,
                     const std::vector<expression>& _inputs)
    : d(new data{_id, _inputs, _algorithm})
{
}

operation::~operation() { delete d; }

void operation::process(const execution_context& _ec, const state& _input_states,
                        state* _output_states) const
{
  std::vector<any_value> input_values;
  for(std::size_t idx = 0; idx < d->inputs.size(); ++idx)
  {
    input_values.push_back(d->inputs[idx].get_value(_input_states, d->id));
  }
  for(std::size_t idx = input_values.size(); idx < d->algorithm.get_input_infos().size(); ++idx)
  {
    input_values.push_back(any_value{});
  }
  for(std::size_t idx = 0; idx < d->algorithm.get_input_infos().size(); ++idx)
  {
    if(not input_values[idx].is_valid()
       and not d->algorithm.get_input_infos()[idx].type.is_optional())
    {
      throw exception("Input {} to algorithm {} is null.", idx, d->id);
    }
  }
  values_ptr_vector output_values = d->algorithm.create_output_values();
  d->algorithm.process(input_values, output_values);
  for(std::size_t idx = 0; idx < output_values.size(); ++idx)
  {
    any_value ov = output_values[idx].dereference();
    std::vector<std::string> mappings
      = _ec.get_states_mappings()[std::format("{}[{}]", d->id, idx)];
    for(const std::string& v : mappings)
    {
      (*_output_states)[v] = ov;
    }
  }
}
