#include "execution_context"

using namespace pralin::compose;

struct execution_context::data
{
  compose::states_mappings sm;
  bool can_output_full_state = true;
};

execution_context::execution_context(const compose::states_mappings& _states_mappings) : d(new data)
{
  d->sm = _states_mappings;
}

execution_context::execution_context() = default;
execution_context::execution_context(const execution_context& _rhs) = default;
execution_context& execution_context::operator=(const execution_context& _rhs) = default;

execution_context::~execution_context() {}

states_mappings execution_context::get_states_mappings() const { return d->sm; }

bool execution_context::can_output_full_state() const { return d->can_output_full_state; }

execution_context execution_context::context_without_full_state() const
{
  if(d->can_output_full_state)
  {
    execution_context ec(d->sm);
    ec.d->can_output_full_state = false;
    return ec;
  }
  else
  {
    return *this;
  }
}
