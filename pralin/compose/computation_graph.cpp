#include "computation_graph"

#include <fstream>
#include <regex>
#include <sstream>

#include <yaml-cpp/yaml.h>

#include <pralin/config>

#include <pralin/algorithm_instance>
#include <pralin/algorithms_registry>
#include <pralin/exception>
#include <pralin/values/iterator>
#include <pralin/values/scalar>

#include "computation_graph_builder"
#include "computation_node"
#include "execution_context"
#include "expression"

using namespace pralin::compose;

struct computation_graph::data
{
  any_value_map inital_parameters;
  any_value_map current_parameters;

  computation_node* node = nullptr;
  std::vector<std::string> inputs;
  std::vector<expression> outputs;
  std::vector<std::string> outputs_labels;
  compose::states_mappings states_mappings;
  std::unordered_map<std::string, any_value> states_init;
  std::unordered_map<std::string, meta_type> states_types;
  std::vector<input_info> input_infos;
  std::vector<output_info> output_infos;

  void check_is_map(const YAML::Node& _node, const char* _context);
  void check_is_sequence(const YAML::Node& _node, const char* _context);
  void check_size(const YAML::Node& _node, std::size_t _size, const char* _context);
  YAML::Node get_child(const YAML::Node& _node, const char* _name, const char* _context);
  void read_node(computation_graph_builder* _builder, const YAML::Node& _node);
  void read_process(computation_graph_builder* _builder, const YAML::Node& _node);
  expression read_expression(const YAML::Node& _node,
                             const std::optional<meta_type>& _meta_type = std::nullopt,
                             const std::optional<std::string>& _variable_name = std::nullopt);
  void read_expression_scalar(const YAML::Node& _node, const std::string& _variable);
  /**
   * Translate a YAML node into a \ref pralin::any_value. Used mainly from the parameters and can
   * only depend on the \ref _node argument.
   */
  static pralin::any_value read_scalar(const YAML::Node& _node);
  std::string create_state(const std::optional<std::string>& _variable_name = std::nullopt,
                           const std::optional<meta_type>& _meta_type = std::nullopt);
  void check_or_set_states_type(const expression& _expression,
                                const expression& _expression_source_type);
  void check_or_set_states_type(const expression& _expression, const meta_type& _meta_type);
  void check_or_set_states_type(const std::string& _variable, const meta_type& _meta_type);
};

namespace
{
  static std::regex is_integer_regex{"^\\d+$"};
  static std::regex is_double_regex{"^[-+]?\\d*\\.?\\d+([eE][-+]?\\d+)?$"};
  static std::regex is_out_ref_regex{"^(\\w+)\\[\\d+\\]"};
} // namespace

void computation_graph::data::check_is_map(const YAML::Node& _node, const char* _context)
{
  if(_node.IsMap())
    return;
  throw exception("Invalid compose definition, expected map{}", _context);
}

void computation_graph::data::check_is_sequence(const YAML::Node& _node, const char* _context)
{
  if(_node.IsSequence())
    return;
  throw exception("Invalid compose definition, expected sequence got {}", _context);
}

void computation_graph::data::check_size(const YAML::Node& _node, std::size_t _size,
                                         const char* _context)
{
  if(_node.size() != _size)
  {
    throw exception("Invalid size, got {} expected {}{}", _node.size(), _size, _context);
  }
}

YAML::Node computation_graph::data::get_child(const YAML::Node& _node, const char* _name,
                                              const char* _context)
{
  YAML::Node n = _node[_name];
  if(n.IsDefined())
  {
    return n;
  }
  else
  {
    throw exception("Invalid compose definition, missing key: {}{}", _name, _context);
  }
}

void computation_graph::data::read_expression_scalar(const YAML::Node& _node,
                                                     const std::string& _variable)
{
  std::string scalar = _node.Scalar();
  if(_node.Tag() == "!param" or _node.Tag() == "tag:cyloncore.com/pralin,2023:param")
  {
    auto it_param = current_parameters.find(scalar);
    if(it_param == current_parameters.end())
    {
      throw pralin::exception("Unknown parameter '{}'!", scalar);
    }
    states_init[_variable] = it_param->second;
    check_or_set_states_type(_variable, it_param->second.get_meta_type());
  }
  else if(_node.Tag() == "tag:yaml.org,2002:str")
  {
    states_init[_variable] = scalar;
    check_or_set_states_type(_variable, meta_type::get<std::string>());
  }
  else if(scalar == "true")
  {
    states_init[_variable] = true;
    check_or_set_states_type(_variable, meta_type::get<bool>());
  }
  else if(scalar == "false")
  {
    states_init[_variable] = false;
    check_or_set_states_type(_variable, meta_type::get<bool>());
  }
  else if(std::regex_search(scalar, is_integer_regex))
  {
    states_init[_variable] = pralin::values::scalar(std::stoi(scalar));
    check_or_set_states_type(_variable, meta_type::get<pralin::values::scalar>());
  }
  else if(std::regex_search(scalar, is_double_regex))
  {
    states_init[_variable] = pralin::values::scalar(std::stod(scalar));
    check_or_set_states_type(_variable, meta_type::get<pralin::values::scalar>());
  }
  else if(std::regex_search(scalar, is_out_ref_regex))
  {
    states_mappings[scalar].push_back(_variable);
  }
  else if(std::find(inputs.begin(), inputs.end(), scalar) != inputs.end())
  {
    states_mappings[scalar].push_back(_variable);
    check_or_set_states_type(scalar, states_types[_variable]);
  }
  else if(_node.Tag() == "!")
  {
    states_init[_variable] = scalar;
    check_or_set_states_type(_variable, meta_type::get<std::string>());
  }
  else
  {
    pralin_debug_vn(_node.Tag());
    throw exception(
      "Invalid expression '{}', if it is supposed to be a string, it should be quoted or marked "
      "with '!!str', if it is supposed to be a parameter, it should be marked with '!param'.",
      scalar);
  }
}

pralin::any_value computation_graph::data::read_scalar(const YAML::Node& _node)
{
  switch(_node.Type())
  {
  case YAML::NodeType::Undefined:
  case YAML::NodeType::Null:
    return pralin::any_value();
  case YAML::NodeType::Map:
  {
    any_value_map expressions;
    for(auto it = _node.begin(); it != _node.end(); ++it)
    {
      expressions[it->first.Scalar()] = read_scalar(it->second);
    }
    return expressions;
  }
  case YAML::NodeType::Sequence:
  {
    values_vector expressions;
    for(std::size_t i = 0; i < _node.size(); ++i)
    {
      expressions.push_back(read_scalar(_node[i]));
    }
    return expressions;
  }
  case YAML::NodeType::Scalar:
  {
    std::string scalar = _node.Scalar();
    if(_node.Tag() == "tag:yaml.org,2002:str" or _node.Tag() == "!")
    {
      return scalar;
    }
    else if(scalar == "true")
    {
      return true;
    }
    else if(scalar == "false")
    {
      return false;
    }
    else if(scalar == "null")
    {
      return any_value();
    }
    else if(std::regex_search(scalar, is_integer_regex))
    {
      return std::stoi(scalar);
    }
    else if(std::regex_search(scalar, is_double_regex))
    {
      return std::stod(scalar);
    }
    else
    {
      throw exception("Invalid expression '{}', if it is supposed to be a string, it should be "
                      "quoted or marked with !!str.",
                      scalar);
    }
  }
  }
  throw exception("Internal error: unhandled YAML node in read_sclar.");
}

expression
  computation_graph::data::read_expression(const YAML::Node& _node,
                                           const std::optional<meta_type>& _meta_type,
                                           const std::optional<std::string>& _variable_name)
{
  switch(_node.Type())
  {
  case YAML::NodeType::Map:
    if(_node.Tag() == "tag:yaml.org,2002:map")
    {
      std::unordered_map<std::string, expression> expressions;
      std::string var_n
        = create_state(_variable_name, pralin::meta_type::get<pralin::any_value_map>());
      for(auto it = _node.begin(); it != _node.end(); ++it)
      {
        expressions[it->first.Scalar()] = read_expression(
          it->second, std::nullopt, std::format("{}[{}]", var_n, it->first.Scalar()));
      }
      return expressions;
    }
    else
    {
      throw exception(
        "Map is not a valid expression, did you forget to explicitly mark it with '!!map'?");
    }
  case YAML::NodeType::Sequence:
  {
    if(_node.Tag() == "tag:yaml.org,2002:seq")
    {
      std::vector<expression> expressions;
      std::string vn
        = create_state(_variable_name, pralin::meta_type::get<pralin::values_vector>());
      for(std::size_t i = 0; i < _node.size(); ++i)
      {
        expressions.push_back(
          read_expression(_node[i], std::nullopt, std::format("{}[{}]", vn, i)));
      }
      return expressions;
    }
    else
    {
      if(_node.size() == 1)
      {
        return read_expression(_node[0], _meta_type);
      }
      std::string var_n = create_state(_variable_name, _meta_type);
      for(std::size_t i = 0; i < _node.size(); ++i)
      {
        if(_node[i].Scalar() == "default")
        {
          states_init[var_n] = _meta_type->create_any_value();
        }
        else
        {
          read_expression_scalar(_node[i], var_n);
        }
      }
      return var_n;
    }
  }
  break;
  case YAML::NodeType::Undefined:
  case YAML::NodeType::Null:
    throw pralin::exception("Invalid null expression.");
  case YAML::NodeType::Scalar:
    if(_node.Tag() != "tag:yaml.org,2002:str"
       and std::regex_match(_node.Scalar(), is_out_ref_regex))
    {
      return _node.Scalar();
    }
    else
    {
      std::string var_n = create_state(_variable_name, _meta_type);
      read_expression_scalar(_node, var_n);
      return var_n;
    }
    break;
  }
  throw exception("Unknown YAML node type.");
}

std::string computation_graph::data::create_state(const std::optional<std::string>& _variable_name,
                                                  const std::optional<meta_type>& _meta_type)
{
  if(_variable_name)
  {
    if(_meta_type)
    {
      check_or_set_states_type(*_variable_name, *_meta_type);
    }
    return *_variable_name;
  }
  else
  {
    std::string name = std::format("__state_{}", states_init.size());
    states_init[name] = any_value();
    states_mappings[name].push_back(name);
    if(_meta_type)
    {
      states_types[name] = *_meta_type;
    }
    return name;
  }
}

void computation_graph::data::check_or_set_states_type(const std::string& _variable,
                                                       const meta_type& _meta_type)
{
  auto it = states_types.find(_variable);
  if(it == states_types.end() or not it->second.is_valid()
     or it->second == meta_type::get<any_value>())
  {
    states_types[_variable] = _meta_type;
  }
  else if(_meta_type == it->second or _meta_type.can_convert_from(it->second))
  {
    // all good, pass
  }
  else if(it->second.can_convert_from(_meta_type))
  {
    // update to more restrictive
    it->second = _meta_type;
  }
  else if(_meta_type != meta_type::get<any_value>())
  {
    throw exception("State {} has different type {} != {}", _variable, it->second.get_name(),
                    _meta_type.get_name());
  }
}

void computation_graph::data::check_or_set_states_type(const expression& _expression,
                                                       const meta_type& _meta_type)
{
  if(_expression.get_type() == expression::type::atomic)
  {
    check_or_set_states_type(_expression.get_state_name(), _meta_type);
  }
}

void computation_graph::data::check_or_set_states_type(const expression& _expression,
                                                       const expression& _expression_source)
{
  if(_expression_source.get_type() == expression::type::atomic)
  {
    meta_type mt = states_types[_expression_source.get_state_name()];
    if(mt.is_valid())
    {
      check_or_set_states_type(_expression, mt);
    }
  }
}

void computation_graph::data::read_process(computation_graph_builder* _builder,
                                           const YAML::Node& _node)
{
  check_is_sequence(_node, ", while reading process.");
  for(std::size_t idx = 0; idx < _node.size(); ++idx)
  {
    read_node(_builder, _node[idx]);
  }
}

void computation_graph::data::read_node(computation_graph_builder* _builder,
                                        const YAML::Node& _node)
{
  check_is_map(_node, ", while reading atomic node");
  check_size(_node, 1, ", while reading atomic node");
  std::string type = _node.begin()->first.as<std::string>();
  YAML::Node main = _node.begin()->second;
  if(main.Type() != YAML::NodeType::Map)
  {
    throw pralin::exception("Node definition should be a directionary.");
  }
  if(type == "sequential")
  {
    YAML::Node process_node = get_child(main, "process", ", while reading 'sequential' node.");
    _builder->start_sequential();
    read_process(_builder, process_node);
    _builder->finish_sequential();
  }
  else if(type == "parallel")
  {
    YAML::Node process_node = get_child(main, "process", ", while reading 'sequential' node.");
    _builder->start_parallel();
    read_process(_builder, process_node);
    _builder->finish_parallel();
  }
  else if(type == "repeat_while")
  {
    YAML::Node process_node = get_child(main, "process", ", while reading 'repeat_while' node.");
    YAML::Node cond_node = get_child(main, "condition", ", while reading 'repeat_while' node.");
    _builder->start_repeat_while(read_expression(cond_node));
    read_process(_builder, process_node);
    _builder->finish_repeat_while();
  }
  else if(type == "for_each")
  {
    YAML::Node process_node = get_child(main, "process", ", while reading 'for_each' node.");
    YAML::Node variable_node = get_child(main, "id", ", while reading 'for_each' node.");
    YAML::Node iterator_node = get_child(main, "iterator", ", while reading 'for_each' node.");
    expression iterator_state = read_expression(iterator_node);
    check_or_set_states_type(iterator_state, meta_type::get<values::any_value_iterator>());
    _builder->start_for_each(variable_node.Scalar() + "[0]", iterator_state);
    read_process(_builder, process_node);
    _builder->finish_for_each();
  }
  else if(type == "conditional")
  {
    YAML::Node process_node = get_child(main, "process", ", while reading 'conditional' node.");
    YAML::Node cond_node = main["condition"];
    if(cond_node)
    {
      _builder->start_conditional(read_expression(cond_node));
    }
    else
    {
      YAML::Node on_node = main["on"];
      YAML::Node equals_node = main["equals"];
      if(on_node and equals_node)
      {
        expression on_expr = read_expression(on_node);
        expression equals_expr = read_expression(equals_node);
        _builder->start_conditional(on_expr, equals_expr);
        check_or_set_states_type(equals_expr, on_expr);
        check_or_set_states_type(on_expr, equals_expr);
      }
      else
      {
        throw exception("No 'condition' nor 'on/equals' in 'conditional' node.");
      }
    }
    read_process(_builder, process_node);
    _builder->finish_conditional();
  }
  else if(type == "template")
  {
    YAML::Node template_node = get_child(main, "template", ", while reading 'template' node.");
    YAML::Node id_node = get_child(main, "id", ", while reading 'template' node.");
    std::string id = id_node.Scalar();
    if(id.empty())
    {
      throw pralin::exception("Missing id for template operation.");
    }

    // Read inputs
    YAML::Node inputs_node = main["inputs"];
    std::size_t inputs_node_size = inputs_node.IsDefined() ? inputs_node.size() : 0;
    std::vector<expression> input_values;
    for(std::size_t idx = 0; idx < inputs_node_size; ++idx)
    {
      input_values.push_back(read_expression(inputs_node[idx]));
    }

    std::string out_name = std::format("{}[{}]", id, 0);
    states_mappings[out_name].push_back(out_name);
    check_or_set_states_type(out_name, pralin::meta_type::get<std::string>());

    _builder->create_string_template(id, template_node.Scalar(), input_values);
  }
  else
  {
    any_value_map parameters;
    YAML::Node parameters_node = main["parameters"];
    if(parameters_node.IsDefined())
    {
      if(parameters_node.IsMap())
      {
        for(auto it = parameters_node.begin(); it != parameters_node.end(); ++it)
        {
          std::string key = it->first.Scalar();
          std::string scalar = it->second.Scalar();
          if(it->second.Tag() == "!param"
             or it->second.Tag() == "tag:cyloncore.com/pralin,2023:param")
          {
            auto it_param = current_parameters.find(scalar);
            if(it_param == current_parameters.end())
            {
              throw pralin::exception("Unknown parameter '{}'!", scalar);
            }
            parameters[key] = it_param->second;
          }
          else if(it->second.Tag() == "tag:yaml.org,2002:str" or it->second.Tag() == "!")
          {
            parameters[key] = scalar;
          }
          else if(scalar == "true")
          {
            parameters[key] = true;
          }
          else if(scalar == "false")
          {
            parameters[key] = false;
          }
          else if(std::regex_search(scalar, is_integer_regex))
          {
            parameters[key] = std::stoi(scalar);
          }
          else if(std::regex_search(scalar, is_double_regex))
          {
            parameters[key] = std::stod(scalar);
          }
          else
          {
            parameters[key] = scalar;
          }
        }
      }
      else
      {
        throw exception("Algorithm parameters must be a map.");
      }
    }
    std::size_t slash_pos = type.rfind('/');
    if(slash_pos == std::string::npos)
    {
      throw pralin::exception("Invalid algorithm name '{}'.", type);
    }
    pralin::algorithm_instance algo;
    if(type.starts_with("any/"))
    {
      std::string lib = type.substr(4, slash_pos);
      std::string alg = type.substr(slash_pos + 1);
      algo = algorithms_registry::create_any(lib, alg, parameters);
    }
    else
    {
      std::string lib = type.substr(0, slash_pos);
      std::string alg = type.substr(slash_pos + 1);
      algo = algorithms_registry::create(lib, alg, parameters);
    }
    if(algo.is_valid())
    {
      std::string id;
      YAML::Node id_node = main["id"];
      if(id_node.IsDefined())
      {
        id = id_node.Scalar();
      }
      YAML::Node inputs_node = main["inputs"];
      std::size_t inputs_node_size = inputs_node.IsDefined() ? inputs_node.size() : 0;
      for(std::size_t idx = inputs_node_size; idx < algo.get_input_infos().size(); ++idx)
      {
        const meta_type input_type = algo.get_input_infos()[idx].type;
        if(not input_type.is_optional())
        {
          throw exception("Required input to algorithm '{}' with id '{}' is missing.", type, id);
        }
      }
      std::vector<expression> input_values;
      for(std::size_t idx = 0; idx < inputs_node_size; ++idx)
      {
        input_values.push_back(read_expression(inputs_node[idx], algo.get_input_infos()[idx].type));
      }
      if(not id.empty())
      {
        for(std::size_t idx = 0; idx < algo.get_output_infos().size(); ++idx)
        {
          std::string out_name = std::format("{}[{}]", id, idx);
          states_mappings[out_name].push_back(out_name);
          check_or_set_states_type(out_name, algo.get_output_infos()[idx].type);
        }
      }
      _builder->create_operation(id, algo, input_values);
    }
    else
    {
      throw exception("Unknown algorithm '{}'", type);
    }
  }
}

computation_graph::computation_graph(const std::optional<computation_graph_parameters>& _parameters)
    : d(new data)
{
  if(_parameters)
  {
    if(_parameters->has_parameters())
    {
      d->inital_parameters = _parameters->get_parameters();
    }
    if(_parameters->has_graph_definition())
    {
      load_from_string(_parameters->get_graph_definition());
      if(_parameters->has_graph_filename())
      {
        pralin_warning("Both filename and definition were provided to 'computation_graph', "
                       "filename is ignored.");
      }
    }
    else if(_parameters->has_graph_filename())
    {
      load_from_file(_parameters->get_graph_filename());
    }
  }
}

computation_graph::~computation_graph() {}

void computation_graph::load(std::istream& _is,
                             const std::optional<pralin::any_value_map>& _parameters)
{
  // Parse YAML
  YAML::Node node;
  try
  {
    node = YAML::Load(_is);
  }
  catch(const YAML::ParserException& pe)
  {
    throw exception("Parse error in compose definition: {}", pe.what());
  }
  // Cleanup
  d->current_parameters.clear();
  d->inputs.clear();
  d->outputs.clear();
  d->outputs_labels.clear();
  d->states_mappings.clear();
  d->states_init.clear();
  d->states_types.clear();
  d->input_infos.clear();
  d->output_infos.clear();
  // Build graph
  computation_graph_builder cgb;
  d->check_is_map(node, ", while reading root.");
  YAML::Node compose_node = d->get_child(node, "compose", ", while reading root.");

  // load default parameters from defintion
  std::vector<std::string> required_parameters;
  YAML::Node parameters_node = compose_node["parameters"];
  if(parameters_node.IsDefined())
  {

    if(parameters_node.Type() == YAML::NodeType::Map)
    {
      for(auto it = parameters_node.begin(); it != parameters_node.end(); ++it)
      {
        d->current_parameters[it->first.Scalar()] = d->read_scalar(it->second);
        if(it->second.Tag() == "!required"
           or it->second.Tag() == "tag:cyloncore.com/pralin,2023:required")
        {
          required_parameters.push_back(it->first.Scalar());
        }
      }
    }
    else
    {
      throw pralin::exception("Parameters should be a dictionary.");
    }
  }

  // Update parameters according to argument
  for(auto it = d->inital_parameters.begin(); it != d->inital_parameters.end(); ++it)
  {
    d->current_parameters[it->first] = it->second;
  }
  if(_parameters)
  {
    for(auto it = _parameters->begin(); it != _parameters->end(); ++it)
    {
      d->current_parameters[it->first] = it->second;
    }
  }

  // Check required parameters
  for(const std::string& parameter_name : required_parameters)
  {
    if(not d->current_parameters[parameter_name].is_valid())
    {
      throw pralin::exception("Paramter '{}' is marked as required, but is missing.",
                              parameter_name);
    }
  }

  // inputs
  YAML::Node inputs_node = compose_node["inputs"];
  if(inputs_node.IsDefined())
  {
    if(inputs_node.IsSequence())
    {
      for(std::size_t i = 0; i < inputs_node.size(); ++i)
      {
        std::string name = inputs_node[i].Scalar();
        d->inputs.push_back(name);
      }
    }
    else
    {
      throw exception("Inputs must be a sequence");
    }
  }
  // outputs
  YAML::Node outputs_node = compose_node["outputs"];
  if(outputs_node.IsDefined())
  {
    switch(outputs_node.Type())
    {
    case YAML::NodeType::Sequence:
      for(std::size_t i = 0; i < outputs_node.size(); ++i)
      {
        d->outputs.push_back(
          d->read_expression(outputs_node[i], std::nullopt, std::format("__output_{}", i)));
        d->outputs_labels.push_back(std::to_string(i));
      }
      break;
    case YAML::NodeType::Map:
      for(auto it = outputs_node.begin(); it != outputs_node.end(); ++it)
      {
        d->outputs.push_back(d->read_expression(it->second, std::nullopt, it->first.Scalar()));
        d->outputs_labels.push_back(it->first.Scalar());
      }
      break;
    default:
      throw exception("Invalid 'outputs' definition in compose.");
    }
  }

  // Read root process
  YAML::Node process_node = d->get_child(compose_node, "process", ", while reading root.");
  cgb.start_sequential();
  d->read_process(&cgb, process_node);
  cgb.finish_sequential();
  d->node = cgb.finish_graph();

  // Check types

  for(auto const& [k, v] : d->states_mappings)
  {
    meta_type mt = d->states_types[k];
    if(mt.is_valid())
    {
      for(const std::string& sn : v)
      {
        d->check_or_set_states_type(sn, mt);
      }
    }
    else
    {
      for(const std::string& sn : v)
      {
        meta_type mt = d->states_types[sn];
        if(mt.is_valid())
        {
          d->check_or_set_states_type(k, mt);
        }
      }
    }
  }

  // Check that all states have a type
  for(auto const& [k, v] : d->states_types)
  {
    if(not v.is_valid())
    {
      throw exception("Could not determine type for state '{}'", k);
    }
  }

  // Build input infos
  for(const std::string& v : d->inputs)
  {
    auto it = d->states_types.find(v);
    if(it == d->states_types.end())
    {
      throw exception("Cannot find type of input '{}'", v);
    }
    d->input_infos.push_back({v, it->second});
  }

  if(d->outputs.size() != d->outputs_labels.size())
  {
    throw exception("Internal error, inconsitent size of outputs.");
  }

  for(std::size_t i = 0; i < d->outputs.size(); ++i)
  {
    const expression& output_expression = d->outputs[i];
    switch(output_expression.get_type())
    {
    case expression::type::invalid:
    {
      throw exception("Cannot find type of output '{}', it has invalid expression.",
                      output_expression.get_state_name());
    }
    case expression::type::atomic:
    {
      auto it = d->states_types.find(output_expression.get_state_name());
      if(it == d->states_types.end())
      {
        throw exception("Cannot find type of output '{}' from state '{}'", d->outputs_labels[i],
                        output_expression.get_state_name());
      }
      d->output_infos.push_back({d->outputs_labels[i], it->second});
      break;
    }
    case expression::type::list:
      d->output_infos.push_back({d->outputs_labels[i], meta_type::get<values_vector>()});
      break;
    case expression::type::map:
      d->output_infos.push_back({d->outputs_labels[i], meta_type::get<any_value_map>()});
      break;
    }
  }
}

void computation_graph::load_from_file(const std::string& _file,
                                       const std::optional<pralin::any_value_map>& _parameters)
{
  std::ifstream ifs(_file);
  if(ifs.is_open())
  {
    load(ifs, _parameters);
  }
  else
  {
    throw pralin::exception("Failed to open file {}", _file);
  }
}

void computation_graph::load_from_string(const std::string& _string,
                                         const std::optional<pralin::any_value_map>& _parameters)
{
  std::stringstream ss(_string);
  load(ss, _parameters);
}

void computation_graph::load_parameters_from_string(const std::string& _text)
{
  YAML::Node node;
  try
  {
    node = YAML::Load(_text);
  }
  catch(const YAML::ParserException& pe)
  {
    throw exception("Parse error in parameters definition: {}", pe.what());
  }

  if(node.IsMap())
  {
    for(auto it = node.begin(); it != node.end(); ++it)
    {
      d->inital_parameters[it->first.Scalar()] = d->read_scalar(it->second);
    }
  }
  else
  {
    throw exception("Algorithm parameters must be a map.");
  }
}

void computation_graph::process(const values_vector& _input, const values_ptr_vector& _output) const
{
  if(_output.size() != d->outputs.size())
  {
    throw exception("Invalid output vector, expected size to be {} got {}", d->outputs.size(),
                    _output.size());
  }
  if(_input.size() != d->inputs.size())
  {
    throw exception("Invalid input vector, expected size to be {} got {}", d->inputs.size(),
                    _input.size());
  }
  if(d->node)
  {
    state s = d->states_init;
    for(std::size_t i = 0; i < d->inputs.size(); ++i)
    {
      any_value i_val = _input[i];
      std::string sn = d->inputs[i];
      if(not d->states_types.contains(sn))
      {
        throw exception("Input {}({}) has unknown type.", sn, i);
      }
      if(not i_val.is_convertible(d->states_types[sn]))
      {
        throw exception("Input {}({}) should be of type {} but got {}", sn, i,
                        d->states_types[sn].get_name(), i_val.get_meta_type().get_name());
      }
      std::vector<std::string> mappings = d->states_mappings[sn];
      for(const std::string& var_m : mappings)
      {
        s[var_m] = i_val;
      }
    }

    execution_context ec{d->states_mappings};
    d->node->process(ec, s, &s);
    for(std::size_t i = 0; i < _output.size(); ++i)
    {
      any_value v = d->outputs[i].get_value(s, d->outputs_labels[i]);
      if(v.is_valid())
      {
        any_value_ptr(_output[i]).assign_value(v);
      }
      else
      {
        throw exception("Value for output {} is invalid.", i);
      }
    }
  }
  else
  {
    throw exception("Computation graph is invalid.");
  }
}

std::vector<pralin::input_info> computation_graph::get_input_infos() const
{
  return d->input_infos;
}

std::vector<pralin::output_info> computation_graph::get_output_infos() const
{
  return d->output_infos;
}

pralin::values_ptr_vector computation_graph::create_output_values() const
{
  values_ptr_vector vv;

  for(const output_info& v : get_output_infos())
  {
    vv.push_back(v.type.create_any_value_pointer());
  }

  return vv;
}
