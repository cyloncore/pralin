#include "documentation"

namespace pralin::documentation
{
  namespace details
  {
    struct input_output_parameter::data
    {
      std::string name;
      std::string type;
    };
    struct algorithm_module::data
    {
      std::string name;
      const char* short_description;
      const char* description;
    };
  } // namespace details
  struct parameter::data : public details::input_output_parameter::data
  {
    bool is_required;
    std::string default_value;
  };
  struct algorithm::data : public details::algorithm_module::data
  {
    std::string module;
    std::vector<input> inputs;
    std::vector<output> outputs;
    std::vector<parameter> parameters;
  };
  struct module::data : public details::algorithm_module::data
  {
    std::map<std::string, algorithm> algorithms;
  };

} // namespace pralin::documentation
