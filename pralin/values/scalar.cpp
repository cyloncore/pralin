#include "scalar"

uint8_t pralin::values::scalar_size(scalar_type _type)
{
  uint8_t size = 0;

  switch(_type)
  {
  case scalar_type::uint_8:
  case scalar_type::int_8:
    size = sizeof(uint8_t);
    break;
  case scalar_type::uint_16:
  case scalar_type::int_16:
    size = sizeof(uint16_t);
    break;
  case scalar_type::uint_32:
  case scalar_type::int_32:
    size = sizeof(uint32_t);
    break;
  case scalar_type::uint_64:
  case scalar_type::int_64:
    size = sizeof(uint64_t);
    break;
  case scalar_type::float_32:
    size = sizeof(float);
    break;
  case scalar_type::float_64:
    size = sizeof(double);
    break;
  }

  return size;
}

using namespace pralin::values;

std::shared_ptr<scalar::data> scalar::create_data(scalar_type _type, const uint8_t* _data)
{
  switch(_type)
  {
  case scalar_type::uint_8:
    return std::shared_ptr<scalar::data>(new data_impl<uint8_t>(*_data));
  case scalar_type::int_8:
    return std::shared_ptr<scalar::data>(
      new data_impl<int8_t>(*reinterpret_cast<const int8_t*>(_data)));
  case scalar_type::uint_16:
    return std::shared_ptr<scalar::data>(
      new data_impl<int16_t>(*reinterpret_cast<const int16_t*>(_data)));
  case scalar_type::int_16:
    return std::shared_ptr<scalar::data>(
      new data_impl<int16_t>(*reinterpret_cast<const int16_t*>(_data)));
  case scalar_type::uint_32:
    return std::shared_ptr<scalar::data>(
      new data_impl<uint32_t>(*reinterpret_cast<const uint32_t*>(_data)));
  case scalar_type::int_32:
    return std::shared_ptr<scalar::data>(
      new data_impl<int32_t>(*reinterpret_cast<const int32_t*>(_data)));
  case scalar_type::uint_64:
    return std::shared_ptr<scalar::data>(
      new data_impl<uint64_t>(*reinterpret_cast<const uint64_t*>(_data)));
  case scalar_type::int_64:
    return std::shared_ptr<scalar::data>(
      new data_impl<int64_t>(*reinterpret_cast<const int64_t*>(_data)));
  case scalar_type::float_32:
    return std::shared_ptr<scalar::data>(
      new data_impl<float>(*reinterpret_cast<const float*>(_data)));
  case scalar_type::float_64:
    return std::shared_ptr<scalar::data>(
      new data_impl<double>(*reinterpret_cast<const double*>(_data)));
  }
  return std::shared_ptr<scalar::data>();
}
