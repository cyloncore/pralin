#include "meta_data"

using namespace pralin::values;

const std::string meta_data::timestamp_key = "timestamp";
const std::string meta_data::frame_id_key = "frame_id";
const std::string meta_data::pose_key = "pose";
const std::string meta_data::filename_key = "filename";
const std::string meta_data::srid_key = "srid";

meta_data::builder& meta_data::builder::set(const meta_data& _data)
{
  d->data.insert(_data.d->data.begin(), _data.d->data.end());
  return *this;
}

meta_data::builder& meta_data::builder::set(const std::optional<meta_data>& _data)
{
  if(_data)
  {
    return set(*_data);
  }
  else
  {
    return *this;
  }
}

meta_data::builder& meta_data::builder::set(const any_value_map& _data)
{
  for(auto const& [key, value] : _data)
  {
    d->data[key] = value;
  }
  return *this;
}
