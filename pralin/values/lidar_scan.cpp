#include "lidar_scan"

#include <pralin/utilities/scalar_array_adaptor>

using namespace pralin::values;

lidar_scan::abstract_backend::~abstract_backend() {}

struct lidar_scan::data
{
  ~data() { delete backend; }
  abstract_backend* backend = nullptr;
};

lidar_scan::lidar_scan() : base(meta_data::empty()), d(new data) {}

lidar_scan::lidar_scan(const meta_data& _meta_data, abstract_backend* _backend)
    : base(_meta_data), d(new data{_backend})
{
}

lidar_scan::~lidar_scan() {}

bool lidar_scan::is_valid() const { return d and d->backend; }

double lidar_scan::get_range_min() const { return d->backend->get_range_min(); }

double lidar_scan::get_range_max() const { return d->backend->get_range_max(); }

double lidar_scan::get_angle_min() const { return d->backend->get_angle_min(); }

double lidar_scan::get_angle_max() const { return d->backend->get_angle_max(); }

double lidar_scan::get_angle_increment() const { return d->backend->get_angle_increment(); }

pralin::utilities::scalar_const_array_adaptor lidar_scan::get_ranges() const
{
  return d->backend->get_ranges();
}

pralin::utilities::scalar_const_array_adaptor lidar_scan::get_intensities() const
{
  return d->backend->get_intensities();
}

scalar_unit lidar_scan::get_scalar_unit() const { return d->backend->get_scalar_unit(); }

const lidar_scan::abstract_backend* lidar_scan::get_backend() const { return d->backend; }
