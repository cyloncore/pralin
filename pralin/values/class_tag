#pragma once

#include <string>

#include <pralin/meta_type>

namespace pralin::values
{
  /**
   * @ingroup pralin_values
   * 
   * This structure can be used to tag an output with a class and a confidence score.
   */
  struct class_tag
  {
    double confidence;
    std::string label;
    bool operator==(const class_tag&) const = default;
    PRALIN_META_INFO(class_tag)
  };
}

#include <clog>

#include <clog_format>

clog_format_declare_formatter(pralin::values::class_tag)
{
  return std::format_to(ctx.out(), "({}, {})", p.label, p.confidence);
}
