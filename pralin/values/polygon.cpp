#include "polygon"

using namespace pralin::values;

// adapted from https://wrfranklin.org/Research/Short_Notes/pnpoly.html
bool polygon::is_inside(const pralin::values::point& _point) const
{
  switch(m_type)
  {
  case type::undefined:
    return false;
  case type::rectangle:
    return _point.x >= m_vertices[0].x and _point.x <= m_vertices[0].x
           and _point.y >= m_vertices[0].y and _point.y <= m_vertices[0].y;
  case type::polygon:
  {
    bool inside = false;
    for(std::size_t i = 0, j = m_vertices.size() - 1; i < m_vertices.size(); j = i++)
    {
      point ptj = m_vertices[j];
      point pti = m_vertices[i];
      if((pti.y > _point.y) != (ptj.y > _point.y)
         and _point.x < (ptj.x - pti.x) * (_point.y - pti.y) / (ptj.y - pti.y) + pti.x)
      {
        inside = !inside;
      }
    }
    return inside;
  }
  }
  pralin_fatal("Unsupported type of polygon");
}
