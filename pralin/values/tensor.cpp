#include "tensor"

#include <pralin/utilities/scalar_array_adaptor>

#include "image"

using namespace pralin::values;

namespace
{
  void row_major_strides(std::vector<std::size_t>* _strides,
                         const std::vector<std::size_t>& dimensions, std::size_t _scalar_size)
  {
    _strides->back() = _scalar_size;
    for(std::size_t idx = dimensions.size() - 2; idx < dimensions.size(); --idx)
    {
      (*_strides)[idx] = dimensions[idx + 1] * (*_strides)[idx + 1];
    }
  }
  void column_major_strides(std::vector<std::size_t>* _strides,
                            const std::vector<std::size_t>& dimensions, std::size_t _scalar_size)
  {
    _strides->front() = _scalar_size;
    for(std::size_t idx = 1; idx < dimensions.size(); ++idx)
    {
      (*_strides)[idx] = dimensions[idx - 1] * (*_strides)[idx - 1];
    }
  }
} // namespace

tensor::abstract_backend::~abstract_backend() {}

struct tensor::default_backend::data
{
  ~data() { delete[] data; }

  uint8_t* data;
  std::vector<std::size_t> dimensions;
  std::size_t count;
  scalar_type s_type;
};

tensor::default_backend::default_backend(creation_flag _creation_flag, uint8_t* _data,
                                         const std::vector<size_t>& _dimensions,
                                         scalar_type _scalar_type)
    : d(new data)
{
  d->dimensions = _dimensions;
  d->s_type = _scalar_type;
  d->count = 1;
  for(std::size_t dim : d->dimensions)
  {
    d->count *= dim;
  }

  switch(_creation_flag)
  {
  case creation_flag::transfert_pointer:
    d->data = _data;
    break;
  case creation_flag::copy:
  {
    std::size_t size = d->count * scalar_size(d->s_type);
    d->data = new uint8_t[size];
    std::copy(_data, _data + size, d->data);
    break;
  }
  case creation_flag::from_column_major:
  {
    std::size_t size = d->count * scalar_size(d->s_type);
    d->data = new uint8_t[size];
    std::size_t sc_s = scalar_size(get_scalar_type());
    std::vector<std::size_t> rm_strides, cm_strides;
    rm_strides.resize(d->dimensions.size());
    cm_strides.resize(d->dimensions.size());
    row_major_strides(&rm_strides, d->dimensions, sc_s);
    column_major_strides(&cm_strides, d->dimensions, sc_s);

    for(std::size_t i = 0; i < size; i += sc_s)
    {
      std::size_t offset = 0;

      std::size_t ci = i;
      for(std::size_t j = d->dimensions.size() - 1; j > 0; --j)
      {
        auto res = std::div((long long)ci, (long long)cm_strides[j]);
        offset += res.quot * rm_strides[j];
        ci = res.rem;
      }
      offset += ci / cm_strides[0] * rm_strides[0];
      std::copy(_data + i, _data + i + sc_s, d->data + offset);
    }
    break;
  }
  }
}

tensor::default_backend::default_backend(const std::vector<size_t>& _dimensions,
                                         scalar_type _scalar_type)
    : d(new data)
{
  d->dimensions = _dimensions;
  d->s_type = _scalar_type;
  d->count = 1;
  for(std::size_t dim : d->dimensions)
  {
    d->count *= dim;
  }

  std::size_t size = d->count * scalar_size(d->s_type);
  d->data = new uint8_t[size];
}

tensor::default_backend::~default_backend() {}

std::vector<std::size_t> tensor::default_backend::get_dimensions() const { return d->dimensions; }

scalar_type tensor::default_backend::get_scalar_type() const { return d->s_type; }

uint8_t* tensor::default_backend::get_data() const { return d->data; }

tensor::abstract_backend* tensor::default_backend::cast(scalar_type _ct, double _scale_factor) const
{
  uint8_t* data_dst = new uint8_t[d->count * scalar_size(_ct)];
  utilities::scalar_const_array_adaptor adaptor(d->s_type, d->data, d->count);
  adaptor.copy_to(_ct, data_dst, _scale_factor);
  return new default_backend(creation_flag::transfert_pointer, data_dst, d->dimensions, _ct);
}

struct tensor::data
{
  ~data() { delete backend; }
  abstract_backend* backend = nullptr;
  std::size_t count;
  std::vector<std::size_t> dimensions, strides;
};

tensor::tensor() : base(meta_data::empty()), d(new data) {}

tensor::tensor(const meta_data& _meta_data, abstract_backend* _backend)
    : base(_meta_data), d(new data)
{
  d->backend = _backend;
  d->dimensions = d->backend->get_dimensions();
  d->strides.resize(_backend->get_dimensions().size());
  if(d->dimensions.size() > 0)
  {
    row_major_strides(&d->strides, d->dimensions, scalar_size(get_scalar_type()));
    d->count = 1;
    for(std::size_t dim : d->dimensions)
    {
      d->count *= dim;
    }
  }
  else
  {
    d->count = 0;
  }
}

tensor::tensor(const tensor& _rhs) : base(_rhs), d(_rhs.d) {}

tensor& tensor::operator=(const tensor& _rhs)
{
  d = _rhs.d;
  return *this;
}

tensor::~tensor() {}

tensor tensor::create_default(const meta_data& _meta_data, creation_flag _creation_flag,
                              uint8_t* _data, const std::vector<size_t>& _dimensions,
                              scalar_type _scalar_type)
{
  return tensor(_meta_data, new default_backend(_creation_flag, _data, _dimensions, _scalar_type));
}

tensor tensor::create_default(const meta_data& _meta_data, const std::vector<size_t>& _dimensions,
                              scalar_type _scalar_type)
{
  return tensor(_meta_data, new default_backend(_dimensions, _scalar_type));
}

tensor tensor::create_default(const image& _image)
{
  return create_default(
    _image.get_meta_data(), creation_flag::copy, const_cast<uint8_t*>(_image.get_data()),
    {_image.get_height(), _image.get_width(), _image.get_channels()}, _image.get_channel_type());
}

bool tensor::is_valid() const { return d and d->backend; }

tensor::abstract_backend* tensor::get_backend() { return d->backend; }

const tensor::abstract_backend* tensor::get_backend() const { return d->backend; }

std::vector<std::size_t> tensor::get_dimensions() const { return d->dimensions; }

std::vector<std::size_t> tensor::get_strides() const { return d->strides; }

std::size_t tensor::get_count() const { return d->count; }

scalar tensor::get_value_at(std::size_t _idx) const
{
  return utilities::scalar_const_array_adaptor(get_scalar_type(), get_data(), d->count).at(_idx);
}

scalar_type tensor::get_scalar_type() const { return d->backend->get_scalar_type(); }

uint8_t* tensor::get_data() const { return d->backend->get_data(); }

tensor tensor::cast(scalar_type _ct, double _scale_factor) const
{
  return tensor(get_meta_data(), d->backend->cast(_ct, _scale_factor));
}
