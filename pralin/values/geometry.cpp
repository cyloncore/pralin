#include "geometry"

#include "base"

using namespace pralin::values::geometry;

struct transformation::data
{
  ~data() { delete backend; }
  abstract_backend* backend = nullptr;
};

transformation::abstract_backend::~abstract_backend() {}

transformation_provider::abstract_backend::~abstract_backend() {}

transformation::transformation() {}

transformation::transformation(abstract_backend* _backend) : d(new data{_backend}) {}

transformation::transformation(const transformation& _rhs) : d(_rhs.d) {}

transformation& transformation::operator=(const transformation& _rhs)
{
  d = _rhs.d;
  return *this;
}

transformation::~transformation() {}

point transformation::transform(const point& _point) const { return d->backend->transform(_point); }

pose transformation::transform(const pose& _pose) const { return d->backend->transform(_pose); }

bool transformation::is_valid() const { return d and d->backend; }

// BEGIN transformation_provider

struct transformation_provider::data
{
  ~data() { delete backend; }
  abstract_backend* backend = nullptr;
};

transformation_provider::transformation_provider() {}

transformation_provider::transformation_provider(abstract_backend* _backend) : d(new data{_backend})
{
}

transformation_provider::transformation_provider(const transformation_provider& _rhs) : d(_rhs.d) {}

transformation_provider& transformation_provider::operator=(const transformation_provider& _rhs)
{
  d = _rhs.d;
  return *this;
}

transformation_provider::~transformation_provider() {}

transformation
  transformation_provider::get_transformation(const meta_data& _meta_data,
                                              const meta_data& _destination_frame_id) const
{
  return d->backend->get_transformation(_meta_data, _destination_frame_id);
}

transformation
  transformation_provider::get_transformation(const base& _value,
                                              const meta_data& _destination_frame_id) const
{
  return get_transformation(_value.get_meta_data(), _destination_frame_id);
}

pose transformation_provider::transform(const pose& _pose,
                                        const meta_data& _destination_meta_data) const
{
  return d->backend
    ->get_transformation(meta_data::create().set_srid(_pose.get_srid()), _destination_meta_data)
    .transform(_pose);
}

// END transformation_provider
