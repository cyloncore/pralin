#include "point_cloud"

using namespace pralin::values;

// BEGIN point_cloud::field

point_cloud::field::type point_cloud::field::compute_type()
{
  if(m_name == "x" or m_name == "X")
  {
    return type::x;
  }
  if(m_name == "y" or m_name == "Y")
  {
    return type::y;
  }
  if(m_name == "z" or m_name == "Z")
  {
    return type::z;
  }
  return type::other;
}

// END point_cloud::field

// BEGIN point_cloud backends

point_cloud::abstract_backend::~abstract_backend() {}

struct point_cloud::default_backend::data
{
  std::vector<uint8_t> data;
  int count;
  int stride;
};

point_cloud::default_backend::default_backend(const std::vector<uint8_t>& _data, int _count,
                                              int _stride)
    : d(new data{_data, _count, _stride})
{
}

point_cloud::default_backend::~default_backend() { delete d; }

const uint8_t* point_cloud::default_backend::get_point(std::size_t _index) const
{
  return d->data.data() + _index * d->stride;
}

std::size_t point_cloud::default_backend::get_count() const { return d->count; }

std::size_t point_cloud::default_backend::get_point_size() const { return d->stride; }

void point_cloud::default_backend::copy_to(uint8_t* _data) const
{
  std::copy(d->data.begin(), d->data.begin() + d->count * d->stride,
            _data); // cannot use d->data.end() as data might hold more data than actual points are
                    // available
}

// END point_cloud backends

// BEGIN point_cloud

struct point_cloud::data
{
  ~data() { delete backend; }
  std::vector<field> fields;
  abstract_backend* backend;
};

point_cloud::point_cloud() : base(meta_data::empty()), d(nullptr) {}

point_cloud::point_cloud(const meta_data& _meta_data, const std::vector<field>& _fields,
                         abstract_backend* _backend)
    : base(_meta_data), d(new data{_fields, _backend})
{
}

point_cloud::point_cloud(const point_cloud& _rhs) : base(_rhs), d(_rhs.d) {}

point_cloud& point_cloud::operator=(const point_cloud& _rhs)
{
  base::operator=(_rhs);
  d = _rhs.d;
  return *this;
}

point_cloud::~point_cloud() {}

const point_cloud::abstract_backend* point_cloud::get_backend() const { return d->backend; }

std::vector<point_cloud::field> point_cloud::get_fields() const { return d->fields; }

const uint8_t* point_cloud::get_point(std::size_t _index) const
{
  return d->backend->get_point(_index);
}

std::size_t point_cloud::get_count() const { return d->backend->get_count(); }

std::size_t point_cloud::get_point_size() const { return d->backend->get_point_size(); }

void point_cloud::copy_to(uint8_t* _data) const { d->backend->copy_to(_data); }

bool point_cloud::is_valid() const { return d and d->backend; }

// END point_cloud

// BEGIN point_cloud_builder backends

struct point_cloud_builder::generic_backend::data
{
  std::vector<point_cloud::field> fields;
  std::vector<uint8_t> data;
  int stride = 0;
  int counts = 0;
  int capacity = 0;
};

point_cloud_builder::generic_backend::generic_backend(const std::vector<point_cloud::field>& _pcf)
    : d(new data{_pcf, {}})
{
  for(const point_cloud::field& f : d->fields)
  {
    d->stride += f.get_count() * scalar_size(f.get_scalar_type());
  }
}

point_cloud_builder::generic_backend::~generic_backend() { delete d; }

void point_cloud_builder::generic_backend::add_point(const uint8_t* _data) const
{
  if(d->counts >= d->capacity)
  {
    d->capacity = 2 * (d->capacity + 1);
    d->data.resize(d->capacity * d->stride);
  }
  std::copy(_data, _data + d->stride, d->data.begin() + d->counts * d->stride);
  ++d->counts;
}

void point_cloud_builder::generic_backend::add_point(
  scalar_unit _unit, const utilities::scalar_const_array_adaptor& _data) const
{
  std::vector<uint8_t> point;
  point.resize(d->stride);
  int data_index = 0;
  for(std::size_t i = 0; i < d->fields.size(); ++i)
  {
    const point_cloud::field& field_info = d->fields[i];
    utilities::scalar_array_adaptor field_adaptor(
      field_info.get_scalar_type(), point.data() + field_info.get_offset(), field_info.get_count());
    for(std::size_t j = 0; j < field_info.get_count(); ++j)
    {
      scalar value = _data.at(data_index++);
      if(_unit != scalar_unit::meter)
      {
        using field_type = point_cloud::field::type;
        switch(d->fields[i].get_type())
        {
        case field_type::x:
        case field_type::y:
        case field_type::z:
          value = convert_unit(value, _unit, scalar_unit::meter);
          break;
        case field_type::other:
          break;
        }
      }
      field_adaptor.set(j, value);
    }
  }
  add_point(point.data());
}

point_cloud point_cloud_builder::generic_backend::to_point_cloud(const meta_data& _meta_data) const
{
  return point_cloud(_meta_data, d->fields,
                     new point_cloud::default_backend(d->data, d->counts, d->stride));
}

// END point_cloud_builder backends

// BEGIN point_cloud_builder

point_cloud_builder::abstract_backend::~abstract_backend() {}

struct point_cloud_builder::data
{
  abstract_backend* backend;
  meta_data::builder meta_data_builder = meta_data::create();
};

point_cloud_builder::point_cloud_builder() : d(nullptr) {}

point_cloud_builder::point_cloud_builder(abstract_backend* _backend) : d(new data{_backend}) {}

point_cloud_builder::point_cloud_builder(const point_cloud_builder& _rhs) : d(_rhs.d) {}

point_cloud_builder point_cloud_builder::create_default(values::scalar_type _type,
                                                        const std::vector<std::string>& _names)
{
  switch(_type)
  {
  case scalar_type::uint_8:
    return create_default<uint8_t>(_names);
  case scalar_type::uint_16:
    return create_default<uint16_t>(_names);
  case scalar_type::uint_32:
    return create_default<uint32_t>(_names);
  case scalar_type::uint_64:
    return create_default<uint64_t>(_names);
  case scalar_type::int_8:
    return create_default<int8_t>(_names);
  case scalar_type::int_16:
    return create_default<int16_t>(_names);
  case scalar_type::int_32:
    return create_default<int32_t>(_names);
  case scalar_type::int_64:
    return create_default<int64_t>(_names);
  case scalar_type::float_32:
    return create_default<float>(_names);
  case scalar_type::float_64:
    return create_default<double>(_names);
  }
  pralin_fatal("Unsupported scalar type");
}

point_cloud_builder point_cloud_builder::create_generic(const std::vector<point_cloud::field>& _pcf)
{
  return new generic_backend(_pcf);
}

point_cloud_builder& point_cloud_builder::operator=(const point_cloud_builder& _rhs)
{
  d = _rhs.d;
  return *this;
}
point_cloud_builder::~point_cloud_builder() {}

meta_data::builder& point_cloud_builder::get_meta_data_builder() const
{
  return d->meta_data_builder;
}

void point_cloud_builder::add_point(const uint8_t* _data) const
{
  return d->backend->add_point(_data);
}

void point_cloud_builder::add_point(scalar_unit _unit,
                                    const utilities::scalar_const_array_adaptor& _data) const
{
  return d->backend->add_point(_unit, _data);
}

point_cloud point_cloud_builder::to_point_cloud() const
{
  return d->backend->to_point_cloud(d->meta_data_builder);
}

bool point_cloud_builder::is_valid() const { return d and d->backend; }

// END point_cloud_builder
