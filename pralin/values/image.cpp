#include "image"

#include "tensor"

using namespace pralin::values;

struct image::data
{
  ~data() { delete backend; }
  abstract_backend* backend = nullptr;
  std::size_t width = 0, width_step = 0, height = 0, channels = 0;
  uint8_t* data = nullptr;
  scalar_type channel_type = scalar_type::uint_8;
  colorspace cs;
};

struct image::colorspace::data
{
  image::colorspace::type type = image::colorspace::type::invalid;
  std::vector<std::string> channels;
};

image::colorspace::colorspace()
    : d(new data){

      };

image::colorspace::colorspace(type _type) : colorspace()
{
  d->type = _type;
  switch(d->type)
  {
  case type::grayscale:
    d->channels = {channel::gray()};
    break;
  case type::rgb:
    d->channels = {channel::red(), channel::green(), channel::blue()};
    break;
  case type::rgba:
    d->channels = {channel::red(), channel::green(), channel::blue(), channel::alpha()};
    break;
  case type::bgr:
    d->channels = {channel::blue(), channel::green(), channel::red()};
    break;
  case type::bgra:
    d->channels = {channel::blue(), channel::green(), channel::red(), channel::alpha()};
    break;
  case type::other:
    break;
  default:
    pralin_fatal("Unknown colorspace type.");
  }
}

image::colorspace::colorspace(const std::vector<std::string>& _channels) : colorspace(type::other)
{
  d->channels = _channels;
}

image::colorspace::~colorspace() {}

image::colorspace image::colorspace::rgb()
{
  static image::colorspace cs(type::rgb);
  return cs;
}

image::colorspace image::colorspace::rgba()
{
  static image::colorspace cs(type::rgba);
  return cs;
}

image::colorspace image::colorspace::bgr()
{
  static image::colorspace cs(type::bgr);
  return cs;
}

image::colorspace image::colorspace::bgra()
{
  static image::colorspace cs(type::bgra);
  return cs;
}

image::colorspace image::colorspace::grayscale()
{
  static image::colorspace cs(type::grayscale);
  return cs;
}

std::string image::colorspace::channel::red()
{
  static std::string c("red");
  return c;
}

std::string image::colorspace::channel::green()
{
  static std::string c("green");
  return c;
}

std::string image::colorspace::channel::blue()
{
  static std::string c("blue");
  return c;
}

std::string image::colorspace::channel::gray()
{
  static std::string c("gray");
  return c;
}

std::string image::colorspace::channel::alpha()
{
  static std::string c("alpha");
  return c;
}

bool image::colorspace::is_valid() const { return d->type != type::invalid; }

image::colorspace::type image::colorspace::get_type() const { return d->type; }

bool image::colorspace::is_rgb() const { return d->type == type::rgb; }

bool image::colorspace::is_rgba() const { return d->type == type::rgba; }

bool image::colorspace::is_bgr() const { return d->type == type::bgr; }

bool image::colorspace::is_bgra() const { return d->type == type::bgra; }

bool image::colorspace::is_grayscale() const { return d->type == type::grayscale; }

std::vector<std::string> image::colorspace::get_channels() { return d->channels; }

image::abstract_backend::~abstract_backend() {}

struct image::default_backend::data
{
  tensor data_tensor;
  colorspace cs;
};

image::default_backend::default_backend(bool _transfer_data, uint8_t* _data, std::size_t _width,
                                        std::size_t _height, std::size_t _channels,
                                        scalar_type _channel_type,
                                        const image::colorspace& _colorspace)
    : default_backend(tensor::create_default(meta_data::empty(),
                                             _transfer_data
                                               ? tensor::creation_flag::transfert_pointer
                                               : tensor::creation_flag::copy,
                                             _data, {_height, _width, _channels}, _channel_type),
                      _colorspace)
{
}

image::default_backend::default_backend(const tensor& _data, const colorspace& _colorspace)
    : d(new data)
{
  d->data_tensor = _data;
  d->cs = _colorspace;
}

image::default_backend::~default_backend() {}

std::size_t image::default_backend::get_width() const { return d->data_tensor.get_dimensions()[1]; }

std::size_t image::default_backend::get_width_step() const
{
  return d->data_tensor.get_dimensions()[1] * d->data_tensor.get_dimensions()[2];
}

std::size_t image::default_backend::get_height() const
{
  return d->data_tensor.get_dimensions()[0];
}

std::size_t image::default_backend::get_channels() const
{
  return d->data_tensor.get_dimensions()[2];
}

uint8_t* image::default_backend::get_data() const { return d->data_tensor.get_data(); }

scalar_type image::default_backend::get_channel_type() const
{
  return d->data_tensor.get_scalar_type();
}

image::colorspace image::default_backend::get_colorspace() const { return d->cs; }

image::abstract_backend* image::default_backend::cast(scalar_type _ct, double _scale_factor) const
{
  return new default_backend(d->data_tensor.cast(_ct, _scale_factor), d->cs);
}

// tensor image::default_backend::to_tensor() const
// {
//   return d->data_tensor;
// }

image::image() : base(meta_data::empty()), d(new data) {}

image::image(const meta_data& _meta_data, abstract_backend* _backend)
    : base(_meta_data), d(new data)
{
  d->backend = _backend;
  // Cache the metainformation
  d->width = d->backend->get_width();
  d->width_step = d->backend->get_width_step();
  d->height = d->backend->get_height();
  d->channels = d->backend->get_channels();
  d->data = d->backend->get_data();
  d->channel_type = d->backend->get_channel_type();
  d->cs = d->backend->get_colorspace();
}

image::image(const image& _rhs) : base(_rhs), d(_rhs.d) {}

image& image::operator=(const image& _rhs)
{
  base::operator=(_rhs);
  d = _rhs.d;
  return *this;
}

image::~image() {}

image image::create_default(const meta_data& _meta_data, bool _transfer_data, uint8_t* _data,
                            std::size_t _width, std::size_t _height, std::size_t _channels,
                            scalar_type _scalar_type, const colorspace& _colorspace)
{
  return image(_meta_data, new default_backend(_transfer_data, _data, _width, _height, _channels,
                                               _scalar_type, _colorspace));
}

image image::create_default(const tensor& _data, const colorspace& _colorspace)
{
  return image(_data.get_meta_data(), new default_backend(_data, _colorspace));
}

bool image::is_valid() const { return d and d->backend; }

image::abstract_backend* image::get_backend() { return d->backend; }

const image::abstract_backend* image::get_backend() const { return d->backend; }

uint8_t* image::get_data() { return d->data; }

const uint8_t* image::get_data() const { return d->data; }

std::size_t image::get_width() const { return d->width; }

std::size_t image::get_width_step() const { return d->width_step; }

std::size_t image::get_height() const { return d->height; }

std::size_t image::get_channels() const { return d->channels; }

scalar_type image::get_channel_type() const { return d->channel_type; }

image::colorspace image::get_colorspace() const { return d->cs; }

double pralin::values::image::scale_factor(pralin::values::scalar_type _channel_type)
{
  switch(_channel_type)
  {
  case pralin::values::scalar_type::uint_8:
    return 1.0 / std::numeric_limits<uint8_t>::max();
  case pralin::values::scalar_type::int_8:
    return 1.0 / std::numeric_limits<int8_t>::max();
  case pralin::values::scalar_type::uint_16:
    return 1.0 / std::numeric_limits<uint16_t>::max();
  case pralin::values::scalar_type::int_16:
    return 1.0 / std::numeric_limits<int16_t>::max();
  case pralin::values::scalar_type::uint_32:
    return 1.0 / std::numeric_limits<uint32_t>::max();
  case pralin::values::scalar_type::int_32:
    return 1.0 / std::numeric_limits<int32_t>::max();
  case pralin::values::scalar_type::uint_64:
    return 1.0 / std::numeric_limits<uint64_t>::max();
  case pralin::values::scalar_type::int_64:
    return 1.0 / std::numeric_limits<int64_t>::max();
  case pralin::values::scalar_type::float_32:
  case pralin::values::scalar_type::float_64:
    return 1.0;
  }

  return 1.0;
}

// tensor pralin::values::image::to_tensor() const
// {
//   return d->backend->to_tensor();
// }
