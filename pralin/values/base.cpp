#include "base"

using namespace pralin::values;

struct base::data
{
  meta_data md;
};

base::base(const meta_data& _meta_data) : d(new data{_meta_data}) {}

base::base(const base& _rhs) : d(_rhs.d) {}

base& base::operator=(const base& _rhs)
{
  d = _rhs.d;
  return *this;
}

base::~base() {}

const meta_data& base::get_meta_data() const { return d->md; }

bool base::operator==(const base& _rhs) const { return d == _rhs.d or d->md == _rhs.d->md; }
