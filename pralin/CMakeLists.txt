
set(PRALIN_SRCS
  abstract_algorithm.cpp
  algorithms_registry.cpp
  algorithm_registry_monitor.cpp
  any_value.cpp
  any_value_converters.cpp
  any_value_ptr.cpp
  background_actions_manager.cpp
  documentation.cpp
  exception.cpp
  global.cpp
  resources.cpp
  # utilities
  utilities/scalar_array_adaptor.cpp
  # values
  values/base.cpp
  values/image.cpp
  values/geometry.cpp
  values/lidar_scan.cpp
  values/meta_data.cpp
  values/point_cloud.cpp
  values/polygon.cpp
  values/scalar.cpp
  values/tensor.cpp
  )

if(CMAKE_SYSTEM_NAME MATCHES "Windows")
  set(PRALIN_WINDOWS  TRUE)
endif()

if(CMAKE_SYSTEM_NAME MATCHES "Linux")
  set(PRALIN_LINUX  TRUE)
  set(PRALIN_UNIX   TRUE)
endif()

if(CMAKE_SYSTEM_NAME MATCHES "FreeBSD")
  set(PRALIN_FREEBSD  TRUE)
  set(PRALIN_UNIX   TRUE)
endif()

if(CMAKE_SYSTEM_NAME MATCHES "Darwin")
  set(PRALIN_MACOS  TRUE)
  set(PRALIN_UNIX   TRUE)
endif()

set(PRALIN_INSTALL_DEFINITIONS_DIRECTORY ${INSTALL_SPECS_DIR})

if(tasks_machine_FOUND)
 set(PRALIN_HAS_TASKS_MACHINE TRUE)
endif()

configure_file(config_p.h.in ${CMAKE_CURRENT_BINARY_DIR}/config_p.h)
configure_file(config.in ${CMAKE_CURRENT_BINARY_DIR}/config)

if(PRALIN_UNIX)
  set(PRALIN_PLATFORM_SRCS platform_unix.cpp)
elseif(PRALIN_WINDOWS)
  set(PRALIN_PLATFORM_SRCS platform_windows.cpp)
endif()

add_library(pralin SHARED ${PRALIN_SRCS} ${PRALIN_PLATFORM_SRCS})
target_link_libraries(pralin PRIVATE stdc++fs)
if(PRALIN_HAVE_CLOG)
target_link_libraries(pralin PUBLIC clog)
endif()
install(TARGETS pralin EXPORT pralinTargets ${INSTALL_TARGETS_DEFAULT_ARGS} )

if(PRALIN_UNIX)
  target_link_libraries(pralin PRIVATE dl)
endif()

pralin_install_headers(
  ${INSTALL_INCLUDE_DIR}/pralin
  ${CMAKE_CURRENT_BINARY_DIR}/config
  abstract_algorithm
  algorithm_definition
  algorithm_instance
  algorithms_registry
  any_value
  any_value_ptr
  documentation
  exception
  fixed_string
  forward
  global
  input
  implementation
  logging
  meta_type
  output
  parameters
  any_value_common.h
  any_value_converters.h
)

pralin_install_headers(
  ${INSTALL_INCLUDE_DIR}/pralin/definitions 
  definitions/geometry
  definitions/image
  definitions/io
  definitions/ml
  definitions/operators
  definitions/point_cloud
  definitions/query
)

pralin_install_headers(
  ${INSTALL_INCLUDE_DIR}/pralin/utilities
  utilities/scalar_array_adaptor
)

pralin_install_headers(
  ${INSTALL_INCLUDE_DIR}/pralin/values
  values/base
  values/class_tag
  values/geometry
  values/image
  values/iterator
  values/lidar_scan
  values/matrix
  values/meta_data
  values/point_cloud
  values/scalar
  values/polygon
  values/tagged
)

add_subdirectory(tests)

if(BUILD_PRALIN_COMPOSE)
add_subdirectory(compose)
endif()

configure_file(version.in ${CMAKE_CURRENT_BINARY_DIR}/version)
