#include "abstract_algorithm"

namespace pralin
{
  struct abstract_algorithm::data
  {
    std::string library, algorithm;
  };
} // namespace pralin
