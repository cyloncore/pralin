#include "catch.h"

#include <pralin/utilities/scalar_array_adaptor>
#include <pralin/values/scalar>

using scalar = pralin::values::scalar;
using scalar_type = pralin::values::scalar_type;
using scalar_const_array_adaptor = pralin::utilities::scalar_const_array_adaptor;
using scalar_array_adaptor = pralin::utilities::scalar_array_adaptor;
using any_value = pralin::any_value;

TEST_CASE("test_scalar", "")
{
  scalar s(10.0);
  REQUIRE(s.get_type() == scalar_type::float_64);
  REQUIRE(s.cast<double>() == 10.0);
  REQUIRE(s.cast<int32_t>() == 10);

  double v = 12.0;
  scalar s2(scalar_type::float_64, reinterpret_cast<uint8_t*>(&v));
  REQUIRE(s2.get_type() == scalar_type::float_64);
  REQUIRE(s2.cast<double>() == v);
  REQUIRE(s2.cast<int32_t>() == int32_t(v));
}

TEST_CASE("test_scalar_any_values", "")
{
  std::vector<any_value> values;
  values.push_back(scalar(1));
  values.push_back(scalar(2));

  REQUIRE(values[0].to_value<scalar>().cast<int>() == 1);
  REQUIRE(values[1].to_value<scalar>().cast<int>() == 2);

  std::vector<any_value> values2 = values;

  REQUIRE(values2[0].to_value<scalar>().cast<int>() == 1);
  REQUIRE(values2[1].to_value<scalar>().cast<int>() == 2);
}

TEST_CASE("scalar_const_array_adaptor", "")
{
  double array[] = {3.0, 4.2, -5.0};
  scalar_const_array_adaptor adap_1(array, 3);
  scalar_const_array_adaptor adap_2(scalar_type::float_64, reinterpret_cast<uint8_t*>(array), 3);

  REQUIRE(adap_1.size() == 3);
  REQUIRE(adap_2.size() == 3);

  REQUIRE(adap_1.at<double>(0) == 3.0);
  REQUIRE(adap_2.at<double>(0) == 3.0);
  REQUIRE(adap_1.at<double>(1) == 4.2);
  REQUIRE(adap_2.at<double>(1) == 4.2);
  REQUIRE(adap_1.at<double>(2) == -5.0);
  REQUIRE(adap_2.at<double>(2) == -5.0);

  REQUIRE(adap_1.at<int32_t>(0) == 3);
  REQUIRE(adap_2.at<int32_t>(0) == 3);
  REQUIRE(adap_1.at<int32_t>(1) == 4);
  REQUIRE(adap_2.at<int32_t>(1) == 4);
  REQUIRE(adap_1.at<int32_t>(2) == -5);
  REQUIRE(adap_2.at<int32_t>(2) == -5);
}

TEST_CASE("scalar_array_adaptor", "")
{
  double array[] = {3.0, 4.2, -5.0};
  scalar_array_adaptor adap_1(array, 3);
  scalar_array_adaptor adap_2(scalar_type::float_64, reinterpret_cast<uint8_t*>(array), 3);

  REQUIRE(adap_1.size() == 3);
  REQUIRE(adap_2.size() == 3);

  REQUIRE(adap_1.at<double>(0) == 3.0);
  REQUIRE(adap_2.at<double>(0) == 3.0);
  REQUIRE(adap_1.at<double>(1) == 4.2);
  REQUIRE(adap_2.at<double>(1) == 4.2);
  REQUIRE(adap_1.at<double>(2) == -5.0);
  REQUIRE(adap_2.at<double>(2) == -5.0);

  REQUIRE(adap_1.at<int32_t>(0) == 3);
  REQUIRE(adap_2.at<int32_t>(0) == 3);
  REQUIRE(adap_1.at<int32_t>(1) == 4);
  REQUIRE(adap_2.at<int32_t>(1) == 4);
  REQUIRE(adap_1.at<int32_t>(2) == -5);
  REQUIRE(adap_2.at<int32_t>(2) == -5);

  adap_1.set(0, -3.0);
  adap_2.set(1, 4);

  REQUIRE(array[0] == -3.0);
  REQUIRE(array[1] == 4.0);

  REQUIRE(adap_1.at<double>(0) == -3.0);
  REQUIRE(adap_2.at<double>(0) == -3.0);
  REQUIRE(adap_1.at<double>(1) == 4.0);
  REQUIRE(adap_2.at<double>(1) == 4.0);
  REQUIRE(adap_1.at<double>(2) == -5.0);
  REQUIRE(adap_2.at<double>(2) == -5.0);

  REQUIRE(adap_1.at<int32_t>(0) == -3);
  REQUIRE(adap_2.at<int32_t>(0) == -3);
  REQUIRE(adap_1.at<int32_t>(1) == 4);
  REQUIRE(adap_2.at<int32_t>(1) == 4);
  REQUIRE(adap_1.at<int32_t>(2) == -5);
  REQUIRE(adap_2.at<int32_t>(2) == -5);
}
