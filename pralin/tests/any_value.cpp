#include "catch.h"

#include <pralin/any_value>

using any_value = pralin::any_value;
using any_value_ptr = pralin::any_value_ptr;

TEST_CASE("any_value/any_value_ptr", "")
{
  int a = 1;
  any_value av(&a);
  CHECK(av.is_pointer_to(pralin::meta_type::get<int>()));
  any_value_ptr av_ptr(av);
  any_value av_deref = av_ptr.dereference();
  CHECK(av_deref.is_convertible(pralin::meta_type::get<int>()));
  CHECK(av_deref.to_value<int>() == 1);
}

TEST_CASE("any_value_ptr", "")
{
  int a = 1;
  any_value_ptr av_ptr(&a);
  any_value av_deref = av_ptr.dereference();
  CHECK(av_deref.is_convertible(pralin::meta_type::get<int>()));
  CHECK(av_deref.to_value<int>() == 1);
}