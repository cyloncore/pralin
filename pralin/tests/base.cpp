#include "catch.h"

#include <pralin/values/base>

TEST_CASE("test_base", "")
{
  pralin::values::base b{
    pralin::values::meta_data::create().set_timestamp(10).set_frame_id("test")};

  REQUIRE(b.get_meta_data().get_timestamp() == 10);
  REQUIRE(b.get_meta_data().get_frame_id() == "test");
}
