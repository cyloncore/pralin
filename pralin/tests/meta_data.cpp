#include "catch.h"

#include <pralin/values/meta_data>

TEST_CASE("test_meta_data", "")
{
  pralin::values::meta_data md
    = pralin::values::meta_data::create().set_timestamp(10).set_frame_id("test");
  pralin::values::meta_data md_copy(md);
  pralin::values::meta_data md_copy2 = pralin::values::meta_data::create();

  REQUIRE(md.get_timestamp() == 10);
  REQUIRE(md.get_frame_id() == "test");
  REQUIRE(md_copy.get_timestamp() == 10);
  REQUIRE(md_copy.get_frame_id() == "test");

  md_copy2 = md;
  REQUIRE(md_copy2.get_timestamp() == 10);
  REQUIRE(md_copy2.get_frame_id() == "test");
}
