#include "catch.h"

#include <pralin/values/point_cloud>

TEST_CASE("test_point_cloud_builder", "")
{
  pralin::values::point_cloud_builder pcb
    = pralin::values::point_cloud_builder::create_default<float>({"x", "y", "z"});

  pralin::values::point_cloud_builder pcb1 = pcb;
  pralin::values::point_cloud_builder pcb2;
  pcb2 = pcb;

  REQUIRE(pcb.is_valid());
  REQUIRE(pcb1.is_valid());
  REQUIRE(pcb2.is_valid());

  pcb.add_point(pralin::values::scalar_unit::meter, 1, 2, 3);
  pcb1.add_point(pralin::values::scalar_unit::meter, 4.0, 5.0, 6.0);
  pcb2.add_point(pralin::values::scalar_unit::meter, 7.0f, 8.0f, 9.0f);

  pralin::values::point_cloud pc = pcb.to_point_cloud();

  REQUIRE(pc.get_count() == 3);
  REQUIRE(pc.get_point_size() == 3 * sizeof(float));

  const float* p1 = reinterpret_cast<const float*>(pc.get_point(0));

  REQUIRE(p1[0] == 1);
  REQUIRE(p1[1] == 2);
  REQUIRE(p1[2] == 3);

  const float* p2 = reinterpret_cast<const float*>(pc.get_point(1));

  REQUIRE(p2[0] == 4);
  REQUIRE(p2[1] == 5);
  REQUIRE(p2[2] == 6);

  const float* p3 = reinterpret_cast<const float*>(pc.get_point(2));

  REQUIRE(p3[0] == 7);
  REQUIRE(p3[1] == 8);
  REQUIRE(p3[2] == 9);

  std::vector<float> data;
  data.resize(9);
  pc.copy_to(reinterpret_cast<uint8_t*>(data.data()));

  REQUIRE(data[0] == 1);
  REQUIRE(data[1] == 2);
  REQUIRE(data[2] == 3);
  REQUIRE(data[3] == 4);
  REQUIRE(data[4] == 5);
  REQUIRE(data[5] == 6);
  REQUIRE(data[6] == 7);
  REQUIRE(data[7] == 8);
  REQUIRE(data[8] == 9);
}
