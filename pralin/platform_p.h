namespace pralin::platform
{
  std::string get_application_data_path();
  bool download_file(const std::string& _url, const std::string& _destination);
  void* load_library(const std::string& _library);
} // namespace pralin::platform
