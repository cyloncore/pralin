#include <dlfcn.h>
#include <pwd.h>
#include <string>
#include <sys/types.h>
#include <unistd.h>

#include "logging"

namespace pralin::platform
{
  std::string get_application_data_path()
  {
    struct passwd* pw = getpwuid(getuid());
    return std::string(pw->pw_dir) + "/.local/share/pralin/";
  }
  bool download_file(const std::string& _url, const std::string& _destination)
  {
    std::string command = "wget -O \"" + _destination + "\" \"" + _url + "\"";

    return system((const char*)command.c_str()) == 0;
  }
  void* load_library(const std::string& _library)
  {
    void* ptr = dlopen(_library.c_str(), RTLD_LAZY);
    if(ptr)
    {
      return ptr;
    }
    else
    {
      throw pralin::exception("Error while loading {}: '{}'", _library, dlerror());
    }
  }
} // namespace pralin::platform
