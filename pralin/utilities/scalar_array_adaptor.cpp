#include "scalar_array_adaptor"

using namespace pralin::utilities;

namespace pralin::details
{
  template<typename _T_>
  void copy_to(values::scalar_type _src_type, const uint8_t* _src, _T_* _dst, int _count,
               double _scale_factor)
  {
    switch(_src_type)
    {
    case values::scalar_type::uint_8:
      std::transform(_src, _src + _count, _dst,
                     [_scale_factor](uint8_t _a) { return uint8_t(_a * _scale_factor); });
      return;
    case values::scalar_type::uint_16:
      std::transform(reinterpret_cast<const uint16_t*>(_src),
                     reinterpret_cast<const uint16_t*>(_src) + _count, _dst,
                     [_scale_factor](uint16_t _a) { return _T_(_a * _scale_factor); });
      return;
    case values::scalar_type::uint_32:
      std::transform(reinterpret_cast<const uint32_t*>(_src),
                     reinterpret_cast<const uint32_t*>(_src) + _count, _dst,
                     [_scale_factor](uint32_t _a) { return _T_(_a * _scale_factor); });
      return;
    case values::scalar_type::uint_64:
      std::transform(reinterpret_cast<const uint64_t*>(_src),
                     reinterpret_cast<const uint64_t*>(_src) + _count, _dst,
                     [_scale_factor](uint64_t _a) { return _T_(_a * _scale_factor); });
      return;
    case values::scalar_type::int_8:
      std::transform(reinterpret_cast<const int8_t*>(_src),
                     reinterpret_cast<const int8_t*>(_src) + _count, _dst,
                     [_scale_factor](int8_t _a) { return _T_(_a * _scale_factor); });
      return;
    case values::scalar_type::int_16:
      std::transform(reinterpret_cast<const int16_t*>(_src),
                     reinterpret_cast<const int16_t*>(_src) + _count, _dst,
                     [_scale_factor](int16_t _a) { return _T_(_a * _scale_factor); });
      return;
    case values::scalar_type::int_32:
      std::transform(reinterpret_cast<const int32_t*>(_src),
                     reinterpret_cast<const int32_t*>(_src) + _count, _dst,
                     [_scale_factor](int32_t _a) { return _T_(_a * _scale_factor); });
      return;
    case values::scalar_type::int_64:
      std::transform(reinterpret_cast<const int64_t*>(_src),
                     reinterpret_cast<const int64_t*>(_src) + _count, _dst,
                     [_scale_factor](int64_t _a) { return _T_(_a * _scale_factor); });
      return;
    case values::scalar_type::float_32:
      std::transform(reinterpret_cast<const float*>(_src),
                     reinterpret_cast<const float*>(_src) + _count, _dst,
                     [_scale_factor](float _a) { return _T_(_a * _scale_factor); });
      return;
    case values::scalar_type::float_64:
      std::transform(reinterpret_cast<const double*>(_src),
                     reinterpret_cast<const double*>(_src) + _count, _dst,
                     [_scale_factor](double _a) { return _T_(_a * _scale_factor); });
      return;
    }
  }
} // namespace pralin::details
void scalar_const_array_adaptor::copy_to(values::scalar_type _target_type, uint8_t* _destination,
                                         double _scale_factor) const
{
  namespace details = pralin::details;
  switch(_target_type)
  {
  case values::scalar_type::uint_8:
    details::copy_to(m_type, m_data, _destination, m_count, _scale_factor);
    return;
  case values::scalar_type::uint_16:
    details::copy_to(m_type, m_data, reinterpret_cast<uint16_t*>(_destination), m_count,
                     _scale_factor);
    return;
  case values::scalar_type::uint_32:
    details::copy_to(m_type, m_data, reinterpret_cast<uint32_t*>(_destination), m_count,
                     _scale_factor);
    return;
  case values::scalar_type::uint_64:
    details::copy_to(m_type, m_data, reinterpret_cast<uint64_t*>(_destination), m_count,
                     _scale_factor);
    return;
  case values::scalar_type::int_8:
    details::copy_to(m_type, m_data, reinterpret_cast<int8_t*>(_destination), m_count,
                     _scale_factor);
    return;
  case values::scalar_type::int_16:
    details::copy_to(m_type, m_data, reinterpret_cast<int16_t*>(_destination), m_count,
                     _scale_factor);
    return;
  case values::scalar_type::int_32:
    details::copy_to(m_type, m_data, reinterpret_cast<int32_t*>(_destination), m_count,
                     _scale_factor);
    return;
  case values::scalar_type::int_64:
    details::copy_to(m_type, m_data, reinterpret_cast<int64_t*>(_destination), m_count,
                     _scale_factor);
    return;
  case values::scalar_type::float_32:
    details::copy_to(m_type, m_data, reinterpret_cast<float*>(_destination), m_count,
                     _scale_factor);
    return;
  case values::scalar_type::float_64:
    details::copy_to(m_type, m_data, reinterpret_cast<double*>(_destination), m_count,
                     _scale_factor);
    return;
  }
}

pralin::values::scalar scalar_const_array_adaptor::at(std::size_t _index) const
{
  return values::scalar(m_type, m_data + _index * m_scalar_size);
}

void scalar_array_adaptor::set(std::size_t _index, const pralin::values::scalar& _scalar)
{
  _scalar.cast_to(m_type, const_cast<uint8_t*>(m_data) + _index * m_scalar_size);
}
