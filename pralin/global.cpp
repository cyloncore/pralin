#include "global"

#ifdef __GNU_LIBRARY__

#include <cxxabi.h>

std::string pralin::details::demangle(const char* _mangled_symbol)
{
  int status;
  char* realname = abi::__cxa_demangle(_mangled_symbol, 0, 0, &status);
  if(status == 0)
  {
    std::string str = realname;
    free(realname);
    return str;
  }
  else
  {
    return std::string(_mangled_symbol);
  }
}

#else

std::string pralin::details::demangle(const char* _mangled_symbol)
{
  return std::string(_mangled_symbol);
}

#endif
