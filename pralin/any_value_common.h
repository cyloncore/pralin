#ifndef _PRALIN_ANY_VALUE_H_
#ifndef _PRALIN_ANY_VALUE_PTR_H_
#error "Do not include 'any_value_common.h' directly, include any_value or any_value_ptr."
#endif
#endif

#ifndef _PRALIN_ANY_VALUE_COMMON_H_
#define _PRALIN_ANY_VALUE_COMMON_H_

#include "any_value"
#include "any_value_ptr"

namespace pralin
{
  namespace details
  {
    template<typename _T_>
      requires std::is_pointer_v<_T_>
    inline any_value_ptr create_any_value_ptr(_T_ _t)
    {
      return any_value_ptr(const_cast<remove_pointer_to_const_t<_T_>>(_t));
    }
    template<typename _T_>
      requires(not std::is_pointer_v<_T_>)
    inline any_value_ptr create_any_value_ptr(_T_)
    {
      throw exception("Cannot create any_value_ptr for non-pointer of type '{}'",
                      meta_type::get<_T_>().get_name());
    }
  } // namespace details
  template<typename _T_>
  any_value_ptr any_value::data_implementation<_T_>::create_any_value_ptr() const
  {
    return details::create_any_value_ptr(*t());
  }

  inline any_value_ptr::any_value_ptr(const any_value& _rhs)
      : any_value_ptr(_rhs.d->create_any_value_ptr())
  {
  }
} // namespace pralin

#endif
