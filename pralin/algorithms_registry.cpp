#include "algorithms_registry"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <unordered_map>

#include <filesystem>

#include "abstract_algorithm_p.h"
#include "algorithm_registry_monitor_p.h"
#include "documentation_p.h"
#include "logging"
#include "platform_p.h"
#include <pralin/any_value>

#include <pralin/config>

using namespace pralin;

namespace
{
  template<class T>
  inline void hash_combine(size_t& seed, T const& v)
  {
    seed ^= std::hash<T>()(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  }
} // namespace

template<typename _T1_, typename _T2_>
struct std::hash<std::pair<_T1_, _T2_>>
{
  size_t operator()(std::pair<_T1_, _T2_> const& v) const
  {
    size_t seed = 0;
    hash_combine(seed, v.first);
    hash_combine(seed, v.second);
    return seed;
  }
};

struct algorithms_registry::data
{
  struct algorithm_factory_info
  {
    uint32_t priority;
    const abstract_factory* factory = nullptr;
  };
  struct module_documentation
  {
    documentation::module module;
    std::vector<std::function<documentation::algorithm()>> algorithm_builders;
  };
  std::unordered_map<std::pair<std::string, std::string>, const abstract_factory*>
    modules_to_factory;
  std::unordered_map<std::pair<std::string, std::string>, algorithm_factory_info>
    algorithms_to_factory;
  static algorithms_registry::data* instance();
  std::vector<const abstract_factory*> factories;
  std::vector<std::string> loaded_libraries;
  void load_library(const std::string& _filename);
  std::map<std::string, module_documentation> modules_documentation;
};

algorithms_registry::data* algorithms_registry::data::instance()
{
  static algorithms_registry::data* s_instance = nullptr;
  if(s_instance == nullptr)
  {
    s_instance = new algorithms_registry::data;
  }
  return s_instance;
}

void algorithms_registry::data::load_library(const std::string& _filename)
{
  if(std::find(loaded_libraries.begin(), loaded_libraries.end(), _filename)
     == loaded_libraries.end())
  {
    if(platform::load_library(_filename))
    {
      loaded_libraries.push_back(_filename);
    }
    else
    {
      std::cerr << "Failed to load " << _filename << std::endl;
    }
  }
}

struct algorithms_registry::abstract_factory::data
{
  std::string definition_name;
  std::string module;
  std::string algorithm;
  uint32_t priority;
};

algorithms_registry::abstract_factory::abstract_factory(const std::string& _definition_name,
                                                        const std::string& _module,
                                                        const std::string& _algorithm,
                                                        uint32_t _priority)
    : d(new data{_definition_name, _module, _algorithm, _priority})
{
}

std::string algorithms_registry::abstract_factory::get_definition_name() const
{
  return d->definition_name;
}

std::string algorithms_registry::abstract_factory::get_module() const { return d->module; }

std::string algorithms_registry::abstract_factory::get_algorithm() const { return d->algorithm; }

uint32_t algorithms_registry::abstract_factory::get_priority() const { return d->priority; }

algorithms_registry::abstract_factory::~abstract_factory() {}

void algorithms_registry::load_default_definitions()
{
  load_definitions_from(PRALIN_INSTALL_DEFINITIONS_DIRECTORY, false);
  char* env_variable_char = std::getenv("PRALIN_DEFINITIONS_DIRECTORY");
  if(env_variable_char)
  {
    std::string env_variable = env_variable_char;
    std::size_t start_pos = 0;
    std::size_t pos = 0;
    for(; pos < env_variable.size(); ++pos)
    {
      if(env_variable[pos] == ':' or env_variable[pos] == ';')
      {
        if(pos != start_pos)
        {
          load_definitions_from(env_variable.substr(start_pos, pos - start_pos), false);
        }
        start_pos = pos + 1;
      }
    }
    if(start_pos < env_variable.size())
    {
      load_definitions_from(env_variable.substr(start_pos), false);
    }
  }
}

void algorithms_registry::load_definitions_from(const std::string& _directory, bool _lazy_load)
{
  try
  {
    for(const auto& entry : std::filesystem::directory_iterator(_directory))
    {
      std::ifstream spec_file;
      spec_file.open(entry.path());

      std::string line;

      struct definition_info
      {
        std::string library, module, algorithm;
        uint32_t priority = 0;
      };

      definition_info current_definition;

      while(std::getline(spec_file, line))
      {
        if(line == "n" or line == "f")
        {
          if(current_definition.library.size() > 0 and current_definition.module.size() > 0
             and current_definition.algorithm.size() > 0)
          {
            std::string full_library = std::filesystem::path(_directory).parent_path().parent_path()
                                       / current_definition.library;
            if(std::filesystem::is_regular_file(full_library))
            {
              if(_lazy_load)
              {
                pralin_fatal("wip");
              }
              else
              {
                data::instance()->load_library(full_library);
              }
            }
            else
            {
              std::cerr << "Missing library: " << full_library << std::endl;
            }
          }
          else if(current_definition.library.size() > 0 or current_definition.module.size() > 0
                  or current_definition.algorithm.size() > 0)
          {
            std::cerr << "Invalid definition in: " << entry << " is ignored";
          } // else is first or empty
          current_definition = definition_info();
          if(line == "f")
          {
            break;
          }
        }
        else if(line.size() > 2)
        {
          char cmd = line[0];
          std::string payload = line.substr(2, line.size() - 2);
          switch(cmd)
          {
          case 'l':
            current_definition.library = payload;
            break;
          case 'm':
            current_definition.module = payload;
            break;
          case 'a':
            current_definition.algorithm = payload;
            break;
          case 'p':
            current_definition.priority = std::stoi(payload);
            break;
          default:
            std::cerr << "Invalid command in: " << entry << ": " << cmd << std::endl;
          }
        }
        else
        {
          std::cerr << "Invalid line in: " << entry << ": " << line << std::endl;
        }
      }
    }
  }
  catch(const std::filesystem::filesystem_error& _error)
  {
    std::cerr << "Failed to read directory: " << _error.what() << std::endl;
  }
}

abstract_algorithm* algorithms_registry::create_aa(const abstract_factory* _factory,
                                                   const any_value_map& _parameters)
{
  pralin_assert(_factory);
  abstract_algorithm* algo = _factory->create(_parameters);
  if(algo)
  {
    algo->d->library = _factory->get_module();
    algo->d->algorithm = _factory->get_algorithm();
    return algo;
  }
  throw pralin::exception("Failed to create algorithm for '{}/{}'", _factory->get_module(),
                          _factory->get_algorithm());
}

abstract_algorithm* algorithms_registry::create_any_aa(const std::string& _definition_name,
                                                       const std::string& _algorithm,
                                                       const any_value_map& _parameters)
{
  const abstract_factory* f
    = data::instance()->algorithms_to_factory[{_definition_name, _algorithm}].factory;
  if(f)
  {
    return create_aa(f, _parameters);
  }
  else
  {
    throw pralin::exception("Unknown algorithm '{}'.", _algorithm);
  }
}

abstract_algorithm* algorithms_registry::create_aa(const std::string& _module,
                                                   const std::string& _algorithm,
                                                   const any_value_map& _parameters)
{
  const abstract_factory* f = data::instance()->modules_to_factory[{_module, _algorithm}];
  if(f)
  {
    return create_aa(f, _parameters);
  }
  else
  {
    throw pralin::exception("Unknown algorithm '{}/{}'.", _module, _algorithm);
  }
}

algorithm_instance algorithms_registry::create_any(const std::string& _definition_name,
                                                   const std::string& _algorithm,
                                                   const any_value_map& _parameters)
{
  return create_any_aa(_definition_name, _algorithm, _parameters);
}

algorithm_instance algorithms_registry::create(const std::string& _module,
                                               const std::string& _algorithm,
                                               const any_value_map& _parameters)
{
  return create_aa(_module, _algorithm, _parameters);
}

void algorithms_registry::register_algorithm(
  abstract_factory* _factory,
  const std::function<documentation::algorithm()>& _documentation_builder)
{
  data::instance()->factories.push_back(_factory);
  data::instance()->modules_to_factory[{_factory->get_module(), _factory->get_algorithm()}]
    = _factory;
  data::algorithm_factory_info& afi
    = data::instance()
        ->algorithms_to_factory[{_factory->get_definition_name(), _factory->get_algorithm()}];
  if(_factory->get_priority() > afi.priority)
  {
    afi.priority = _factory->get_priority();
    afi.factory = _factory;
  }
  abstract_algorithm_registry_monitor* monitor
    = abstract_algorithm_registry_monitor::current_monitor();
  if(monitor)
  {
    monitor->add_factory(_factory->get_module(), _factory->get_algorithm(),
                         _factory->get_priority());
  }
  data::module_documentation& m
    = algorithms_registry::data::instance()->modules_documentation[_factory->get_module()];
  m.module.d->name = _factory->get_module();
  m.algorithm_builders.push_back(_documentation_builder);
}

std::vector<const algorithms_registry::abstract_factory*> algorithms_registry::factories()
{
  return data::instance()->factories;
}

std::vector<documentation::module> algorithms_registry::all_modules_documentation()
{
  std::vector<documentation::module> ms;
  for(auto const& [k, md] : data::instance()->modules_documentation)
  {
    ms.push_back(update_module(md.module, md.algorithm_builders));
  }
  return ms;
}

documentation::module algorithms_registry::get_module_documentation(const std::string& _module)
{
  data::module_documentation md = data::instance()->modules_documentation[_module];
  return update_module(md.module, md.algorithm_builders);
}

bool algorithms_registry::has_module_documentation(const std::string& _module)
{
  return data::instance()->modules_documentation.find(_module)
         != data::instance()->modules_documentation.end();
}

documentation::module algorithms_registry::update_module(
  const documentation::module& _module,
  const std::vector<std::function<documentation::algorithm()>>& _algorithm_builders)
{
  if(_module.D()->algorithms.size() != _algorithm_builders.size())
  {
    documentation::module m = _module;
    m.D()->algorithms.clear();
    for(const std::function<documentation::algorithm()>& f : _algorithm_builders)
    {
      documentation::algorithm a = f();
      m.D()->algorithms[a.get_name()] = a;
    }
  }
  return _module;
}

register_module_documentation::register_module_documentation(
  const char* _module, const documentation::description& _description)
{
  algorithms_registry::data::module_documentation& m
    = algorithms_registry::data::instance()->modules_documentation[_module];
  m.module.d->name = _module;
  m.module.d->short_description = _description.short_description;
  m.module.d->description = _description.long_description;
}
