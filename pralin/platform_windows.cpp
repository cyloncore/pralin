#include <windows.h>

namespace pralin::platform
{
  std::string get_application_data_path()
  {
    PWSTR path = NULL;
    SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, NULL, &path);

    std::wstringstream ss;
    ss << path << "\\pralin\\";
    CoTaskMemFree(path);
    return ss.str();
  }
  bool download_file(const std::string& _url, const std::string& _destination)
  {
    return URLDownloadToFile(NULL, _url.c_str(), _destination.c_str(), 0, NULL) == S_OK;
  }
  void* load_library(const std::string& _library) { return LoadLibrary(_library.c_str()); }
} // namespace pralin::platform
