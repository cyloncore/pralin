#pragma once

#include "documentation"
#include "meta_type"

// Not strictly needed by this header, but any implementation will need the following includes:
#include "algorithm_definition"
#include "parameters"

namespace pralin
{
  /**
   * @brief is a helper class to use as the base class of an implementation
   * 
   * The class allows to interface with the parameters system of pralin.
   * It also define the \ref create function used in the \ref algorithms_registry to
   * instantiate this algorithm.
   */
  template<typename _TDerived_, typename _TDefinition_, typename _TParameters_ = _TDefinition_::parameters>
  class implementation : public _TDefinition_
  {
    template<typename _T_>
    friend class register_algorithm;
  public:
    using implementation_t = class implementation<_TDerived_, _TDefinition_, _TParameters_>;
    using parameters = _TParameters_;
    PRALIN_META_INFO(implementation_t, inherits<_TDefinition_>)
  protected:
    implementation() = default;
  public:
    ~implementation() = default;
    parameters get_parameters() const
    {
      return m_parameters;
    }
  private:
    void init() { }
  private:
    template<typename... _Args_>
    static _TDerived_* create_ptr(const parameters& _parameters, const _Args_&... _args)
    {
      _TDerived_* derived = new _TDerived_();
      derived->m_parameters = _parameters;
      try
      {
        derived->init(_args...);
        return derived;
      } catch(const pralin::exception& _ex)
      {
        delete derived;
        throw _ex;
      }
    }
  public:
    using instance = details::algorithm_instance<_TDerived_, _TDefinition_,
      typename _TDefinition_::inputs_types, typename _TDefinition_::outputs_types>;
    template<typename... _Args_>
    static instance create(const parameters& _parameters, const _Args_&... _args)
    {
      return create_ptr(_parameters, _args...);
    }
  private:
    parameters m_parameters;
  };
}
