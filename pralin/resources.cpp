#include "resources"

#include <filesystem>

#include "exception"
#include "logging"
#include "platform_p.h"

using namespace pralin;

namespace pralin::details
{
  std::filesystem::path get_filename(const std::string& _module, const std::string& _file)
  {
    std::filesystem::path module_path = platform::get_application_data_path() + "/" + _module + "/";
    if(not std::filesystem::exists(module_path))
    {
      std::filesystem::create_directories(module_path);
    }
    if(not std::filesystem::is_directory(module_path))
    {
      pralin_fatal("'" + (std::string)module_path + "' is not a directory!");
    }
    return (std::string)module_path + "/" + _file;
  }
} // namespace pralin::details

std::string resources::get(const std::string& _module, const std::string& _file,
                           const std::string& _source)
{
  std::filesystem::path fn = details::get_filename(_module, _file);
  if(_source.empty())
  {
    return _module.empty() ? _file : std::string(fn);
  }
  if(not std::filesystem::exists(fn))
  {
    // Dowload
    if(not platform::download_file(_source, fn))
    {
      throw pralin::exception("Failed to download '{}' to '{}'.", _source, std::string(fn));
    }
  }

  return fn;
}
