#pragma once

#include <cstdint>
#include <string>

namespace pralin
{
  struct abstract_algorithm_registry_monitor
  {
  public:
    virtual ~abstract_algorithm_registry_monitor();
    virtual void add_factory(const std::string& _module, const std::string& _algorithm,
                             uint32_t _priority)
      = 0;
    static abstract_algorithm_registry_monitor* current_monitor();
    static void set_current_monitor(abstract_algorithm_registry_monitor* _monitor);
  private:
    static abstract_algorithm_registry_monitor* s_monitor;
  };
} // namespace pralin
