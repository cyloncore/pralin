#include <pralin/any_value>

using namespace pralin;

namespace
{
  struct handler
  {
    ~handler() { deleter(data); }
    std::function<void(void*)> deleter;
    void* data;
  };
} // namespace

void* details::any_value_converters_interface::get_data(const std::type_index& _type,
                                                        const std::function<void*()>& _creator,
                                                        const std::function<void(void*)>& _deleter)
{
  static std::unordered_map<std::type_index, std::shared_ptr<handler>> storage;
  auto it = storage.find(_type);
  if(it == storage.end())
  {
    void* data = _creator();
    storage[_type] = std::make_shared<handler>(_deleter, data);
    return data;
  }
  else
  {
    return it->second->data;
  }
}
