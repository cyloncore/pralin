#include "abstract_algorithm_p.h"

using namespace pralin;

abstract_algorithm::abstract_algorithm() : d(new data) {}

abstract_algorithm::~abstract_algorithm() {}

std::string abstract_algorithm::get_full_name() const { return d->library + "/" + d->algorithm; }
