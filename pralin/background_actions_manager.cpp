#include "background_actions_manager"

#include <thread>
#include <vector>

using namespace pralin;

struct background_actions_manager::s_data
{
  std::mutex m;
  std::condition_variable cv;
  std::vector<std::function<void()>> actions;
  std::thread t;
  bool running = false;
};

background_actions_manager::s_data background_actions_manager::sd;
#include <iostream>
void background_actions_manager::step()
{
  std::vector<std::function<void()>> acts;
  {
    std::unique_lock<std::mutex> l(sd.m);
    std::swap(acts, sd.actions);
  }
  for(const std::function<void()>& a : acts)
  {
    a();
  }
}

void background_actions_manager::exec()
{
  sd.running = true;
  while(sd.running)
  {
    {
      std::unique_lock<std::mutex> l(sd.m);
      if(sd.actions.empty())
      {
        sd.cv.wait(l);
      }
    }
    step();
    sd.cv.notify_all();
  }
}

void background_actions_manager::exec_in_thread()
{
  sd.t = std::thread([]() { exec(); });
}

void background_actions_manager::exit()
{
  while(true)
  {
    std::unique_lock<std::mutex> l(sd.m);
    if(sd.actions.empty())
    {
      break;
    }
    else
    {
      sd.cv.wait(l);
    }
  }

  sd.running = false;
  sd.cv.notify_all();
  if(sd.t.joinable())
  {
    sd.t.join();
  }
}

void background_actions_manager::enqueue_p(const std::function<void()>& _action)
{
  std::unique_lock<std::mutex> l(sd.m);
  sd.actions.push_back(_action);
  sd.cv.notify_all();
}
