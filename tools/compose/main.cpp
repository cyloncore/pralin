#include <iostream>

#include <clog_print>

#include <pralin/algorithms_registry>
#include <pralin/background_actions_manager>
#include <pralin/compose/computation_graph>
#include <pralin/values/meta_data>

#include <interfaces/pralin/algorithms/yaml>

void check_argument_index(int _index, int _argc, const char* _argument_name)
{
  if(_index >= _argc)
  {
    clog_print<clog_print_flag::red>("Missing argument after {}. Check usage with --help.",
                                     _argument_name);
    std::exit(255);
  }
}

int main(int _argc, char** _argv)
{
  std::string filename;
  std::string inputs_str;
  std::string parameters_str;

  for(int i = 1; i < _argc; ++i)
  {
    std::string a = _argv[i];
    if(a == "--inputs")
    {
      check_argument_index(++i, _argc, "--inputs");
      inputs_str = _argv[i];
    }
    else if(a == "--parameters")
    {
      check_argument_index(++i, _argc, "--parameters");
      parameters_str = _argv[i];
    }
    else if(a == "--help")
    {
      clog_print<clog_print_flag::blue>(
        "pralin compose [--inputs <inputs>] [--parameters <parameters>] filename");
      std::exit(_argc == 2 ? 0 : 255);
    }
    else
    {
      if(filename.empty())
      {
        filename = a;
      }
      else
      {
        clog_print("{} {}", filename, a);
        clog_print<clog_print_flag::red>(
          "Only one filename should be specified. Check usage with --help.");
        std::exit(255);
      }
    }
  }

  if(filename.empty())
  {
    clog_print<clog_print_flag::red>("One filename should be specified. Check usage with --help.");
    std::exit(255);
  }

  pralin::algorithms_registry::load_default_definitions();
  pralin::compose::computation_graph cg;

  // Read parameters
  if(not parameters_str.empty())
  {
    try
    {
      cg.load_parameters_from_string(parameters_str);
    }
    catch(pralin::exception ex)
    {
      clog_print<clog_print_flag::red>("An error occured while parsing parameters: {}", ex.what());
      std::exit(255);
    }
  }

  // Read computation graph
  try
  {
    if(filename == "--")
    {
      std::string text;
      std::string line;
      clog_print<clog_print_flag::green>("Enter composition, terminate with an empty line:");
      while(std::getline(std::cin, line))
      {
        if(line.empty())
        {
          break;
        }
        else
        {
          text += line + '\n';
        }
      }
      cg.load_from_string(text);
    }
    else
    {
      cg.load_from_file(filename);
    }
  }
  catch(pralin::exception ex)
  {
    clog_print<clog_print_flag::red>("An error occured while parsing computing graph: {}",
                                     ex.what());
    std::exit(255);
  }

  // Setup inputs
  pralin::values_vector inputs_vector;
  pralin::any_value_map inputs_vm;
  if(not inputs_str.empty())
  {
    try
    {
      pralin::any_value val;
      pralin::algorithms::yaml::parser().process(inputs_str, std::nullopt, &val);
      inputs_vm = val.to_value<pralin::any_value_map>();
    }
    catch(pralin::exception ex)
    {
      clog_print<clog_print_flag::red>("An error occured while parsing inputs: {}", ex.what());
      std::exit(255);
    }
  }

  for(const pralin::input_info& ii : cg.get_input_infos())
  {
    pralin::any_value_map::iterator inputs_vm_it = inputs_vm.find(ii.name);
    pralin::any_value vm;
    if(inputs_vm_it == inputs_vm.end())
    {
      std::cout << ii.name << ": " << std::flush;
      std::string value;
      std::cin >> value;
      pralin::algorithms::yaml::parser().process(value, std::nullopt, &vm);
    }
    else
    {
      vm = inputs_vm_it->second;
    }
    if(vm.is_convertible(ii.type))
    {
      inputs_vector.push_back(vm);
    }
    else
    {
      clog_print<clog_print_flag::red>("Input {} was set to type {} but expect {}", ii.name,
                                       vm.get_meta_type().get_name(), ii.type.get_name());
      std::exit(255);
    }
  }

  // Run composition
  pralin::values_ptr_vector outputs_vector = cg.create_output_values();
  try
  {
    std::thread pt(
      [&cg, &inputs_vector, &outputs_vector]()
      {
        cg.process(inputs_vector, outputs_vector);
        pralin::background_actions_manager::exit();
      });
    pralin::background_actions_manager::exec();
    pt.join();
  }
  catch(pralin::exception ex)
  {
    clog_print<clog_print_flag::red>("An error occured while running graph: {}", ex.what());
    std::exit(255);
  }

  // Print outputs
  for(std::size_t i = 0; i < outputs_vector.size(); ++i)
  {
    clog_print("{}: {}", cg.get_output_infos()[i].name, outputs_vector[i].dereference());
  }

  return 0;
}