add_executable(compose main.cpp )
target_link_libraries(compose PRIVATE pralin_compose pralin_yaml_algorithms)
install(TARGETS compose RUNTIME DESTINATION "${INSTALL_PRALIN_COMMANDS_DIR}" COMPONENT bin)
