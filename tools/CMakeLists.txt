add_subdirectory(specgen/)
add_subdirectory(pralin/)
add_subdirectory(documentation/)


if(BUILD_PRALIN_COMPOSE)
add_subdirectory(compose)
endif()

if(BUILD_PRALIN_QT6QUICK)
add_subdirectory(studio)
endif()
