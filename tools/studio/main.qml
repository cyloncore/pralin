import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import Cyqlops.TextEdit
import Cyqlops.TextEdit.Controls

import Pralin
import Pralin.Compose
import Pralin.Controls

ApplicationWindow
{
  id: _root_
  width: 800
  height: 600
  visible: true
  property bool graphNeedUpdate: true
  property bool graphRunning: false
  Action
  {
    id: open_action
  }
  Action
  {
    id: save_action
  }
  Action
  {
    id: save_as_action
  }
  Action
  {
    id: compile_action
  }
  Action
  {
    id: run_action
  }
  header: ToolBar
  {
    RowLayout
    {
      ToolButton
      {
        action: open_action
      }
      ToolButton
      {
        action: save_action
      }
      ToolButton
      {
        action: save_as_action
      }
      ToolButton
      {
        action: compile_action
      }
      ToolButton
      {
        action: run_action
      }
    }
  }
  ComputationGraph
  {
    id: _computation_graph_
  }
  SplitView
  {
    ColumnLayout
    {
      TextEditorArea
      {
        id: _compose_definition_
        document.highlightingDefinition: "YAML"
        text: "compose: { process: [] }"
        Layout.fillHeight: true
        onTextChanged:
        {
          _root_.graphNeedUpdate = true
        }
      }
      Text
      {
        text: _computation_graph_.lastError
        visible: text.length > 0
        color: "red"
      }
      SplitView.fillWidth: true
    }
    ColumnLayout
    {
      RowLayout
      {
        Button
        {
          text: "Compile"
          onClicked:
          {
            _computation_graph_.definition = _compose_definition_.text
            _root_.graphNeedUpdate = false
          }
          enabled: _root_.graphNeedUpdate && !_computation_graph_.running
        }
        Button
        {
          text: "Start"
          onClicked: _computation_graph_.process()
          enabled: _computation_graph_.lastError.length == 0 && !graphNeedUpdate && !_computation_graph_.running
        }
      }
      Text
      {
        text: "Inputs:"
      }
      Repeater
      {
        model: _computation_graph_.inputsInfos
        RowLayout
        {
          Text
          {
            text: modelData["name"]
          }
          ValueEditor
          {
            type: modelData["type"]
            onValueChanged:
            {
              var inputs = _computation_graph_.inputs
              inputs[index] = value
              _computation_graph_.inputs = inputs
            }
          }
        }
      }
      Text
      {
        text: "Outputs:"
      }
      Repeater
      {
        model: _computation_graph_.outputsInfos
        RowLayout
        {
          Text
          {
            text: modelData["name"]
          }
          ValueView
          {
            type: modelData["type"]
            value: _computation_graph_.outputs[index]
          }
        }
      }
      Item
      {
        Layout.minimumWidth: 1
        Layout.fillHeight: true
      }
    }
    anchors.fill: parent
  }
  Component.onCompleted:
  {
    Pralin.loadInstalledDefinitions()
  }
}
