add_executable(documentation main.cpp )
target_link_libraries(documentation PRIVATE pralin)
install(TARGETS documentation RUNTIME DESTINATION "${INSTALL_PRALIN_COMMANDS_DIR}" COMPONENT bin)
