#include <iostream>

#include <clog_print>

#include <pralin/algorithms_registry>

void check_argument_index(int _index, int _argc, const char* _argument_name)
{
  if(_index >= _argc)
  {
    clog_print<clog_print_flag::red>("Missing argument after {}. Check usage with --help.",
                                     _argument_name);
    std::exit(255);
  }
}

int main(int _argc, char** _argv)
{
  pralin::algorithms_registry::load_default_definitions();
  switch(_argc)
  {
  case 1:
  {
    clog_print("There are {} modules.",
               pralin::algorithms_registry::all_modules_documentation().size());
    for(const pralin::documentation::module m :
        pralin::algorithms_registry::all_modules_documentation())
    {
      clog_print("* {}{}{}: {}", clog_print_flag::bold | clog_print_flag::green, m.get_name(),
                 clog_print_flag::reset, m.get_short_description());
    }
    return 0;
  }
  case 2:
  {
    if(not pralin::algorithms_registry::has_module_documentation(_argv[1]))
    {
      clog_print<clog_print_flag::red>("Unknown module {}.", _argv[1]);
      return 255;
    }
    pralin::documentation::module m
      = pralin::algorithms_registry::get_module_documentation(_argv[1]);
    clog_print<clog_print_flag::bold>("{}", m.get_name());
    clog_print("{:=>{}}\n\n{}", "", m.get_name().size(), m.get_description());
    for(auto const& [k, a] : m.get_algorithms())
    {
      clog_print("* {}{}{}: {}", clog_print_flag::bold | clog_print_flag::green, a.get_name(),
                 clog_print_flag::reset, m.get_short_description());
    }
    return 0;
  }
  case 3:
  {
    pralin::documentation::module m
      = pralin::algorithms_registry::get_module_documentation(_argv[1]);
    if(not pralin::algorithms_registry::has_module_documentation(_argv[1]))
    {
      clog_print<clog_print_flag::red>("Unknown module {}.", _argv[1]);
      return 255;
    }
    if(not m.has_algorithm(_argv[2]))
    {
      clog_print<clog_print_flag::red>("No algorithm {} in module {}", _argv[2], _argv[1]);
      return 255;
    }
    pralin::documentation::algorithm a = m.get_algorithm(_argv[2]);
    clog_print<clog_print_flag::bold>("{}", a.get_fullname());
    clog_print("{:=>{}}\n\n{}", "", a.get_fullname().size(), a.get_description());
    if(not a.get_inputs().empty())
    {
      clog_print<clog_print_flag::bold>("\ninputs");
      clog_print("------");
      for(const pralin::documentation::input& i : a.get_inputs())
      {
        clog_print("* {}: {}", i.get_name(), i.get_type());
      }
    }
    if(not a.get_outputs().empty())
    {
      clog_print<clog_print_flag::bold>("\noutputs");
      clog_print("-------");
      for(const pralin::documentation::output& o : a.get_outputs())
      {
        clog_print("* {}: {}", o.get_name(), o.get_type());
      }
    }
    if(not a.get_parameters().empty())
    {
      clog_print<clog_print_flag::bold>("parameters");
      clog_print("----------");
      for(const pralin::documentation::parameter& p : a.get_parameters())
      {
        const char* required = p.is_required() ? " required" : "";
        if(p.get_default_value().empty())
        {
          clog_print("* {}: {}{}", p.get_name(), p.get_type(), required);
        }
        else
        {
          clog_print("* {}: {}{} (default: {})", p.get_name(), p.get_type(), required,
                     p.get_default_value());
        }
      }
    }
    return 0;
  }
  default:
    clog_print<clog_print_flag::red>(
      "Invalid number of arguments, usage is:\npralin documentation # list all modules\n pralin "
      "documentation module_name # list all algorithms in a module\n pralin documentaion "
      "module_name algorithm_name # show documentation for a specific algorithm");
    std::exit(255);
  }

  return 0;
}