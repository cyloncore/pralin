#include <fstream>
#include <iostream>

#include <filesystem>

#include <pralin/algorithm_registry_monitor_p.h>
#include <pralin/logging>
#include <pralin/platform_p.h>

struct specgen_arm : public pralin::abstract_algorithm_registry_monitor
{
  specgen_arm(const std::string& _fn, const std::string _ln) : library_name(_ln)
  {
    output.open(_fn, std::ios::out);
    if(not output.is_open())
    {
      std::cerr << "Failed to open file: " << _fn << std::endl;
      std::exit(-1);
    }
  }
  ~specgen_arm() { output << "f\n"; }
  void add_factory(const std::string& _module, const std::string& _algorithm,
                   uint32_t _priority) override
  {
    output << "n\n"
           << "l " << library_name << "\nm " << _module << "\na " << _algorithm << "\np "
           << _priority << std::endl;
  }
  std::string library_name;
  std::ofstream output;
};

int main(int _argc, char** _argv)
{
  if(_argc != 3)
  {
    std::cerr << "pralin specgen: expected 2 arguments got " << _argc - 1 << std::endl;
    return -1;
  }

  std::filesystem::path p(_argv[2]);
  std::error_code error;
  if(not std::filesystem::is_directory(p.parent_path())
     and not std::filesystem::create_directories(p.parent_path(), error))
  {
    std::cerr << "cannot create output directory: " << p << " with error: " << error << std::endl;
    return -1;
  }

  pralin::abstract_algorithm_registry_monitor::set_current_monitor(
    new specgen_arm(_argv[2], std::filesystem::path(_argv[1]).filename()));

  if(pralin::platform::load_library(_argv[1]))
  {
    pralin::abstract_algorithm_registry_monitor::set_current_monitor(nullptr);
    return 0;
  }
  else
  {
    std::cerr << "Failed to load library: " << _argv[1] << std::endl;
    return -1;
  }
}
