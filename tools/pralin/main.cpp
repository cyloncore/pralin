#include "config.h"

#include <filesystem>
#include <iostream>
#include <vector>

#include <pralin/string_p.h>

std::vector<std::filesystem::path> list_commands()
{
  static std::vector<std::filesystem::path> commands;
  if(commands.empty())
  {
    const std::filesystem::path cmd_dir{PRALIN_COMMANDS_DIR};
    for(const std::filesystem::directory_entry& de : std::filesystem::directory_iterator(cmd_dir))
    {
      if(de.is_regular_file()
         and int(de.status().permissions()
                 & (std::filesystem::perms::group_exec | std::filesystem::perms::owner_exec
                    | std::filesystem::perms::others_exec))
               != 0)
      {
        commands.push_back(de.path());
      }
    }
  }
  return commands;
}

int main(int _argc, char** _argv)
{
  if(_argc < 2)
  {
    std::cerr << "pralin [cmd]\nAvailable commands:";
    for(const std::filesystem::path& cmd : list_commands())
    {
      std::cerr << " " << std::string(cmd.filename());
    }
    std::cerr << std::endl;
    return -1;
  }
  std::string cmdline;
  for(const std::filesystem::path& cmd : list_commands())
  {
    if(cmd.filename() == _argv[1])
    {
      cmdline = cmd;
      break;
    }
  }
  if(cmdline.empty())
  {
    std::cerr << "Unknown command: " << _argv[1] << std::endl;
    return -1;
  }
  for(std::size_t i = 2; i < _argc; ++i)
  {
    std::string a = _argv[i];
    pralin::string::replace_all(a, "\"", "\\\"");
    cmdline += " \"" + a + "\"";
  }
  std::cout << cmdline << std::endl;

  return std::system(cmdline.c_str());
}
